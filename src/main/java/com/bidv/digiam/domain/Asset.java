package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Asset.
 */
@Entity
@Table(name = "asset")
public class Asset implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "price")
    private Double price;

    @Column(name = "expiry_date_from")
    private Instant expiryDateFrom;

    @Column(name = "expiry_date_to")
    private Instant expiryDateTo;

    @Column(name = "status")
    private Integer status;

    @Column(name = "image")
    //    @JsonIgnore
    private String image;

    @Column(name = "qr_code")
    private String qrCode;

    @Column(name = "location")
    private String location;

    @Column(name = "note")
    private String note;

    @Column(name = "serial_no")
    private String serialNo;

    @Column(name = "guarantee")
    private Instant guarantee;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private String updatedBy;

    //    @OneToMany(mappedBy = "asset")
    //    @JsonIgnoreProperties(value = { "oldOwner", "asset" }, allowSetters = true)
    //    private Set<AssetHistory> assetHistories = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "assets", "assetSets" }, allowSetters = true)
    private AssetType assetType;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private EmpMgt owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Asset id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Asset name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public Asset code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getPrice() {
        return this.price;
    }

    public Asset price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Instant getExpiryDateFrom() {
        return this.expiryDateFrom;
    }

    public Asset expiryDateFrom(Instant expiryDateFrom) {
        this.expiryDateFrom = expiryDateFrom;
        return this;
    }

    public void setExpiryDateFrom(Instant expiryDateFrom) {
        this.expiryDateFrom = expiryDateFrom;
    }

    public Instant getExpiryDateTo() {
        return this.expiryDateTo;
    }

    public Asset expiryDateTo(Instant expiryDateTo) {
        this.expiryDateTo = expiryDateTo;
        return this;
    }

    public void setExpiryDateTo(Instant expiryDateTo) {
        this.expiryDateTo = expiryDateTo;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Asset status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImage() {
        return this.image;
    }

    public Asset image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQrCode() {
        return this.qrCode;
    }

    public Asset qrCode(String qrCode) {
        this.qrCode = qrCode;
        return this;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getLocation() {
        return this.location;
    }

    public Asset location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNote() {
        return this.note;
    }

    public Asset note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSerialNo() {
        return this.serialNo;
    }

    public Asset serialNo(String serialNo) {
        this.serialNo = serialNo;
        return this;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Instant getGuarantee() {
        return this.guarantee;
    }

    public Asset guarantee(Instant guarantee) {
        this.guarantee = guarantee;
        return this;
    }

    public void setGuarantee(Instant guarantee) {
        this.guarantee = guarantee;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Asset createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Asset createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public Asset updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public Asset updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    //    public Set<AssetHistory> getAssetHistories() {
    //        return this.assetHistories;
    //    }

    //    public Asset assetHistories(Set<AssetHistory> assetHistories) {
    //        this.setAssetHistories(assetHistories);
    //        return this;
    //    }

    //    public Asset addAssetHistory(AssetHistory assetHistory) {
    //        this.assetHistories.add(assetHistory);
    //        assetHistory.setAsset(this);
    //        return this;
    //    }
    //
    //    public Asset removeAssetHistory(AssetHistory assetHistory) {
    //        this.assetHistories.remove(assetHistory);
    //        assetHistory.setAsset(null);
    //        return this;
    //    }
    //
    //    public void setAssetHistories(Set<AssetHistory> assetHistories) {
    //        if (this.assetHistories != null) {
    //            this.assetHistories.forEach(i -> i.setAsset(null));
    //        }
    //        if (assetHistories != null) {
    //            assetHistories.forEach(i -> i.setAsset(this));
    //        }
    //        this.assetHistories = assetHistories;
    //    }

    public AssetType getAssetType() {
        return this.assetType;
    }

    public Asset assetType(AssetType assetType) {
        this.setAssetType(assetType);
        return this;
    }

    public void setAssetType(AssetType assetType) {
        this.assetType = assetType;
    }

    public EmpMgt getOwner() {
        return this.owner;
    }

    public Asset owner(EmpMgt empMgt) {
        this.setOwner(empMgt);
        return this;
    }

    public void setOwner(EmpMgt empMgt) {
        this.owner = empMgt;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Asset)) {
            return false;
        }
        return id != null && id.equals(((Asset) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Asset{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", price=" + getPrice() +
            ", expiryDateFrom='" + getExpiryDateFrom() + "'" +
            ", expiryDateTo='" + getExpiryDateTo() + "'" +
            ", status=" + getStatus() +
            ", image='" + getImage() + "'" +
            ", qrCode='" + getQrCode() + "'" +
            ", location='" + getLocation() + "'" +
            ", note='" + getNote() + "'" +
            ", serialNo='" + getSerialNo() + "'" +
            ", guarantee='" + getGuarantee() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
