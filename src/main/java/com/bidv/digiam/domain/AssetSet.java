package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A AssetSet.
 */
@Entity
@Table(name = "asset_set")
public class AssetSet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @OneToMany(mappedBy = "asset")
    @JsonIgnoreProperties(value = { "room", "asset" }, allowSetters = true)
    private Set<AssetsInRoom> assetsInRooms = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "assets", "assetSets" }, allowSetters = true)
    private AssetType assetType;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AssetSet id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public AssetSet name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public AssetSet code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public AssetSet updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public AssetSet updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Set<AssetsInRoom> getAssetsInRooms() {
        return this.assetsInRooms;
    }

    public AssetSet assetsInRooms(Set<AssetsInRoom> assetsInRooms) {
        this.setAssetsInRooms(assetsInRooms);
        return this;
    }

    public AssetSet addAssetsInRoom(AssetsInRoom assetsInRoom) {
        this.assetsInRooms.add(assetsInRoom);
        assetsInRoom.setAsset(this);
        return this;
    }

    public AssetSet removeAssetsInRoom(AssetsInRoom assetsInRoom) {
        this.assetsInRooms.remove(assetsInRoom);
        assetsInRoom.setAsset(null);
        return this;
    }

    public void setAssetsInRooms(Set<AssetsInRoom> assetsInRooms) {
        if (this.assetsInRooms != null) {
            this.assetsInRooms.forEach(i -> i.setAsset(null));
        }
        if (assetsInRooms != null) {
            assetsInRooms.forEach(i -> i.setAsset(this));
        }
        this.assetsInRooms = assetsInRooms;
    }

    public AssetType getAssetType() {
        return this.assetType;
    }

    public AssetSet assetType(AssetType assetType) {
        this.setAssetType(assetType);
        return this;
    }

    public void setAssetType(AssetType assetType) {
        this.assetType = assetType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetSet)) {
            return false;
        }
        return id != null && id.equals(((AssetSet) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AssetSet{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
