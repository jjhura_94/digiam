package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A AssetType.
 */
@Entity
@Table(name = "asset_type")
public class AssetType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "depreciation")
    private Instant depreciation;

    @OneToMany(mappedBy = "assetType")
    @JsonIgnoreProperties(value = { "assetHistories", "assetType", "owner" }, allowSetters = true)
    private Set<Asset> assets = new HashSet<>();

    @OneToMany(mappedBy = "assetType")
    @JsonIgnoreProperties(value = { "assetsInRooms", "assetType" }, allowSetters = true)
    private Set<AssetSet> assetSets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AssetType id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public AssetType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public AssetType code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Instant getDepreciation() {
        return this.depreciation;
    }

    public AssetType depreciation(Instant depreciation) {
        this.depreciation = depreciation;
        return this;
    }

    public void setDepreciation(Instant depreciation) {
        this.depreciation = depreciation;
    }

    public Set<Asset> getAssets() {
        return this.assets;
    }

    public AssetType assets(Set<Asset> assets) {
        this.setAssets(assets);
        return this;
    }

    public AssetType addAsset(Asset asset) {
        this.assets.add(asset);
        asset.setAssetType(this);
        return this;
    }

    public AssetType removeAsset(Asset asset) {
        this.assets.remove(asset);
        asset.setAssetType(null);
        return this;
    }

    public void setAssets(Set<Asset> assets) {
        if (this.assets != null) {
            this.assets.forEach(i -> i.setAssetType(null));
        }
        if (assets != null) {
            assets.forEach(i -> i.setAssetType(this));
        }
        this.assets = assets;
    }

    public Set<AssetSet> getAssetSets() {
        return this.assetSets;
    }

    public AssetType assetSets(Set<AssetSet> assetSets) {
        this.setAssetSets(assetSets);
        return this;
    }

    public AssetType addAssetSet(AssetSet assetSet) {
        this.assetSets.add(assetSet);
        assetSet.setAssetType(this);
        return this;
    }

    public AssetType removeAssetSet(AssetSet assetSet) {
        this.assetSets.remove(assetSet);
        assetSet.setAssetType(null);
        return this;
    }

    public void setAssetSets(Set<AssetSet> assetSets) {
        if (this.assetSets != null) {
            this.assetSets.forEach(i -> i.setAssetType(null));
        }
        if (assetSets != null) {
            assetSets.forEach(i -> i.setAssetType(this));
        }
        this.assetSets = assetSets;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetType)) {
            return false;
        }
        return id != null && id.equals(((AssetType) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AssetType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", depreciation='" + getDepreciation() + "'" +
            "}";
    }
}
