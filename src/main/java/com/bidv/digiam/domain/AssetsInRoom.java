package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A AssetsInRoom.
 */
@Entity
@Table(name = "assets_in_room")
public class AssetsInRoom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties(value = { "assetsInRooms", "meetings" }, allowSetters = true)
    private MeetingRoom room;

    @ManyToOne
    @JsonIgnoreProperties(value = { "assetsInRooms", "assetType" }, allowSetters = true)
    private AssetSet asset;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AssetsInRoom id(Long id) {
        this.id = id;
        return this;
    }

    public MeetingRoom getRoom() {
        return this.room;
    }

    public AssetsInRoom room(MeetingRoom meetingRoom) {
        this.setRoom(meetingRoom);
        return this;
    }

    public void setRoom(MeetingRoom meetingRoom) {
        this.room = meetingRoom;
    }

    public AssetSet getAsset() {
        return this.asset;
    }

    public AssetsInRoom asset(AssetSet assetSet) {
        this.setAsset(assetSet);
        return this;
    }

    public void setAsset(AssetSet assetSet) {
        this.asset = assetSet;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssetsInRoom)) {
            return false;
        }
        return id != null && id.equals(((AssetsInRoom) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AssetsInRoom{" +
            "id=" + getId() +
            "}";
    }
}
