package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;

/**
 * A DayOffTicket.
 */
@Entity
@Table(name = "day_off_ticket")
public class DayOffTicket implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "number_of_day_left")
    private Integer numberOfDayLeft;

    @Column(name = "reason")
    private String reason;

    @Column(name = "watcher")
    private String watcher;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "from_date")
    private Instant fromDate;

    @Column(name = "to_date")
    private Instant toDate;

    @Column(name = "note")
    private String note;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private EmpMgt approver;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private EmpMgt approverByCeo;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private EmpMgt createdBy;

    @ManyToOne
    @JsonIgnoreProperties(value = { "dayOffUsers", "dayOffTickets" }, allowSetters = true)
    private DayOffType dayOffType;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DayOffTicket id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getNumberOfDayLeft() {
        return this.numberOfDayLeft;
    }

    public DayOffTicket numberOfDayLeft(Integer numberOfDayLeft) {
        this.numberOfDayLeft = numberOfDayLeft;
        return this;
    }

    public void setNumberOfDayLeft(Integer numberOfDayLeft) {
        this.numberOfDayLeft = numberOfDayLeft;
    }

    public String getReason() {
        return this.reason;
    }

    public DayOffTicket reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getWatcher() {
        return this.watcher;
    }

    public DayOffTicket watcher(String watcher) {
        this.watcher = watcher;
        return this;
    }

    public void setWatcher(String watcher) {
        this.watcher = watcher;
    }

    public Boolean getStatus() {
        return this.status;
    }

    public DayOffTicket status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Instant getFromDate() {
        return this.fromDate;
    }

    public DayOffTicket fromDate(Instant fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public void setFromDate(Instant fromDate) {
        this.fromDate = fromDate;
    }

    public Instant getToDate() {
        return this.toDate;
    }

    public DayOffTicket toDate(Instant toDate) {
        this.toDate = toDate;
        return this;
    }

    public void setToDate(Instant toDate) {
        this.toDate = toDate;
    }

    public String getNote() {
        return this.note;
    }

    public DayOffTicket note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public DayOffTicket createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public DayOffTicket updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public DayOffTicket updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public EmpMgt getApprover() {
        return this.approver;
    }

    public DayOffTicket approver(EmpMgt empMgt) {
        this.setApprover(empMgt);
        return this;
    }

    public void setApprover(EmpMgt empMgt) {
        this.approver = empMgt;
    }

    public EmpMgt getApproverByCeo() {
        return this.approverByCeo;
    }

    public DayOffTicket approverByCeo(EmpMgt empMgt) {
        this.setApproverByCeo(empMgt);
        return this;
    }

    public void setApproverByCeo(EmpMgt empMgt) {
        this.approverByCeo = empMgt;
    }

    public EmpMgt getCreatedBy() {
        return this.createdBy;
    }

    public DayOffTicket createdBy(EmpMgt empMgt) {
        this.setCreatedBy(empMgt);
        return this;
    }

    public void setCreatedBy(EmpMgt empMgt) {
        this.createdBy = empMgt;
    }

    public DayOffType getDayOffType() {
        return this.dayOffType;
    }

    public DayOffTicket dayOffType(DayOffType dayOffType) {
        this.setDayOffType(dayOffType);
        return this;
    }

    public void setDayOffType(DayOffType dayOffType) {
        this.dayOffType = dayOffType;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DayOffTicket)) {
            return false;
        }
        return id != null && id.equals(((DayOffTicket) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DayOffTicket{" +
            "id=" + getId() +
            ", numberOfDayLeft=" + getNumberOfDayLeft() +
            ", reason='" + getReason() + "'" +
            ", watcher='" + getWatcher() + "'" +
            ", status='" + getStatus() + "'" +
            ", fromDate='" + getFromDate() + "'" +
            ", toDate='" + getToDate() + "'" +
            ", note='" + getNote() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
