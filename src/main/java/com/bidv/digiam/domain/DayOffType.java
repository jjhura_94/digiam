package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A DayOffType.
 */
@Entity
@Table(name = "day_off_type")
public class DayOffType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "number_of_day")
    private Integer numberOfDay;

    @Column(name = "no_salary")
    private Boolean noSalary;

    @OneToMany(mappedBy = "dayOffType")
    @JsonIgnoreProperties(value = { "dayOffType", "emp" }, allowSetters = true)
    private Set<DayOffUser> dayOffUsers = new HashSet<>();

    @OneToMany(mappedBy = "dayOffType")
    @JsonIgnoreProperties(value = { "approver", "approverByCeo", "createdBy", "dayOffType" }, allowSetters = true)
    private Set<DayOffTicket> dayOffTickets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DayOffType id(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public DayOffType title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNumberOfDay() {
        return this.numberOfDay;
    }

    public DayOffType numberOfDay(Integer numberOfDay) {
        this.numberOfDay = numberOfDay;
        return this;
    }

    public void setNumberOfDay(Integer numberOfDay) {
        this.numberOfDay = numberOfDay;
    }

    public Boolean getNoSalary() {
        return this.noSalary;
    }

    public DayOffType noSalary(Boolean noSalary) {
        this.noSalary = noSalary;
        return this;
    }

    public void setNoSalary(Boolean noSalary) {
        this.noSalary = noSalary;
    }

    public Set<DayOffUser> getDayOffUsers() {
        return this.dayOffUsers;
    }

    public DayOffType dayOffUsers(Set<DayOffUser> dayOffUsers) {
        this.setDayOffUsers(dayOffUsers);
        return this;
    }

    public DayOffType addDayOffUser(DayOffUser dayOffUser) {
        this.dayOffUsers.add(dayOffUser);
        dayOffUser.setDayOffType(this);
        return this;
    }

    public DayOffType removeDayOffUser(DayOffUser dayOffUser) {
        this.dayOffUsers.remove(dayOffUser);
        dayOffUser.setDayOffType(null);
        return this;
    }

    public void setDayOffUsers(Set<DayOffUser> dayOffUsers) {
        if (this.dayOffUsers != null) {
            this.dayOffUsers.forEach(i -> i.setDayOffType(null));
        }
        if (dayOffUsers != null) {
            dayOffUsers.forEach(i -> i.setDayOffType(this));
        }
        this.dayOffUsers = dayOffUsers;
    }

    public Set<DayOffTicket> getDayOffTickets() {
        return this.dayOffTickets;
    }

    public DayOffType dayOffTickets(Set<DayOffTicket> dayOffTickets) {
        this.setDayOffTickets(dayOffTickets);
        return this;
    }

    public DayOffType addDayOffTicket(DayOffTicket dayOffTicket) {
        this.dayOffTickets.add(dayOffTicket);
        dayOffTicket.setDayOffType(this);
        return this;
    }

    public DayOffType removeDayOffTicket(DayOffTicket dayOffTicket) {
        this.dayOffTickets.remove(dayOffTicket);
        dayOffTicket.setDayOffType(null);
        return this;
    }

    public void setDayOffTickets(Set<DayOffTicket> dayOffTickets) {
        if (this.dayOffTickets != null) {
            this.dayOffTickets.forEach(i -> i.setDayOffType(null));
        }
        if (dayOffTickets != null) {
            dayOffTickets.forEach(i -> i.setDayOffType(this));
        }
        this.dayOffTickets = dayOffTickets;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DayOffType)) {
            return false;
        }
        return id != null && id.equals(((DayOffType) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DayOffType{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", numberOfDay=" + getNumberOfDay() +
            ", noSalary='" + getNoSalary() + "'" +
            "}";
    }
}
