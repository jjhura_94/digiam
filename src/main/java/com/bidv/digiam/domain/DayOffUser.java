package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A DayOffUser.
 */
@Entity
@Table(name = "day_off_user")
public class DayOffUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "day_left")
    private Integer dayLeft;

    @Column(name = "year")
    private Integer year;

    @ManyToOne
    @JsonIgnoreProperties(value = { "dayOffUsers", "dayOffTickets" }, allowSetters = true)
    private DayOffType dayOffType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private EmpMgt emp;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DayOffUser id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getDayLeft() {
        return this.dayLeft;
    }

    public DayOffUser dayLeft(Integer dayLeft) {
        this.dayLeft = dayLeft;
        return this;
    }

    public void setDayLeft(Integer dayLeft) {
        this.dayLeft = dayLeft;
    }

    public Integer getYear() {
        return this.year;
    }

    public DayOffUser year(Integer year) {
        this.year = year;
        return this;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public DayOffType getDayOffType() {
        return this.dayOffType;
    }

    public DayOffUser dayOffType(DayOffType dayOffType) {
        this.setDayOffType(dayOffType);
        return this;
    }

    public void setDayOffType(DayOffType dayOffType) {
        this.dayOffType = dayOffType;
    }

    public EmpMgt getEmp() {
        return this.emp;
    }

    public DayOffUser emp(EmpMgt empMgt) {
        this.setEmp(empMgt);
        return this;
    }

    public void setEmp(EmpMgt empMgt) {
        this.emp = empMgt;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DayOffUser)) {
            return false;
        }
        return id != null && id.equals(((DayOffUser) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DayOffUser{" +
            "id=" + getId() +
            ", dayLeft=" + getDayLeft() +
            ", year=" + getYear() +
            "}";
    }
}
