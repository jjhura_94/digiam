package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A DeptMgt.
 */
@Entity
@Table(name = "dept_mgt")
public class DeptMgt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "dept_code")
    private String deptCode;

    @Column(name = "dept_name")
    private String deptName;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "parent_dept_code")
    private String parentDeptCode;

    @OneToMany(mappedBy = "dept")
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private Set<EmpMgt> empMgts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DeptMgt id(Long id) {
        this.id = id;
        return this;
    }

    public String getDeptCode() {
        return this.deptCode;
    }

    public DeptMgt deptCode(String deptCode) {
        this.deptCode = deptCode;
        return this;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptName() {
        return this.deptName;
    }

    public DeptMgt deptName(String deptName) {
        this.deptName = deptName;
        return this;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Boolean getStatus() {
        return this.status;
    }

    public DeptMgt status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getParentDeptCode() {
        return this.parentDeptCode;
    }

    public DeptMgt parentDeptCode(String parentDeptCode) {
        this.parentDeptCode = parentDeptCode;
        return this;
    }

    public void setParentDeptCode(String parentDeptCode) {
        this.parentDeptCode = parentDeptCode;
    }

    public Set<EmpMgt> getEmpMgts() {
        return this.empMgts;
    }

    public DeptMgt empMgts(Set<EmpMgt> empMgts) {
        this.setEmpMgts(empMgts);
        return this;
    }

    public DeptMgt addEmpMgt(EmpMgt empMgt) {
        this.empMgts.add(empMgt);
        empMgt.setDept(this);
        return this;
    }

    public DeptMgt removeEmpMgt(EmpMgt empMgt) {
        this.empMgts.remove(empMgt);
        empMgt.setDept(null);
        return this;
    }

    public void setEmpMgts(Set<EmpMgt> empMgts) {
        if (this.empMgts != null) {
            this.empMgts.forEach(i -> i.setDept(null));
        }
        if (empMgts != null) {
            empMgts.forEach(i -> i.setDept(this));
        }
        this.empMgts = empMgts;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeptMgt)) {
            return false;
        }
        return id != null && id.equals(((DeptMgt) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DeptMgt{" +
            "id=" + getId() +
            ", deptCode='" + getDeptCode() + "'" +
            ", deptName='" + getDeptName() + "'" +
            ", status='" + getStatus() + "'" +
            ", parentDeptCode='" + getParentDeptCode() + "'" +
            "}";
    }
}
