package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A DutyMgt.
 */
@Entity
@Table(name = "duty_mgt")
public class DutyMgt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "duty_code")
    private String dutyCode;

    @Column(name = "duty_name")
    private String dutyName;

    @Column(name = "use_yn")
    private Boolean useYn;

    @Column(name = "duty_ord")
    private String dutyOrd;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @OneToMany(mappedBy = "duty")
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private Set<EmpMgt> empMgts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DutyMgt id(Long id) {
        this.id = id;
        return this;
    }

    public String getDutyCode() {
        return this.dutyCode;
    }

    public DutyMgt dutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
        return this;
    }

    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    public Boolean getUseYn() {
        return useYn;
    }

    public void setUseYn(Boolean useYn) {
        this.useYn = useYn;
    }

    public String getDutyOrd() {
        return this.dutyOrd;
    }

    public DutyMgt dutyOrd(String dutyOrd) {
        this.dutyOrd = dutyOrd;
        return this;
    }

    public void setDutyOrd(String dutyOrd) {
        this.dutyOrd = dutyOrd;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public DutyMgt createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public DutyMgt createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public DutyMgt updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public DutyMgt updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Set<EmpMgt> getEmpMgts() {
        return this.empMgts;
    }

    public DutyMgt empMgts(Set<EmpMgt> empMgts) {
        this.setEmpMgts(empMgts);
        return this;
    }

    public DutyMgt addEmpMgt(EmpMgt empMgt) {
        this.empMgts.add(empMgt);
        empMgt.setDuty(this);
        return this;
    }

    public DutyMgt removeEmpMgt(EmpMgt empMgt) {
        this.empMgts.remove(empMgt);
        empMgt.setDuty(null);
        return this;
    }

    public void setEmpMgts(Set<EmpMgt> empMgts) {
        if (this.empMgts != null) {
            this.empMgts.forEach(i -> i.setDuty(null));
        }
        if (empMgts != null) {
            empMgts.forEach(i -> i.setDuty(this));
        }
        this.empMgts = empMgts;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DutyMgt)) {
            return false;
        }
        return id != null && id.equals(((DutyMgt) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DutyMgt{" +
            "id=" + getId() +
            ", dutyCode='" + getDutyCode() + "'" +
            ", dutyName='" + getDutyName() + "'" +
            ", useYn='" + getUseYn() + "'" +
            ", dutyOrd='" + getDutyOrd() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
