package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A EmpJoinMeeting.
 */
@Entity
@Table(name = "emp_join_meeting")
public class EmpJoinMeeting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "user", "dayOffUsers", "empJoinMeetings", "assets", "assetHistories", "dept", "duty" },
        allowSetters = true
    )
    private EmpMgt emp;

    @ManyToOne
    @JsonIgnoreProperties(value = { "empJoinMeetings", "room" }, allowSetters = true)
    private Meeting meeting;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmpJoinMeeting id(Long id) {
        this.id = id;
        return this;
    }

    public EmpMgt getEmp() {
        return this.emp;
    }

    public EmpJoinMeeting emp(EmpMgt empMgt) {
        this.setEmp(empMgt);
        return this;
    }

    public void setEmp(EmpMgt empMgt) {
        this.emp = empMgt;
    }

    public Meeting getMeeting() {
        return this.meeting;
    }

    public EmpJoinMeeting meeting(Meeting meeting) {
        this.setMeeting(meeting);
        return this;
    }

    public void setMeeting(Meeting meeting) {
        this.meeting = meeting;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmpJoinMeeting)) {
            return false;
        }
        return id != null && id.equals(((EmpJoinMeeting) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmpJoinMeeting{" +
            "id=" + getId() +
            "}";
    }
}
