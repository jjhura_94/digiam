package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A EmpMgt.
 */
@Entity
@Table(name = "emp_mgt")
public class EmpMgt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "emp_no")
    private String empNo;

    @Column(name = "name")
    private String name;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "dob")
    private Instant dob;

    @Column(name = "email")
    private String email;

    @Column(name = "cell_phone")
    private String cellPhone;

    @Column(name = "ext_number")
    private Integer extNumber;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "address")
    private String address;

    @Column(name = "gender")
    private String gender;

    @Column(name = "img_path")
    private String imgPath;

    @Column(name = "status")
    private String status;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "emp", fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = { "dayOffType", "emp" }, allowSetters = true)
    private Set<DayOffUser> dayOffUsers = new HashSet<>();

    @OneToMany(mappedBy = "emp")
    @JsonIgnoreProperties(value = { "emp", "meeting" }, allowSetters = true)
    private Set<EmpJoinMeeting> empJoinMeetings = new HashSet<>();

    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = { "assetHistories", "assetType", "owner" }, allowSetters = true)
    private Set<Asset> assets = new HashSet<>();

    @OneToMany(mappedBy = "oldOwner")
    @JsonIgnoreProperties(value = { "oldOwner", "asset" }, allowSetters = true)
    private Set<AssetHistory> assetHistories = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "empMgts" }, allowSetters = true)
    private DeptMgt dept;

    @ManyToOne
    @JsonIgnoreProperties(value = { "empMgts" }, allowSetters = true)
    private DutyMgt duty;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmpMgt id(Long id) {
        this.id = id;
        return this;
    }

    public String getEmpNo() {
        return this.empNo;
    }

    public EmpMgt empNo(String empNo) {
        this.empNo = empNo;
        return this;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }

    public String getName() {
        return this.name;
    }

    public EmpMgt name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartDate() {
        return this.startDate;
    }

    public EmpMgt startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return this.endDate;
    }

    public EmpMgt endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getDob() {
        return this.dob;
    }

    public EmpMgt dob(Instant dob) {
        this.dob = dob;
        return this;
    }

    public void setDob(Instant dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return this.email;
    }

    public EmpMgt email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhone() {
        return this.cellPhone;
    }

    public EmpMgt cellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
        return this;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public Integer getExtNumber() {
        return this.extNumber;
    }

    public EmpMgt extNumber(Integer extNumber) {
        this.extNumber = extNumber;
        return this;
    }

    public void setExtNumber(Integer extNumber) {
        this.extNumber = extNumber;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public EmpMgt zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return this.address;
    }

    public EmpMgt address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return this.gender;
    }

    public EmpMgt gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImgPath() {
        return this.imgPath;
    }

    public EmpMgt imgPath(String imgPath) {
        this.imgPath = imgPath;
        return this;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getStatus() {
        return this.status;
    }

    public EmpMgt status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return this.user;
    }

    public EmpMgt user(User user) {
        this.setUser(user);
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<DayOffUser> getDayOffUsers() {
        return this.dayOffUsers;
    }

    public EmpMgt dayOffUsers(Set<DayOffUser> dayOffUsers) {
        this.setDayOffUsers(dayOffUsers);
        return this;
    }

    public EmpMgt addDayOffUser(DayOffUser dayOffUser) {
        this.dayOffUsers.add(dayOffUser);
        dayOffUser.setEmp(this);
        return this;
    }

    public EmpMgt removeDayOffUser(DayOffUser dayOffUser) {
        this.dayOffUsers.remove(dayOffUser);
        dayOffUser.setEmp(null);
        return this;
    }

    public void setDayOffUsers(Set<DayOffUser> dayOffUsers) {
        if (this.dayOffUsers != null) {
            this.dayOffUsers.forEach(i -> i.setEmp(null));
        }
        if (dayOffUsers != null) {
            dayOffUsers.forEach(i -> i.setEmp(this));
        }
        this.dayOffUsers = dayOffUsers;
    }

    public Set<EmpJoinMeeting> getEmpJoinMeetings() {
        return this.empJoinMeetings;
    }

    public EmpMgt empJoinMeetings(Set<EmpJoinMeeting> empJoinMeetings) {
        this.setEmpJoinMeetings(empJoinMeetings);
        return this;
    }

    public EmpMgt addEmpJoinMeeting(EmpJoinMeeting empJoinMeeting) {
        this.empJoinMeetings.add(empJoinMeeting);
        empJoinMeeting.setEmp(this);
        return this;
    }

    public EmpMgt removeEmpJoinMeeting(EmpJoinMeeting empJoinMeeting) {
        this.empJoinMeetings.remove(empJoinMeeting);
        empJoinMeeting.setEmp(null);
        return this;
    }

    public void setEmpJoinMeetings(Set<EmpJoinMeeting> empJoinMeetings) {
        if (this.empJoinMeetings != null) {
            this.empJoinMeetings.forEach(i -> i.setEmp(null));
        }
        if (empJoinMeetings != null) {
            empJoinMeetings.forEach(i -> i.setEmp(this));
        }
        this.empJoinMeetings = empJoinMeetings;
    }

    public Set<Asset> getAssets() {
        return this.assets;
    }

    public EmpMgt assets(Set<Asset> assets) {
        this.setAssets(assets);
        return this;
    }

    public EmpMgt addAsset(Asset asset) {
        this.assets.add(asset);
        asset.setOwner(this);
        return this;
    }

    public EmpMgt removeAsset(Asset asset) {
        this.assets.remove(asset);
        asset.setOwner(null);
        return this;
    }

    public void setAssets(Set<Asset> assets) {
        if (this.assets != null) {
            this.assets.forEach(i -> i.setOwner(null));
        }
        if (assets != null) {
            assets.forEach(i -> i.setOwner(this));
        }
        this.assets = assets;
    }

    public Set<AssetHistory> getAssetHistories() {
        return this.assetHistories;
    }

    public EmpMgt assetHistories(Set<AssetHistory> assetHistories) {
        this.setAssetHistories(assetHistories);
        return this;
    }

    public EmpMgt addAssetHistory(AssetHistory assetHistory) {
        this.assetHistories.add(assetHistory);
        assetHistory.setOldOwner(this);
        return this;
    }

    public EmpMgt removeAssetHistory(AssetHistory assetHistory) {
        this.assetHistories.remove(assetHistory);
        assetHistory.setOldOwner(null);
        return this;
    }

    public void setAssetHistories(Set<AssetHistory> assetHistories) {
        if (this.assetHistories != null) {
            this.assetHistories.forEach(i -> i.setOldOwner(null));
        }
        if (assetHistories != null) {
            assetHistories.forEach(i -> i.setOldOwner(this));
        }
        this.assetHistories = assetHistories;
    }

    public DeptMgt getDept() {
        return this.dept;
    }

    public EmpMgt dept(DeptMgt deptMgt) {
        this.setDept(deptMgt);
        return this;
    }

    public void setDept(DeptMgt deptMgt) {
        this.dept = deptMgt;
    }

    public DutyMgt getDuty() {
        return this.duty;
    }

    public EmpMgt duty(DutyMgt dutyMgt) {
        this.setDuty(dutyMgt);
        return this;
    }

    public void setDuty(DutyMgt dutyMgt) {
        this.duty = dutyMgt;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmpMgt)) {
            return false;
        }
        return id != null && id.equals(((EmpMgt) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmpMgt{" +
            "id=" + getId() +
            ", empNo='" + getEmpNo() + "'" +
            ", name='" + getName() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", dob='" + getDob() + "'" +
            ", email='" + getEmail() + "'" +
            ", cellPhone='" + getCellPhone() + "'" +
            ", extNumber=" + getExtNumber() +
            ", zipCode='" + getZipCode() + "'" +
            ", address='" + getAddress() + "'" +
            ", gender='" + getGender() + "'" +
            ", imgPath='" + getImgPath() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
