package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 * A Meeting.
 */
@Entity
@Table(name = "meeting")
public class Meeting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "meeting_from")
    private Instant meetingFrom;

    @Column(name = "meeting_to")
    private Instant meetingTo;

    @Column(name = "offline")
    private Boolean offline;

    @Column(name = "room_online_link")
    private String roomOnlineLink;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @OneToMany(mappedBy = "meeting")
    @JsonIgnoreProperties(value = { "emp", "meeting" }, allowSetters = true)
    private Set<EmpJoinMeeting> empJoinMeetings = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "assetsInRooms", "meetings" }, allowSetters = true)
    private MeetingRoom room;

    @Column(name = "title")
    private String title;

    @Column(name = "note")
    private String note;

    @JsonInclude
    @Transient
    private List<EmpMgt> empsJoinMeeting = new ArrayList<EmpMgt>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Meeting id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getMeetingFrom() {
        return this.meetingFrom;
    }

    public Meeting meetingFrom(Instant meetingFrom) {
        this.meetingFrom = meetingFrom;
        return this;
    }

    public void setMeetingFrom(Instant meetingFrom) {
        this.meetingFrom = meetingFrom;
    }

    public Instant getMeetingTo() {
        return this.meetingTo;
    }

    public Meeting meetingTo(Instant meetingTo) {
        this.meetingTo = meetingTo;
        return this;
    }

    public void setMeetingTo(Instant meetingTo) {
        this.meetingTo = meetingTo;
    }

    public Boolean getOffline() {
        return this.offline;
    }

    public Meeting offline(Boolean offline) {
        this.offline = offline;
        return this;
    }

    public void setOffline(Boolean offline) {
        this.offline = offline;
    }

    public String getRoomOnlineLink() {
        return this.roomOnlineLink;
    }

    public Meeting roomOnlineLink(String roomOnlineLink) {
        this.roomOnlineLink = roomOnlineLink;
        return this;
    }

    public void setRoomOnlineLink(String roomOnlineLink) {
        this.roomOnlineLink = roomOnlineLink;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Meeting createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Meeting createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public Meeting updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public Meeting updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<EmpJoinMeeting> getEmpJoinMeetings() {
        return this.empJoinMeetings;
    }

    public Meeting empJoinMeetings(Set<EmpJoinMeeting> empJoinMeetings) {
        this.setEmpJoinMeetings(empJoinMeetings);
        return this;
    }

    public Meeting addEmpJoinMeeting(EmpJoinMeeting empJoinMeeting) {
        this.empJoinMeetings.add(empJoinMeeting);
        empJoinMeeting.setMeeting(this);
        return this;
    }

    public Meeting removeEmpJoinMeeting(EmpJoinMeeting empJoinMeeting) {
        this.empJoinMeetings.remove(empJoinMeeting);
        empJoinMeeting.setMeeting(null);
        return this;
    }

    public void setEmpJoinMeetings(Set<EmpJoinMeeting> empJoinMeetings) {
        if (this.empJoinMeetings != null) {
            this.empJoinMeetings.forEach(i -> i.setMeeting(null));
        }
        if (empJoinMeetings != null) {
            empJoinMeetings.forEach(i -> i.setMeeting(this));
        }
        this.empJoinMeetings = empJoinMeetings;
    }

    public MeetingRoom getRoom() {
        return this.room;
    }

    public Meeting room(MeetingRoom meetingRoom) {
        this.setRoom(meetingRoom);
        return this;
    }

    public void setRoom(MeetingRoom meetingRoom) {
        this.room = meetingRoom;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Meeting)) {
            return false;
        }
        return id != null && id.equals(((Meeting) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Meeting{" +
            "id=" + getId() +
            ", meetingFrom='" + getMeetingFrom() + "'" +
            ", meetingTo='" + getMeetingTo() + "'" +
            ", offline='" + getOffline() + "'" +
            ", roomOnlineLink='" + getRoomOnlineLink() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<EmpMgt> getEmpsJoinMeeting() {
        return empsJoinMeeting;
    }

    public void setEmpsJoinMeeting(List<EmpMgt> empsJoinMeeting) {
        this.empsJoinMeeting = empsJoinMeeting;
    }
}
