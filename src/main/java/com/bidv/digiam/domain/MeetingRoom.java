package com.bidv.digiam.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A MeetingRoom.
 */
@Entity
@Table(name = "meeting_room")
public class MeetingRoom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "room_code")
    private String roomCode;

    @Column(name = "room_name")
    private String roomName;

    @Column(name = "location")
    private String location;

    @Column(name = "slot")
    private Integer slot;

    @Column(name = "max_slot")
    private Integer maxSlot;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "online")
    private Boolean online;

    @Column(name = "link_room_online")
    private String linkRoomOnline;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "updated_by")
    private String updatedBy;

    @OneToMany(mappedBy = "room")
    @JsonIgnoreProperties(value = { "room", "asset" }, allowSetters = true)
    private Set<AssetsInRoom> assetsInRooms = new HashSet<>();

    @OneToMany(mappedBy = "room")
    @JsonIgnoreProperties(value = { "empJoinMeetings", "room" }, allowSetters = true)
    private Set<Meeting> meetings = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MeetingRoom id(Long id) {
        this.id = id;
        return this;
    }

    public String getRoomCode() {
        return this.roomCode;
    }

    public MeetingRoom roomCode(String roomCode) {
        this.roomCode = roomCode;
        return this;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getRoomName() {
        return this.roomName;
    }

    public MeetingRoom roomName(String roomName) {
        this.roomName = roomName;
        return this;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getLocation() {
        return this.location;
    }

    public MeetingRoom location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getSlot() {
        return this.slot;
    }

    public MeetingRoom slot(Integer slot) {
        this.slot = slot;
        return this;
    }

    public void setSlot(Integer slot) {
        this.slot = slot;
    }

    public Integer getMaxSlot() {
        return this.maxSlot;
    }

    public MeetingRoom maxSlot(Integer maxSlot) {
        this.maxSlot = maxSlot;
        return this;
    }

    public void setMaxSlot(Integer maxSlot) {
        this.maxSlot = maxSlot;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public MeetingRoom createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public MeetingRoom createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return this.updatedDate;
    }

    public MeetingRoom updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public MeetingRoom updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<AssetsInRoom> getAssetsInRooms() {
        return this.assetsInRooms;
    }

    public MeetingRoom assetsInRooms(Set<AssetsInRoom> assetsInRooms) {
        this.setAssetsInRooms(assetsInRooms);
        return this;
    }

    public MeetingRoom addAssetsInRoom(AssetsInRoom assetsInRoom) {
        this.assetsInRooms.add(assetsInRoom);
        assetsInRoom.setRoom(this);
        return this;
    }

    public MeetingRoom removeAssetsInRoom(AssetsInRoom assetsInRoom) {
        this.assetsInRooms.remove(assetsInRoom);
        assetsInRoom.setRoom(null);
        return this;
    }

    public void setAssetsInRooms(Set<AssetsInRoom> assetsInRooms) {
        if (this.assetsInRooms != null) {
            this.assetsInRooms.forEach(i -> i.setRoom(null));
        }
        if (assetsInRooms != null) {
            assetsInRooms.forEach(i -> i.setRoom(this));
        }
        this.assetsInRooms = assetsInRooms;
    }

    public Set<Meeting> getMeetings() {
        return this.meetings;
    }

    public MeetingRoom meetings(Set<Meeting> meetings) {
        this.setMeetings(meetings);
        return this;
    }

    public MeetingRoom addMeeting(Meeting meeting) {
        this.meetings.add(meeting);
        meeting.setRoom(this);
        return this;
    }

    public MeetingRoom removeMeeting(Meeting meeting) {
        this.meetings.remove(meeting);
        meeting.setRoom(null);
        return this;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public void setMeetings(Set<Meeting> meetings) {
        if (this.meetings != null) {
            this.meetings.forEach(i -> i.setRoom(null));
        }
        if (meetings != null) {
            meetings.forEach(i -> i.setRoom(this));
        }
        this.meetings = meetings;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MeetingRoom)) {
            return false;
        }
        return id != null && id.equals(((MeetingRoom) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MeetingRoom{" +
            "id=" + getId() +
            ", roomCode='" + getRoomCode() + "'" +
            ", roomName='" + getRoomName() + "'" +
            ", location='" + getLocation() + "'" +
            ", slot=" + getSlot() +
            ", maxSlot=" + getMaxSlot() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }

    public String getLinkRoomOnline() {
        return linkRoomOnline;
    }

    public void setLinkRoomOnline(String linkRoomOnline) {
        this.linkRoomOnline = linkRoomOnline;
    }
}
