package com.bidv.digiam.repository;

import com.bidv.digiam.domain.AssetHistory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AssetHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssetHistoryRepository extends JpaRepository<AssetHistory, Long> {}
