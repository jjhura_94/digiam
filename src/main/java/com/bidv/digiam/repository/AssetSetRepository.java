package com.bidv.digiam.repository;

import com.bidv.digiam.domain.AssetSet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AssetSet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssetSetRepository extends JpaRepository<AssetSet, Long> {}
