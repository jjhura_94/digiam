package com.bidv.digiam.repository;

import com.bidv.digiam.domain.AssetType;
import com.bidv.digiam.domain.EmpMgt;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AssetType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssetTypeRepository extends JpaRepository<AssetType, Long> {
    @Modifying
    @Query(value = "select * from asset_type where UPPER(code) = UPPER(:code)", nativeQuery = true)
    public List<AssetType> getAssetType(@Param("code") String code);
}
