package com.bidv.digiam.repository;

import com.bidv.digiam.domain.AssetsInRoom;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the AssetsInRoom entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssetsInRoomRepository extends JpaRepository<AssetsInRoom, Long> {}
