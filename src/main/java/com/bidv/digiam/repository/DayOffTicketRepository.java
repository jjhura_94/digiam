package com.bidv.digiam.repository;

import com.bidv.digiam.domain.DayOffTicket;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DayOffTicket entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayOffTicketRepository extends JpaRepository<DayOffTicket, Long> {
    @Modifying
    @Query(
        value = "select * from day_off_ticket where  " +
        " TO_CHAR (from_date, 'yyyy-MM-dd') <= :currentDay and TO_CHAR(to_date, 'yyyy-MM-dd') >= :currentDay ",
        nativeQuery = true
    )
    public List<DayOffTicket> search(@Param("currentDay") String currentDay);

    @Modifying
    @Query(
        value = "select * from day_off_ticket where  created_by_id = :empId and" +
        " TO_CHAR (from_date, 'yyyy-MM-dd') <= :currentDay and TO_CHAR(to_date, 'yyyy-MM-dd') >= :currentDay ",
        nativeQuery = true
    )
    public List<DayOffTicket> searchByIdEmpAndDay(@Param("currentDay") String currentDay, @Param("empId") Long empId);
}
