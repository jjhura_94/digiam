package com.bidv.digiam.repository;

import com.bidv.digiam.domain.DayOffType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DayOffType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayOffTypeRepository extends JpaRepository<DayOffType, Long> {}
