package com.bidv.digiam.repository;

import com.bidv.digiam.domain.DayOffUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DayOffUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayOffUserRepository extends JpaRepository<DayOffUser, Long> {}
