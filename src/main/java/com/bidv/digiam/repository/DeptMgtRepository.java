package com.bidv.digiam.repository;

import com.bidv.digiam.domain.DeptMgt;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DeptMgt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeptMgtRepository extends JpaRepository<DeptMgt, Long> {}
