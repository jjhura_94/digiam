package com.bidv.digiam.repository;

import com.bidv.digiam.domain.DutyMgt;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DutyMgt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DutyMgtRepository extends JpaRepository<DutyMgt, Long> {}
