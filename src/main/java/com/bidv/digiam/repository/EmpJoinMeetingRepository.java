package com.bidv.digiam.repository;

import com.bidv.digiam.domain.EmpJoinMeeting;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the EmpJoinMeeting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmpJoinMeetingRepository extends JpaRepository<EmpJoinMeeting, Long> {
    @Modifying
    @Query(value = "delete from emp_join_meeting where meeting_id = :meetingId", nativeQuery = true)
    public void deleteEmpJoinMeetingByMeetingId(@Param("meetingId") Long meetingId);

    @Modifying
    @Query(value = "select * from emp_join_meeting where meeting_id = :meetingId", nativeQuery = true)
    public List<EmpJoinMeeting> getEmpJoinMeetingByMeetingId(@Param("meetingId") Long meetingId);
}
