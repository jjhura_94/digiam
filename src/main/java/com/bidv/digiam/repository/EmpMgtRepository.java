package com.bidv.digiam.repository;

import com.bidv.digiam.domain.EmpMgt;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the EmpMgt entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmpMgtRepository extends JpaRepository<EmpMgt, Long> {
    @Modifying
    @Query(value = "select * from emp_mgt where UPPER(emp_no) = UPPER(:empNo)", nativeQuery = true)
    public List<EmpMgt> getEmpByNo(@Param("empNo") String empNo);
}
