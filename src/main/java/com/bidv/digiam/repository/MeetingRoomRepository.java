package com.bidv.digiam.repository;

import com.bidv.digiam.domain.MeetingRoom;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MeetingRoom entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MeetingRoomRepository extends JpaRepository<MeetingRoom, Long> {}
