package com.bidv.digiam.service;

import com.bidv.digiam.web.rest.MeetingResource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailService extends Thread {

    private final Logger log = LoggerFactory.getLogger(EmailService.class);
    private String host = "";
    private int port = 0;
    private String username = "";
    private String password = "";

    private MimeMessage message;

    public EmailService(String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        //            sendMail();
    }

    private void sendMail(String mail) {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.ssl.trust", host);

        Session session = Session.getInstance(
            prop,
            new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            }
        );

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(this.username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
            message.setSubject("Mail Subject");

            String msg = "This is my first email using JavaMailer";

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg, "text/html");

            //            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            //            attachmentBodyPart.attachFile(new File("pom.xml"));

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            //            multipart.addBodyPart(attachmentBodyPart);

            message.setContent(multipart);

            Transport.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void execute(String msg, String[] mailTo) throws Exception {
        if (mailTo != null) {
            for (String to : mailTo) {
                //    			log.info("... preparing send mail to : " + to);
                Properties props = new Properties();
                props.put("mail.transport.protocol", "smtp");
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.host", "10.53.7.117");

                Authenticator auth = new SMTPAuthenticator();
                Session mailSession = Session.getDefaultInstance(props, auth);
                Transport transport = mailSession.getTransport();

                MimeMessage message = new MimeMessage(mailSession);
                message.setHeader("Content-Type", "text/html; charset=UTF-8");
                message.setFrom(new InternetAddress("dungnv35@bidv.com.vn"));
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
                message.setSubject("Thanh toán lương qua phần mềm Vnresource");
                message.setContent(msg, "text/html; charset=UTF-8");

                transport.connect();
                transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
                transport.close();
                System.out.println("send mail to " + to + " successfully! ");
            }
        }
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        public PasswordAuthentication getPasswordAuthentication() {
            String username = "dungnv35@bidv.com.vn";
            String password = "jjhura@123456";
            return new PasswordAuthentication(username, password);
        }
    }

    public void send(String mailTo, Date from, Date to, String contentMeeting, String subject) throws Exception {
        try {
            Properties prop = new Properties();
            prop.put("mail.smtp.auth", true);
            prop.put("mail.smtp.starttls.enable", "true");
            prop.put("mail.smtp.host", host);
            prop.put("mail.smtp.port", port);
            prop.put("mail.smtp.ssl.trust", host);

            Session session = Session.getInstance(
                prop,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                }
            );
            // Define message
            MimeMessage message = new MimeMessage(session);
            message.addHeaderLine("method=REQUEST");
            message.addHeaderLine("charset=UTF-8");
            message.addHeaderLine("component=VEVENT");

            message.setFrom(new InternetAddress(this.username));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailTo));
            message.setSubject(subject);

            StringBuffer sb = new StringBuffer();

            //            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmmss");
            SimpleDateFormat fm = new SimpleDateFormat("yyyyMMdd HHmmss");

            StringBuffer buffer = sb.append(
                "BEGIN:VCALENDAR\n" +
                "PRODID:-//Microsoft Corporation//Outlook 9.0 MIMEDIR//EN\n" +
                "VERSION:2.0\n" +
                "METHOD:REQUEST\n" +
                "BEGIN:VEVENT\n" +
                "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" +
                this.username +
                " \n" +
                "ORGANIZER:MAILTO:" +
                this.username +
                "\n" +
                "DTSTART:" +
                fm.format(from).replace(" ", "T") +
                "\n" +
                "DTEND:" +
                fm.format(to).replace(" ", "T") +
                "\n" +
                "LOCATION:Conference room\n" +
                "TRANSP:OPAQUE\n" +
                "SEQUENCE:0\n" +
                "UID:040000008200E00074C5B7101A82E00800000000002FF466CE3AC5010000000000000000100\n" +
                " 000004377FE5C37984842BF9440448399EB02\n" +
                "DTSTAMP:20051206T120102Z\n" +
                "CATEGORIES:" +
                subject +
                "\n" +
                "DESCRIPTION:" +
                contentMeeting +
                " \n\n" +
                "SUMMARY:\n" +
                "PRIORITY:5\n" +
                "CLASS:PUBLIC\n" +
                "BEGIN:VALARM\n" +
                "TRIGGER:PT1440M\n" +
                "ACTION:DISPLAY\n" +
                "DESCRIPTION:Reminder\n" +
                "END:VALARM\n" +
                "END:VEVENT\n" +
                "END:VCALENDAR"
            );

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setHeader("Content-Class", "urn:content-  classes:calendarmessage");
            messageBodyPart.setHeader("Content-ID", "calendar_message");
            messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(buffer.toString(), "text/calendar"))); // very important

            // Create a Multipart
            Multipart multipart = new MimeMultipart();

            // Add part one
            multipart.addBodyPart(messageBodyPart);

            // Put parts in message
            message.setContent(multipart);

            // send message
            Transport.send(message);
        } catch (MessagingException me) {
            me.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createMessage(String mailTo, Date from, Date to, String contentMeeting, String subject) throws Exception {
        try {
            Properties prop = new Properties();
            prop.put("mail.smtp.auth", true);
            prop.put("mail.smtp.starttls.enable", "true");
            prop.put("mail.smtp.host", host);
            prop.put("mail.smtp.port", port);
            prop.put("mail.smtp.ssl.trust", host);

            Session session = Session.getInstance(
                prop,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                }
            );
            // Define message
            MimeMessage message = new MimeMessage(session);
            message.addHeaderLine("method=REQUEST");
            message.addHeaderLine("charset=UTF-8");
            message.addHeaderLine("component=VEVENT");

            message.setFrom(new InternetAddress(this.username));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailTo));
            message.setSubject(subject);

            StringBuffer sb = new StringBuffer();

            //            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmmss");
            SimpleDateFormat fm = new SimpleDateFormat("yyyyMMdd HHmmss");

            StringBuffer buffer = sb.append(
                "BEGIN:VCALENDAR\n" +
                "PRODID:-//Microsoft Corporation//Outlook 9.0 MIMEDIR//EN\n" +
                "VERSION:2.0\n" +
                "METHOD:REQUEST\n" +
                "BEGIN:VEVENT\n" +
                "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" +
                this.username +
                " \n" +
                "ORGANIZER:MAILTO:" +
                this.username +
                "\n" +
                "DTSTART:" +
                fm.format(from).replace(" ", "T") +
                "\n" +
                "DTEND:" +
                fm.format(to).replace(" ", "T") +
                "\n" +
                "LOCATION:Conference room\n" +
                "TRANSP:OPAQUE\n" +
                "SEQUENCE:0\n" +
                "UID:040000008200E00074C5B7101A82E00800000000002FF466CE3AC5010000000000000000100\n" +
                " 000004377FE5C37984842BF9440448399EB02\n" +
                "DTSTAMP:20051206T120102Z\n" +
                "CATEGORIES:" +
                subject +
                "\n" +
                "DESCRIPTION:" +
                contentMeeting +
                " \n\n" +
                "SUMMARY:\n" +
                "PRIORITY:5\n" +
                "CLASS:PUBLIC\n" +
                "BEGIN:VALARM\n" +
                "TRIGGER:PT1440M\n" +
                "ACTION:DISPLAY\n" +
                "DESCRIPTION:Reminder\n" +
                "END:VALARM\n" +
                "END:VEVENT\n" +
                "END:VCALENDAR"
            );

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setHeader("Content-Class", "urn:content-  classes:calendarmessage");
            messageBodyPart.setHeader("Content-ID", "calendar_message");
            messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(buffer.toString(), "text/calendar"))); // very important

            // Create a Multipart
            Multipart multipart = new MimeMultipart();

            // Add part one
            multipart.addBodyPart(messageBodyPart);

            // Put parts in message
            message.setContent(multipart);

            this.message = message;
            // send message
            //            Transport.send(message);

        } catch (MessagingException me) {
            me.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String... args) throws Exception {
        EmailService emailService = new EmailService("smtp.gmail.com", 25, "nvdung2802@gmail.com", "vlzxkajxnkuinknj");
        //    	emailService.sendMail("dungnv35@bidv.com.vn");

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, 2);
        //    	emailService.send("dungnv35@bidv.com.vn",new Date(),calendar.getTime(), "có 1 cuộc họp diễn ra về vấn đề acn");
        //    	emailService.send("nvdung2802@gmail.com",new Date(),calendar.getTime(), "có 1 cuộc họp diễn ra về vấn đề acn111111");
        //        new EmailService("10.53.7.117", 25, "dungnv35@bidv.com.vn", "jjhura@123456");

        //        EmailService emailService = new EmailService();
        //        String[] mailTo = { "dungnv35@bidv.com.vn" };
        //        emailService.execute("nguyendung test mail", mailTo);

        System.out.println(emailService.method("2.02107050954311e+23"));
    }

    public static String method(String param) {
        try {
            return new BigDecimal("2.02107050954311e+23").toBigInteger().toString();
        } catch (NumberFormatException e) {
            return param;
        }
    }

    private Thread t;
    private String threadName;

    @Override
    public void run() {
        try {
            if (this.message != null) {
                Transport.send(this.message);
                log.info("send mail success");
            }
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void start(String email) {
        this.threadName = email;
        log.info("Starting send mail " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}
