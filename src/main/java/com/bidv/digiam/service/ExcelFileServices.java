package com.bidv.digiam.service;

import com.bidv.digiam.domain.Asset;
import com.bidv.digiam.repository.AssetRepository;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ExcelFileServices {

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private ExcelUtils excelUtils;

    public void store(MultipartFile file) {
        try {
            List lstAsset = excelUtils.parseExcelFile(file.getInputStream());
            // Save Customers to DataBase
            assetRepository.saveAll(lstAsset);
        } catch (IOException e) {
            throw new RuntimeException("FAIL! -> message = " + e.getMessage());
        }
    }

    // Load Data to Excel File
    public ByteArrayInputStream loadFile() {
        List<Asset> assets = (List<Asset>) assetRepository.findAll();

        try {
            ByteArrayInputStream in = ExcelUtils.assetsToExcel(assets);
            return in;
        } catch (IOException e) {}

        return null;
    }
}
