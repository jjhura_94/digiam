package com.bidv.digiam.service;

import com.bidv.digiam.domain.Asset;
import com.bidv.digiam.domain.AssetType;
import com.bidv.digiam.domain.EmpMgt;
import com.bidv.digiam.repository.AssetTypeRepository;
import com.bidv.digiam.repository.EmpMgtRepository;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ExcelUtils {

    @Autowired
    private EmpMgtRepository empMgtRepository;

    @Autowired
    private AssetTypeRepository assetTypeRepository;

    @Autowired
    private QrCodeService qrCodeService;

    public static String EXCELTYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    public static ByteArrayInputStream assetsToExcel(List<Asset> assets) throws IOException {
        String[] COLUMNs = { "Id", "Name", "Serial_No", "Note" };
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("assets");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle ageCellStyle = workbook.createCellStyle();
            ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));

            int rowIdx = 1;
            for (Asset asset : assets) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(asset.getId());
                row.createCell(1).setCellValue(asset.getName());
                row.createCell(2).setCellValue(asset.getSerialNo());
                row.createCell(3).setCellValue(asset.getNote());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }

    public String generateQr(String content) {
        File file = new File("temp" + new Date().getTime() + ".png");
        String fileBase64 = qrCodeService.generateQRCodeToBase64(file, content);
        file.delete();
        return fileBase64;
    }

    public List<Asset> parseExcelFile(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet("Assets");
            Iterator<Row> rows = sheet.iterator();

            List<Asset> lstAssets = new ArrayList<Asset>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Asset cust = new Asset();

                int cellIndex = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    if (cellIndex == 0) { // id
                        //                        cust.setId((long) currentCell.getNumericCellValue());
                    } else if (cellIndex == 1) { // name
                        cust.setName(currentCell.getStringCellValue());
                    } else if (cellIndex == 2) { // serial_no
                        cust.setSerialNo(currentCell.getStringCellValue());
                    } else if (cellIndex == 3) { // note
                        cust.setNote(currentCell.getStringCellValue());
                    } else if (cellIndex == 4) {
                        cust.setPrice(currentCell.getNumericCellValue());
                    } else if (cellIndex == 5) {
                        cust.setStatus((int) currentCell.getNumericCellValue());
                    } else if (cellIndex == 6) {
                        cust.setExpiryDateFrom(currentCell.getDateCellValue() == null ? null : currentCell.getDateCellValue().toInstant());
                    } else if (cellIndex == 7) {
                        cust.setExpiryDateTo(currentCell.getDateCellValue() == null ? null : currentCell.getDateCellValue().toInstant());
                    } else if (cellIndex == 8) {
                        cust.setLocation(currentCell.getStringCellValue());
                    } else if (cellIndex == 9) {
                        List<EmpMgt> emps = empMgtRepository.getEmpByNo(currentCell.getStringCellValue());
                        if (emps != null && emps.size() > 0) {
                            cust.setOwner(emps.get(0));
                        }
                    } else if (cellIndex == 10) {
                        List<AssetType> assetTypes = assetTypeRepository.getAssetType(currentCell.getStringCellValue());
                        if (assetTypes != null && assetTypes.size() > 0) {
                            cust.setAssetType(assetTypes.get(0));
                        }
                    }

                    String content =
                        "Tên : " +
                        cust.getName() +
                        " ; Mã : " +
                        cust.getCode() +
                        " ; Vị trí : " +
                        cust.getLocation() +
                        cust.getId() +
                        "/view";

                    cust.setQrCode(generateQr(content));
                    cust.setCode("TSNHS" + new Date().getTime());
                    cellIndex++;
                }

                lstAssets.add(cust);
            }

            // Close WorkBook
            workbook.close();

            return lstAssets;
        } catch (IOException e) {
            throw new RuntimeException("FAIL! -> message = " + e.getMessage());
        }
    }

    public static boolean isExcelFile(MultipartFile file) {
        if (!EXCELTYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }
}
