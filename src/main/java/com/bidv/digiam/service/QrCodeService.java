package com.bidv.digiam.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.springframework.stereotype.Service;

@Service
public class QrCodeService {

    public static BufferedImage generateEAN13BarcodeImage(String barcodeText) throws IOException {
        EAN13Bean barcodeGenerator = new EAN13Bean();
        BitmapCanvasProvider canvas = new BitmapCanvasProvider(160, BufferedImage.TYPE_BYTE_BINARY, false, 0);

        barcodeGenerator.generateBarcode(canvas, barcodeText);
        File outputfile = new File("image" + ".jpg");
        ImageIO.write(canvas.getBufferedImage(), "jpg", outputfile);
        return canvas.getBufferedImage();
    }

    public static void generateCode(File file, String content, int width, int height) {
        BitMatrix matrix = null;
        try {
            // Encode using code_128 format to generate a 100*25 barcode
            MultiFormatWriter writer = new MultiFormatWriter();

            matrix = writer.encode(content, BarcodeFormat.CODE_128, width, height, null);
            // matrix = writer.encode(code,BarcodeFormat.EAN_13, width, height, null);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        try (FileOutputStream outStream = new FileOutputStream(file)) {
            ImageIO.write(MatrixToImageWriter.toBufferedImage(matrix), "png", outStream);
            outStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int WIDTH = 300;
    private static int HEIGHT = 300;
    private static String FORMAT = "png";

    public static void generateQRCode(File file, String content) {
        Map<EncodeHintType, Object> hints = new HashMap<>();

        hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); // Set the encoding
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M); // Set the fault tolerance level
        hints.put(EncodeHintType.MARGIN, 2); // Set margins default to 5

        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints);
            Path path = file.toPath();
            MatrixToImageWriter.writeToPath(bitMatrix, FORMAT, path); // write to the specified path
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String generateQRCodeToBase64(File file, String content) {
        Map<EncodeHintType, Object> hints = new HashMap<>();

        hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); // Set the encoding
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M); // Set the fault tolerance level
        hints.put(EncodeHintType.MARGIN, 2); // Set margins default to 5

        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints);
            Path path = file.toPath();
            MatrixToImageWriter.writeToPath(bitMatrix, FORMAT, path); // write to the specified path
            return Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String[] args) throws IOException {
        String testInfo = "Tên : tài sản ngân hàng số ; Mã : TSNHS01 ; Vị trí : phòng giám đốc 1 ; Người sở hữu : Nguyễn Văn A";
        File fileResult = new File("image" + new Date().getTime() + ".jpg");
        //		generateEAN13BarcodeImage("123445465611");
        //		generateCode(fileResult, testInfo, 300, 300);
        generateQRCode(fileResult, testInfo);
    }
}
