package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.AssetHistory;
import com.bidv.digiam.repository.AssetHistoryRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.AssetHistory}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AssetHistoryResource {

    private final Logger log = LoggerFactory.getLogger(AssetHistoryResource.class);

    private static final String ENTITY_NAME = "assetHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssetHistoryRepository assetHistoryRepository;

    public AssetHistoryResource(AssetHistoryRepository assetHistoryRepository) {
        this.assetHistoryRepository = assetHistoryRepository;
    }

    /**
     * {@code POST  /asset-histories} : Create a new assetHistory.
     *
     * @param assetHistory the assetHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new assetHistory, or with status {@code 400 (Bad Request)} if the assetHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/asset-histories")
    public ResponseEntity<AssetHistory> createAssetHistory(@RequestBody AssetHistory assetHistory) throws URISyntaxException {
        log.debug("REST request to save AssetHistory : {}", assetHistory);
        if (assetHistory.getId() != null) {
            throw new BadRequestAlertException("A new assetHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AssetHistory result = assetHistoryRepository.save(assetHistory);
        return ResponseEntity
            .created(new URI("/api/asset-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /asset-histories/:id} : Updates an existing assetHistory.
     *
     * @param id the id of the assetHistory to save.
     * @param assetHistory the assetHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assetHistory,
     * or with status {@code 400 (Bad Request)} if the assetHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the assetHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/asset-histories/{id}")
    public ResponseEntity<AssetHistory> updateAssetHistory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AssetHistory assetHistory
    ) throws URISyntaxException {
        log.debug("REST request to update AssetHistory : {}, {}", id, assetHistory);
        if (assetHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, assetHistory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetHistoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AssetHistory result = assetHistoryRepository.save(assetHistory);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, assetHistory.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /asset-histories/:id} : Partial updates given fields of an existing assetHistory, field will ignore if it is null
     *
     * @param id the id of the assetHistory to save.
     * @param assetHistory the assetHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assetHistory,
     * or with status {@code 400 (Bad Request)} if the assetHistory is not valid,
     * or with status {@code 404 (Not Found)} if the assetHistory is not found,
     * or with status {@code 500 (Internal Server Error)} if the assetHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/asset-histories/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<AssetHistory> partialUpdateAssetHistory(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AssetHistory assetHistory
    ) throws URISyntaxException {
        log.debug("REST request to partial update AssetHistory partially : {}, {}", id, assetHistory);
        if (assetHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, assetHistory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetHistoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AssetHistory> result = assetHistoryRepository
            .findById(assetHistory.getId())
            .map(
                existingAssetHistory -> {
                    if (assetHistory.getUpdatedBy() != null) {
                        existingAssetHistory.setUpdatedBy(assetHistory.getUpdatedBy());
                    }
                    if (assetHistory.getUpdatedDate() != null) {
                        existingAssetHistory.setUpdatedDate(assetHistory.getUpdatedDate());
                    }
                    if (assetHistory.getStatus() != null) {
                        existingAssetHistory.setStatus(assetHistory.getStatus());
                    }
                    if (assetHistory.getNote() != null) {
                        existingAssetHistory.setNote(assetHistory.getNote());
                    }

                    return existingAssetHistory;
                }
            )
            .map(assetHistoryRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, assetHistory.getId().toString())
        );
    }

    /**
     * {@code GET  /asset-histories} : get all the assetHistories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assetHistories in body.
     */
    @GetMapping("/asset-histories")
    public List<AssetHistory> getAllAssetHistories() {
        log.debug("REST request to get all AssetHistories");
        return assetHistoryRepository.findAll();
    }

    /**
     * {@code GET  /asset-histories/:id} : get the "id" assetHistory.
     *
     * @param id the id of the assetHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the assetHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/asset-histories/{id}")
    public ResponseEntity<AssetHistory> getAssetHistory(@PathVariable Long id) {
        log.debug("REST request to get AssetHistory : {}", id);
        Optional<AssetHistory> assetHistory = assetHistoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(assetHistory);
    }

    /**
     * {@code DELETE  /asset-histories/:id} : delete the "id" assetHistory.
     *
     * @param id the id of the assetHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/asset-histories/{id}")
    public ResponseEntity<Void> deleteAssetHistory(@PathVariable Long id) {
        log.debug("REST request to delete AssetHistory : {}", id);
        assetHistoryRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
