package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.Asset;
import com.bidv.digiam.repository.AssetRepository;
import com.bidv.digiam.service.ExcelFileServices;
import com.bidv.digiam.service.ExcelUtils;
import com.bidv.digiam.service.QrCodeService;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.Asset}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AssetResource {

    private final Logger log = LoggerFactory.getLogger(AssetResource.class);

    private static final String ENTITY_NAME = "asset";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssetRepository assetRepository;

    @Autowired
    private QrCodeService qrCodeService;

    public AssetResource(AssetRepository assetRepository) {
        this.assetRepository = assetRepository;
    }

    /**
     * {@code POST  /assets} : Create a new asset.
     *
     * @param asset the asset to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new asset, or with status {@code 400 (Bad Request)} if the asset has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/assets")
    public ResponseEntity<Asset> createAsset(@RequestBody Asset asset) throws URISyntaxException {
        log.debug("REST request to save Asset : {}", asset);
        if (asset.getId() != null) {
            throw new BadRequestAlertException("A new asset cannot already have an ID", ENTITY_NAME, "idexists");
        }

        String content =
            "Tên : " +
            asset.getName() +
            " ; Mã : " +
            asset.getCode() +
            " ; Vị trí : " +
            asset.getLocation() +
            " ; Người sở hữu :" +
            asset.getOwner().getName();
        File file = new File("temp" + new Date().getTime() + ".png");
        String fileBase64 = qrCodeService.generateQRCodeToBase64(file, content);
        asset.setQrCode(fileBase64);
        Asset result = assetRepository.save(asset);
        return ResponseEntity
            .created(new URI("/api/assets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /assets/:id} : Updates an existing asset.
     *
     * @param id the id of the asset to save.
     * @param asset the asset to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated asset,
     * or with status {@code 400 (Bad Request)} if the asset is not valid,
     * or with status {@code 500 (Internal Server Error)} if the asset couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/assets/{id}")
    public ResponseEntity<Asset> updateAsset(@PathVariable(value = "id", required = false) final Long id, @RequestBody Asset asset)
        throws URISyntaxException {
        log.debug("REST request to update Asset : {}, {}", id, asset);
        if (asset.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, asset.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        String content =
            "Tên : " +
            asset.getName() +
            " ; Mã : " +
            asset.getCode() +
            " ; Vị trí : " +
            asset.getLocation() +
            " ; Link refer : http://localhost:4200/pages/entity/asset/" +
            asset.getId() +
            "/view";
        File file = new File("temp" + new Date().getTime() + ".png");
        String fileBase64 = qrCodeService.generateQRCodeToBase64(file, content);
        asset.setQrCode(fileBase64);
        Asset result = assetRepository.save(asset);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, asset.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /assets/:id} : Partial updates given fields of an existing asset, field will ignore if it is null
     *
     * @param id the id of the asset to save.
     * @param asset the asset to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated asset,
     * or with status {@code 400 (Bad Request)} if the asset is not valid,
     * or with status {@code 404 (Not Found)} if the asset is not found,
     * or with status {@code 500 (Internal Server Error)} if the asset couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/assets/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Asset> partialUpdateAsset(@PathVariable(value = "id", required = false) final Long id, @RequestBody Asset asset)
        throws URISyntaxException {
        log.debug("REST request to partial update Asset partially : {}, {}", id, asset);
        if (asset.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, asset.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Asset> result = assetRepository
            .findById(asset.getId())
            .map(
                existingAsset -> {
                    if (asset.getName() != null) {
                        existingAsset.setName(asset.getName());
                    }
                    if (asset.getCode() != null) {
                        existingAsset.setCode(asset.getCode());
                    }
                    if (asset.getPrice() != null) {
                        existingAsset.setPrice(asset.getPrice());
                    }
                    if (asset.getExpiryDateFrom() != null) {
                        existingAsset.setExpiryDateFrom(asset.getExpiryDateFrom());
                    }
                    if (asset.getExpiryDateTo() != null) {
                        existingAsset.setExpiryDateTo(asset.getExpiryDateTo());
                    }
                    if (asset.getStatus() != null) {
                        existingAsset.setStatus(asset.getStatus());
                    }
                    if (asset.getImage() != null) {
                        existingAsset.setImage(asset.getImage());
                    }
                    if (asset.getQrCode() != null) {
                        existingAsset.setQrCode(asset.getQrCode());
                    }
                    if (asset.getLocation() != null) {
                        existingAsset.setLocation(asset.getLocation());
                    }
                    if (asset.getNote() != null) {
                        existingAsset.setNote(asset.getNote());
                    }
                    if (asset.getSerialNo() != null) {
                        existingAsset.setSerialNo(asset.getSerialNo());
                    }
                    if (asset.getGuarantee() != null) {
                        existingAsset.setGuarantee(asset.getGuarantee());
                    }
                    if (asset.getCreatedDate() != null) {
                        existingAsset.setCreatedDate(asset.getCreatedDate());
                    }
                    if (asset.getCreatedBy() != null) {
                        existingAsset.setCreatedBy(asset.getCreatedBy());
                    }
                    if (asset.getUpdatedDate() != null) {
                        existingAsset.setUpdatedDate(asset.getUpdatedDate());
                    }
                    if (asset.getUpdatedBy() != null) {
                        existingAsset.setUpdatedBy(asset.getUpdatedBy());
                    }

                    return existingAsset;
                }
            )
            .map(assetRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, asset.getId().toString())
        );
    }

    /**
     * {@code GET  /assets} : get all the assets.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assets in body.
     */
    @GetMapping("/assets")
    public List<Asset> getAllAssets() {
        log.debug("REST request to get all Assets");
        return assetRepository.findAll();
    }

    /**
     * {@code GET  /assets/:id} : get the "id" asset.
     *
     * @param id the id of the asset to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the asset, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/assets/{id}")
    public ResponseEntity<Asset> getAsset(@PathVariable Long id) {
        log.debug("REST request to get Asset : {}", id);
        Optional<Asset> asset = assetRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(asset);
    }

    /**
     * {@code DELETE  /assets/:id} : delete the "id" asset.
     *
     * @param id the id of the asset to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/assets/{id}")
    public ResponseEntity<Void> deleteAsset(@PathVariable Long id) {
        log.debug("REST request to delete Asset : {}", id);
        assetRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }

    @Autowired
    ExcelFileServices fileServices;

    @PostMapping("/assets/uploadfiles")
    public ResponseEntity<Void> uploadFileMulti(@RequestParam("uploadfiles") MultipartFile[] uploadfiles) {
        // Get file name
        String uploadedFileName = Arrays
            .stream(uploadfiles)
            .map(x -> x.getOriginalFilename())
            .filter(x -> !StringUtils.isEmpty(x))
            .collect(Collectors.joining(" , "));

        if (StringUtils.isEmpty(uploadedFileName)) {
            return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, "please select a file!"))
                .build();
        }

        String notExcelFiles = Arrays
            .stream(uploadfiles)
            .filter(x -> !ExcelUtils.isExcelFile(x))
            .map(x -> x.getOriginalFilename())
            .collect(Collectors.joining(" , "));

        try {
            for (MultipartFile file : uploadfiles) {
                fileServices.store(file);
            }
        } catch (Exception e) {
            return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, "Upload fail"))
                .build();
        }

        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, "Upload Successfully"))
            .build();
    }
}
