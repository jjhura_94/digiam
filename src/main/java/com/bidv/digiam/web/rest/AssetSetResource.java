package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.AssetSet;
import com.bidv.digiam.repository.AssetSetRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.AssetSet}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AssetSetResource {

    private final Logger log = LoggerFactory.getLogger(AssetSetResource.class);

    private static final String ENTITY_NAME = "assetSet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssetSetRepository assetSetRepository;

    public AssetSetResource(AssetSetRepository assetSetRepository) {
        this.assetSetRepository = assetSetRepository;
    }

    /**
     * {@code POST  /asset-sets} : Create a new assetSet.
     *
     * @param assetSet the assetSet to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new assetSet, or with status {@code 400 (Bad Request)} if the assetSet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/asset-sets")
    public ResponseEntity<AssetSet> createAssetSet(@RequestBody AssetSet assetSet) throws URISyntaxException {
        log.debug("REST request to save AssetSet : {}", assetSet);
        if (assetSet.getId() != null) {
            throw new BadRequestAlertException("A new assetSet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AssetSet result = assetSetRepository.save(assetSet);
        return ResponseEntity
            .created(new URI("/api/asset-sets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /asset-sets/:id} : Updates an existing assetSet.
     *
     * @param id the id of the assetSet to save.
     * @param assetSet the assetSet to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assetSet,
     * or with status {@code 400 (Bad Request)} if the assetSet is not valid,
     * or with status {@code 500 (Internal Server Error)} if the assetSet couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/asset-sets/{id}")
    public ResponseEntity<AssetSet> updateAssetSet(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AssetSet assetSet
    ) throws URISyntaxException {
        log.debug("REST request to update AssetSet : {}, {}", id, assetSet);
        if (assetSet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, assetSet.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetSetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AssetSet result = assetSetRepository.save(assetSet);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, assetSet.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /asset-sets/:id} : Partial updates given fields of an existing assetSet, field will ignore if it is null
     *
     * @param id the id of the assetSet to save.
     * @param assetSet the assetSet to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assetSet,
     * or with status {@code 400 (Bad Request)} if the assetSet is not valid,
     * or with status {@code 404 (Not Found)} if the assetSet is not found,
     * or with status {@code 500 (Internal Server Error)} if the assetSet couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/asset-sets/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<AssetSet> partialUpdateAssetSet(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AssetSet assetSet
    ) throws URISyntaxException {
        log.debug("REST request to partial update AssetSet partially : {}, {}", id, assetSet);
        if (assetSet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, assetSet.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetSetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AssetSet> result = assetSetRepository
            .findById(assetSet.getId())
            .map(
                existingAssetSet -> {
                    if (assetSet.getName() != null) {
                        existingAssetSet.setName(assetSet.getName());
                    }
                    if (assetSet.getCode() != null) {
                        existingAssetSet.setCode(assetSet.getCode());
                    }
                    if (assetSet.getUpdatedBy() != null) {
                        existingAssetSet.setUpdatedBy(assetSet.getUpdatedBy());
                    }
                    if (assetSet.getUpdatedDate() != null) {
                        existingAssetSet.setUpdatedDate(assetSet.getUpdatedDate());
                    }

                    return existingAssetSet;
                }
            )
            .map(assetSetRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, assetSet.getId().toString())
        );
    }

    /**
     * {@code GET  /asset-sets} : get all the assetSets.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assetSets in body.
     */
    @GetMapping("/asset-sets")
    public List<AssetSet> getAllAssetSets() {
        log.debug("REST request to get all AssetSets");
        return assetSetRepository.findAll();
    }

    /**
     * {@code GET  /asset-sets/:id} : get the "id" assetSet.
     *
     * @param id the id of the assetSet to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the assetSet, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/asset-sets/{id}")
    public ResponseEntity<AssetSet> getAssetSet(@PathVariable Long id) {
        log.debug("REST request to get AssetSet : {}", id);
        Optional<AssetSet> assetSet = assetSetRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(assetSet);
    }

    /**
     * {@code DELETE  /asset-sets/:id} : delete the "id" assetSet.
     *
     * @param id the id of the assetSet to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/asset-sets/{id}")
    public ResponseEntity<Void> deleteAssetSet(@PathVariable Long id) {
        log.debug("REST request to delete AssetSet : {}", id);
        assetSetRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
