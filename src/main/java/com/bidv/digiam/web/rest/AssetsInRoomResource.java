package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.AssetsInRoom;
import com.bidv.digiam.repository.AssetsInRoomRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.AssetsInRoom}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AssetsInRoomResource {

    private final Logger log = LoggerFactory.getLogger(AssetsInRoomResource.class);

    private static final String ENTITY_NAME = "assetsInRoom";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssetsInRoomRepository assetsInRoomRepository;

    public AssetsInRoomResource(AssetsInRoomRepository assetsInRoomRepository) {
        this.assetsInRoomRepository = assetsInRoomRepository;
    }

    /**
     * {@code POST  /assets-in-rooms} : Create a new assetsInRoom.
     *
     * @param assetsInRoom the assetsInRoom to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new assetsInRoom, or with status {@code 400 (Bad Request)} if the assetsInRoom has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/assets-in-rooms")
    public ResponseEntity<AssetsInRoom> createAssetsInRoom(@RequestBody AssetsInRoom assetsInRoom) throws URISyntaxException {
        log.debug("REST request to save AssetsInRoom : {}", assetsInRoom);
        if (assetsInRoom.getId() != null) {
            throw new BadRequestAlertException("A new assetsInRoom cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AssetsInRoom result = assetsInRoomRepository.save(assetsInRoom);
        return ResponseEntity
            .created(new URI("/api/assets-in-rooms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /assets-in-rooms/:id} : Updates an existing assetsInRoom.
     *
     * @param id the id of the assetsInRoom to save.
     * @param assetsInRoom the assetsInRoom to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assetsInRoom,
     * or with status {@code 400 (Bad Request)} if the assetsInRoom is not valid,
     * or with status {@code 500 (Internal Server Error)} if the assetsInRoom couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/assets-in-rooms/{id}")
    public ResponseEntity<AssetsInRoom> updateAssetsInRoom(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AssetsInRoom assetsInRoom
    ) throws URISyntaxException {
        log.debug("REST request to update AssetsInRoom : {}, {}", id, assetsInRoom);
        if (assetsInRoom.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, assetsInRoom.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetsInRoomRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AssetsInRoom result = assetsInRoomRepository.save(assetsInRoom);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, assetsInRoom.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /assets-in-rooms/:id} : Partial updates given fields of an existing assetsInRoom, field will ignore if it is null
     *
     * @param id the id of the assetsInRoom to save.
     * @param assetsInRoom the assetsInRoom to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assetsInRoom,
     * or with status {@code 400 (Bad Request)} if the assetsInRoom is not valid,
     * or with status {@code 404 (Not Found)} if the assetsInRoom is not found,
     * or with status {@code 500 (Internal Server Error)} if the assetsInRoom couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/assets-in-rooms/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<AssetsInRoom> partialUpdateAssetsInRoom(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AssetsInRoom assetsInRoom
    ) throws URISyntaxException {
        log.debug("REST request to partial update AssetsInRoom partially : {}, {}", id, assetsInRoom);
        if (assetsInRoom.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, assetsInRoom.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!assetsInRoomRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AssetsInRoom> result = assetsInRoomRepository
            .findById(assetsInRoom.getId())
            .map(
                existingAssetsInRoom -> {
                    return existingAssetsInRoom;
                }
            )
            .map(assetsInRoomRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, assetsInRoom.getId().toString())
        );
    }

    /**
     * {@code GET  /assets-in-rooms} : get all the assetsInRooms.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assetsInRooms in body.
     */
    @GetMapping("/assets-in-rooms")
    public List<AssetsInRoom> getAllAssetsInRooms() {
        log.debug("REST request to get all AssetsInRooms");
        return assetsInRoomRepository.findAll();
    }

    /**
     * {@code GET  /assets-in-rooms/:id} : get the "id" assetsInRoom.
     *
     * @param id the id of the assetsInRoom to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the assetsInRoom, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/assets-in-rooms/{id}")
    public ResponseEntity<AssetsInRoom> getAssetsInRoom(@PathVariable Long id) {
        log.debug("REST request to get AssetsInRoom : {}", id);
        Optional<AssetsInRoom> assetsInRoom = assetsInRoomRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(assetsInRoom);
    }

    /**
     * {@code DELETE  /assets-in-rooms/:id} : delete the "id" assetsInRoom.
     *
     * @param id the id of the assetsInRoom to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/assets-in-rooms/{id}")
    public ResponseEntity<Void> deleteAssetsInRoom(@PathVariable Long id) {
        log.debug("REST request to delete AssetsInRoom : {}", id);
        assetsInRoomRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
