package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.DayOffTicket;
import com.bidv.digiam.repository.DayOffTicketRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.DayOffTicket}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DayOffTicketResource {

    private final Logger log = LoggerFactory.getLogger(DayOffTicketResource.class);

    private static final String ENTITY_NAME = "dayOffTicket";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DayOffTicketRepository dayOffTicketRepository;

    public DayOffTicketResource(DayOffTicketRepository dayOffTicketRepository) {
        this.dayOffTicketRepository = dayOffTicketRepository;
    }

    /**
     * {@code POST  /day-off-tickets} : Create a new dayOffTicket.
     *
     * @param dayOffTicket the dayOffTicket to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dayOffTicket, or with status {@code 400 (Bad Request)} if the dayOffTicket has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/day-off-tickets")
    public ResponseEntity<Object> createDayOffTicket(@RequestBody DayOffTicket dayOffTicket) throws URISyntaxException {
        log.debug("REST request to save DayOffTicket : {}", dayOffTicket);
        if (dayOffTicket.getId() != null) {
            throw new BadRequestAlertException("A new dayOffTicket cannot already have an ID", ENTITY_NAME, "idexists");
        }
        String currentDate = "";
        if (dayOffTicket.getFromDate() != null) {
            //        	DateFormat df = new SimpleDateFormat("ddMMyyyy");
            currentDate = dayOffTicket.getFromDate().toString().split("T")[0];
        }

        if (dayOffTicketRepository.searchByIdEmpAndDay(currentDate, dayOffTicket.getCreatedBy().getId()).size() > 0) {
            Map<String, String> result = new HashMap<String, String>();
            result.put("message", "Bạn đã có 1 báo xin nghỉ trùng ngày với báo vừa tạo!");
            return ResponseEntity
                .created(new URI("/api/day-off-tickets/"))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, ""))
                .body(result);
        }

        DayOffTicket result = dayOffTicketRepository.save(dayOffTicket);
        return ResponseEntity
            .created(new URI("/api/day-off-tickets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /day-off-tickets/:id} : Updates an existing dayOffTicket.
     *
     * @param id the id of the dayOffTicket to save.
     * @param dayOffTicket the dayOffTicket to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayOffTicket,
     * or with status {@code 400 (Bad Request)} if the dayOffTicket is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dayOffTicket couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/day-off-tickets/{id}")
    public ResponseEntity<DayOffTicket> updateDayOffTicket(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DayOffTicket dayOffTicket
    ) throws URISyntaxException {
        log.debug("REST request to update DayOffTicket : {}, {}", id, dayOffTicket);
        if (dayOffTicket.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dayOffTicket.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dayOffTicketRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DayOffTicket result = dayOffTicketRepository.save(dayOffTicket);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dayOffTicket.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /day-off-tickets/:id} : Partial updates given fields of an existing dayOffTicket, field will ignore if it is null
     *
     * @param id the id of the dayOffTicket to save.
     * @param dayOffTicket the dayOffTicket to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayOffTicket,
     * or with status {@code 400 (Bad Request)} if the dayOffTicket is not valid,
     * or with status {@code 404 (Not Found)} if the dayOffTicket is not found,
     * or with status {@code 500 (Internal Server Error)} if the dayOffTicket couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/day-off-tickets/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DayOffTicket> partialUpdateDayOffTicket(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DayOffTicket dayOffTicket
    ) throws URISyntaxException {
        log.debug("REST request to partial update DayOffTicket partially : {}, {}", id, dayOffTicket);
        if (dayOffTicket.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dayOffTicket.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dayOffTicketRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DayOffTicket> result = dayOffTicketRepository
            .findById(dayOffTicket.getId())
            .map(
                existingDayOffTicket -> {
                    if (dayOffTicket.getNumberOfDayLeft() != null) {
                        existingDayOffTicket.setNumberOfDayLeft(dayOffTicket.getNumberOfDayLeft());
                    }
                    if (dayOffTicket.getReason() != null) {
                        existingDayOffTicket.setReason(dayOffTicket.getReason());
                    }
                    if (dayOffTicket.getWatcher() != null) {
                        existingDayOffTicket.setWatcher(dayOffTicket.getWatcher());
                    }
                    if (dayOffTicket.getStatus() != null) {
                        existingDayOffTicket.setStatus(dayOffTicket.getStatus());
                    }
                    if (dayOffTicket.getFromDate() != null) {
                        existingDayOffTicket.setFromDate(dayOffTicket.getFromDate());
                    }
                    if (dayOffTicket.getToDate() != null) {
                        existingDayOffTicket.setToDate(dayOffTicket.getToDate());
                    }
                    if (dayOffTicket.getNote() != null) {
                        existingDayOffTicket.setNote(dayOffTicket.getNote());
                    }
                    if (dayOffTicket.getCreatedDate() != null) {
                        existingDayOffTicket.setCreatedDate(dayOffTicket.getCreatedDate());
                    }
                    if (dayOffTicket.getUpdatedBy() != null) {
                        existingDayOffTicket.setUpdatedBy(dayOffTicket.getUpdatedBy());
                    }
                    if (dayOffTicket.getUpdatedDate() != null) {
                        existingDayOffTicket.setUpdatedDate(dayOffTicket.getUpdatedDate());
                    }

                    return existingDayOffTicket;
                }
            )
            .map(dayOffTicketRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dayOffTicket.getId().toString())
        );
    }

    /**
     * {@code GET  /day-off-tickets} : get all the dayOffTickets.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dayOffTickets in body.
     */
    @GetMapping("/day-off-tickets")
    public List<DayOffTicket> getAllDayOffTickets() {
        log.debug("REST request to get all DayOffTickets");
        return dayOffTicketRepository.findAll();
    }

    /**
     * {@code GET  /day-off-tickets} : get all the dayOffTickets.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dayOffTickets in body.
     */
    @PostMapping("/day-off-tickets/search")
    public List<DayOffTicket> searchDayOffTickets(@RequestBody DayOffTicket dayOffTicket) {
        log.debug("REST request to get all DayOffTickets");
        dayOffTicket.getFromDate().toString();
        String currentDate = "";
        if (dayOffTicket.getFromDate() != null) {
            //        	DateFormat df = new SimpleDateFormat("ddMMyyyy");
            currentDate = dayOffTicket.getFromDate().toString().split("T")[0];
        }
        return dayOffTicketRepository.search(currentDate);
    }

    /**
     * {@code GET  /day-off-tickets/:id} : get the "id" dayOffTicket.
     *
     * @param id the id of the dayOffTicket to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dayOffTicket, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/day-off-tickets/{id}")
    public ResponseEntity<DayOffTicket> getDayOffTicket(@PathVariable Long id) {
        log.debug("REST request to get DayOffTicket : {}", id);
        Optional<DayOffTicket> dayOffTicket = dayOffTicketRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dayOffTicket);
    }

    /**
     * {@code DELETE  /day-off-tickets/:id} : delete the "id" dayOffTicket.
     *
     * @param id the id of the dayOffTicket to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/day-off-tickets/{id}")
    public ResponseEntity<Void> deleteDayOffTicket(@PathVariable Long id) {
        log.debug("REST request to delete DayOffTicket : {}", id);
        dayOffTicketRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
