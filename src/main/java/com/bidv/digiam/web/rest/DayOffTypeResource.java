package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.DayOffType;
import com.bidv.digiam.repository.DayOffTypeRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.DayOffType}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DayOffTypeResource {

    private final Logger log = LoggerFactory.getLogger(DayOffTypeResource.class);

    private static final String ENTITY_NAME = "dayOffType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DayOffTypeRepository dayOffTypeRepository;

    public DayOffTypeResource(DayOffTypeRepository dayOffTypeRepository) {
        this.dayOffTypeRepository = dayOffTypeRepository;
    }

    /**
     * {@code POST  /day-off-types} : Create a new dayOffType.
     *
     * @param dayOffType the dayOffType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dayOffType, or with status {@code 400 (Bad Request)} if the dayOffType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/day-off-types")
    public ResponseEntity<DayOffType> createDayOffType(@RequestBody DayOffType dayOffType) throws URISyntaxException {
        log.debug("REST request to save DayOffType : {}", dayOffType);
        if (dayOffType.getId() != null) {
            throw new BadRequestAlertException("A new dayOffType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DayOffType result = dayOffTypeRepository.save(dayOffType);
        return ResponseEntity
            .created(new URI("/api/day-off-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /day-off-types/:id} : Updates an existing dayOffType.
     *
     * @param id the id of the dayOffType to save.
     * @param dayOffType the dayOffType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayOffType,
     * or with status {@code 400 (Bad Request)} if the dayOffType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dayOffType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/day-off-types/{id}")
    public ResponseEntity<DayOffType> updateDayOffType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DayOffType dayOffType
    ) throws URISyntaxException {
        log.debug("REST request to update DayOffType : {}, {}", id, dayOffType);
        if (dayOffType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dayOffType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dayOffTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DayOffType result = dayOffTypeRepository.save(dayOffType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dayOffType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /day-off-types/:id} : Partial updates given fields of an existing dayOffType, field will ignore if it is null
     *
     * @param id the id of the dayOffType to save.
     * @param dayOffType the dayOffType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayOffType,
     * or with status {@code 400 (Bad Request)} if the dayOffType is not valid,
     * or with status {@code 404 (Not Found)} if the dayOffType is not found,
     * or with status {@code 500 (Internal Server Error)} if the dayOffType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/day-off-types/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DayOffType> partialUpdateDayOffType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DayOffType dayOffType
    ) throws URISyntaxException {
        log.debug("REST request to partial update DayOffType partially : {}, {}", id, dayOffType);
        if (dayOffType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dayOffType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dayOffTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DayOffType> result = dayOffTypeRepository
            .findById(dayOffType.getId())
            .map(
                existingDayOffType -> {
                    if (dayOffType.getTitle() != null) {
                        existingDayOffType.setTitle(dayOffType.getTitle());
                    }
                    if (dayOffType.getNumberOfDay() != null) {
                        existingDayOffType.setNumberOfDay(dayOffType.getNumberOfDay());
                    }
                    if (dayOffType.getNoSalary() != null) {
                        existingDayOffType.setNoSalary(dayOffType.getNoSalary());
                    }

                    return existingDayOffType;
                }
            )
            .map(dayOffTypeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dayOffType.getId().toString())
        );
    }

    /**
     * {@code GET  /day-off-types} : get all the dayOffTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dayOffTypes in body.
     */
    @GetMapping("/day-off-types")
    public List<DayOffType> getAllDayOffTypes() {
        log.debug("REST request to get all DayOffTypes");
        return dayOffTypeRepository.findAll();
    }

    /**
     * {@code GET  /day-off-types/:id} : get the "id" dayOffType.
     *
     * @param id the id of the dayOffType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dayOffType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/day-off-types/{id}")
    public ResponseEntity<DayOffType> getDayOffType(@PathVariable Long id) {
        log.debug("REST request to get DayOffType : {}", id);
        Optional<DayOffType> dayOffType = dayOffTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dayOffType);
    }

    /**
     * {@code DELETE  /day-off-types/:id} : delete the "id" dayOffType.
     *
     * @param id the id of the dayOffType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/day-off-types/{id}")
    public ResponseEntity<Void> deleteDayOffType(@PathVariable Long id) {
        log.debug("REST request to delete DayOffType : {}", id);
        dayOffTypeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
