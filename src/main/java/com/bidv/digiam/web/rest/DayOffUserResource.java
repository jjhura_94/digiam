package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.DayOffUser;
import com.bidv.digiam.repository.DayOffUserRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.DayOffUser}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DayOffUserResource {

    private final Logger log = LoggerFactory.getLogger(DayOffUserResource.class);

    private static final String ENTITY_NAME = "dayOffUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DayOffUserRepository dayOffUserRepository;

    public DayOffUserResource(DayOffUserRepository dayOffUserRepository) {
        this.dayOffUserRepository = dayOffUserRepository;
    }

    /**
     * {@code POST  /day-off-users} : Create a new dayOffUser.
     *
     * @param dayOffUser the dayOffUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dayOffUser, or with status {@code 400 (Bad Request)} if the dayOffUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/day-off-users")
    public ResponseEntity<DayOffUser> createDayOffUser(@RequestBody DayOffUser dayOffUser) throws URISyntaxException {
        log.debug("REST request to save DayOffUser : {}", dayOffUser);
        if (dayOffUser.getId() != null) {
            throw new BadRequestAlertException("A new dayOffUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DayOffUser result = dayOffUserRepository.save(dayOffUser);
        return ResponseEntity
            .created(new URI("/api/day-off-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /day-off-users/:id} : Updates an existing dayOffUser.
     *
     * @param id the id of the dayOffUser to save.
     * @param dayOffUser the dayOffUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayOffUser,
     * or with status {@code 400 (Bad Request)} if the dayOffUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dayOffUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/day-off-users/{id}")
    public ResponseEntity<DayOffUser> updateDayOffUser(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DayOffUser dayOffUser
    ) throws URISyntaxException {
        log.debug("REST request to update DayOffUser : {}, {}", id, dayOffUser);
        if (dayOffUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dayOffUser.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dayOffUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DayOffUser result = dayOffUserRepository.save(dayOffUser);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dayOffUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /day-off-users/:id} : Partial updates given fields of an existing dayOffUser, field will ignore if it is null
     *
     * @param id the id of the dayOffUser to save.
     * @param dayOffUser the dayOffUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dayOffUser,
     * or with status {@code 400 (Bad Request)} if the dayOffUser is not valid,
     * or with status {@code 404 (Not Found)} if the dayOffUser is not found,
     * or with status {@code 500 (Internal Server Error)} if the dayOffUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/day-off-users/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DayOffUser> partialUpdateDayOffUser(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DayOffUser dayOffUser
    ) throws URISyntaxException {
        log.debug("REST request to partial update DayOffUser partially : {}, {}", id, dayOffUser);
        if (dayOffUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dayOffUser.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dayOffUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DayOffUser> result = dayOffUserRepository
            .findById(dayOffUser.getId())
            .map(
                existingDayOffUser -> {
                    if (dayOffUser.getDayLeft() != null) {
                        existingDayOffUser.setDayLeft(dayOffUser.getDayLeft());
                    }
                    if (dayOffUser.getYear() != null) {
                        existingDayOffUser.setYear(dayOffUser.getYear());
                    }

                    return existingDayOffUser;
                }
            )
            .map(dayOffUserRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dayOffUser.getId().toString())
        );
    }

    /**
     * {@code GET  /day-off-users} : get all the dayOffUsers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dayOffUsers in body.
     */
    @GetMapping("/day-off-users")
    public List<DayOffUser> getAllDayOffUsers() {
        log.debug("REST request to get all DayOffUsers");
        return dayOffUserRepository.findAll();
    }

    /**
     * {@code GET  /day-off-users/:id} : get the "id" dayOffUser.
     *
     * @param id the id of the dayOffUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dayOffUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/day-off-users/{id}")
    public ResponseEntity<DayOffUser> getDayOffUser(@PathVariable Long id) {
        log.debug("REST request to get DayOffUser : {}", id);
        Optional<DayOffUser> dayOffUser = dayOffUserRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dayOffUser);
    }

    /**
     * {@code DELETE  /day-off-users/:id} : delete the "id" dayOffUser.
     *
     * @param id the id of the dayOffUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/day-off-users/{id}")
    public ResponseEntity<Void> deleteDayOffUser(@PathVariable Long id) {
        log.debug("REST request to delete DayOffUser : {}", id);
        dayOffUserRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
