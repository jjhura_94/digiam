package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.DeptMgt;
import com.bidv.digiam.repository.DeptMgtRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.DeptMgt}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DeptMgtResource {

    private final Logger log = LoggerFactory.getLogger(DeptMgtResource.class);

    private static final String ENTITY_NAME = "deptMgt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeptMgtRepository deptMgtRepository;

    public DeptMgtResource(DeptMgtRepository deptMgtRepository) {
        this.deptMgtRepository = deptMgtRepository;
    }

    /**
     * {@code POST  /dept-mgts} : Create a new deptMgt.
     *
     * @param deptMgt the deptMgt to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deptMgt, or with status {@code 400 (Bad Request)} if the deptMgt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/dept-mgts")
    public ResponseEntity<DeptMgt> createDeptMgt(@RequestBody DeptMgt deptMgt) throws URISyntaxException {
        log.debug("REST request to save DeptMgt : {}", deptMgt);
        if (deptMgt.getId() != null) {
            throw new BadRequestAlertException("A new deptMgt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DeptMgt result = deptMgtRepository.save(deptMgt);
        return ResponseEntity
            .created(new URI("/api/dept-mgts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /dept-mgts/:id} : Updates an existing deptMgt.
     *
     * @param id the id of the deptMgt to save.
     * @param deptMgt the deptMgt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deptMgt,
     * or with status {@code 400 (Bad Request)} if the deptMgt is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deptMgt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/dept-mgts/{id}")
    public ResponseEntity<DeptMgt> updateDeptMgt(@PathVariable(value = "id", required = false) final Long id, @RequestBody DeptMgt deptMgt)
        throws URISyntaxException {
        log.debug("REST request to update DeptMgt : {}, {}", id, deptMgt);
        if (deptMgt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deptMgt.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!deptMgtRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DeptMgt result = deptMgtRepository.save(deptMgt);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, deptMgt.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /dept-mgts/:id} : Partial updates given fields of an existing deptMgt, field will ignore if it is null
     *
     * @param id the id of the deptMgt to save.
     * @param deptMgt the deptMgt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deptMgt,
     * or with status {@code 400 (Bad Request)} if the deptMgt is not valid,
     * or with status {@code 404 (Not Found)} if the deptMgt is not found,
     * or with status {@code 500 (Internal Server Error)} if the deptMgt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/dept-mgts/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DeptMgt> partialUpdateDeptMgt(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DeptMgt deptMgt
    ) throws URISyntaxException {
        log.debug("REST request to partial update DeptMgt partially : {}, {}", id, deptMgt);
        if (deptMgt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, deptMgt.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!deptMgtRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DeptMgt> result = deptMgtRepository
            .findById(deptMgt.getId())
            .map(
                existingDeptMgt -> {
                    if (deptMgt.getDeptCode() != null) {
                        existingDeptMgt.setDeptCode(deptMgt.getDeptCode());
                    }
                    if (deptMgt.getDeptName() != null) {
                        existingDeptMgt.setDeptName(deptMgt.getDeptName());
                    }
                    if (deptMgt.getStatus() != null) {
                        existingDeptMgt.setStatus(deptMgt.getStatus());
                    }
                    if (deptMgt.getParentDeptCode() != null) {
                        existingDeptMgt.setParentDeptCode(deptMgt.getParentDeptCode());
                    }

                    return existingDeptMgt;
                }
            )
            .map(deptMgtRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, deptMgt.getId().toString())
        );
    }

    /**
     * {@code GET  /dept-mgts} : get all the deptMgts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deptMgts in body.
     */
    @GetMapping("/dept-mgts")
    public List<DeptMgt> getAllDeptMgts() {
        log.debug("REST request to get all DeptMgts");
        return deptMgtRepository.findAll();
    }

    /**
     * {@code GET  /dept-mgts/:id} : get the "id" deptMgt.
     *
     * @param id the id of the deptMgt to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deptMgt, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dept-mgts/{id}")
    public ResponseEntity<DeptMgt> getDeptMgt(@PathVariable Long id) {
        log.debug("REST request to get DeptMgt : {}", id);
        Optional<DeptMgt> deptMgt = deptMgtRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(deptMgt);
    }

    /**
     * {@code DELETE  /dept-mgts/:id} : delete the "id" deptMgt.
     *
     * @param id the id of the deptMgt to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/dept-mgts/{id}")
    public ResponseEntity<Void> deleteDeptMgt(@PathVariable Long id) {
        log.debug("REST request to delete DeptMgt : {}", id);
        deptMgtRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
