package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.DutyMgt;
import com.bidv.digiam.repository.DutyMgtRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.DutyMgt}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DutyMgtResource {

    private final Logger log = LoggerFactory.getLogger(DutyMgtResource.class);

    private static final String ENTITY_NAME = "dutyMgt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DutyMgtRepository dutyMgtRepository;

    public DutyMgtResource(DutyMgtRepository dutyMgtRepository) {
        this.dutyMgtRepository = dutyMgtRepository;
    }

    /**
     * {@code POST  /duty-mgts} : Create a new dutyMgt.
     *
     * @param dutyMgt the dutyMgt to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dutyMgt, or with status {@code 400 (Bad Request)} if the dutyMgt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/duty-mgts")
    public ResponseEntity<DutyMgt> createDutyMgt(@RequestBody DutyMgt dutyMgt) throws URISyntaxException {
        log.debug("REST request to save DutyMgt : {}", dutyMgt);
        if (dutyMgt.getId() != null) {
            throw new BadRequestAlertException("A new dutyMgt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DutyMgt result = dutyMgtRepository.save(dutyMgt);
        return ResponseEntity
            .created(new URI("/api/duty-mgts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /duty-mgts/:id} : Updates an existing dutyMgt.
     *
     * @param id the id of the dutyMgt to save.
     * @param dutyMgt the dutyMgt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dutyMgt,
     * or with status {@code 400 (Bad Request)} if the dutyMgt is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dutyMgt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/duty-mgts/{id}")
    public ResponseEntity<DutyMgt> updateDutyMgt(@PathVariable(value = "id", required = false) final Long id, @RequestBody DutyMgt dutyMgt)
        throws URISyntaxException {
        log.debug("REST request to update DutyMgt : {}, {}", id, dutyMgt);
        if (dutyMgt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dutyMgt.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dutyMgtRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DutyMgt result = dutyMgtRepository.save(dutyMgt);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dutyMgt.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /duty-mgts/:id} : Partial updates given fields of an existing dutyMgt, field will ignore if it is null
     *
     * @param id the id of the dutyMgt to save.
     * @param dutyMgt the dutyMgt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dutyMgt,
     * or with status {@code 400 (Bad Request)} if the dutyMgt is not valid,
     * or with status {@code 404 (Not Found)} if the dutyMgt is not found,
     * or with status {@code 500 (Internal Server Error)} if the dutyMgt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/duty-mgts/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DutyMgt> partialUpdateDutyMgt(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DutyMgt dutyMgt
    ) throws URISyntaxException {
        log.debug("REST request to partial update DutyMgt partially : {}, {}", id, dutyMgt);
        if (dutyMgt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dutyMgt.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dutyMgtRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DutyMgt> result = dutyMgtRepository
            .findById(dutyMgt.getId())
            .map(
                existingDutyMgt -> {
                    if (dutyMgt.getDutyCode() != null) {
                        existingDutyMgt.setDutyCode(dutyMgt.getDutyCode());
                    }
                    if (dutyMgt.getDutyName() != null) {
                        existingDutyMgt.setDutyName(dutyMgt.getDutyName());
                    }
                    if (dutyMgt.getUseYn() != null) {
                        existingDutyMgt.setUseYn(dutyMgt.getUseYn());
                    }
                    if (dutyMgt.getDutyOrd() != null) {
                        existingDutyMgt.setDutyOrd(dutyMgt.getDutyOrd());
                    }
                    if (dutyMgt.getCreatedBy() != null) {
                        existingDutyMgt.setCreatedBy(dutyMgt.getCreatedBy());
                    }
                    if (dutyMgt.getCreatedDate() != null) {
                        existingDutyMgt.setCreatedDate(dutyMgt.getCreatedDate());
                    }
                    if (dutyMgt.getUpdatedBy() != null) {
                        existingDutyMgt.setUpdatedBy(dutyMgt.getUpdatedBy());
                    }
                    if (dutyMgt.getUpdatedDate() != null) {
                        existingDutyMgt.setUpdatedDate(dutyMgt.getUpdatedDate());
                    }

                    return existingDutyMgt;
                }
            )
            .map(dutyMgtRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dutyMgt.getId().toString())
        );
    }

    /**
     * {@code GET  /duty-mgts} : get all the dutyMgts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dutyMgts in body.
     */
    @GetMapping("/duty-mgts")
    public List<DutyMgt> getAllDutyMgts() {
        log.debug("REST request to get all DutyMgts");
        return dutyMgtRepository.findAll();
    }

    /**
     * {@code GET  /duty-mgts/:id} : get the "id" dutyMgt.
     *
     * @param id the id of the dutyMgt to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dutyMgt, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/duty-mgts/{id}")
    public ResponseEntity<DutyMgt> getDutyMgt(@PathVariable Long id) {
        log.debug("REST request to get DutyMgt : {}", id);
        Optional<DutyMgt> dutyMgt = dutyMgtRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dutyMgt);
    }

    /**
     * {@code DELETE  /duty-mgts/:id} : delete the "id" dutyMgt.
     *
     * @param id the id of the dutyMgt to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/duty-mgts/{id}")
    public ResponseEntity<Void> deleteDutyMgt(@PathVariable Long id) {
        log.debug("REST request to delete DutyMgt : {}", id);
        dutyMgtRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
