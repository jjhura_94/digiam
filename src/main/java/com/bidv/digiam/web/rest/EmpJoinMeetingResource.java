package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.EmpJoinMeeting;
import com.bidv.digiam.repository.EmpJoinMeetingRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.EmpJoinMeeting}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class EmpJoinMeetingResource {

    private final Logger log = LoggerFactory.getLogger(EmpJoinMeetingResource.class);

    private static final String ENTITY_NAME = "empJoinMeeting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmpJoinMeetingRepository empJoinMeetingRepository;

    public EmpJoinMeetingResource(EmpJoinMeetingRepository empJoinMeetingRepository) {
        this.empJoinMeetingRepository = empJoinMeetingRepository;
    }

    /**
     * {@code POST  /emp-join-meetings} : Create a new empJoinMeeting.
     *
     * @param empJoinMeeting the empJoinMeeting to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new empJoinMeeting, or with status {@code 400 (Bad Request)} if the empJoinMeeting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/emp-join-meetings")
    public ResponseEntity<EmpJoinMeeting> createEmpJoinMeeting(@RequestBody EmpJoinMeeting empJoinMeeting) throws URISyntaxException {
        log.debug("REST request to save EmpJoinMeeting : {}", empJoinMeeting);
        if (empJoinMeeting.getId() != null) {
            throw new BadRequestAlertException("A new empJoinMeeting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmpJoinMeeting result = empJoinMeetingRepository.save(empJoinMeeting);
        return ResponseEntity
            .created(new URI("/api/emp-join-meetings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /emp-join-meetings/:id} : Updates an existing empJoinMeeting.
     *
     * @param id the id of the empJoinMeeting to save.
     * @param empJoinMeeting the empJoinMeeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated empJoinMeeting,
     * or with status {@code 400 (Bad Request)} if the empJoinMeeting is not valid,
     * or with status {@code 500 (Internal Server Error)} if the empJoinMeeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/emp-join-meetings/{id}")
    public ResponseEntity<EmpJoinMeeting> updateEmpJoinMeeting(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EmpJoinMeeting empJoinMeeting
    ) throws URISyntaxException {
        log.debug("REST request to update EmpJoinMeeting : {}, {}", id, empJoinMeeting);
        if (empJoinMeeting.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, empJoinMeeting.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!empJoinMeetingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EmpJoinMeeting result = empJoinMeetingRepository.save(empJoinMeeting);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, empJoinMeeting.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /emp-join-meetings/:id} : Partial updates given fields of an existing empJoinMeeting, field will ignore if it is null
     *
     * @param id the id of the empJoinMeeting to save.
     * @param empJoinMeeting the empJoinMeeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated empJoinMeeting,
     * or with status {@code 400 (Bad Request)} if the empJoinMeeting is not valid,
     * or with status {@code 404 (Not Found)} if the empJoinMeeting is not found,
     * or with status {@code 500 (Internal Server Error)} if the empJoinMeeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/emp-join-meetings/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<EmpJoinMeeting> partialUpdateEmpJoinMeeting(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EmpJoinMeeting empJoinMeeting
    ) throws URISyntaxException {
        log.debug("REST request to partial update EmpJoinMeeting partially : {}, {}", id, empJoinMeeting);
        if (empJoinMeeting.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, empJoinMeeting.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!empJoinMeetingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EmpJoinMeeting> result = empJoinMeetingRepository
            .findById(empJoinMeeting.getId())
            .map(
                existingEmpJoinMeeting -> {
                    return existingEmpJoinMeeting;
                }
            )
            .map(empJoinMeetingRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, empJoinMeeting.getId().toString())
        );
    }

    /**
     * {@code GET  /emp-join-meetings} : get all the empJoinMeetings.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of empJoinMeetings in body.
     */
    @GetMapping("/emp-join-meetings")
    public List<EmpJoinMeeting> getAllEmpJoinMeetings() {
        log.debug("REST request to get all EmpJoinMeetings");
        return empJoinMeetingRepository.findAll();
    }

    @GetMapping("/emp-join-meetings-by-meeting/{meetingId}")
    public List<EmpJoinMeeting> getEmpJoinMeetingsByMeetingId(@PathVariable(value = "meetingId", required = false) Long meetingId) {
        log.debug("REST request to get  EmpJoinMeetings by meeting");
        return empJoinMeetingRepository.getEmpJoinMeetingByMeetingId(meetingId);
    }

    /**
     * {@code GET  /emp-join-meetings/:id} : get the "id" empJoinMeeting.
     *
     * @param id the id of the empJoinMeeting to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the empJoinMeeting, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/emp-join-meetings/{id}")
    public ResponseEntity<EmpJoinMeeting> getEmpJoinMeeting(@PathVariable Long id) {
        log.debug("REST request to get EmpJoinMeeting : {}", id);
        Optional<EmpJoinMeeting> empJoinMeeting = empJoinMeetingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(empJoinMeeting);
    }

    /**
     * {@code DELETE  /emp-join-meetings/:id} : delete the "id" empJoinMeeting.
     *
     * @param id the id of the empJoinMeeting to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/emp-join-meetings/{id}")
    public ResponseEntity<Void> deleteEmpJoinMeeting(@PathVariable Long id) {
        log.debug("REST request to delete EmpJoinMeeting : {}", id);
        empJoinMeetingRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
