package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.EmpMgt;
import com.bidv.digiam.repository.EmpMgtRepository;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.EmpMgt}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class EmpMgtResource {

    private final Logger log = LoggerFactory.getLogger(EmpMgtResource.class);

    private static final String ENTITY_NAME = "empMgt";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmpMgtRepository empMgtRepository;

    public EmpMgtResource(EmpMgtRepository empMgtRepository) {
        this.empMgtRepository = empMgtRepository;
    }

    /**
     * {@code POST  /emp-mgts} : Create a new empMgt.
     *
     * @param empMgt the empMgt to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new empMgt, or with status {@code 400 (Bad Request)} if the empMgt has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/emp-mgts")
    public ResponseEntity<EmpMgt> createEmpMgt(@RequestBody EmpMgt empMgt) throws URISyntaxException {
        log.debug("REST request to save EmpMgt : {}", empMgt);
        if (empMgt.getId() != null) {
            throw new BadRequestAlertException("A new empMgt cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmpMgt result = empMgtRepository.save(empMgt);
        return ResponseEntity
            .created(new URI("/api/emp-mgts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /emp-mgts/:id} : Updates an existing empMgt.
     *
     * @param id the id of the empMgt to save.
     * @param empMgt the empMgt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated empMgt,
     * or with status {@code 400 (Bad Request)} if the empMgt is not valid,
     * or with status {@code 500 (Internal Server Error)} if the empMgt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/emp-mgts/{id}")
    public ResponseEntity<EmpMgt> updateEmpMgt(@PathVariable(value = "id", required = false) final Long id, @RequestBody EmpMgt empMgt)
        throws URISyntaxException {
        log.debug("REST request to update EmpMgt : {}, {}", id, empMgt);
        if (empMgt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, empMgt.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!empMgtRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EmpMgt result = empMgtRepository.save(empMgt);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, empMgt.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /emp-mgts/:id} : Partial updates given fields of an existing empMgt, field will ignore if it is null
     *
     * @param id the id of the empMgt to save.
     * @param empMgt the empMgt to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated empMgt,
     * or with status {@code 400 (Bad Request)} if the empMgt is not valid,
     * or with status {@code 404 (Not Found)} if the empMgt is not found,
     * or with status {@code 500 (Internal Server Error)} if the empMgt couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/emp-mgts/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<EmpMgt> partialUpdateEmpMgt(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EmpMgt empMgt
    ) throws URISyntaxException {
        log.debug("REST request to partial update EmpMgt partially : {}, {}", id, empMgt);
        if (empMgt.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, empMgt.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!empMgtRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EmpMgt> result = empMgtRepository
            .findById(empMgt.getId())
            .map(
                existingEmpMgt -> {
                    if (empMgt.getEmpNo() != null) {
                        existingEmpMgt.setEmpNo(empMgt.getEmpNo());
                    }
                    if (empMgt.getName() != null) {
                        existingEmpMgt.setName(empMgt.getName());
                    }
                    if (empMgt.getStartDate() != null) {
                        existingEmpMgt.setStartDate(empMgt.getStartDate());
                    }
                    if (empMgt.getEndDate() != null) {
                        existingEmpMgt.setEndDate(empMgt.getEndDate());
                    }
                    if (empMgt.getDob() != null) {
                        existingEmpMgt.setDob(empMgt.getDob());
                    }
                    if (empMgt.getEmail() != null) {
                        existingEmpMgt.setEmail(empMgt.getEmail());
                    }
                    if (empMgt.getCellPhone() != null) {
                        existingEmpMgt.setCellPhone(empMgt.getCellPhone());
                    }
                    if (empMgt.getExtNumber() != null) {
                        existingEmpMgt.setExtNumber(empMgt.getExtNumber());
                    }
                    if (empMgt.getZipCode() != null) {
                        existingEmpMgt.setZipCode(empMgt.getZipCode());
                    }
                    if (empMgt.getAddress() != null) {
                        existingEmpMgt.setAddress(empMgt.getAddress());
                    }
                    if (empMgt.getGender() != null) {
                        existingEmpMgt.setGender(empMgt.getGender());
                    }
                    if (empMgt.getImgPath() != null) {
                        existingEmpMgt.setImgPath(empMgt.getImgPath());
                    }
                    if (empMgt.getStatus() != null) {
                        existingEmpMgt.setStatus(empMgt.getStatus());
                    }

                    return existingEmpMgt;
                }
            )
            .map(empMgtRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, empMgt.getId().toString())
        );
    }

    /**
     * {@code GET  /emp-mgts} : get all the empMgts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of empMgts in body.
     */
    @GetMapping("/emp-mgts")
    public List<EmpMgt> getAllEmpMgts() {
        log.debug("REST request to get all EmpMgts");
        return empMgtRepository.findAll();
    }

    /**
     * {@code GET  /emp-mgts/:id} : get the "id" empMgt.
     *
     * @param id the id of the empMgt to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the empMgt, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/emp-mgts/{id}")
    public ResponseEntity<EmpMgt> getEmpMgt(@PathVariable Long id) {
        log.debug("REST request to get EmpMgt : {}", id);
        Optional<EmpMgt> empMgt = empMgtRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(empMgt);
    }

    /**
     * {@code DELETE  /emp-mgts/:id} : delete the "id" empMgt.
     *
     * @param id the id of the empMgt to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/emp-mgts/{id}")
    public ResponseEntity<Void> deleteEmpMgt(@PathVariable Long id) {
        log.debug("REST request to delete EmpMgt : {}", id);
        empMgtRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
