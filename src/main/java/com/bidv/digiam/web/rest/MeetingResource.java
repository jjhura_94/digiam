package com.bidv.digiam.web.rest;

import com.bidv.digiam.domain.EmpJoinMeeting;
import com.bidv.digiam.domain.Meeting;
import com.bidv.digiam.repository.EmpJoinMeetingRepository;
import com.bidv.digiam.repository.MeetingRepository;
import com.bidv.digiam.service.EmailService;
import com.bidv.digiam.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bidv.digiam.domain.Meeting}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MeetingResource {

    private final Logger log = LoggerFactory.getLogger(MeetingResource.class);

    private static final String ENTITY_NAME = "meeting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MeetingRepository meetingRepository;

    @Autowired
    private EmpJoinMeetingRepository empJoinMeetingRepository;

    private EmailService emailService;

    public MeetingResource(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    /**
     * {@code POST  /meetings} : Create a new meeting.
     *
     * @param meeting the meeting to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new meeting, or with status {@code 400 (Bad Request)} if the meeting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/meetings")
    public ResponseEntity<Meeting> createMeeting(@RequestBody Meeting meeting) throws URISyntaxException {
        log.debug("REST request to save Meeting : {}", meeting);
        if (meeting.getId() != null) {
            throw new BadRequestAlertException("A new meeting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        emailService = new EmailService("smtp.gmail.com", 25, "nvdung2802@gmail.com", "vlzxkajxnkuinknj");
        if (meeting.getEmpJoinMeetings() != null) {
            //        	empJoinMeetingRepository.deleteEmpJoinMeetingByMeetingId(meeting.getId());
            meeting
                .getEmpJoinMeetings()
                .forEach(
                    emp -> {
                        EmpJoinMeeting empJoinMeeting = new EmpJoinMeeting();
                        empJoinMeeting.setMeeting(meeting);
                        empJoinMeeting.setEmp(emp.getEmp());
                        empJoinMeetingRepository.save(empJoinMeeting);
                        //                        try {
                        //                        	String content = "Nội dung cuộc họp : "+ meeting.getNote() +" - Diễn ra tại phòng họp : "+ meeting.getRoom().getLocation();
                        //                        	if(meeting.getRoom().getOnline()) {
                        //                        		content = content + "\n  " + "Link phòng họp : " +  meeting.getRoom().getLinkRoomOnline();
                        //                        	}
                        //
                        //							emailService.createMessage(empJoinMeeting.getEmp().getEmail(), Date.from(meeting.getMeetingFrom()) ,
                        //									Date.from(meeting.getMeetingTo()),
                        //									content,
                        //									meeting.getTitle());
                        //							emailService.start();
                        ////							log.info("send mail success");
                        //                        } catch (Exception e) {
                        //							log.info("send mail fail to :" + empJoinMeeting.getEmp().getEmail());
                        //							e.printStackTrace();
                        //						}

                    }
                );
        }
        Meeting result = meetingRepository.save(meeting);
        return ResponseEntity
            .created(new URI("/api/meetings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /meetings/:id} : Updates an existing meeting.
     *
     * @param id the id of the meeting to save.
     * @param meeting the meeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meeting,
     * or with status {@code 400 (Bad Request)} if the meeting is not valid,
     * or with status {@code 500 (Internal Server Error)} if the meeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/meetings/{id}")
    public ResponseEntity<Meeting> updateMeeting(@PathVariable(value = "id", required = false) final Long id, @RequestBody Meeting meeting)
        throws URISyntaxException {
        log.debug("REST request to update Meeting : {}, {}", id, meeting);
        if (meeting.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, meeting.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!meetingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }
        if (meeting.getEmpJoinMeetings() != null) {
            empJoinMeetingRepository.deleteEmpJoinMeetingByMeetingId(meeting.getId());
            meeting
                .getEmpJoinMeetings()
                .forEach(
                    emp -> {
                        EmpJoinMeeting empJoinMeeting = new EmpJoinMeeting();
                        empJoinMeeting.setMeeting(meeting);
                        empJoinMeeting.setEmp(emp.getEmp());
                        empJoinMeetingRepository.save(empJoinMeeting);
                    }
                );
        }

        Meeting result = meetingRepository.save(meeting);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, meeting.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /meetings/:id} : Partial updates given fields of an existing meeting, field will ignore if it is null
     *
     * @param id the id of the meeting to save.
     * @param meeting the meeting to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated meeting,
     * or with status {@code 400 (Bad Request)} if the meeting is not valid,
     * or with status {@code 404 (Not Found)} if the meeting is not found,
     * or with status {@code 500 (Internal Server Error)} if the meeting couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/meetings/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Meeting> partialUpdateMeeting(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Meeting meeting
    ) throws URISyntaxException {
        log.debug("REST request to partial update Meeting partially : {}, {}", id, meeting);
        if (meeting.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, meeting.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!meetingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Meeting> result = meetingRepository
            .findById(meeting.getId())
            .map(
                existingMeeting -> {
                    if (meeting.getMeetingFrom() != null) {
                        existingMeeting.setMeetingFrom(meeting.getMeetingFrom());
                    }
                    if (meeting.getMeetingTo() != null) {
                        existingMeeting.setMeetingTo(meeting.getMeetingTo());
                    }
                    if (meeting.getOffline() != null) {
                        existingMeeting.setOffline(meeting.getOffline());
                    }
                    if (meeting.getRoomOnlineLink() != null) {
                        existingMeeting.setRoomOnlineLink(meeting.getRoomOnlineLink());
                    }
                    if (meeting.getCreatedDate() != null) {
                        existingMeeting.setCreatedDate(meeting.getCreatedDate());
                    }
                    if (meeting.getCreatedBy() != null) {
                        existingMeeting.setCreatedBy(meeting.getCreatedBy());
                    }
                    if (meeting.getUpdatedDate() != null) {
                        existingMeeting.setUpdatedDate(meeting.getUpdatedDate());
                    }
                    if (meeting.getUpdatedBy() != null) {
                        existingMeeting.setUpdatedBy(meeting.getUpdatedBy());
                    }

                    return existingMeeting;
                }
            )
            .map(meetingRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, meeting.getId().toString())
        );
    }

    /**
     * {@code GET  /meetings} : get all the meetings.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of meetings in body.
     */
    @GetMapping("/meetings")
    public List<Meeting> getAllMeetings() {
        log.debug("REST request to get all Meetings");
        //        List<Meeting> tem =  meetingRepository.findAll();
        return meetingRepository.findAll();
    }

    /**
     * {@code GET  /meetings/:id} : get the "id" meeting.
     *
     * @param id the id of the meeting to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the meeting, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/meetings/{id}")
    public ResponseEntity<Meeting> getMeeting(@PathVariable Long id) {
        log.debug("REST request to get Meeting : {}", id);
        Optional<Meeting> meeting = meetingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(meeting);
    }

    /**
     * {@code DELETE  /meetings/:id} : delete the "id" meeting.
     *
     * @param id the id of the meeting to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/meetings/{id}")
    @Transactional
    public ResponseEntity<Void> deleteMeeting(@PathVariable Long id) {
        log.debug("REST request to delete Meeting : {}", id);
        empJoinMeetingRepository.deleteEmpJoinMeetingByMeetingId(id);
        meetingRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
