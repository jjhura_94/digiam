import * as dayjs from 'dayjs';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { IAsset } from 'app/entities/asset/asset.model';

export interface IAssetHistory {
  id?: number;
  updatedBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  status?: string | null;
  note?: string | null;
  oldOwner?: IEmpMgt | null;
  asset?: IAsset | null;
}

export class AssetHistory implements IAssetHistory {
  constructor(
    public id?: number,
    public updatedBy?: string | null,
    public updatedDate?: dayjs.Dayjs | null,
    public status?: string | null,
    public note?: string | null,
    public oldOwner?: IEmpMgt | null,
    public asset?: IAsset | null
  ) {}
}

export function getAssetHistoryIdentifier(assetHistory: IAssetHistory): number | undefined {
  return assetHistory.id;
}
