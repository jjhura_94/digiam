import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AssetHistoryComponent } from './list/asset-history.component';
import { AssetHistoryDetailComponent } from './detail/asset-history-detail.component';
import { AssetHistoryUpdateComponent } from './update/asset-history-update.component';
import { AssetHistoryDeleteDialogComponent } from './delete/asset-history-delete-dialog.component';
import { AssetHistoryRoutingModule } from './route/asset-history-routing.module';

@NgModule({
  imports: [SharedModule, AssetHistoryRoutingModule],
  declarations: [AssetHistoryComponent, AssetHistoryDetailComponent, AssetHistoryUpdateComponent, AssetHistoryDeleteDialogComponent],
  entryComponents: [AssetHistoryDeleteDialogComponent],
})
export class AssetHistoryModule {}
