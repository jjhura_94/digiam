import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssetHistory } from '../asset-history.model';
import { AssetHistoryService } from '../service/asset-history.service';

@Component({
  templateUrl: './asset-history-delete-dialog.component.html',
})
export class AssetHistoryDeleteDialogComponent {
  assetHistory?: IAssetHistory;

  constructor(protected assetHistoryService: AssetHistoryService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.assetHistoryService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
