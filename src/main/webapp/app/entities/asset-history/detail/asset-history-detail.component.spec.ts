import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AssetHistoryDetailComponent } from './asset-history-detail.component';

describe('Component Tests', () => {
  describe('AssetHistory Management Detail Component', () => {
    let comp: AssetHistoryDetailComponent;
    let fixture: ComponentFixture<AssetHistoryDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [AssetHistoryDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ assetHistory: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(AssetHistoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AssetHistoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load assetHistory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.assetHistory).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
