import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAssetHistory } from '../asset-history.model';

@Component({
  selector: 'jhi-asset-history-detail',
  templateUrl: './asset-history-detail.component.html',
})
export class AssetHistoryDetailComponent implements OnInit {
  assetHistory: IAssetHistory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assetHistory }) => {
      this.assetHistory = assetHistory;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
