import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AssetHistoryService } from '../service/asset-history.service';

import { AssetHistoryComponent } from './asset-history.component';

describe('Component Tests', () => {
  describe('AssetHistory Management Component', () => {
    let comp: AssetHistoryComponent;
    let fixture: ComponentFixture<AssetHistoryComponent>;
    let service: AssetHistoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [AssetHistoryComponent],
      })
        .overrideTemplate(AssetHistoryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssetHistoryComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(AssetHistoryService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.assetHistories?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
