import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssetHistory } from '../asset-history.model';
import { AssetHistoryService } from '../service/asset-history.service';
import { AssetHistoryDeleteDialogComponent } from '../delete/asset-history-delete-dialog.component';

@Component({
  selector: 'jhi-asset-history',
  templateUrl: './asset-history.component.html',
})
export class AssetHistoryComponent implements OnInit {
  assetHistories?: IAssetHistory[];
  isLoading = false;

  constructor(protected assetHistoryService: AssetHistoryService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.assetHistoryService.query().subscribe(
      (res: HttpResponse<IAssetHistory[]>) => {
        this.isLoading = false;
        this.assetHistories = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IAssetHistory): number {
    return item.id!;
  }

  delete(assetHistory: IAssetHistory): void {
    const modalRef = this.modalService.open(AssetHistoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.assetHistory = assetHistory;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
