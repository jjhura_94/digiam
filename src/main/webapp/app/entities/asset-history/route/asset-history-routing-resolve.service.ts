import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAssetHistory, AssetHistory } from '../asset-history.model';
import { AssetHistoryService } from '../service/asset-history.service';

@Injectable({ providedIn: 'root' })
export class AssetHistoryRoutingResolveService implements Resolve<IAssetHistory> {
  constructor(protected service: AssetHistoryService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAssetHistory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((assetHistory: HttpResponse<AssetHistory>) => {
          if (assetHistory.body) {
            return of(assetHistory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AssetHistory());
  }
}
