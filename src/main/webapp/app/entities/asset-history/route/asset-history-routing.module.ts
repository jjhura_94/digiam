import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AssetHistoryComponent } from '../list/asset-history.component';
import { AssetHistoryDetailComponent } from '../detail/asset-history-detail.component';
import { AssetHistoryUpdateComponent } from '../update/asset-history-update.component';
import { AssetHistoryRoutingResolveService } from './asset-history-routing-resolve.service';

const assetHistoryRoute: Routes = [
  {
    path: '',
    component: AssetHistoryComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AssetHistoryDetailComponent,
    resolve: {
      assetHistory: AssetHistoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AssetHistoryUpdateComponent,
    resolve: {
      assetHistory: AssetHistoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AssetHistoryUpdateComponent,
    resolve: {
      assetHistory: AssetHistoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(assetHistoryRoute)],
  exports: [RouterModule],
})
export class AssetHistoryRoutingModule {}
