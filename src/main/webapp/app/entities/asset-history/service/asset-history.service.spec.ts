import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IAssetHistory, AssetHistory } from '../asset-history.model';

import { AssetHistoryService } from './asset-history.service';

describe('Service Tests', () => {
  describe('AssetHistory Service', () => {
    let service: AssetHistoryService;
    let httpMock: HttpTestingController;
    let elemDefault: IAssetHistory;
    let expectedResult: IAssetHistory | IAssetHistory[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(AssetHistoryService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        updatedBy: 'AAAAAAA',
        updatedDate: currentDate,
        status: 'AAAAAAA',
        note: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a AssetHistory', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.create(new AssetHistory()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a AssetHistory', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            note: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a AssetHistory', () => {
        const patchObject = Object.assign(
          {
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          new AssetHistory()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of AssetHistory', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            note: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a AssetHistory', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addAssetHistoryToCollectionIfMissing', () => {
        it('should add a AssetHistory to an empty array', () => {
          const assetHistory: IAssetHistory = { id: 123 };
          expectedResult = service.addAssetHistoryToCollectionIfMissing([], assetHistory);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(assetHistory);
        });

        it('should not add a AssetHistory to an array that contains it', () => {
          const assetHistory: IAssetHistory = { id: 123 };
          const assetHistoryCollection: IAssetHistory[] = [
            {
              ...assetHistory,
            },
            { id: 456 },
          ];
          expectedResult = service.addAssetHistoryToCollectionIfMissing(assetHistoryCollection, assetHistory);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a AssetHistory to an array that doesn't contain it", () => {
          const assetHistory: IAssetHistory = { id: 123 };
          const assetHistoryCollection: IAssetHistory[] = [{ id: 456 }];
          expectedResult = service.addAssetHistoryToCollectionIfMissing(assetHistoryCollection, assetHistory);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(assetHistory);
        });

        it('should add only unique AssetHistory to an array', () => {
          const assetHistoryArray: IAssetHistory[] = [{ id: 123 }, { id: 456 }, { id: 64013 }];
          const assetHistoryCollection: IAssetHistory[] = [{ id: 123 }];
          expectedResult = service.addAssetHistoryToCollectionIfMissing(assetHistoryCollection, ...assetHistoryArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const assetHistory: IAssetHistory = { id: 123 };
          const assetHistory2: IAssetHistory = { id: 456 };
          expectedResult = service.addAssetHistoryToCollectionIfMissing([], assetHistory, assetHistory2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(assetHistory);
          expect(expectedResult).toContain(assetHistory2);
        });

        it('should accept null and undefined values', () => {
          const assetHistory: IAssetHistory = { id: 123 };
          expectedResult = service.addAssetHistoryToCollectionIfMissing([], null, assetHistory, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(assetHistory);
        });

        it('should return initial array if no AssetHistory is added', () => {
          const assetHistoryCollection: IAssetHistory[] = [{ id: 123 }];
          expectedResult = service.addAssetHistoryToCollectionIfMissing(assetHistoryCollection, undefined, null);
          expect(expectedResult).toEqual(assetHistoryCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
