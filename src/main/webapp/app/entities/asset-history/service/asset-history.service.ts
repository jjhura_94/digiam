import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAssetHistory, getAssetHistoryIdentifier } from '../asset-history.model';

export type EntityResponseType = HttpResponse<IAssetHistory>;
export type EntityArrayResponseType = HttpResponse<IAssetHistory[]>;

@Injectable({ providedIn: 'root' })
export class AssetHistoryService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/asset-histories');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(assetHistory: IAssetHistory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetHistory);
    return this.http
      .post<IAssetHistory>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(assetHistory: IAssetHistory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetHistory);
    return this.http
      .put<IAssetHistory>(`${this.resourceUrl}/${getAssetHistoryIdentifier(assetHistory) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(assetHistory: IAssetHistory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetHistory);
    return this.http
      .patch<IAssetHistory>(`${this.resourceUrl}/${getAssetHistoryIdentifier(assetHistory) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAssetHistory>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAssetHistory[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAssetHistoryToCollectionIfMissing(
    assetHistoryCollection: IAssetHistory[],
    ...assetHistoriesToCheck: (IAssetHistory | null | undefined)[]
  ): IAssetHistory[] {
    const assetHistories: IAssetHistory[] = assetHistoriesToCheck.filter(isPresent);
    if (assetHistories.length > 0) {
      const assetHistoryCollectionIdentifiers = assetHistoryCollection.map(
        assetHistoryItem => getAssetHistoryIdentifier(assetHistoryItem)!
      );
      const assetHistoriesToAdd = assetHistories.filter(assetHistoryItem => {
        const assetHistoryIdentifier = getAssetHistoryIdentifier(assetHistoryItem);
        if (assetHistoryIdentifier == null || assetHistoryCollectionIdentifiers.includes(assetHistoryIdentifier)) {
          return false;
        }
        assetHistoryCollectionIdentifiers.push(assetHistoryIdentifier);
        return true;
      });
      return [...assetHistoriesToAdd, ...assetHistoryCollection];
    }
    return assetHistoryCollection;
  }

  protected convertDateFromClient(assetHistory: IAssetHistory): IAssetHistory {
    return Object.assign({}, assetHistory, {
      updatedDate: assetHistory.updatedDate?.isValid() ? assetHistory.updatedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.updatedDate = res.body.updatedDate ? dayjs(res.body.updatedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((assetHistory: IAssetHistory) => {
        assetHistory.updatedDate = assetHistory.updatedDate ? dayjs(assetHistory.updatedDate) : undefined;
      });
    }
    return res;
  }
}
