jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AssetHistoryService } from '../service/asset-history.service';
import { IAssetHistory, AssetHistory } from '../asset-history.model';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';
import { IAsset } from 'app/entities/asset/asset.model';
import { AssetService } from 'app/entities/asset/service/asset.service';

import { AssetHistoryUpdateComponent } from './asset-history-update.component';

describe('Component Tests', () => {
  describe('AssetHistory Management Update Component', () => {
    let comp: AssetHistoryUpdateComponent;
    let fixture: ComponentFixture<AssetHistoryUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let assetHistoryService: AssetHistoryService;
    let empMgtService: EmpMgtService;
    let assetService: AssetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [AssetHistoryUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(AssetHistoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssetHistoryUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      assetHistoryService = TestBed.inject(AssetHistoryService);
      empMgtService = TestBed.inject(EmpMgtService);
      assetService = TestBed.inject(AssetService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call EmpMgt query and add missing value', () => {
        const assetHistory: IAssetHistory = { id: 456 };
        const oldOwner: IEmpMgt = { id: 3402 };
        assetHistory.oldOwner = oldOwner;

        const empMgtCollection: IEmpMgt[] = [{ id: 67907 }];
        jest.spyOn(empMgtService, 'query').mockReturnValue(of(new HttpResponse({ body: empMgtCollection })));
        const additionalEmpMgts = [oldOwner];
        const expectedCollection: IEmpMgt[] = [...additionalEmpMgts, ...empMgtCollection];
        jest.spyOn(empMgtService, 'addEmpMgtToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ assetHistory });
        comp.ngOnInit();

        expect(empMgtService.query).toHaveBeenCalled();
        expect(empMgtService.addEmpMgtToCollectionIfMissing).toHaveBeenCalledWith(empMgtCollection, ...additionalEmpMgts);
        expect(comp.empMgtsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Asset query and add missing value', () => {
        const assetHistory: IAssetHistory = { id: 456 };
        const asset: IAsset = { id: 58078 };
        assetHistory.asset = asset;

        const assetCollection: IAsset[] = [{ id: 43903 }];
        jest.spyOn(assetService, 'query').mockReturnValue(of(new HttpResponse({ body: assetCollection })));
        const additionalAssets = [asset];
        const expectedCollection: IAsset[] = [...additionalAssets, ...assetCollection];
        jest.spyOn(assetService, 'addAssetToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ assetHistory });
        comp.ngOnInit();

        expect(assetService.query).toHaveBeenCalled();
        expect(assetService.addAssetToCollectionIfMissing).toHaveBeenCalledWith(assetCollection, ...additionalAssets);
        expect(comp.assetsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const assetHistory: IAssetHistory = { id: 456 };
        const oldOwner: IEmpMgt = { id: 67011 };
        assetHistory.oldOwner = oldOwner;
        const asset: IAsset = { id: 67694 };
        assetHistory.asset = asset;

        activatedRoute.data = of({ assetHistory });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(assetHistory));
        expect(comp.empMgtsSharedCollection).toContain(oldOwner);
        expect(comp.assetsSharedCollection).toContain(asset);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetHistory>>();
        const assetHistory = { id: 123 };
        jest.spyOn(assetHistoryService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetHistory });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: assetHistory }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(assetHistoryService.update).toHaveBeenCalledWith(assetHistory);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetHistory>>();
        const assetHistory = new AssetHistory();
        jest.spyOn(assetHistoryService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetHistory });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: assetHistory }));
        saveSubject.complete();

        // THEN
        expect(assetHistoryService.create).toHaveBeenCalledWith(assetHistory);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetHistory>>();
        const assetHistory = { id: 123 };
        jest.spyOn(assetHistoryService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetHistory });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(assetHistoryService.update).toHaveBeenCalledWith(assetHistory);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackEmpMgtById', () => {
        it('Should return tracked EmpMgt primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackEmpMgtById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackAssetById', () => {
        it('Should return tracked Asset primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackAssetById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
