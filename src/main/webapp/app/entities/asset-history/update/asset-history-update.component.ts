import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IAssetHistory, AssetHistory } from '../asset-history.model';
import { AssetHistoryService } from '../service/asset-history.service';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';
import { IAsset } from 'app/entities/asset/asset.model';
import { AssetService } from 'app/entities/asset/service/asset.service';

@Component({
  selector: 'jhi-asset-history-update',
  templateUrl: './asset-history-update.component.html',
})
export class AssetHistoryUpdateComponent implements OnInit {
  isSaving = false;

  empMgtsSharedCollection: IEmpMgt[] = [];
  assetsSharedCollection: IAsset[] = [];

  editForm = this.fb.group({
    id: [],
    updatedBy: [],
    updatedDate: [],
    status: [],
    note: [],
    oldOwner: [],
    asset: [],
  });

  constructor(
    protected assetHistoryService: AssetHistoryService,
    protected empMgtService: EmpMgtService,
    protected assetService: AssetService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assetHistory }) => {
      if (assetHistory.id === undefined) {
        const today = dayjs().startOf('day');
        assetHistory.updatedDate = today;
      }

      this.updateForm(assetHistory);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const assetHistory = this.createFromForm();
    if (assetHistory.id !== undefined) {
      this.subscribeToSaveResponse(this.assetHistoryService.update(assetHistory));
    } else {
      this.subscribeToSaveResponse(this.assetHistoryService.create(assetHistory));
    }
  }

  trackEmpMgtById(index: number, item: IEmpMgt): number {
    return item.id!;
  }

  trackAssetById(index: number, item: IAsset): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAssetHistory>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(assetHistory: IAssetHistory): void {
    this.editForm.patchValue({
      id: assetHistory.id,
      updatedBy: assetHistory.updatedBy,
      updatedDate: assetHistory.updatedDate ? assetHistory.updatedDate.format(DATE_TIME_FORMAT) : null,
      status: assetHistory.status,
      note: assetHistory.note,
      oldOwner: assetHistory.oldOwner,
      asset: assetHistory.asset,
    });

    this.empMgtsSharedCollection = this.empMgtService.addEmpMgtToCollectionIfMissing(this.empMgtsSharedCollection, assetHistory.oldOwner);
    this.assetsSharedCollection = this.assetService.addAssetToCollectionIfMissing(this.assetsSharedCollection, assetHistory.asset);
  }

  protected loadRelationshipsOptions(): void {
    this.empMgtService
      .query()
      .pipe(map((res: HttpResponse<IEmpMgt[]>) => res.body ?? []))
      .pipe(map((empMgts: IEmpMgt[]) => this.empMgtService.addEmpMgtToCollectionIfMissing(empMgts, this.editForm.get('oldOwner')!.value)))
      .subscribe((empMgts: IEmpMgt[]) => (this.empMgtsSharedCollection = empMgts));

    this.assetService
      .query()
      .pipe(map((res: HttpResponse<IAsset[]>) => res.body ?? []))
      .pipe(map((assets: IAsset[]) => this.assetService.addAssetToCollectionIfMissing(assets, this.editForm.get('asset')!.value)))
      .subscribe((assets: IAsset[]) => (this.assetsSharedCollection = assets));
  }

  protected createFromForm(): IAssetHistory {
    return {
      ...new AssetHistory(),
      id: this.editForm.get(['id'])!.value,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      updatedDate: this.editForm.get(['updatedDate'])!.value
        ? dayjs(this.editForm.get(['updatedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      status: this.editForm.get(['status'])!.value,
      note: this.editForm.get(['note'])!.value,
      oldOwner: this.editForm.get(['oldOwner'])!.value,
      asset: this.editForm.get(['asset'])!.value,
    };
  }
}
