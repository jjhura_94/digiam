import * as dayjs from 'dayjs';
import { IAssetsInRoom } from 'app/entities/assets-in-room/assets-in-room.model';
import { IAssetType } from 'app/entities/asset-type/asset-type.model';

export interface IAssetSet {
  id?: number;
  name?: string | null;
  code?: string | null;
  updatedBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  assetsInRooms?: IAssetsInRoom[] | null;
  assetType?: IAssetType | null;
}

export class AssetSet implements IAssetSet {
  constructor(
    public id?: number,
    public name?: string | null,
    public code?: string | null,
    public updatedBy?: string | null,
    public updatedDate?: dayjs.Dayjs | null,
    public assetsInRooms?: IAssetsInRoom[] | null,
    public assetType?: IAssetType | null
  ) {}
}

export function getAssetSetIdentifier(assetSet: IAssetSet): number | undefined {
  return assetSet.id;
}
