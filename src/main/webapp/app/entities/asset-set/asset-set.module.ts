import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AssetSetComponent } from './list/asset-set.component';
import { AssetSetDetailComponent } from './detail/asset-set-detail.component';
import { AssetSetUpdateComponent } from './update/asset-set-update.component';
import { AssetSetDeleteDialogComponent } from './delete/asset-set-delete-dialog.component';
import { AssetSetRoutingModule } from './route/asset-set-routing.module';

@NgModule({
  imports: [SharedModule, AssetSetRoutingModule],
  declarations: [AssetSetComponent, AssetSetDetailComponent, AssetSetUpdateComponent, AssetSetDeleteDialogComponent],
  entryComponents: [AssetSetDeleteDialogComponent],
})
export class AssetSetModule {}
