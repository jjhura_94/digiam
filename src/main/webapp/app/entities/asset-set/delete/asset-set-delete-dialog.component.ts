import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssetSet } from '../asset-set.model';
import { AssetSetService } from '../service/asset-set.service';

@Component({
  templateUrl: './asset-set-delete-dialog.component.html',
})
export class AssetSetDeleteDialogComponent {
  assetSet?: IAssetSet;

  constructor(protected assetSetService: AssetSetService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.assetSetService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
