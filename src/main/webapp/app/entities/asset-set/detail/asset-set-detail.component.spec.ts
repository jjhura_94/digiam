import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AssetSetDetailComponent } from './asset-set-detail.component';

describe('Component Tests', () => {
  describe('AssetSet Management Detail Component', () => {
    let comp: AssetSetDetailComponent;
    let fixture: ComponentFixture<AssetSetDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [AssetSetDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ assetSet: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(AssetSetDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AssetSetDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load assetSet on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.assetSet).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
