import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAssetSet } from '../asset-set.model';

@Component({
  selector: 'jhi-asset-set-detail',
  templateUrl: './asset-set-detail.component.html',
})
export class AssetSetDetailComponent implements OnInit {
  assetSet: IAssetSet | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assetSet }) => {
      this.assetSet = assetSet;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
