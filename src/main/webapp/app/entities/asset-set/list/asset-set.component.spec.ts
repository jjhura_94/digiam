import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AssetSetService } from '../service/asset-set.service';

import { AssetSetComponent } from './asset-set.component';

describe('Component Tests', () => {
  describe('AssetSet Management Component', () => {
    let comp: AssetSetComponent;
    let fixture: ComponentFixture<AssetSetComponent>;
    let service: AssetSetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [AssetSetComponent],
      })
        .overrideTemplate(AssetSetComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssetSetComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(AssetSetService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.assetSets?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
