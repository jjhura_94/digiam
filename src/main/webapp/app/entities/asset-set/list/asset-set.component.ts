import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssetSet } from '../asset-set.model';
import { AssetSetService } from '../service/asset-set.service';
import { AssetSetDeleteDialogComponent } from '../delete/asset-set-delete-dialog.component';

@Component({
  selector: 'jhi-asset-set',
  templateUrl: './asset-set.component.html',
})
export class AssetSetComponent implements OnInit {
  assetSets?: IAssetSet[];
  isLoading = false;

  constructor(protected assetSetService: AssetSetService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.assetSetService.query().subscribe(
      (res: HttpResponse<IAssetSet[]>) => {
        this.isLoading = false;
        this.assetSets = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IAssetSet): number {
    return item.id!;
  }

  delete(assetSet: IAssetSet): void {
    const modalRef = this.modalService.open(AssetSetDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.assetSet = assetSet;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
