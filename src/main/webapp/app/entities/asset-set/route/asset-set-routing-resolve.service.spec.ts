jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IAssetSet, AssetSet } from '../asset-set.model';
import { AssetSetService } from '../service/asset-set.service';

import { AssetSetRoutingResolveService } from './asset-set-routing-resolve.service';

describe('Service Tests', () => {
  describe('AssetSet routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: AssetSetRoutingResolveService;
    let service: AssetSetService;
    let resultAssetSet: IAssetSet | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(AssetSetRoutingResolveService);
      service = TestBed.inject(AssetSetService);
      resultAssetSet = undefined;
    });

    describe('resolve', () => {
      it('should return IAssetSet returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultAssetSet = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultAssetSet).toEqual({ id: 123 });
      });

      it('should return new IAssetSet if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultAssetSet = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultAssetSet).toEqual(new AssetSet());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as AssetSet })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultAssetSet = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultAssetSet).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
