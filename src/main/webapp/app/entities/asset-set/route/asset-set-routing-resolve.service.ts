import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAssetSet, AssetSet } from '../asset-set.model';
import { AssetSetService } from '../service/asset-set.service';

@Injectable({ providedIn: 'root' })
export class AssetSetRoutingResolveService implements Resolve<IAssetSet> {
  constructor(protected service: AssetSetService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAssetSet> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((assetSet: HttpResponse<AssetSet>) => {
          if (assetSet.body) {
            return of(assetSet.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AssetSet());
  }
}
