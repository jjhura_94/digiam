import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AssetSetComponent } from '../list/asset-set.component';
import { AssetSetDetailComponent } from '../detail/asset-set-detail.component';
import { AssetSetUpdateComponent } from '../update/asset-set-update.component';
import { AssetSetRoutingResolveService } from './asset-set-routing-resolve.service';

const assetSetRoute: Routes = [
  {
    path: '',
    component: AssetSetComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AssetSetDetailComponent,
    resolve: {
      assetSet: AssetSetRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AssetSetUpdateComponent,
    resolve: {
      assetSet: AssetSetRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AssetSetUpdateComponent,
    resolve: {
      assetSet: AssetSetRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(assetSetRoute)],
  exports: [RouterModule],
})
export class AssetSetRoutingModule {}
