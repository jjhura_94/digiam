import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IAssetSet, AssetSet } from '../asset-set.model';

import { AssetSetService } from './asset-set.service';

describe('Service Tests', () => {
  describe('AssetSet Service', () => {
    let service: AssetSetService;
    let httpMock: HttpTestingController;
    let elemDefault: IAssetSet;
    let expectedResult: IAssetSet | IAssetSet[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(AssetSetService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
        code: 'AAAAAAA',
        updatedBy: 'AAAAAAA',
        updatedDate: currentDate,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a AssetSet', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.create(new AssetSet()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a AssetSet', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            code: 'BBBBBB',
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a AssetSet', () => {
        const patchObject = Object.assign({}, new AssetSet());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of AssetSet', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            code: 'BBBBBB',
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a AssetSet', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addAssetSetToCollectionIfMissing', () => {
        it('should add a AssetSet to an empty array', () => {
          const assetSet: IAssetSet = { id: 123 };
          expectedResult = service.addAssetSetToCollectionIfMissing([], assetSet);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(assetSet);
        });

        it('should not add a AssetSet to an array that contains it', () => {
          const assetSet: IAssetSet = { id: 123 };
          const assetSetCollection: IAssetSet[] = [
            {
              ...assetSet,
            },
            { id: 456 },
          ];
          expectedResult = service.addAssetSetToCollectionIfMissing(assetSetCollection, assetSet);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a AssetSet to an array that doesn't contain it", () => {
          const assetSet: IAssetSet = { id: 123 };
          const assetSetCollection: IAssetSet[] = [{ id: 456 }];
          expectedResult = service.addAssetSetToCollectionIfMissing(assetSetCollection, assetSet);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(assetSet);
        });

        it('should add only unique AssetSet to an array', () => {
          const assetSetArray: IAssetSet[] = [{ id: 123 }, { id: 456 }, { id: 69124 }];
          const assetSetCollection: IAssetSet[] = [{ id: 123 }];
          expectedResult = service.addAssetSetToCollectionIfMissing(assetSetCollection, ...assetSetArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const assetSet: IAssetSet = { id: 123 };
          const assetSet2: IAssetSet = { id: 456 };
          expectedResult = service.addAssetSetToCollectionIfMissing([], assetSet, assetSet2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(assetSet);
          expect(expectedResult).toContain(assetSet2);
        });

        it('should accept null and undefined values', () => {
          const assetSet: IAssetSet = { id: 123 };
          expectedResult = service.addAssetSetToCollectionIfMissing([], null, assetSet, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(assetSet);
        });

        it('should return initial array if no AssetSet is added', () => {
          const assetSetCollection: IAssetSet[] = [{ id: 123 }];
          expectedResult = service.addAssetSetToCollectionIfMissing(assetSetCollection, undefined, null);
          expect(expectedResult).toEqual(assetSetCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
