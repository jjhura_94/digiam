import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAssetSet, getAssetSetIdentifier } from '../asset-set.model';

export type EntityResponseType = HttpResponse<IAssetSet>;
export type EntityArrayResponseType = HttpResponse<IAssetSet[]>;

@Injectable({ providedIn: 'root' })
export class AssetSetService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/asset-sets');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(assetSet: IAssetSet): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetSet);
    return this.http
      .post<IAssetSet>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(assetSet: IAssetSet): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetSet);
    return this.http
      .put<IAssetSet>(`${this.resourceUrl}/${getAssetSetIdentifier(assetSet) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(assetSet: IAssetSet): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetSet);
    return this.http
      .patch<IAssetSet>(`${this.resourceUrl}/${getAssetSetIdentifier(assetSet) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAssetSet>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAssetSet[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAssetSetToCollectionIfMissing(assetSetCollection: IAssetSet[], ...assetSetsToCheck: (IAssetSet | null | undefined)[]): IAssetSet[] {
    const assetSets: IAssetSet[] = assetSetsToCheck.filter(isPresent);
    if (assetSets.length > 0) {
      const assetSetCollectionIdentifiers = assetSetCollection.map(assetSetItem => getAssetSetIdentifier(assetSetItem)!);
      const assetSetsToAdd = assetSets.filter(assetSetItem => {
        const assetSetIdentifier = getAssetSetIdentifier(assetSetItem);
        if (assetSetIdentifier == null || assetSetCollectionIdentifiers.includes(assetSetIdentifier)) {
          return false;
        }
        assetSetCollectionIdentifiers.push(assetSetIdentifier);
        return true;
      });
      return [...assetSetsToAdd, ...assetSetCollection];
    }
    return assetSetCollection;
  }

  protected convertDateFromClient(assetSet: IAssetSet): IAssetSet {
    return Object.assign({}, assetSet, {
      updatedDate: assetSet.updatedDate?.isValid() ? assetSet.updatedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.updatedDate = res.body.updatedDate ? dayjs(res.body.updatedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((assetSet: IAssetSet) => {
        assetSet.updatedDate = assetSet.updatedDate ? dayjs(assetSet.updatedDate) : undefined;
      });
    }
    return res;
  }
}
