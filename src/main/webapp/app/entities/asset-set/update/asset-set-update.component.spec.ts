jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AssetSetService } from '../service/asset-set.service';
import { IAssetSet, AssetSet } from '../asset-set.model';
import { IAssetType } from 'app/entities/asset-type/asset-type.model';
import { AssetTypeService } from 'app/entities/asset-type/service/asset-type.service';

import { AssetSetUpdateComponent } from './asset-set-update.component';

describe('Component Tests', () => {
  describe('AssetSet Management Update Component', () => {
    let comp: AssetSetUpdateComponent;
    let fixture: ComponentFixture<AssetSetUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let assetSetService: AssetSetService;
    let assetTypeService: AssetTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [AssetSetUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(AssetSetUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssetSetUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      assetSetService = TestBed.inject(AssetSetService);
      assetTypeService = TestBed.inject(AssetTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call AssetType query and add missing value', () => {
        const assetSet: IAssetSet = { id: 456 };
        const assetType: IAssetType = { id: 65362 };
        assetSet.assetType = assetType;

        const assetTypeCollection: IAssetType[] = [{ id: 1521 }];
        jest.spyOn(assetTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: assetTypeCollection })));
        const additionalAssetTypes = [assetType];
        const expectedCollection: IAssetType[] = [...additionalAssetTypes, ...assetTypeCollection];
        jest.spyOn(assetTypeService, 'addAssetTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ assetSet });
        comp.ngOnInit();

        expect(assetTypeService.query).toHaveBeenCalled();
        expect(assetTypeService.addAssetTypeToCollectionIfMissing).toHaveBeenCalledWith(assetTypeCollection, ...additionalAssetTypes);
        expect(comp.assetTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const assetSet: IAssetSet = { id: 456 };
        const assetType: IAssetType = { id: 35357 };
        assetSet.assetType = assetType;

        activatedRoute.data = of({ assetSet });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(assetSet));
        expect(comp.assetTypesSharedCollection).toContain(assetType);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetSet>>();
        const assetSet = { id: 123 };
        jest.spyOn(assetSetService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetSet });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: assetSet }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(assetSetService.update).toHaveBeenCalledWith(assetSet);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetSet>>();
        const assetSet = new AssetSet();
        jest.spyOn(assetSetService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetSet });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: assetSet }));
        saveSubject.complete();

        // THEN
        expect(assetSetService.create).toHaveBeenCalledWith(assetSet);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetSet>>();
        const assetSet = { id: 123 };
        jest.spyOn(assetSetService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetSet });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(assetSetService.update).toHaveBeenCalledWith(assetSet);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackAssetTypeById', () => {
        it('Should return tracked AssetType primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackAssetTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
