import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IAssetSet, AssetSet } from '../asset-set.model';
import { AssetSetService } from '../service/asset-set.service';
import { IAssetType } from 'app/entities/asset-type/asset-type.model';
import { AssetTypeService } from 'app/entities/asset-type/service/asset-type.service';

@Component({
  selector: 'jhi-asset-set-update',
  templateUrl: './asset-set-update.component.html',
})
export class AssetSetUpdateComponent implements OnInit {
  isSaving = false;

  assetTypesSharedCollection: IAssetType[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
    updatedBy: [],
    updatedDate: [],
    assetType: [],
  });

  constructor(
    protected assetSetService: AssetSetService,
    protected assetTypeService: AssetTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assetSet }) => {
      if (assetSet.id === undefined) {
        const today = dayjs().startOf('day');
        assetSet.updatedDate = today;
      }

      this.updateForm(assetSet);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const assetSet = this.createFromForm();
    if (assetSet.id !== undefined) {
      this.subscribeToSaveResponse(this.assetSetService.update(assetSet));
    } else {
      this.subscribeToSaveResponse(this.assetSetService.create(assetSet));
    }
  }

  trackAssetTypeById(index: number, item: IAssetType): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAssetSet>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(assetSet: IAssetSet): void {
    this.editForm.patchValue({
      id: assetSet.id,
      name: assetSet.name,
      code: assetSet.code,
      updatedBy: assetSet.updatedBy,
      updatedDate: assetSet.updatedDate ? assetSet.updatedDate.format(DATE_TIME_FORMAT) : null,
      assetType: assetSet.assetType,
    });

    this.assetTypesSharedCollection = this.assetTypeService.addAssetTypeToCollectionIfMissing(
      this.assetTypesSharedCollection,
      assetSet.assetType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.assetTypeService
      .query()
      .pipe(map((res: HttpResponse<IAssetType[]>) => res.body ?? []))
      .pipe(
        map((assetTypes: IAssetType[]) =>
          this.assetTypeService.addAssetTypeToCollectionIfMissing(assetTypes, this.editForm.get('assetType')!.value)
        )
      )
      .subscribe((assetTypes: IAssetType[]) => (this.assetTypesSharedCollection = assetTypes));
  }

  protected createFromForm(): IAssetSet {
    return {
      ...new AssetSet(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      updatedDate: this.editForm.get(['updatedDate'])!.value
        ? dayjs(this.editForm.get(['updatedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      assetType: this.editForm.get(['assetType'])!.value,
    };
  }
}
