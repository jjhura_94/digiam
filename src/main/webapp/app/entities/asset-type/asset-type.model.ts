import * as dayjs from 'dayjs';
import { IAsset } from 'app/entities/asset/asset.model';
import { IAssetSet } from 'app/entities/asset-set/asset-set.model';

export interface IAssetType {
  id?: number;
  name?: string | null;
  code?: string | null;
  depreciation?: dayjs.Dayjs | null;
  assets?: IAsset[] | null;
  assetSets?: IAssetSet[] | null;
}

export class AssetType implements IAssetType {
  constructor(
    public id?: number,
    public name?: string | null,
    public code?: string | null,
    public depreciation?: dayjs.Dayjs | null,
    public assets?: IAsset[] | null,
    public assetSets?: IAssetSet[] | null
  ) {}
}

export function getAssetTypeIdentifier(assetType: IAssetType): number | undefined {
  return assetType.id;
}
