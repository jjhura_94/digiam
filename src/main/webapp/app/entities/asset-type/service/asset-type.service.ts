import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAssetType, getAssetTypeIdentifier } from '../asset-type.model';

export type EntityResponseType = HttpResponse<IAssetType>;
export type EntityArrayResponseType = HttpResponse<IAssetType[]>;

@Injectable({ providedIn: 'root' })
export class AssetTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/asset-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(assetType: IAssetType): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetType);
    return this.http
      .post<IAssetType>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(assetType: IAssetType): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetType);
    return this.http
      .put<IAssetType>(`${this.resourceUrl}/${getAssetTypeIdentifier(assetType) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(assetType: IAssetType): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(assetType);
    return this.http
      .patch<IAssetType>(`${this.resourceUrl}/${getAssetTypeIdentifier(assetType) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAssetType>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAssetType[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAssetTypeToCollectionIfMissing(
    assetTypeCollection: IAssetType[],
    ...assetTypesToCheck: (IAssetType | null | undefined)[]
  ): IAssetType[] {
    const assetTypes: IAssetType[] = assetTypesToCheck.filter(isPresent);
    if (assetTypes.length > 0) {
      const assetTypeCollectionIdentifiers = assetTypeCollection.map(assetTypeItem => getAssetTypeIdentifier(assetTypeItem)!);
      const assetTypesToAdd = assetTypes.filter(assetTypeItem => {
        const assetTypeIdentifier = getAssetTypeIdentifier(assetTypeItem);
        if (assetTypeIdentifier == null || assetTypeCollectionIdentifiers.includes(assetTypeIdentifier)) {
          return false;
        }
        assetTypeCollectionIdentifiers.push(assetTypeIdentifier);
        return true;
      });
      return [...assetTypesToAdd, ...assetTypeCollection];
    }
    return assetTypeCollection;
  }

  protected convertDateFromClient(assetType: IAssetType): IAssetType {
    return Object.assign({}, assetType, {
      depreciation: assetType.depreciation?.isValid() ? assetType.depreciation.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.depreciation = res.body.depreciation ? dayjs(res.body.depreciation) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((assetType: IAssetType) => {
        assetType.depreciation = assetType.depreciation ? dayjs(assetType.depreciation) : undefined;
      });
    }
    return res;
  }
}
