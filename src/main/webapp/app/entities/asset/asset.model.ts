import * as dayjs from 'dayjs';
import { IAssetHistory } from 'app/entities/asset-history/asset-history.model';
import { IAssetType } from 'app/entities/asset-type/asset-type.model';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';

export interface IAsset {
  id?: number;
  name?: string | null;
  code?: string | null;
  price?: number | null;
  expiryDateFrom?: dayjs.Dayjs | null;
  expiryDateTo?: dayjs.Dayjs | null;
  status?: number | null;
  image?: string | null;
  qrCode?: string | null;
  location?: string | null;
  note?: string | null;
  serialNo?: string | null;
  guarantee?: dayjs.Dayjs | null;
  createdDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  assetHistories?: IAssetHistory[] | null;
  assetType?: IAssetType | null;
  owner?: IEmpMgt | null;
}

export class Asset implements IAsset {
  constructor(
    public id?: number,
    public name?: string | null,
    public code?: string | null,
    public price?: number | null,
    public expiryDateFrom?: dayjs.Dayjs | null,
    public expiryDateTo?: dayjs.Dayjs | null,
    public status?: number | null,
    public image?: string | null,
    public qrCode?: string | null,
    public location?: string | null,
    public note?: string | null,
    public serialNo?: string | null,
    public guarantee?: dayjs.Dayjs | null,
    public createdDate?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public updatedDate?: dayjs.Dayjs | null,
    public updatedBy?: string | null,
    public assetHistories?: IAssetHistory[] | null,
    public assetType?: IAssetType | null,
    public owner?: IEmpMgt | null
  ) {}
}

export function getAssetIdentifier(asset: IAsset): number | undefined {
  return asset.id;
}
