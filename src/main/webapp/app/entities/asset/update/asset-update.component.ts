import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IAsset, Asset } from '../asset.model';
import { AssetService } from '../service/asset.service';
import { IAssetType } from 'app/entities/asset-type/asset-type.model';
import { AssetTypeService } from 'app/entities/asset-type/service/asset-type.service';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';

@Component({
  selector: 'jhi-asset-update',
  templateUrl: './asset-update.component.html',
})
export class AssetUpdateComponent implements OnInit {
  isSaving = false;

  assetTypesSharedCollection: IAssetType[] = [];
  empMgtsSharedCollection: IEmpMgt[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
    price: [],
    expiryDateFrom: [],
    expiryDateTo: [],
    status: [],
    image: [],
    qrCode: [],
    location: [],
    note: [],
    serialNo: [],
    guarantee: [],
    createdDate: [],
    createdBy: [],
    updatedDate: [],
    updatedBy: [],
    assetType: [],
    owner: [],
  });

  constructor(
    protected assetService: AssetService,
    protected assetTypeService: AssetTypeService,
    protected empMgtService: EmpMgtService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ asset }) => {
      if (asset.id === undefined) {
        const today = dayjs().startOf('day');
        asset.expiryDateFrom = today;
        asset.expiryDateTo = today;
        asset.guarantee = today;
        asset.createdDate = today;
        asset.updatedDate = today;
      }

      this.updateForm(asset);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const asset = this.createFromForm();
    if (asset.id !== undefined) {
      this.subscribeToSaveResponse(this.assetService.update(asset));
    } else {
      this.subscribeToSaveResponse(this.assetService.create(asset));
    }
  }

  trackAssetTypeById(index: number, item: IAssetType): number {
    return item.id!;
  }

  trackEmpMgtById(index: number, item: IEmpMgt): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAsset>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(asset: IAsset): void {
    this.editForm.patchValue({
      id: asset.id,
      name: asset.name,
      code: asset.code,
      price: asset.price,
      expiryDateFrom: asset.expiryDateFrom ? asset.expiryDateFrom.format(DATE_TIME_FORMAT) : null,
      expiryDateTo: asset.expiryDateTo ? asset.expiryDateTo.format(DATE_TIME_FORMAT) : null,
      status: asset.status,
      image: asset.image,
      qrCode: asset.qrCode,
      location: asset.location,
      note: asset.note,
      serialNo: asset.serialNo,
      guarantee: asset.guarantee ? asset.guarantee.format(DATE_TIME_FORMAT) : null,
      createdDate: asset.createdDate ? asset.createdDate.format(DATE_TIME_FORMAT) : null,
      createdBy: asset.createdBy,
      updatedDate: asset.updatedDate ? asset.updatedDate.format(DATE_TIME_FORMAT) : null,
      updatedBy: asset.updatedBy,
      assetType: asset.assetType,
      owner: asset.owner,
    });

    this.assetTypesSharedCollection = this.assetTypeService.addAssetTypeToCollectionIfMissing(
      this.assetTypesSharedCollection,
      asset.assetType
    );
    this.empMgtsSharedCollection = this.empMgtService.addEmpMgtToCollectionIfMissing(this.empMgtsSharedCollection, asset.owner);
  }

  protected loadRelationshipsOptions(): void {
    this.assetTypeService
      .query()
      .pipe(map((res: HttpResponse<IAssetType[]>) => res.body ?? []))
      .pipe(
        map((assetTypes: IAssetType[]) =>
          this.assetTypeService.addAssetTypeToCollectionIfMissing(assetTypes, this.editForm.get('assetType')!.value)
        )
      )
      .subscribe((assetTypes: IAssetType[]) => (this.assetTypesSharedCollection = assetTypes));

    this.empMgtService
      .query()
      .pipe(map((res: HttpResponse<IEmpMgt[]>) => res.body ?? []))
      .pipe(map((empMgts: IEmpMgt[]) => this.empMgtService.addEmpMgtToCollectionIfMissing(empMgts, this.editForm.get('owner')!.value)))
      .subscribe((empMgts: IEmpMgt[]) => (this.empMgtsSharedCollection = empMgts));
  }

  protected createFromForm(): IAsset {
    return {
      ...new Asset(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
      price: this.editForm.get(['price'])!.value,
      expiryDateFrom: this.editForm.get(['expiryDateFrom'])!.value
        ? dayjs(this.editForm.get(['expiryDateFrom'])!.value, DATE_TIME_FORMAT)
        : undefined,
      expiryDateTo: this.editForm.get(['expiryDateTo'])!.value
        ? dayjs(this.editForm.get(['expiryDateTo'])!.value, DATE_TIME_FORMAT)
        : undefined,
      status: this.editForm.get(['status'])!.value,
      image: this.editForm.get(['image'])!.value,
      qrCode: this.editForm.get(['qrCode'])!.value,
      location: this.editForm.get(['location'])!.value,
      note: this.editForm.get(['note'])!.value,
      serialNo: this.editForm.get(['serialNo'])!.value,
      guarantee: this.editForm.get(['guarantee'])!.value ? dayjs(this.editForm.get(['guarantee'])!.value, DATE_TIME_FORMAT) : undefined,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      updatedDate: this.editForm.get(['updatedDate'])!.value
        ? dayjs(this.editForm.get(['updatedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      assetType: this.editForm.get(['assetType'])!.value,
      owner: this.editForm.get(['owner'])!.value,
    };
  }
}
