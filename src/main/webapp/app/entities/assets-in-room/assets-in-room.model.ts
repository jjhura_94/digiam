import { IMeetingRoom } from 'app/entities/meeting-room/meeting-room.model';
import { IAssetSet } from 'app/entities/asset-set/asset-set.model';

export interface IAssetsInRoom {
  id?: number;
  room?: IMeetingRoom | null;
  asset?: IAssetSet | null;
}

export class AssetsInRoom implements IAssetsInRoom {
  constructor(public id?: number, public room?: IMeetingRoom | null, public asset?: IAssetSet | null) {}
}

export function getAssetsInRoomIdentifier(assetsInRoom: IAssetsInRoom): number | undefined {
  return assetsInRoom.id;
}
