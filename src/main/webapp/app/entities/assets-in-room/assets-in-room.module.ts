import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AssetsInRoomComponent } from './list/assets-in-room.component';
import { AssetsInRoomDetailComponent } from './detail/assets-in-room-detail.component';
import { AssetsInRoomUpdateComponent } from './update/assets-in-room-update.component';
import { AssetsInRoomDeleteDialogComponent } from './delete/assets-in-room-delete-dialog.component';
import { AssetsInRoomRoutingModule } from './route/assets-in-room-routing.module';

@NgModule({
  imports: [SharedModule, AssetsInRoomRoutingModule],
  declarations: [AssetsInRoomComponent, AssetsInRoomDetailComponent, AssetsInRoomUpdateComponent, AssetsInRoomDeleteDialogComponent],
  entryComponents: [AssetsInRoomDeleteDialogComponent],
})
export class AssetsInRoomModule {}
