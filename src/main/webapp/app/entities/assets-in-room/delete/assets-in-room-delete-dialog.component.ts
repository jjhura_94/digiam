import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssetsInRoom } from '../assets-in-room.model';
import { AssetsInRoomService } from '../service/assets-in-room.service';

@Component({
  templateUrl: './assets-in-room-delete-dialog.component.html',
})
export class AssetsInRoomDeleteDialogComponent {
  assetsInRoom?: IAssetsInRoom;

  constructor(protected assetsInRoomService: AssetsInRoomService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.assetsInRoomService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
