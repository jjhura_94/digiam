import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AssetsInRoomDetailComponent } from './assets-in-room-detail.component';

describe('Component Tests', () => {
  describe('AssetsInRoom Management Detail Component', () => {
    let comp: AssetsInRoomDetailComponent;
    let fixture: ComponentFixture<AssetsInRoomDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [AssetsInRoomDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ assetsInRoom: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(AssetsInRoomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AssetsInRoomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load assetsInRoom on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.assetsInRoom).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
