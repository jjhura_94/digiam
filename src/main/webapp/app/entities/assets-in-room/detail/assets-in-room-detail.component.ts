import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAssetsInRoom } from '../assets-in-room.model';

@Component({
  selector: 'jhi-assets-in-room-detail',
  templateUrl: './assets-in-room-detail.component.html',
})
export class AssetsInRoomDetailComponent implements OnInit {
  assetsInRoom: IAssetsInRoom | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assetsInRoom }) => {
      this.assetsInRoom = assetsInRoom;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
