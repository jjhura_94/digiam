import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AssetsInRoomService } from '../service/assets-in-room.service';

import { AssetsInRoomComponent } from './assets-in-room.component';

describe('Component Tests', () => {
  describe('AssetsInRoom Management Component', () => {
    let comp: AssetsInRoomComponent;
    let fixture: ComponentFixture<AssetsInRoomComponent>;
    let service: AssetsInRoomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [AssetsInRoomComponent],
      })
        .overrideTemplate(AssetsInRoomComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssetsInRoomComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(AssetsInRoomService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.assetsInRooms?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
