import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssetsInRoom } from '../assets-in-room.model';
import { AssetsInRoomService } from '../service/assets-in-room.service';
import { AssetsInRoomDeleteDialogComponent } from '../delete/assets-in-room-delete-dialog.component';

@Component({
  selector: 'jhi-assets-in-room',
  templateUrl: './assets-in-room.component.html',
})
export class AssetsInRoomComponent implements OnInit {
  assetsInRooms?: IAssetsInRoom[];
  isLoading = false;

  constructor(protected assetsInRoomService: AssetsInRoomService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.assetsInRoomService.query().subscribe(
      (res: HttpResponse<IAssetsInRoom[]>) => {
        this.isLoading = false;
        this.assetsInRooms = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IAssetsInRoom): number {
    return item.id!;
  }

  delete(assetsInRoom: IAssetsInRoom): void {
    const modalRef = this.modalService.open(AssetsInRoomDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.assetsInRoom = assetsInRoom;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
