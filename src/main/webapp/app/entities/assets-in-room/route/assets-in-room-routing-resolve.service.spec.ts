jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IAssetsInRoom, AssetsInRoom } from '../assets-in-room.model';
import { AssetsInRoomService } from '../service/assets-in-room.service';

import { AssetsInRoomRoutingResolveService } from './assets-in-room-routing-resolve.service';

describe('Service Tests', () => {
  describe('AssetsInRoom routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: AssetsInRoomRoutingResolveService;
    let service: AssetsInRoomService;
    let resultAssetsInRoom: IAssetsInRoom | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(AssetsInRoomRoutingResolveService);
      service = TestBed.inject(AssetsInRoomService);
      resultAssetsInRoom = undefined;
    });

    describe('resolve', () => {
      it('should return IAssetsInRoom returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultAssetsInRoom = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultAssetsInRoom).toEqual({ id: 123 });
      });

      it('should return new IAssetsInRoom if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultAssetsInRoom = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultAssetsInRoom).toEqual(new AssetsInRoom());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as AssetsInRoom })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultAssetsInRoom = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultAssetsInRoom).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
