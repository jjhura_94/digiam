import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAssetsInRoom, AssetsInRoom } from '../assets-in-room.model';
import { AssetsInRoomService } from '../service/assets-in-room.service';

@Injectable({ providedIn: 'root' })
export class AssetsInRoomRoutingResolveService implements Resolve<IAssetsInRoom> {
  constructor(protected service: AssetsInRoomService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAssetsInRoom> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((assetsInRoom: HttpResponse<AssetsInRoom>) => {
          if (assetsInRoom.body) {
            return of(assetsInRoom.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AssetsInRoom());
  }
}
