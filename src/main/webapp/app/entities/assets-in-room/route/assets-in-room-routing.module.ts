import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AssetsInRoomComponent } from '../list/assets-in-room.component';
import { AssetsInRoomDetailComponent } from '../detail/assets-in-room-detail.component';
import { AssetsInRoomUpdateComponent } from '../update/assets-in-room-update.component';
import { AssetsInRoomRoutingResolveService } from './assets-in-room-routing-resolve.service';

const assetsInRoomRoute: Routes = [
  {
    path: '',
    component: AssetsInRoomComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AssetsInRoomDetailComponent,
    resolve: {
      assetsInRoom: AssetsInRoomRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AssetsInRoomUpdateComponent,
    resolve: {
      assetsInRoom: AssetsInRoomRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AssetsInRoomUpdateComponent,
    resolve: {
      assetsInRoom: AssetsInRoomRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(assetsInRoomRoute)],
  exports: [RouterModule],
})
export class AssetsInRoomRoutingModule {}
