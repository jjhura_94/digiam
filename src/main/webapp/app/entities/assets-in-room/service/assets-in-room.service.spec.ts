import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IAssetsInRoom, AssetsInRoom } from '../assets-in-room.model';

import { AssetsInRoomService } from './assets-in-room.service';

describe('Service Tests', () => {
  describe('AssetsInRoom Service', () => {
    let service: AssetsInRoomService;
    let httpMock: HttpTestingController;
    let elemDefault: IAssetsInRoom;
    let expectedResult: IAssetsInRoom | IAssetsInRoom[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(AssetsInRoomService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a AssetsInRoom', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new AssetsInRoom()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a AssetsInRoom', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a AssetsInRoom', () => {
        const patchObject = Object.assign({}, new AssetsInRoom());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of AssetsInRoom', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a AssetsInRoom', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addAssetsInRoomToCollectionIfMissing', () => {
        it('should add a AssetsInRoom to an empty array', () => {
          const assetsInRoom: IAssetsInRoom = { id: 123 };
          expectedResult = service.addAssetsInRoomToCollectionIfMissing([], assetsInRoom);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(assetsInRoom);
        });

        it('should not add a AssetsInRoom to an array that contains it', () => {
          const assetsInRoom: IAssetsInRoom = { id: 123 };
          const assetsInRoomCollection: IAssetsInRoom[] = [
            {
              ...assetsInRoom,
            },
            { id: 456 },
          ];
          expectedResult = service.addAssetsInRoomToCollectionIfMissing(assetsInRoomCollection, assetsInRoom);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a AssetsInRoom to an array that doesn't contain it", () => {
          const assetsInRoom: IAssetsInRoom = { id: 123 };
          const assetsInRoomCollection: IAssetsInRoom[] = [{ id: 456 }];
          expectedResult = service.addAssetsInRoomToCollectionIfMissing(assetsInRoomCollection, assetsInRoom);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(assetsInRoom);
        });

        it('should add only unique AssetsInRoom to an array', () => {
          const assetsInRoomArray: IAssetsInRoom[] = [{ id: 123 }, { id: 456 }, { id: 19364 }];
          const assetsInRoomCollection: IAssetsInRoom[] = [{ id: 123 }];
          expectedResult = service.addAssetsInRoomToCollectionIfMissing(assetsInRoomCollection, ...assetsInRoomArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const assetsInRoom: IAssetsInRoom = { id: 123 };
          const assetsInRoom2: IAssetsInRoom = { id: 456 };
          expectedResult = service.addAssetsInRoomToCollectionIfMissing([], assetsInRoom, assetsInRoom2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(assetsInRoom);
          expect(expectedResult).toContain(assetsInRoom2);
        });

        it('should accept null and undefined values', () => {
          const assetsInRoom: IAssetsInRoom = { id: 123 };
          expectedResult = service.addAssetsInRoomToCollectionIfMissing([], null, assetsInRoom, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(assetsInRoom);
        });

        it('should return initial array if no AssetsInRoom is added', () => {
          const assetsInRoomCollection: IAssetsInRoom[] = [{ id: 123 }];
          expectedResult = service.addAssetsInRoomToCollectionIfMissing(assetsInRoomCollection, undefined, null);
          expect(expectedResult).toEqual(assetsInRoomCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
