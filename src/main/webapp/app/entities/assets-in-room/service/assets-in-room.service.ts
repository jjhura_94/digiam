import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAssetsInRoom, getAssetsInRoomIdentifier } from '../assets-in-room.model';

export type EntityResponseType = HttpResponse<IAssetsInRoom>;
export type EntityArrayResponseType = HttpResponse<IAssetsInRoom[]>;

@Injectable({ providedIn: 'root' })
export class AssetsInRoomService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/assets-in-rooms');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(assetsInRoom: IAssetsInRoom): Observable<EntityResponseType> {
    return this.http.post<IAssetsInRoom>(this.resourceUrl, assetsInRoom, { observe: 'response' });
  }

  update(assetsInRoom: IAssetsInRoom): Observable<EntityResponseType> {
    return this.http.put<IAssetsInRoom>(`${this.resourceUrl}/${getAssetsInRoomIdentifier(assetsInRoom) as number}`, assetsInRoom, {
      observe: 'response',
    });
  }

  partialUpdate(assetsInRoom: IAssetsInRoom): Observable<EntityResponseType> {
    return this.http.patch<IAssetsInRoom>(`${this.resourceUrl}/${getAssetsInRoomIdentifier(assetsInRoom) as number}`, assetsInRoom, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAssetsInRoom>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAssetsInRoom[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAssetsInRoomToCollectionIfMissing(
    assetsInRoomCollection: IAssetsInRoom[],
    ...assetsInRoomsToCheck: (IAssetsInRoom | null | undefined)[]
  ): IAssetsInRoom[] {
    const assetsInRooms: IAssetsInRoom[] = assetsInRoomsToCheck.filter(isPresent);
    if (assetsInRooms.length > 0) {
      const assetsInRoomCollectionIdentifiers = assetsInRoomCollection.map(
        assetsInRoomItem => getAssetsInRoomIdentifier(assetsInRoomItem)!
      );
      const assetsInRoomsToAdd = assetsInRooms.filter(assetsInRoomItem => {
        const assetsInRoomIdentifier = getAssetsInRoomIdentifier(assetsInRoomItem);
        if (assetsInRoomIdentifier == null || assetsInRoomCollectionIdentifiers.includes(assetsInRoomIdentifier)) {
          return false;
        }
        assetsInRoomCollectionIdentifiers.push(assetsInRoomIdentifier);
        return true;
      });
      return [...assetsInRoomsToAdd, ...assetsInRoomCollection];
    }
    return assetsInRoomCollection;
  }
}
