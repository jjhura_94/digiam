jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AssetsInRoomService } from '../service/assets-in-room.service';
import { IAssetsInRoom, AssetsInRoom } from '../assets-in-room.model';
import { IMeetingRoom } from 'app/entities/meeting-room/meeting-room.model';
import { MeetingRoomService } from 'app/entities/meeting-room/service/meeting-room.service';
import { IAssetSet } from 'app/entities/asset-set/asset-set.model';
import { AssetSetService } from 'app/entities/asset-set/service/asset-set.service';

import { AssetsInRoomUpdateComponent } from './assets-in-room-update.component';

describe('Component Tests', () => {
  describe('AssetsInRoom Management Update Component', () => {
    let comp: AssetsInRoomUpdateComponent;
    let fixture: ComponentFixture<AssetsInRoomUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let assetsInRoomService: AssetsInRoomService;
    let meetingRoomService: MeetingRoomService;
    let assetSetService: AssetSetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [AssetsInRoomUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(AssetsInRoomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssetsInRoomUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      assetsInRoomService = TestBed.inject(AssetsInRoomService);
      meetingRoomService = TestBed.inject(MeetingRoomService);
      assetSetService = TestBed.inject(AssetSetService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call MeetingRoom query and add missing value', () => {
        const assetsInRoom: IAssetsInRoom = { id: 456 };
        const room: IMeetingRoom = { id: 78558 };
        assetsInRoom.room = room;

        const meetingRoomCollection: IMeetingRoom[] = [{ id: 33273 }];
        jest.spyOn(meetingRoomService, 'query').mockReturnValue(of(new HttpResponse({ body: meetingRoomCollection })));
        const additionalMeetingRooms = [room];
        const expectedCollection: IMeetingRoom[] = [...additionalMeetingRooms, ...meetingRoomCollection];
        jest.spyOn(meetingRoomService, 'addMeetingRoomToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ assetsInRoom });
        comp.ngOnInit();

        expect(meetingRoomService.query).toHaveBeenCalled();
        expect(meetingRoomService.addMeetingRoomToCollectionIfMissing).toHaveBeenCalledWith(
          meetingRoomCollection,
          ...additionalMeetingRooms
        );
        expect(comp.meetingRoomsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call AssetSet query and add missing value', () => {
        const assetsInRoom: IAssetsInRoom = { id: 456 };
        const asset: IAssetSet = { id: 54920 };
        assetsInRoom.asset = asset;

        const assetSetCollection: IAssetSet[] = [{ id: 88455 }];
        jest.spyOn(assetSetService, 'query').mockReturnValue(of(new HttpResponse({ body: assetSetCollection })));
        const additionalAssetSets = [asset];
        const expectedCollection: IAssetSet[] = [...additionalAssetSets, ...assetSetCollection];
        jest.spyOn(assetSetService, 'addAssetSetToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ assetsInRoom });
        comp.ngOnInit();

        expect(assetSetService.query).toHaveBeenCalled();
        expect(assetSetService.addAssetSetToCollectionIfMissing).toHaveBeenCalledWith(assetSetCollection, ...additionalAssetSets);
        expect(comp.assetSetsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const assetsInRoom: IAssetsInRoom = { id: 456 };
        const room: IMeetingRoom = { id: 46708 };
        assetsInRoom.room = room;
        const asset: IAssetSet = { id: 23486 };
        assetsInRoom.asset = asset;

        activatedRoute.data = of({ assetsInRoom });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(assetsInRoom));
        expect(comp.meetingRoomsSharedCollection).toContain(room);
        expect(comp.assetSetsSharedCollection).toContain(asset);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetsInRoom>>();
        const assetsInRoom = { id: 123 };
        jest.spyOn(assetsInRoomService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetsInRoom });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: assetsInRoom }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(assetsInRoomService.update).toHaveBeenCalledWith(assetsInRoom);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetsInRoom>>();
        const assetsInRoom = new AssetsInRoom();
        jest.spyOn(assetsInRoomService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetsInRoom });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: assetsInRoom }));
        saveSubject.complete();

        // THEN
        expect(assetsInRoomService.create).toHaveBeenCalledWith(assetsInRoom);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<AssetsInRoom>>();
        const assetsInRoom = { id: 123 };
        jest.spyOn(assetsInRoomService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ assetsInRoom });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(assetsInRoomService.update).toHaveBeenCalledWith(assetsInRoom);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackMeetingRoomById', () => {
        it('Should return tracked MeetingRoom primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMeetingRoomById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackAssetSetById', () => {
        it('Should return tracked AssetSet primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackAssetSetById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
