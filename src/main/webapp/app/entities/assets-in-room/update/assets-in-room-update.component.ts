import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IAssetsInRoom, AssetsInRoom } from '../assets-in-room.model';
import { AssetsInRoomService } from '../service/assets-in-room.service';
import { IMeetingRoom } from 'app/entities/meeting-room/meeting-room.model';
import { MeetingRoomService } from 'app/entities/meeting-room/service/meeting-room.service';
import { IAssetSet } from 'app/entities/asset-set/asset-set.model';
import { AssetSetService } from 'app/entities/asset-set/service/asset-set.service';

@Component({
  selector: 'jhi-assets-in-room-update',
  templateUrl: './assets-in-room-update.component.html',
})
export class AssetsInRoomUpdateComponent implements OnInit {
  isSaving = false;

  meetingRoomsSharedCollection: IMeetingRoom[] = [];
  assetSetsSharedCollection: IAssetSet[] = [];

  editForm = this.fb.group({
    id: [],
    room: [],
    asset: [],
  });

  constructor(
    protected assetsInRoomService: AssetsInRoomService,
    protected meetingRoomService: MeetingRoomService,
    protected assetSetService: AssetSetService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assetsInRoom }) => {
      this.updateForm(assetsInRoom);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const assetsInRoom = this.createFromForm();
    if (assetsInRoom.id !== undefined) {
      this.subscribeToSaveResponse(this.assetsInRoomService.update(assetsInRoom));
    } else {
      this.subscribeToSaveResponse(this.assetsInRoomService.create(assetsInRoom));
    }
  }

  trackMeetingRoomById(index: number, item: IMeetingRoom): number {
    return item.id!;
  }

  trackAssetSetById(index: number, item: IAssetSet): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAssetsInRoom>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(assetsInRoom: IAssetsInRoom): void {
    this.editForm.patchValue({
      id: assetsInRoom.id,
      room: assetsInRoom.room,
      asset: assetsInRoom.asset,
    });

    this.meetingRoomsSharedCollection = this.meetingRoomService.addMeetingRoomToCollectionIfMissing(
      this.meetingRoomsSharedCollection,
      assetsInRoom.room
    );
    this.assetSetsSharedCollection = this.assetSetService.addAssetSetToCollectionIfMissing(
      this.assetSetsSharedCollection,
      assetsInRoom.asset
    );
  }

  protected loadRelationshipsOptions(): void {
    this.meetingRoomService
      .query()
      .pipe(map((res: HttpResponse<IMeetingRoom[]>) => res.body ?? []))
      .pipe(
        map((meetingRooms: IMeetingRoom[]) =>
          this.meetingRoomService.addMeetingRoomToCollectionIfMissing(meetingRooms, this.editForm.get('room')!.value)
        )
      )
      .subscribe((meetingRooms: IMeetingRoom[]) => (this.meetingRoomsSharedCollection = meetingRooms));

    this.assetSetService
      .query()
      .pipe(map((res: HttpResponse<IAssetSet[]>) => res.body ?? []))
      .pipe(
        map((assetSets: IAssetSet[]) => this.assetSetService.addAssetSetToCollectionIfMissing(assetSets, this.editForm.get('asset')!.value))
      )
      .subscribe((assetSets: IAssetSet[]) => (this.assetSetsSharedCollection = assetSets));
  }

  protected createFromForm(): IAssetsInRoom {
    return {
      ...new AssetsInRoom(),
      id: this.editForm.get(['id'])!.value,
      room: this.editForm.get(['room'])!.value,
      asset: this.editForm.get(['asset'])!.value,
    };
  }
}
