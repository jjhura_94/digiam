import * as dayjs from 'dayjs';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { IDayOffType } from 'app/entities/day-off-type/day-off-type.model';

export interface IDayOffTicket {
  id?: number;
  numberOfDayLeft?: number | null;
  reason?: string | null;
  watcher?: string | null;
  status?: boolean | null;
  fromDate?: dayjs.Dayjs | null;
  toDate?: dayjs.Dayjs | null;
  note?: string | null;
  createdDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  approver?: IEmpMgt | null;
  approverByCeo?: IEmpMgt | null;
  createdBy?: IEmpMgt | null;
  dayOffType?: IDayOffType | null;
}

export class DayOffTicket implements IDayOffTicket {
  constructor(
    public id?: number,
    public numberOfDayLeft?: number | null,
    public reason?: string | null,
    public watcher?: string | null,
    public status?: boolean | null,
    public fromDate?: dayjs.Dayjs | null,
    public toDate?: dayjs.Dayjs | null,
    public note?: string | null,
    public createdDate?: dayjs.Dayjs | null,
    public updatedBy?: string | null,
    public updatedDate?: dayjs.Dayjs | null,
    public approver?: IEmpMgt | null,
    public approverByCeo?: IEmpMgt | null,
    public createdBy?: IEmpMgt | null,
    public dayOffType?: IDayOffType | null
  ) {
    this.status = this.status ?? false;
  }
}

export function getDayOffTicketIdentifier(dayOffTicket: IDayOffTicket): number | undefined {
  return dayOffTicket.id;
}
