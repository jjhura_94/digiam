import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DayOffTicketComponent } from './list/day-off-ticket.component';
import { DayOffTicketDetailComponent } from './detail/day-off-ticket-detail.component';
import { DayOffTicketUpdateComponent } from './update/day-off-ticket-update.component';
import { DayOffTicketDeleteDialogComponent } from './delete/day-off-ticket-delete-dialog.component';
import { DayOffTicketRoutingModule } from './route/day-off-ticket-routing.module';

@NgModule({
  imports: [SharedModule, DayOffTicketRoutingModule],
  declarations: [DayOffTicketComponent, DayOffTicketDetailComponent, DayOffTicketUpdateComponent, DayOffTicketDeleteDialogComponent],
  entryComponents: [DayOffTicketDeleteDialogComponent],
})
export class DayOffTicketModule {}
