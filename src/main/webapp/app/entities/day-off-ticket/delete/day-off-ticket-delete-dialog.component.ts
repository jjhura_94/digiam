import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDayOffTicket } from '../day-off-ticket.model';
import { DayOffTicketService } from '../service/day-off-ticket.service';

@Component({
  templateUrl: './day-off-ticket-delete-dialog.component.html',
})
export class DayOffTicketDeleteDialogComponent {
  dayOffTicket?: IDayOffTicket;

  constructor(protected dayOffTicketService: DayOffTicketService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dayOffTicketService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
