import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DayOffTicketDetailComponent } from './day-off-ticket-detail.component';

describe('Component Tests', () => {
  describe('DayOffTicket Management Detail Component', () => {
    let comp: DayOffTicketDetailComponent;
    let fixture: ComponentFixture<DayOffTicketDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DayOffTicketDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ dayOffTicket: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(DayOffTicketDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DayOffTicketDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dayOffTicket on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dayOffTicket).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
