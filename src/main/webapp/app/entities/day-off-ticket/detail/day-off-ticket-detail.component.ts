import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDayOffTicket } from '../day-off-ticket.model';

@Component({
  selector: 'jhi-day-off-ticket-detail',
  templateUrl: './day-off-ticket-detail.component.html',
})
export class DayOffTicketDetailComponent implements OnInit {
  dayOffTicket: IDayOffTicket | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dayOffTicket }) => {
      this.dayOffTicket = dayOffTicket;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
