import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DayOffTicketService } from '../service/day-off-ticket.service';

import { DayOffTicketComponent } from './day-off-ticket.component';

describe('Component Tests', () => {
  describe('DayOffTicket Management Component', () => {
    let comp: DayOffTicketComponent;
    let fixture: ComponentFixture<DayOffTicketComponent>;
    let service: DayOffTicketService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DayOffTicketComponent],
      })
        .overrideTemplate(DayOffTicketComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DayOffTicketComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DayOffTicketService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.dayOffTickets?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
