import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDayOffTicket } from '../day-off-ticket.model';
import { DayOffTicketService } from '../service/day-off-ticket.service';
import { DayOffTicketDeleteDialogComponent } from '../delete/day-off-ticket-delete-dialog.component';

@Component({
  selector: 'jhi-day-off-ticket',
  templateUrl: './day-off-ticket.component.html',
})
export class DayOffTicketComponent implements OnInit {
  dayOffTickets?: IDayOffTicket[];
  isLoading = false;

  constructor(protected dayOffTicketService: DayOffTicketService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.dayOffTicketService.query().subscribe(
      (res: HttpResponse<IDayOffTicket[]>) => {
        this.isLoading = false;
        this.dayOffTickets = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDayOffTicket): number {
    return item.id!;
  }

  delete(dayOffTicket: IDayOffTicket): void {
    const modalRef = this.modalService.open(DayOffTicketDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.dayOffTicket = dayOffTicket;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
