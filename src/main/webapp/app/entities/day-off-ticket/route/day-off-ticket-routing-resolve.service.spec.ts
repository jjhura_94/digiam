jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IDayOffTicket, DayOffTicket } from '../day-off-ticket.model';
import { DayOffTicketService } from '../service/day-off-ticket.service';

import { DayOffTicketRoutingResolveService } from './day-off-ticket-routing-resolve.service';

describe('Service Tests', () => {
  describe('DayOffTicket routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: DayOffTicketRoutingResolveService;
    let service: DayOffTicketService;
    let resultDayOffTicket: IDayOffTicket | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(DayOffTicketRoutingResolveService);
      service = TestBed.inject(DayOffTicketService);
      resultDayOffTicket = undefined;
    });

    describe('resolve', () => {
      it('should return IDayOffTicket returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDayOffTicket = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDayOffTicket).toEqual({ id: 123 });
      });

      it('should return new IDayOffTicket if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDayOffTicket = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultDayOffTicket).toEqual(new DayOffTicket());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as DayOffTicket })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDayOffTicket = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDayOffTicket).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
