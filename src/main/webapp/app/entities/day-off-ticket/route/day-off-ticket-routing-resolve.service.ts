import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDayOffTicket, DayOffTicket } from '../day-off-ticket.model';
import { DayOffTicketService } from '../service/day-off-ticket.service';

@Injectable({ providedIn: 'root' })
export class DayOffTicketRoutingResolveService implements Resolve<IDayOffTicket> {
  constructor(protected service: DayOffTicketService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDayOffTicket> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dayOffTicket: HttpResponse<DayOffTicket>) => {
          if (dayOffTicket.body) {
            return of(dayOffTicket.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DayOffTicket());
  }
}
