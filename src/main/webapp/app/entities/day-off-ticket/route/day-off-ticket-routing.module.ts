import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DayOffTicketComponent } from '../list/day-off-ticket.component';
import { DayOffTicketDetailComponent } from '../detail/day-off-ticket-detail.component';
import { DayOffTicketUpdateComponent } from '../update/day-off-ticket-update.component';
import { DayOffTicketRoutingResolveService } from './day-off-ticket-routing-resolve.service';

const dayOffTicketRoute: Routes = [
  {
    path: '',
    component: DayOffTicketComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DayOffTicketDetailComponent,
    resolve: {
      dayOffTicket: DayOffTicketRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DayOffTicketUpdateComponent,
    resolve: {
      dayOffTicket: DayOffTicketRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DayOffTicketUpdateComponent,
    resolve: {
      dayOffTicket: DayOffTicketRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dayOffTicketRoute)],
  exports: [RouterModule],
})
export class DayOffTicketRoutingModule {}
