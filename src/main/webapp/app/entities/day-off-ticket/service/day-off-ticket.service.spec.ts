import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IDayOffTicket, DayOffTicket } from '../day-off-ticket.model';

import { DayOffTicketService } from './day-off-ticket.service';

describe('Service Tests', () => {
  describe('DayOffTicket Service', () => {
    let service: DayOffTicketService;
    let httpMock: HttpTestingController;
    let elemDefault: IDayOffTicket;
    let expectedResult: IDayOffTicket | IDayOffTicket[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DayOffTicketService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        numberOfDayLeft: 0,
        reason: 'AAAAAAA',
        watcher: 'AAAAAAA',
        status: false,
        fromDate: currentDate,
        toDate: currentDate,
        note: 'AAAAAAA',
        createdDate: currentDate,
        updatedBy: 'AAAAAAA',
        updatedDate: currentDate,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            fromDate: currentDate.format(DATE_TIME_FORMAT),
            toDate: currentDate.format(DATE_TIME_FORMAT),
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DayOffTicket', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            fromDate: currentDate.format(DATE_TIME_FORMAT),
            toDate: currentDate.format(DATE_TIME_FORMAT),
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fromDate: currentDate,
            toDate: currentDate,
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.create(new DayOffTicket()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DayOffTicket', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            numberOfDayLeft: 1,
            reason: 'BBBBBB',
            watcher: 'BBBBBB',
            status: true,
            fromDate: currentDate.format(DATE_TIME_FORMAT),
            toDate: currentDate.format(DATE_TIME_FORMAT),
            note: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fromDate: currentDate,
            toDate: currentDate,
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DayOffTicket', () => {
        const patchObject = Object.assign(
          {
            numberOfDayLeft: 1,
            reason: 'BBBBBB',
            watcher: 'BBBBBB',
            fromDate: currentDate.format(DATE_TIME_FORMAT),
            toDate: currentDate.format(DATE_TIME_FORMAT),
            note: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
          },
          new DayOffTicket()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            fromDate: currentDate,
            toDate: currentDate,
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DayOffTicket', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            numberOfDayLeft: 1,
            reason: 'BBBBBB',
            watcher: 'BBBBBB',
            status: true,
            fromDate: currentDate.format(DATE_TIME_FORMAT),
            toDate: currentDate.format(DATE_TIME_FORMAT),
            note: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            fromDate: currentDate,
            toDate: currentDate,
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DayOffTicket', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDayOffTicketToCollectionIfMissing', () => {
        it('should add a DayOffTicket to an empty array', () => {
          const dayOffTicket: IDayOffTicket = { id: 123 };
          expectedResult = service.addDayOffTicketToCollectionIfMissing([], dayOffTicket);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dayOffTicket);
        });

        it('should not add a DayOffTicket to an array that contains it', () => {
          const dayOffTicket: IDayOffTicket = { id: 123 };
          const dayOffTicketCollection: IDayOffTicket[] = [
            {
              ...dayOffTicket,
            },
            { id: 456 },
          ];
          expectedResult = service.addDayOffTicketToCollectionIfMissing(dayOffTicketCollection, dayOffTicket);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DayOffTicket to an array that doesn't contain it", () => {
          const dayOffTicket: IDayOffTicket = { id: 123 };
          const dayOffTicketCollection: IDayOffTicket[] = [{ id: 456 }];
          expectedResult = service.addDayOffTicketToCollectionIfMissing(dayOffTicketCollection, dayOffTicket);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dayOffTicket);
        });

        it('should add only unique DayOffTicket to an array', () => {
          const dayOffTicketArray: IDayOffTicket[] = [{ id: 123 }, { id: 456 }, { id: 61253 }];
          const dayOffTicketCollection: IDayOffTicket[] = [{ id: 123 }];
          expectedResult = service.addDayOffTicketToCollectionIfMissing(dayOffTicketCollection, ...dayOffTicketArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dayOffTicket: IDayOffTicket = { id: 123 };
          const dayOffTicket2: IDayOffTicket = { id: 456 };
          expectedResult = service.addDayOffTicketToCollectionIfMissing([], dayOffTicket, dayOffTicket2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dayOffTicket);
          expect(expectedResult).toContain(dayOffTicket2);
        });

        it('should accept null and undefined values', () => {
          const dayOffTicket: IDayOffTicket = { id: 123 };
          expectedResult = service.addDayOffTicketToCollectionIfMissing([], null, dayOffTicket, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dayOffTicket);
        });

        it('should return initial array if no DayOffTicket is added', () => {
          const dayOffTicketCollection: IDayOffTicket[] = [{ id: 123 }];
          expectedResult = service.addDayOffTicketToCollectionIfMissing(dayOffTicketCollection, undefined, null);
          expect(expectedResult).toEqual(dayOffTicketCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
