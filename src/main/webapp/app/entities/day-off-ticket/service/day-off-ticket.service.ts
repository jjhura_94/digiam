import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDayOffTicket, getDayOffTicketIdentifier } from '../day-off-ticket.model';

export type EntityResponseType = HttpResponse<IDayOffTicket>;
export type EntityArrayResponseType = HttpResponse<IDayOffTicket[]>;

@Injectable({ providedIn: 'root' })
export class DayOffTicketService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/day-off-tickets');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(dayOffTicket: IDayOffTicket): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dayOffTicket);
    return this.http
      .post<IDayOffTicket>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(dayOffTicket: IDayOffTicket): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dayOffTicket);
    return this.http
      .put<IDayOffTicket>(`${this.resourceUrl}/${getDayOffTicketIdentifier(dayOffTicket) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(dayOffTicket: IDayOffTicket): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dayOffTicket);
    return this.http
      .patch<IDayOffTicket>(`${this.resourceUrl}/${getDayOffTicketIdentifier(dayOffTicket) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDayOffTicket>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDayOffTicket[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDayOffTicketToCollectionIfMissing(
    dayOffTicketCollection: IDayOffTicket[],
    ...dayOffTicketsToCheck: (IDayOffTicket | null | undefined)[]
  ): IDayOffTicket[] {
    const dayOffTickets: IDayOffTicket[] = dayOffTicketsToCheck.filter(isPresent);
    if (dayOffTickets.length > 0) {
      const dayOffTicketCollectionIdentifiers = dayOffTicketCollection.map(
        dayOffTicketItem => getDayOffTicketIdentifier(dayOffTicketItem)!
      );
      const dayOffTicketsToAdd = dayOffTickets.filter(dayOffTicketItem => {
        const dayOffTicketIdentifier = getDayOffTicketIdentifier(dayOffTicketItem);
        if (dayOffTicketIdentifier == null || dayOffTicketCollectionIdentifiers.includes(dayOffTicketIdentifier)) {
          return false;
        }
        dayOffTicketCollectionIdentifiers.push(dayOffTicketIdentifier);
        return true;
      });
      return [...dayOffTicketsToAdd, ...dayOffTicketCollection];
    }
    return dayOffTicketCollection;
  }

  protected convertDateFromClient(dayOffTicket: IDayOffTicket): IDayOffTicket {
    return Object.assign({}, dayOffTicket, {
      fromDate: dayOffTicket.fromDate?.isValid() ? dayOffTicket.fromDate.toJSON() : undefined,
      toDate: dayOffTicket.toDate?.isValid() ? dayOffTicket.toDate.toJSON() : undefined,
      createdDate: dayOffTicket.createdDate?.isValid() ? dayOffTicket.createdDate.toJSON() : undefined,
      updatedDate: dayOffTicket.updatedDate?.isValid() ? dayOffTicket.updatedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fromDate = res.body.fromDate ? dayjs(res.body.fromDate) : undefined;
      res.body.toDate = res.body.toDate ? dayjs(res.body.toDate) : undefined;
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.updatedDate = res.body.updatedDate ? dayjs(res.body.updatedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((dayOffTicket: IDayOffTicket) => {
        dayOffTicket.fromDate = dayOffTicket.fromDate ? dayjs(dayOffTicket.fromDate) : undefined;
        dayOffTicket.toDate = dayOffTicket.toDate ? dayjs(dayOffTicket.toDate) : undefined;
        dayOffTicket.createdDate = dayOffTicket.createdDate ? dayjs(dayOffTicket.createdDate) : undefined;
        dayOffTicket.updatedDate = dayOffTicket.updatedDate ? dayjs(dayOffTicket.updatedDate) : undefined;
      });
    }
    return res;
  }
}
