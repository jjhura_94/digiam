jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DayOffTicketService } from '../service/day-off-ticket.service';
import { IDayOffTicket, DayOffTicket } from '../day-off-ticket.model';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';
import { IDayOffType } from 'app/entities/day-off-type/day-off-type.model';
import { DayOffTypeService } from 'app/entities/day-off-type/service/day-off-type.service';

import { DayOffTicketUpdateComponent } from './day-off-ticket-update.component';

describe('Component Tests', () => {
  describe('DayOffTicket Management Update Component', () => {
    let comp: DayOffTicketUpdateComponent;
    let fixture: ComponentFixture<DayOffTicketUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dayOffTicketService: DayOffTicketService;
    let empMgtService: EmpMgtService;
    let dayOffTypeService: DayOffTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DayOffTicketUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DayOffTicketUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DayOffTicketUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dayOffTicketService = TestBed.inject(DayOffTicketService);
      empMgtService = TestBed.inject(EmpMgtService);
      dayOffTypeService = TestBed.inject(DayOffTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call EmpMgt query and add missing value', () => {
        const dayOffTicket: IDayOffTicket = { id: 456 };
        const approver: IEmpMgt = { id: 99530 };
        dayOffTicket.approver = approver;
        const approverByCeo: IEmpMgt = { id: 32091 };
        dayOffTicket.approverByCeo = approverByCeo;
        const createdBy: IEmpMgt = { id: 59617 };
        dayOffTicket.createdBy = createdBy;

        const empMgtCollection: IEmpMgt[] = [{ id: 54743 }];
        jest.spyOn(empMgtService, 'query').mockReturnValue(of(new HttpResponse({ body: empMgtCollection })));
        const additionalEmpMgts = [approver, approverByCeo, createdBy];
        const expectedCollection: IEmpMgt[] = [...additionalEmpMgts, ...empMgtCollection];
        jest.spyOn(empMgtService, 'addEmpMgtToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ dayOffTicket });
        comp.ngOnInit();

        expect(empMgtService.query).toHaveBeenCalled();
        expect(empMgtService.addEmpMgtToCollectionIfMissing).toHaveBeenCalledWith(empMgtCollection, ...additionalEmpMgts);
        expect(comp.empMgtsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call DayOffType query and add missing value', () => {
        const dayOffTicket: IDayOffTicket = { id: 456 };
        const dayOffType: IDayOffType = { id: 16231 };
        dayOffTicket.dayOffType = dayOffType;

        const dayOffTypeCollection: IDayOffType[] = [{ id: 82718 }];
        jest.spyOn(dayOffTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: dayOffTypeCollection })));
        const additionalDayOffTypes = [dayOffType];
        const expectedCollection: IDayOffType[] = [...additionalDayOffTypes, ...dayOffTypeCollection];
        jest.spyOn(dayOffTypeService, 'addDayOffTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ dayOffTicket });
        comp.ngOnInit();

        expect(dayOffTypeService.query).toHaveBeenCalled();
        expect(dayOffTypeService.addDayOffTypeToCollectionIfMissing).toHaveBeenCalledWith(dayOffTypeCollection, ...additionalDayOffTypes);
        expect(comp.dayOffTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const dayOffTicket: IDayOffTicket = { id: 456 };
        const approver: IEmpMgt = { id: 5619 };
        dayOffTicket.approver = approver;
        const approverByCeo: IEmpMgt = { id: 96053 };
        dayOffTicket.approverByCeo = approverByCeo;
        const createdBy: IEmpMgt = { id: 51945 };
        dayOffTicket.createdBy = createdBy;
        const dayOffType: IDayOffType = { id: 62548 };
        dayOffTicket.dayOffType = dayOffType;

        activatedRoute.data = of({ dayOffTicket });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dayOffTicket));
        expect(comp.empMgtsSharedCollection).toContain(approver);
        expect(comp.empMgtsSharedCollection).toContain(approverByCeo);
        expect(comp.empMgtsSharedCollection).toContain(createdBy);
        expect(comp.dayOffTypesSharedCollection).toContain(dayOffType);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffTicket>>();
        const dayOffTicket = { id: 123 };
        jest.spyOn(dayOffTicketService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffTicket });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dayOffTicket }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dayOffTicketService.update).toHaveBeenCalledWith(dayOffTicket);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffTicket>>();
        const dayOffTicket = new DayOffTicket();
        jest.spyOn(dayOffTicketService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffTicket });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dayOffTicket }));
        saveSubject.complete();

        // THEN
        expect(dayOffTicketService.create).toHaveBeenCalledWith(dayOffTicket);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffTicket>>();
        const dayOffTicket = { id: 123 };
        jest.spyOn(dayOffTicketService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffTicket });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dayOffTicketService.update).toHaveBeenCalledWith(dayOffTicket);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackEmpMgtById', () => {
        it('Should return tracked EmpMgt primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackEmpMgtById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackDayOffTypeById', () => {
        it('Should return tracked DayOffType primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackDayOffTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
