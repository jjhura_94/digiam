import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IDayOffTicket, DayOffTicket } from '../day-off-ticket.model';
import { DayOffTicketService } from '../service/day-off-ticket.service';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';
import { IDayOffType } from 'app/entities/day-off-type/day-off-type.model';
import { DayOffTypeService } from 'app/entities/day-off-type/service/day-off-type.service';

@Component({
  selector: 'jhi-day-off-ticket-update',
  templateUrl: './day-off-ticket-update.component.html',
})
export class DayOffTicketUpdateComponent implements OnInit {
  isSaving = false;

  empMgtsSharedCollection: IEmpMgt[] = [];
  dayOffTypesSharedCollection: IDayOffType[] = [];

  editForm = this.fb.group({
    id: [],
    numberOfDayLeft: [],
    reason: [],
    watcher: [],
    status: [],
    fromDate: [],
    toDate: [],
    note: [],
    createdDate: [],
    updatedBy: [],
    updatedDate: [],
    approver: [],
    approverByCeo: [],
    createdBy: [],
    dayOffType: [],
  });

  constructor(
    protected dayOffTicketService: DayOffTicketService,
    protected empMgtService: EmpMgtService,
    protected dayOffTypeService: DayOffTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dayOffTicket }) => {
      if (dayOffTicket.id === undefined) {
        const today = dayjs().startOf('day');
        dayOffTicket.fromDate = today;
        dayOffTicket.toDate = today;
        dayOffTicket.createdDate = today;
        dayOffTicket.updatedDate = today;
      }

      this.updateForm(dayOffTicket);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dayOffTicket = this.createFromForm();
    if (dayOffTicket.id !== undefined) {
      this.subscribeToSaveResponse(this.dayOffTicketService.update(dayOffTicket));
    } else {
      this.subscribeToSaveResponse(this.dayOffTicketService.create(dayOffTicket));
    }
  }

  trackEmpMgtById(index: number, item: IEmpMgt): number {
    return item.id!;
  }

  trackDayOffTypeById(index: number, item: IDayOffType): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDayOffTicket>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dayOffTicket: IDayOffTicket): void {
    this.editForm.patchValue({
      id: dayOffTicket.id,
      numberOfDayLeft: dayOffTicket.numberOfDayLeft,
      reason: dayOffTicket.reason,
      watcher: dayOffTicket.watcher,
      status: dayOffTicket.status,
      fromDate: dayOffTicket.fromDate ? dayOffTicket.fromDate.format(DATE_TIME_FORMAT) : null,
      toDate: dayOffTicket.toDate ? dayOffTicket.toDate.format(DATE_TIME_FORMAT) : null,
      note: dayOffTicket.note,
      createdDate: dayOffTicket.createdDate ? dayOffTicket.createdDate.format(DATE_TIME_FORMAT) : null,
      updatedBy: dayOffTicket.updatedBy,
      updatedDate: dayOffTicket.updatedDate ? dayOffTicket.updatedDate.format(DATE_TIME_FORMAT) : null,
      approver: dayOffTicket.approver,
      approverByCeo: dayOffTicket.approverByCeo,
      createdBy: dayOffTicket.createdBy,
      dayOffType: dayOffTicket.dayOffType,
    });

    this.empMgtsSharedCollection = this.empMgtService.addEmpMgtToCollectionIfMissing(
      this.empMgtsSharedCollection,
      dayOffTicket.approver,
      dayOffTicket.approverByCeo,
      dayOffTicket.createdBy
    );
    this.dayOffTypesSharedCollection = this.dayOffTypeService.addDayOffTypeToCollectionIfMissing(
      this.dayOffTypesSharedCollection,
      dayOffTicket.dayOffType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.empMgtService
      .query()
      .pipe(map((res: HttpResponse<IEmpMgt[]>) => res.body ?? []))
      .pipe(
        map((empMgts: IEmpMgt[]) =>
          this.empMgtService.addEmpMgtToCollectionIfMissing(
            empMgts,
            this.editForm.get('approver')!.value,
            this.editForm.get('approverByCeo')!.value,
            this.editForm.get('createdBy')!.value
          )
        )
      )
      .subscribe((empMgts: IEmpMgt[]) => (this.empMgtsSharedCollection = empMgts));

    this.dayOffTypeService
      .query()
      .pipe(map((res: HttpResponse<IDayOffType[]>) => res.body ?? []))
      .pipe(
        map((dayOffTypes: IDayOffType[]) =>
          this.dayOffTypeService.addDayOffTypeToCollectionIfMissing(dayOffTypes, this.editForm.get('dayOffType')!.value)
        )
      )
      .subscribe((dayOffTypes: IDayOffType[]) => (this.dayOffTypesSharedCollection = dayOffTypes));
  }

  protected createFromForm(): IDayOffTicket {
    return {
      ...new DayOffTicket(),
      id: this.editForm.get(['id'])!.value,
      numberOfDayLeft: this.editForm.get(['numberOfDayLeft'])!.value,
      reason: this.editForm.get(['reason'])!.value,
      watcher: this.editForm.get(['watcher'])!.value,
      status: this.editForm.get(['status'])!.value,
      fromDate: this.editForm.get(['fromDate'])!.value ? dayjs(this.editForm.get(['fromDate'])!.value, DATE_TIME_FORMAT) : undefined,
      toDate: this.editForm.get(['toDate'])!.value ? dayjs(this.editForm.get(['toDate'])!.value, DATE_TIME_FORMAT) : undefined,
      note: this.editForm.get(['note'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      updatedDate: this.editForm.get(['updatedDate'])!.value
        ? dayjs(this.editForm.get(['updatedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      approver: this.editForm.get(['approver'])!.value,
      approverByCeo: this.editForm.get(['approverByCeo'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      dayOffType: this.editForm.get(['dayOffType'])!.value,
    };
  }
}
