import { IDayOffUser } from 'app/entities/day-off-user/day-off-user.model';
import { IDayOffTicket } from 'app/entities/day-off-ticket/day-off-ticket.model';

export interface IDayOffType {
  id?: number;
  title?: string | null;
  numberOfDay?: number | null;
  noSalary?: boolean | null;
  dayOffUsers?: IDayOffUser[] | null;
  dayOffTickets?: IDayOffTicket[] | null;
}

export class DayOffType implements IDayOffType {
  constructor(
    public id?: number,
    public title?: string | null,
    public numberOfDay?: number | null,
    public noSalary?: boolean | null,
    public dayOffUsers?: IDayOffUser[] | null,
    public dayOffTickets?: IDayOffTicket[] | null
  ) {
    this.noSalary = this.noSalary ?? false;
  }
}

export function getDayOffTypeIdentifier(dayOffType: IDayOffType): number | undefined {
  return dayOffType.id;
}
