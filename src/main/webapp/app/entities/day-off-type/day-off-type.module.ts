import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DayOffTypeComponent } from './list/day-off-type.component';
import { DayOffTypeDetailComponent } from './detail/day-off-type-detail.component';
import { DayOffTypeUpdateComponent } from './update/day-off-type-update.component';
import { DayOffTypeDeleteDialogComponent } from './delete/day-off-type-delete-dialog.component';
import { DayOffTypeRoutingModule } from './route/day-off-type-routing.module';

@NgModule({
  imports: [SharedModule, DayOffTypeRoutingModule],
  declarations: [DayOffTypeComponent, DayOffTypeDetailComponent, DayOffTypeUpdateComponent, DayOffTypeDeleteDialogComponent],
  entryComponents: [DayOffTypeDeleteDialogComponent],
})
export class DayOffTypeModule {}
