import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDayOffType } from '../day-off-type.model';
import { DayOffTypeService } from '../service/day-off-type.service';

@Component({
  templateUrl: './day-off-type-delete-dialog.component.html',
})
export class DayOffTypeDeleteDialogComponent {
  dayOffType?: IDayOffType;

  constructor(protected dayOffTypeService: DayOffTypeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dayOffTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
