import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DayOffTypeDetailComponent } from './day-off-type-detail.component';

describe('Component Tests', () => {
  describe('DayOffType Management Detail Component', () => {
    let comp: DayOffTypeDetailComponent;
    let fixture: ComponentFixture<DayOffTypeDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DayOffTypeDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ dayOffType: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(DayOffTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DayOffTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dayOffType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dayOffType).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
