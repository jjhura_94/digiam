import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDayOffType } from '../day-off-type.model';

@Component({
  selector: 'jhi-day-off-type-detail',
  templateUrl: './day-off-type-detail.component.html',
})
export class DayOffTypeDetailComponent implements OnInit {
  dayOffType: IDayOffType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dayOffType }) => {
      this.dayOffType = dayOffType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
