import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DayOffTypeService } from '../service/day-off-type.service';

import { DayOffTypeComponent } from './day-off-type.component';

describe('Component Tests', () => {
  describe('DayOffType Management Component', () => {
    let comp: DayOffTypeComponent;
    let fixture: ComponentFixture<DayOffTypeComponent>;
    let service: DayOffTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DayOffTypeComponent],
      })
        .overrideTemplate(DayOffTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DayOffTypeComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DayOffTypeService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.dayOffTypes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
