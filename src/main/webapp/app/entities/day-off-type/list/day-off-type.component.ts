import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDayOffType } from '../day-off-type.model';
import { DayOffTypeService } from '../service/day-off-type.service';
import { DayOffTypeDeleteDialogComponent } from '../delete/day-off-type-delete-dialog.component';

@Component({
  selector: 'jhi-day-off-type',
  templateUrl: './day-off-type.component.html',
})
export class DayOffTypeComponent implements OnInit {
  dayOffTypes?: IDayOffType[];
  isLoading = false;

  constructor(protected dayOffTypeService: DayOffTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.dayOffTypeService.query().subscribe(
      (res: HttpResponse<IDayOffType[]>) => {
        this.isLoading = false;
        this.dayOffTypes = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDayOffType): number {
    return item.id!;
  }

  delete(dayOffType: IDayOffType): void {
    const modalRef = this.modalService.open(DayOffTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.dayOffType = dayOffType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
