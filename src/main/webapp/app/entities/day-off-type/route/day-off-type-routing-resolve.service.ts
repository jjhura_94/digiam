import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDayOffType, DayOffType } from '../day-off-type.model';
import { DayOffTypeService } from '../service/day-off-type.service';

@Injectable({ providedIn: 'root' })
export class DayOffTypeRoutingResolveService implements Resolve<IDayOffType> {
  constructor(protected service: DayOffTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDayOffType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dayOffType: HttpResponse<DayOffType>) => {
          if (dayOffType.body) {
            return of(dayOffType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DayOffType());
  }
}
