import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DayOffTypeComponent } from '../list/day-off-type.component';
import { DayOffTypeDetailComponent } from '../detail/day-off-type-detail.component';
import { DayOffTypeUpdateComponent } from '../update/day-off-type-update.component';
import { DayOffTypeRoutingResolveService } from './day-off-type-routing-resolve.service';

const dayOffTypeRoute: Routes = [
  {
    path: '',
    component: DayOffTypeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DayOffTypeDetailComponent,
    resolve: {
      dayOffType: DayOffTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DayOffTypeUpdateComponent,
    resolve: {
      dayOffType: DayOffTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DayOffTypeUpdateComponent,
    resolve: {
      dayOffType: DayOffTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dayOffTypeRoute)],
  exports: [RouterModule],
})
export class DayOffTypeRoutingModule {}
