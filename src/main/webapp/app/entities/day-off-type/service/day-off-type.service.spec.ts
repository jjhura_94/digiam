import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDayOffType, DayOffType } from '../day-off-type.model';

import { DayOffTypeService } from './day-off-type.service';

describe('Service Tests', () => {
  describe('DayOffType Service', () => {
    let service: DayOffTypeService;
    let httpMock: HttpTestingController;
    let elemDefault: IDayOffType;
    let expectedResult: IDayOffType | IDayOffType[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DayOffTypeService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        title: 'AAAAAAA',
        numberOfDay: 0,
        noSalary: false,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DayOffType', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new DayOffType()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DayOffType', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            title: 'BBBBBB',
            numberOfDay: 1,
            noSalary: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DayOffType', () => {
        const patchObject = Object.assign(
          {
            numberOfDay: 1,
            noSalary: true,
          },
          new DayOffType()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DayOffType', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            title: 'BBBBBB',
            numberOfDay: 1,
            noSalary: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DayOffType', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDayOffTypeToCollectionIfMissing', () => {
        it('should add a DayOffType to an empty array', () => {
          const dayOffType: IDayOffType = { id: 123 };
          expectedResult = service.addDayOffTypeToCollectionIfMissing([], dayOffType);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dayOffType);
        });

        it('should not add a DayOffType to an array that contains it', () => {
          const dayOffType: IDayOffType = { id: 123 };
          const dayOffTypeCollection: IDayOffType[] = [
            {
              ...dayOffType,
            },
            { id: 456 },
          ];
          expectedResult = service.addDayOffTypeToCollectionIfMissing(dayOffTypeCollection, dayOffType);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DayOffType to an array that doesn't contain it", () => {
          const dayOffType: IDayOffType = { id: 123 };
          const dayOffTypeCollection: IDayOffType[] = [{ id: 456 }];
          expectedResult = service.addDayOffTypeToCollectionIfMissing(dayOffTypeCollection, dayOffType);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dayOffType);
        });

        it('should add only unique DayOffType to an array', () => {
          const dayOffTypeArray: IDayOffType[] = [{ id: 123 }, { id: 456 }, { id: 60810 }];
          const dayOffTypeCollection: IDayOffType[] = [{ id: 123 }];
          expectedResult = service.addDayOffTypeToCollectionIfMissing(dayOffTypeCollection, ...dayOffTypeArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dayOffType: IDayOffType = { id: 123 };
          const dayOffType2: IDayOffType = { id: 456 };
          expectedResult = service.addDayOffTypeToCollectionIfMissing([], dayOffType, dayOffType2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dayOffType);
          expect(expectedResult).toContain(dayOffType2);
        });

        it('should accept null and undefined values', () => {
          const dayOffType: IDayOffType = { id: 123 };
          expectedResult = service.addDayOffTypeToCollectionIfMissing([], null, dayOffType, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dayOffType);
        });

        it('should return initial array if no DayOffType is added', () => {
          const dayOffTypeCollection: IDayOffType[] = [{ id: 123 }];
          expectedResult = service.addDayOffTypeToCollectionIfMissing(dayOffTypeCollection, undefined, null);
          expect(expectedResult).toEqual(dayOffTypeCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
