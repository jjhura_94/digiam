import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDayOffType, getDayOffTypeIdentifier } from '../day-off-type.model';

export type EntityResponseType = HttpResponse<IDayOffType>;
export type EntityArrayResponseType = HttpResponse<IDayOffType[]>;

@Injectable({ providedIn: 'root' })
export class DayOffTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/day-off-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(dayOffType: IDayOffType): Observable<EntityResponseType> {
    return this.http.post<IDayOffType>(this.resourceUrl, dayOffType, { observe: 'response' });
  }

  update(dayOffType: IDayOffType): Observable<EntityResponseType> {
    return this.http.put<IDayOffType>(`${this.resourceUrl}/${getDayOffTypeIdentifier(dayOffType) as number}`, dayOffType, {
      observe: 'response',
    });
  }

  partialUpdate(dayOffType: IDayOffType): Observable<EntityResponseType> {
    return this.http.patch<IDayOffType>(`${this.resourceUrl}/${getDayOffTypeIdentifier(dayOffType) as number}`, dayOffType, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDayOffType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDayOffType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDayOffTypeToCollectionIfMissing(
    dayOffTypeCollection: IDayOffType[],
    ...dayOffTypesToCheck: (IDayOffType | null | undefined)[]
  ): IDayOffType[] {
    const dayOffTypes: IDayOffType[] = dayOffTypesToCheck.filter(isPresent);
    if (dayOffTypes.length > 0) {
      const dayOffTypeCollectionIdentifiers = dayOffTypeCollection.map(dayOffTypeItem => getDayOffTypeIdentifier(dayOffTypeItem)!);
      const dayOffTypesToAdd = dayOffTypes.filter(dayOffTypeItem => {
        const dayOffTypeIdentifier = getDayOffTypeIdentifier(dayOffTypeItem);
        if (dayOffTypeIdentifier == null || dayOffTypeCollectionIdentifiers.includes(dayOffTypeIdentifier)) {
          return false;
        }
        dayOffTypeCollectionIdentifiers.push(dayOffTypeIdentifier);
        return true;
      });
      return [...dayOffTypesToAdd, ...dayOffTypeCollection];
    }
    return dayOffTypeCollection;
  }
}
