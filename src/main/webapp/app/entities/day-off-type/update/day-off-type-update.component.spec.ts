jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DayOffTypeService } from '../service/day-off-type.service';
import { IDayOffType, DayOffType } from '../day-off-type.model';

import { DayOffTypeUpdateComponent } from './day-off-type-update.component';

describe('Component Tests', () => {
  describe('DayOffType Management Update Component', () => {
    let comp: DayOffTypeUpdateComponent;
    let fixture: ComponentFixture<DayOffTypeUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dayOffTypeService: DayOffTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DayOffTypeUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DayOffTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DayOffTypeUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dayOffTypeService = TestBed.inject(DayOffTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const dayOffType: IDayOffType = { id: 456 };

        activatedRoute.data = of({ dayOffType });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dayOffType));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffType>>();
        const dayOffType = { id: 123 };
        jest.spyOn(dayOffTypeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dayOffType }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dayOffTypeService.update).toHaveBeenCalledWith(dayOffType);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffType>>();
        const dayOffType = new DayOffType();
        jest.spyOn(dayOffTypeService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dayOffType }));
        saveSubject.complete();

        // THEN
        expect(dayOffTypeService.create).toHaveBeenCalledWith(dayOffType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffType>>();
        const dayOffType = { id: 123 };
        jest.spyOn(dayOffTypeService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dayOffTypeService.update).toHaveBeenCalledWith(dayOffType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
