import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IDayOffType, DayOffType } from '../day-off-type.model';
import { DayOffTypeService } from '../service/day-off-type.service';

@Component({
  selector: 'jhi-day-off-type-update',
  templateUrl: './day-off-type-update.component.html',
})
export class DayOffTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    title: [],
    numberOfDay: [],
    noSalary: [],
  });

  constructor(protected dayOffTypeService: DayOffTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dayOffType }) => {
      this.updateForm(dayOffType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dayOffType = this.createFromForm();
    if (dayOffType.id !== undefined) {
      this.subscribeToSaveResponse(this.dayOffTypeService.update(dayOffType));
    } else {
      this.subscribeToSaveResponse(this.dayOffTypeService.create(dayOffType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDayOffType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dayOffType: IDayOffType): void {
    this.editForm.patchValue({
      id: dayOffType.id,
      title: dayOffType.title,
      numberOfDay: dayOffType.numberOfDay,
      noSalary: dayOffType.noSalary,
    });
  }

  protected createFromForm(): IDayOffType {
    return {
      ...new DayOffType(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      numberOfDay: this.editForm.get(['numberOfDay'])!.value,
      noSalary: this.editForm.get(['noSalary'])!.value,
    };
  }
}
