import { IDayOffType } from 'app/entities/day-off-type/day-off-type.model';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';

export interface IDayOffUser {
  id?: number;
  dayLeft?: number | null;
  year?: number | null;
  dayOffType?: IDayOffType | null;
  emp?: IEmpMgt | null;
}

export class DayOffUser implements IDayOffUser {
  constructor(
    public id?: number,
    public dayLeft?: number | null,
    public year?: number | null,
    public dayOffType?: IDayOffType | null,
    public emp?: IEmpMgt | null
  ) {}
}

export function getDayOffUserIdentifier(dayOffUser: IDayOffUser): number | undefined {
  return dayOffUser.id;
}
