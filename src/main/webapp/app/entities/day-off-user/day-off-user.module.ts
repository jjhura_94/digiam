import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DayOffUserComponent } from './list/day-off-user.component';
import { DayOffUserDetailComponent } from './detail/day-off-user-detail.component';
import { DayOffUserUpdateComponent } from './update/day-off-user-update.component';
import { DayOffUserDeleteDialogComponent } from './delete/day-off-user-delete-dialog.component';
import { DayOffUserRoutingModule } from './route/day-off-user-routing.module';

@NgModule({
  imports: [SharedModule, DayOffUserRoutingModule],
  declarations: [DayOffUserComponent, DayOffUserDetailComponent, DayOffUserUpdateComponent, DayOffUserDeleteDialogComponent],
  entryComponents: [DayOffUserDeleteDialogComponent],
})
export class DayOffUserModule {}
