import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDayOffUser } from '../day-off-user.model';
import { DayOffUserService } from '../service/day-off-user.service';

@Component({
  templateUrl: './day-off-user-delete-dialog.component.html',
})
export class DayOffUserDeleteDialogComponent {
  dayOffUser?: IDayOffUser;

  constructor(protected dayOffUserService: DayOffUserService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dayOffUserService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
