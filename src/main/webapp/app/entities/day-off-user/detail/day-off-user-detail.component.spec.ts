import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DayOffUserDetailComponent } from './day-off-user-detail.component';

describe('Component Tests', () => {
  describe('DayOffUser Management Detail Component', () => {
    let comp: DayOffUserDetailComponent;
    let fixture: ComponentFixture<DayOffUserDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DayOffUserDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ dayOffUser: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(DayOffUserDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DayOffUserDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dayOffUser on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dayOffUser).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
