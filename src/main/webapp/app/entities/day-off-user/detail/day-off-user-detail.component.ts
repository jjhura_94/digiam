import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDayOffUser } from '../day-off-user.model';

@Component({
  selector: 'jhi-day-off-user-detail',
  templateUrl: './day-off-user-detail.component.html',
})
export class DayOffUserDetailComponent implements OnInit {
  dayOffUser: IDayOffUser | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dayOffUser }) => {
      this.dayOffUser = dayOffUser;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
