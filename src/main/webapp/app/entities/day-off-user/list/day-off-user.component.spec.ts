import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DayOffUserService } from '../service/day-off-user.service';

import { DayOffUserComponent } from './day-off-user.component';

describe('Component Tests', () => {
  describe('DayOffUser Management Component', () => {
    let comp: DayOffUserComponent;
    let fixture: ComponentFixture<DayOffUserComponent>;
    let service: DayOffUserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DayOffUserComponent],
      })
        .overrideTemplate(DayOffUserComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DayOffUserComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DayOffUserService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.dayOffUsers?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
