import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDayOffUser } from '../day-off-user.model';
import { DayOffUserService } from '../service/day-off-user.service';
import { DayOffUserDeleteDialogComponent } from '../delete/day-off-user-delete-dialog.component';

@Component({
  selector: 'jhi-day-off-user',
  templateUrl: './day-off-user.component.html',
})
export class DayOffUserComponent implements OnInit {
  dayOffUsers?: IDayOffUser[];
  isLoading = false;

  constructor(protected dayOffUserService: DayOffUserService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.dayOffUserService.query().subscribe(
      (res: HttpResponse<IDayOffUser[]>) => {
        this.isLoading = false;
        this.dayOffUsers = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDayOffUser): number {
    return item.id!;
  }

  delete(dayOffUser: IDayOffUser): void {
    const modalRef = this.modalService.open(DayOffUserDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.dayOffUser = dayOffUser;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
