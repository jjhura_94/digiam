jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IDayOffUser, DayOffUser } from '../day-off-user.model';
import { DayOffUserService } from '../service/day-off-user.service';

import { DayOffUserRoutingResolveService } from './day-off-user-routing-resolve.service';

describe('Service Tests', () => {
  describe('DayOffUser routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: DayOffUserRoutingResolveService;
    let service: DayOffUserService;
    let resultDayOffUser: IDayOffUser | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(DayOffUserRoutingResolveService);
      service = TestBed.inject(DayOffUserService);
      resultDayOffUser = undefined;
    });

    describe('resolve', () => {
      it('should return IDayOffUser returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDayOffUser = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDayOffUser).toEqual({ id: 123 });
      });

      it('should return new IDayOffUser if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDayOffUser = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultDayOffUser).toEqual(new DayOffUser());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as DayOffUser })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDayOffUser = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDayOffUser).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
