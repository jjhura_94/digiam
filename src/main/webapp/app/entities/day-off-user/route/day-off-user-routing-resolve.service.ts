import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDayOffUser, DayOffUser } from '../day-off-user.model';
import { DayOffUserService } from '../service/day-off-user.service';

@Injectable({ providedIn: 'root' })
export class DayOffUserRoutingResolveService implements Resolve<IDayOffUser> {
  constructor(protected service: DayOffUserService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDayOffUser> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((dayOffUser: HttpResponse<DayOffUser>) => {
          if (dayOffUser.body) {
            return of(dayOffUser.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DayOffUser());
  }
}
