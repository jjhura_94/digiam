import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DayOffUserComponent } from '../list/day-off-user.component';
import { DayOffUserDetailComponent } from '../detail/day-off-user-detail.component';
import { DayOffUserUpdateComponent } from '../update/day-off-user-update.component';
import { DayOffUserRoutingResolveService } from './day-off-user-routing-resolve.service';

const dayOffUserRoute: Routes = [
  {
    path: '',
    component: DayOffUserComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DayOffUserDetailComponent,
    resolve: {
      dayOffUser: DayOffUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DayOffUserUpdateComponent,
    resolve: {
      dayOffUser: DayOffUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DayOffUserUpdateComponent,
    resolve: {
      dayOffUser: DayOffUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dayOffUserRoute)],
  exports: [RouterModule],
})
export class DayOffUserRoutingModule {}
