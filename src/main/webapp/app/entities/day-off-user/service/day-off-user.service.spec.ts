import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDayOffUser, DayOffUser } from '../day-off-user.model';

import { DayOffUserService } from './day-off-user.service';

describe('Service Tests', () => {
  describe('DayOffUser Service', () => {
    let service: DayOffUserService;
    let httpMock: HttpTestingController;
    let elemDefault: IDayOffUser;
    let expectedResult: IDayOffUser | IDayOffUser[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DayOffUserService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        dayLeft: 0,
        year: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DayOffUser', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new DayOffUser()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DayOffUser', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            dayLeft: 1,
            year: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DayOffUser', () => {
        const patchObject = Object.assign({}, new DayOffUser());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DayOffUser', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            dayLeft: 1,
            year: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DayOffUser', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDayOffUserToCollectionIfMissing', () => {
        it('should add a DayOffUser to an empty array', () => {
          const dayOffUser: IDayOffUser = { id: 123 };
          expectedResult = service.addDayOffUserToCollectionIfMissing([], dayOffUser);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dayOffUser);
        });

        it('should not add a DayOffUser to an array that contains it', () => {
          const dayOffUser: IDayOffUser = { id: 123 };
          const dayOffUserCollection: IDayOffUser[] = [
            {
              ...dayOffUser,
            },
            { id: 456 },
          ];
          expectedResult = service.addDayOffUserToCollectionIfMissing(dayOffUserCollection, dayOffUser);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DayOffUser to an array that doesn't contain it", () => {
          const dayOffUser: IDayOffUser = { id: 123 };
          const dayOffUserCollection: IDayOffUser[] = [{ id: 456 }];
          expectedResult = service.addDayOffUserToCollectionIfMissing(dayOffUserCollection, dayOffUser);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dayOffUser);
        });

        it('should add only unique DayOffUser to an array', () => {
          const dayOffUserArray: IDayOffUser[] = [{ id: 123 }, { id: 456 }, { id: 80031 }];
          const dayOffUserCollection: IDayOffUser[] = [{ id: 123 }];
          expectedResult = service.addDayOffUserToCollectionIfMissing(dayOffUserCollection, ...dayOffUserArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dayOffUser: IDayOffUser = { id: 123 };
          const dayOffUser2: IDayOffUser = { id: 456 };
          expectedResult = service.addDayOffUserToCollectionIfMissing([], dayOffUser, dayOffUser2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dayOffUser);
          expect(expectedResult).toContain(dayOffUser2);
        });

        it('should accept null and undefined values', () => {
          const dayOffUser: IDayOffUser = { id: 123 };
          expectedResult = service.addDayOffUserToCollectionIfMissing([], null, dayOffUser, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dayOffUser);
        });

        it('should return initial array if no DayOffUser is added', () => {
          const dayOffUserCollection: IDayOffUser[] = [{ id: 123 }];
          expectedResult = service.addDayOffUserToCollectionIfMissing(dayOffUserCollection, undefined, null);
          expect(expectedResult).toEqual(dayOffUserCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
