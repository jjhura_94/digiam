import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDayOffUser, getDayOffUserIdentifier } from '../day-off-user.model';

export type EntityResponseType = HttpResponse<IDayOffUser>;
export type EntityArrayResponseType = HttpResponse<IDayOffUser[]>;

@Injectable({ providedIn: 'root' })
export class DayOffUserService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/day-off-users');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(dayOffUser: IDayOffUser): Observable<EntityResponseType> {
    return this.http.post<IDayOffUser>(this.resourceUrl, dayOffUser, { observe: 'response' });
  }

  update(dayOffUser: IDayOffUser): Observable<EntityResponseType> {
    return this.http.put<IDayOffUser>(`${this.resourceUrl}/${getDayOffUserIdentifier(dayOffUser) as number}`, dayOffUser, {
      observe: 'response',
    });
  }

  partialUpdate(dayOffUser: IDayOffUser): Observable<EntityResponseType> {
    return this.http.patch<IDayOffUser>(`${this.resourceUrl}/${getDayOffUserIdentifier(dayOffUser) as number}`, dayOffUser, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDayOffUser>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDayOffUser[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDayOffUserToCollectionIfMissing(
    dayOffUserCollection: IDayOffUser[],
    ...dayOffUsersToCheck: (IDayOffUser | null | undefined)[]
  ): IDayOffUser[] {
    const dayOffUsers: IDayOffUser[] = dayOffUsersToCheck.filter(isPresent);
    if (dayOffUsers.length > 0) {
      const dayOffUserCollectionIdentifiers = dayOffUserCollection.map(dayOffUserItem => getDayOffUserIdentifier(dayOffUserItem)!);
      const dayOffUsersToAdd = dayOffUsers.filter(dayOffUserItem => {
        const dayOffUserIdentifier = getDayOffUserIdentifier(dayOffUserItem);
        if (dayOffUserIdentifier == null || dayOffUserCollectionIdentifiers.includes(dayOffUserIdentifier)) {
          return false;
        }
        dayOffUserCollectionIdentifiers.push(dayOffUserIdentifier);
        return true;
      });
      return [...dayOffUsersToAdd, ...dayOffUserCollection];
    }
    return dayOffUserCollection;
  }
}
