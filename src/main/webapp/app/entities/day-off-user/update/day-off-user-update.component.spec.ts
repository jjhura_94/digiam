jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DayOffUserService } from '../service/day-off-user.service';
import { IDayOffUser, DayOffUser } from '../day-off-user.model';
import { IDayOffType } from 'app/entities/day-off-type/day-off-type.model';
import { DayOffTypeService } from 'app/entities/day-off-type/service/day-off-type.service';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';

import { DayOffUserUpdateComponent } from './day-off-user-update.component';

describe('Component Tests', () => {
  describe('DayOffUser Management Update Component', () => {
    let comp: DayOffUserUpdateComponent;
    let fixture: ComponentFixture<DayOffUserUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dayOffUserService: DayOffUserService;
    let dayOffTypeService: DayOffTypeService;
    let empMgtService: EmpMgtService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DayOffUserUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DayOffUserUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DayOffUserUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dayOffUserService = TestBed.inject(DayOffUserService);
      dayOffTypeService = TestBed.inject(DayOffTypeService);
      empMgtService = TestBed.inject(EmpMgtService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call DayOffType query and add missing value', () => {
        const dayOffUser: IDayOffUser = { id: 456 };
        const dayOffType: IDayOffType = { id: 30233 };
        dayOffUser.dayOffType = dayOffType;

        const dayOffTypeCollection: IDayOffType[] = [{ id: 65577 }];
        jest.spyOn(dayOffTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: dayOffTypeCollection })));
        const additionalDayOffTypes = [dayOffType];
        const expectedCollection: IDayOffType[] = [...additionalDayOffTypes, ...dayOffTypeCollection];
        jest.spyOn(dayOffTypeService, 'addDayOffTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ dayOffUser });
        comp.ngOnInit();

        expect(dayOffTypeService.query).toHaveBeenCalled();
        expect(dayOffTypeService.addDayOffTypeToCollectionIfMissing).toHaveBeenCalledWith(dayOffTypeCollection, ...additionalDayOffTypes);
        expect(comp.dayOffTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should call EmpMgt query and add missing value', () => {
        const dayOffUser: IDayOffUser = { id: 456 };
        const emp: IEmpMgt = { id: 74687 };
        dayOffUser.emp = emp;

        const empMgtCollection: IEmpMgt[] = [{ id: 66990 }];
        jest.spyOn(empMgtService, 'query').mockReturnValue(of(new HttpResponse({ body: empMgtCollection })));
        const additionalEmpMgts = [emp];
        const expectedCollection: IEmpMgt[] = [...additionalEmpMgts, ...empMgtCollection];
        jest.spyOn(empMgtService, 'addEmpMgtToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ dayOffUser });
        comp.ngOnInit();

        expect(empMgtService.query).toHaveBeenCalled();
        expect(empMgtService.addEmpMgtToCollectionIfMissing).toHaveBeenCalledWith(empMgtCollection, ...additionalEmpMgts);
        expect(comp.empMgtsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const dayOffUser: IDayOffUser = { id: 456 };
        const dayOffType: IDayOffType = { id: 68198 };
        dayOffUser.dayOffType = dayOffType;
        const emp: IEmpMgt = { id: 69675 };
        dayOffUser.emp = emp;

        activatedRoute.data = of({ dayOffUser });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dayOffUser));
        expect(comp.dayOffTypesSharedCollection).toContain(dayOffType);
        expect(comp.empMgtsSharedCollection).toContain(emp);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffUser>>();
        const dayOffUser = { id: 123 };
        jest.spyOn(dayOffUserService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffUser });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dayOffUser }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dayOffUserService.update).toHaveBeenCalledWith(dayOffUser);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffUser>>();
        const dayOffUser = new DayOffUser();
        jest.spyOn(dayOffUserService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffUser });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dayOffUser }));
        saveSubject.complete();

        // THEN
        expect(dayOffUserService.create).toHaveBeenCalledWith(dayOffUser);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DayOffUser>>();
        const dayOffUser = { id: 123 };
        jest.spyOn(dayOffUserService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dayOffUser });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dayOffUserService.update).toHaveBeenCalledWith(dayOffUser);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackDayOffTypeById', () => {
        it('Should return tracked DayOffType primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackDayOffTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackEmpMgtById', () => {
        it('Should return tracked EmpMgt primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackEmpMgtById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
