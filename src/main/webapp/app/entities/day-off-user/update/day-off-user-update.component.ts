import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IDayOffUser, DayOffUser } from '../day-off-user.model';
import { DayOffUserService } from '../service/day-off-user.service';
import { IDayOffType } from 'app/entities/day-off-type/day-off-type.model';
import { DayOffTypeService } from 'app/entities/day-off-type/service/day-off-type.service';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';

@Component({
  selector: 'jhi-day-off-user-update',
  templateUrl: './day-off-user-update.component.html',
})
export class DayOffUserUpdateComponent implements OnInit {
  isSaving = false;

  dayOffTypesSharedCollection: IDayOffType[] = [];
  empMgtsSharedCollection: IEmpMgt[] = [];

  editForm = this.fb.group({
    id: [],
    dayLeft: [],
    year: [],
    dayOffType: [],
    emp: [],
  });

  constructor(
    protected dayOffUserService: DayOffUserService,
    protected dayOffTypeService: DayOffTypeService,
    protected empMgtService: EmpMgtService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dayOffUser }) => {
      this.updateForm(dayOffUser);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dayOffUser = this.createFromForm();
    if (dayOffUser.id !== undefined) {
      this.subscribeToSaveResponse(this.dayOffUserService.update(dayOffUser));
    } else {
      this.subscribeToSaveResponse(this.dayOffUserService.create(dayOffUser));
    }
  }

  trackDayOffTypeById(index: number, item: IDayOffType): number {
    return item.id!;
  }

  trackEmpMgtById(index: number, item: IEmpMgt): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDayOffUser>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dayOffUser: IDayOffUser): void {
    this.editForm.patchValue({
      id: dayOffUser.id,
      dayLeft: dayOffUser.dayLeft,
      year: dayOffUser.year,
      dayOffType: dayOffUser.dayOffType,
      emp: dayOffUser.emp,
    });

    this.dayOffTypesSharedCollection = this.dayOffTypeService.addDayOffTypeToCollectionIfMissing(
      this.dayOffTypesSharedCollection,
      dayOffUser.dayOffType
    );
    this.empMgtsSharedCollection = this.empMgtService.addEmpMgtToCollectionIfMissing(this.empMgtsSharedCollection, dayOffUser.emp);
  }

  protected loadRelationshipsOptions(): void {
    this.dayOffTypeService
      .query()
      .pipe(map((res: HttpResponse<IDayOffType[]>) => res.body ?? []))
      .pipe(
        map((dayOffTypes: IDayOffType[]) =>
          this.dayOffTypeService.addDayOffTypeToCollectionIfMissing(dayOffTypes, this.editForm.get('dayOffType')!.value)
        )
      )
      .subscribe((dayOffTypes: IDayOffType[]) => (this.dayOffTypesSharedCollection = dayOffTypes));

    this.empMgtService
      .query()
      .pipe(map((res: HttpResponse<IEmpMgt[]>) => res.body ?? []))
      .pipe(map((empMgts: IEmpMgt[]) => this.empMgtService.addEmpMgtToCollectionIfMissing(empMgts, this.editForm.get('emp')!.value)))
      .subscribe((empMgts: IEmpMgt[]) => (this.empMgtsSharedCollection = empMgts));
  }

  protected createFromForm(): IDayOffUser {
    return {
      ...new DayOffUser(),
      id: this.editForm.get(['id'])!.value,
      dayLeft: this.editForm.get(['dayLeft'])!.value,
      year: this.editForm.get(['year'])!.value,
      dayOffType: this.editForm.get(['dayOffType'])!.value,
      emp: this.editForm.get(['emp'])!.value,
    };
  }
}
