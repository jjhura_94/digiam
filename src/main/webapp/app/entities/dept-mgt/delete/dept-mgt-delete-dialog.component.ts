import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDeptMgt } from '../dept-mgt.model';
import { DeptMgtService } from '../service/dept-mgt.service';

@Component({
  templateUrl: './dept-mgt-delete-dialog.component.html',
})
export class DeptMgtDeleteDialogComponent {
  deptMgt?: IDeptMgt;

  constructor(protected deptMgtService: DeptMgtService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.deptMgtService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
