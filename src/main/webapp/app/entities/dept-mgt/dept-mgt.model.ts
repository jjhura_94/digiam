import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';

export interface IDeptMgt {
  id?: number;
  deptCode?: string | null;
  deptName?: string | null;
  status?: string | null;
  parentDeptCode?: string | null;
  empMgts?: IEmpMgt[] | null;
}

export class DeptMgt implements IDeptMgt {
  constructor(
    public id?: number,
    public deptCode?: string | null,
    public deptName?: string | null,
    public status?: string | null,
    public parentDeptCode?: string | null,
    public empMgts?: IEmpMgt[] | null
  ) {}
}

export function getDeptMgtIdentifier(deptMgt: IDeptMgt): number | undefined {
  return deptMgt.id;
}
