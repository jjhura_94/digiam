import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DeptMgtComponent } from './list/dept-mgt.component';
import { DeptMgtDetailComponent } from './detail/dept-mgt-detail.component';
import { DeptMgtUpdateComponent } from './update/dept-mgt-update.component';
import { DeptMgtDeleteDialogComponent } from './delete/dept-mgt-delete-dialog.component';
import { DeptMgtRoutingModule } from './route/dept-mgt-routing.module';

@NgModule({
  imports: [SharedModule, DeptMgtRoutingModule],
  declarations: [DeptMgtComponent, DeptMgtDetailComponent, DeptMgtUpdateComponent, DeptMgtDeleteDialogComponent],
  entryComponents: [DeptMgtDeleteDialogComponent],
})
export class DeptMgtModule {}
