import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DeptMgtDetailComponent } from './dept-mgt-detail.component';

describe('Component Tests', () => {
  describe('DeptMgt Management Detail Component', () => {
    let comp: DeptMgtDetailComponent;
    let fixture: ComponentFixture<DeptMgtDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DeptMgtDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ deptMgt: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(DeptMgtDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DeptMgtDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load deptMgt on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.deptMgt).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
