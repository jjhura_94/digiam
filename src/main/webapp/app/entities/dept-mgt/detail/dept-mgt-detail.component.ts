import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDeptMgt } from '../dept-mgt.model';

@Component({
  selector: 'jhi-dept-mgt-detail',
  templateUrl: './dept-mgt-detail.component.html',
})
export class DeptMgtDetailComponent implements OnInit {
  deptMgt: IDeptMgt | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ deptMgt }) => {
      this.deptMgt = deptMgt;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
