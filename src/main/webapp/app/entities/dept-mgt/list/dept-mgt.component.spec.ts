import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DeptMgtService } from '../service/dept-mgt.service';

import { DeptMgtComponent } from './dept-mgt.component';

describe('Component Tests', () => {
  describe('DeptMgt Management Component', () => {
    let comp: DeptMgtComponent;
    let fixture: ComponentFixture<DeptMgtComponent>;
    let service: DeptMgtService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DeptMgtComponent],
      })
        .overrideTemplate(DeptMgtComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeptMgtComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DeptMgtService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.deptMgts?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
