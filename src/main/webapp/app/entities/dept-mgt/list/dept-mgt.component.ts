import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDeptMgt } from '../dept-mgt.model';
import { DeptMgtService } from '../service/dept-mgt.service';
import { DeptMgtDeleteDialogComponent } from '../delete/dept-mgt-delete-dialog.component';

@Component({
  selector: 'jhi-dept-mgt',
  templateUrl: './dept-mgt.component.html',
})
export class DeptMgtComponent implements OnInit {
  deptMgts?: IDeptMgt[];
  isLoading = false;

  constructor(protected deptMgtService: DeptMgtService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.deptMgtService.query().subscribe(
      (res: HttpResponse<IDeptMgt[]>) => {
        this.isLoading = false;
        this.deptMgts = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDeptMgt): number {
    return item.id!;
  }

  delete(deptMgt: IDeptMgt): void {
    const modalRef = this.modalService.open(DeptMgtDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.deptMgt = deptMgt;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
