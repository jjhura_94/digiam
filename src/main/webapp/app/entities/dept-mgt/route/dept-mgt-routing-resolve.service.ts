import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDeptMgt, DeptMgt } from '../dept-mgt.model';
import { DeptMgtService } from '../service/dept-mgt.service';

@Injectable({ providedIn: 'root' })
export class DeptMgtRoutingResolveService implements Resolve<IDeptMgt> {
  constructor(protected service: DeptMgtService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDeptMgt> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((deptMgt: HttpResponse<DeptMgt>) => {
          if (deptMgt.body) {
            return of(deptMgt.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DeptMgt());
  }
}
