import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DeptMgtComponent } from '../list/dept-mgt.component';
import { DeptMgtDetailComponent } from '../detail/dept-mgt-detail.component';
import { DeptMgtUpdateComponent } from '../update/dept-mgt-update.component';
import { DeptMgtRoutingResolveService } from './dept-mgt-routing-resolve.service';

const deptMgtRoute: Routes = [
  {
    path: '',
    component: DeptMgtComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DeptMgtDetailComponent,
    resolve: {
      deptMgt: DeptMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DeptMgtUpdateComponent,
    resolve: {
      deptMgt: DeptMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DeptMgtUpdateComponent,
    resolve: {
      deptMgt: DeptMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(deptMgtRoute)],
  exports: [RouterModule],
})
export class DeptMgtRoutingModule {}
