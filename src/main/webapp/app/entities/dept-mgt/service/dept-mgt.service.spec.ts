import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDeptMgt, DeptMgt } from '../dept-mgt.model';

import { DeptMgtService } from './dept-mgt.service';

describe('Service Tests', () => {
  describe('DeptMgt Service', () => {
    let service: DeptMgtService;
    let httpMock: HttpTestingController;
    let elemDefault: IDeptMgt;
    let expectedResult: IDeptMgt | IDeptMgt[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DeptMgtService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        deptCode: 'AAAAAAA',
        deptName: 'AAAAAAA',
        status: 'AAAAAAA',
        parentDeptCode: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DeptMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new DeptMgt()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DeptMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            deptCode: 'BBBBBB',
            deptName: 'BBBBBB',
            status: 'BBBBBB',
            parentDeptCode: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DeptMgt', () => {
        const patchObject = Object.assign(
          {
            deptName: 'BBBBBB',
            parentDeptCode: 'BBBBBB',
          },
          new DeptMgt()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DeptMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            deptCode: 'BBBBBB',
            deptName: 'BBBBBB',
            status: 'BBBBBB',
            parentDeptCode: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DeptMgt', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDeptMgtToCollectionIfMissing', () => {
        it('should add a DeptMgt to an empty array', () => {
          const deptMgt: IDeptMgt = { id: 123 };
          expectedResult = service.addDeptMgtToCollectionIfMissing([], deptMgt);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(deptMgt);
        });

        it('should not add a DeptMgt to an array that contains it', () => {
          const deptMgt: IDeptMgt = { id: 123 };
          const deptMgtCollection: IDeptMgt[] = [
            {
              ...deptMgt,
            },
            { id: 456 },
          ];
          expectedResult = service.addDeptMgtToCollectionIfMissing(deptMgtCollection, deptMgt);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DeptMgt to an array that doesn't contain it", () => {
          const deptMgt: IDeptMgt = { id: 123 };
          const deptMgtCollection: IDeptMgt[] = [{ id: 456 }];
          expectedResult = service.addDeptMgtToCollectionIfMissing(deptMgtCollection, deptMgt);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(deptMgt);
        });

        it('should add only unique DeptMgt to an array', () => {
          const deptMgtArray: IDeptMgt[] = [{ id: 123 }, { id: 456 }, { id: 88767 }];
          const deptMgtCollection: IDeptMgt[] = [{ id: 123 }];
          expectedResult = service.addDeptMgtToCollectionIfMissing(deptMgtCollection, ...deptMgtArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const deptMgt: IDeptMgt = { id: 123 };
          const deptMgt2: IDeptMgt = { id: 456 };
          expectedResult = service.addDeptMgtToCollectionIfMissing([], deptMgt, deptMgt2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(deptMgt);
          expect(expectedResult).toContain(deptMgt2);
        });

        it('should accept null and undefined values', () => {
          const deptMgt: IDeptMgt = { id: 123 };
          expectedResult = service.addDeptMgtToCollectionIfMissing([], null, deptMgt, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(deptMgt);
        });

        it('should return initial array if no DeptMgt is added', () => {
          const deptMgtCollection: IDeptMgt[] = [{ id: 123 }];
          expectedResult = service.addDeptMgtToCollectionIfMissing(deptMgtCollection, undefined, null);
          expect(expectedResult).toEqual(deptMgtCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
