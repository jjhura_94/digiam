import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDeptMgt, getDeptMgtIdentifier } from '../dept-mgt.model';

export type EntityResponseType = HttpResponse<IDeptMgt>;
export type EntityArrayResponseType = HttpResponse<IDeptMgt[]>;

@Injectable({ providedIn: 'root' })
export class DeptMgtService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/dept-mgts');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(deptMgt: IDeptMgt): Observable<EntityResponseType> {
    return this.http.post<IDeptMgt>(this.resourceUrl, deptMgt, { observe: 'response' });
  }

  update(deptMgt: IDeptMgt): Observable<EntityResponseType> {
    return this.http.put<IDeptMgt>(`${this.resourceUrl}/${getDeptMgtIdentifier(deptMgt) as number}`, deptMgt, { observe: 'response' });
  }

  partialUpdate(deptMgt: IDeptMgt): Observable<EntityResponseType> {
    return this.http.patch<IDeptMgt>(`${this.resourceUrl}/${getDeptMgtIdentifier(deptMgt) as number}`, deptMgt, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDeptMgt>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDeptMgt[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDeptMgtToCollectionIfMissing(deptMgtCollection: IDeptMgt[], ...deptMgtsToCheck: (IDeptMgt | null | undefined)[]): IDeptMgt[] {
    const deptMgts: IDeptMgt[] = deptMgtsToCheck.filter(isPresent);
    if (deptMgts.length > 0) {
      const deptMgtCollectionIdentifiers = deptMgtCollection.map(deptMgtItem => getDeptMgtIdentifier(deptMgtItem)!);
      const deptMgtsToAdd = deptMgts.filter(deptMgtItem => {
        const deptMgtIdentifier = getDeptMgtIdentifier(deptMgtItem);
        if (deptMgtIdentifier == null || deptMgtCollectionIdentifiers.includes(deptMgtIdentifier)) {
          return false;
        }
        deptMgtCollectionIdentifiers.push(deptMgtIdentifier);
        return true;
      });
      return [...deptMgtsToAdd, ...deptMgtCollection];
    }
    return deptMgtCollection;
  }
}
