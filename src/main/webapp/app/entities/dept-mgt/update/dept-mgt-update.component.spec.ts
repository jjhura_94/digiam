jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DeptMgtService } from '../service/dept-mgt.service';
import { IDeptMgt, DeptMgt } from '../dept-mgt.model';

import { DeptMgtUpdateComponent } from './dept-mgt-update.component';

describe('Component Tests', () => {
  describe('DeptMgt Management Update Component', () => {
    let comp: DeptMgtUpdateComponent;
    let fixture: ComponentFixture<DeptMgtUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let deptMgtService: DeptMgtService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DeptMgtUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DeptMgtUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeptMgtUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      deptMgtService = TestBed.inject(DeptMgtService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const deptMgt: IDeptMgt = { id: 456 };

        activatedRoute.data = of({ deptMgt });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(deptMgt));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DeptMgt>>();
        const deptMgt = { id: 123 };
        jest.spyOn(deptMgtService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ deptMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: deptMgt }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(deptMgtService.update).toHaveBeenCalledWith(deptMgt);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DeptMgt>>();
        const deptMgt = new DeptMgt();
        jest.spyOn(deptMgtService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ deptMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: deptMgt }));
        saveSubject.complete();

        // THEN
        expect(deptMgtService.create).toHaveBeenCalledWith(deptMgt);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DeptMgt>>();
        const deptMgt = { id: 123 };
        jest.spyOn(deptMgtService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ deptMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(deptMgtService.update).toHaveBeenCalledWith(deptMgt);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
