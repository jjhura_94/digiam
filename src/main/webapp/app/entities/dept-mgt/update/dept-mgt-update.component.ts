import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IDeptMgt, DeptMgt } from '../dept-mgt.model';
import { DeptMgtService } from '../service/dept-mgt.service';

@Component({
  selector: 'jhi-dept-mgt-update',
  templateUrl: './dept-mgt-update.component.html',
})
export class DeptMgtUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    deptCode: [],
    deptName: [],
    status: [],
    parentDeptCode: [],
  });

  constructor(protected deptMgtService: DeptMgtService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ deptMgt }) => {
      this.updateForm(deptMgt);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const deptMgt = this.createFromForm();
    if (deptMgt.id !== undefined) {
      this.subscribeToSaveResponse(this.deptMgtService.update(deptMgt));
    } else {
      this.subscribeToSaveResponse(this.deptMgtService.create(deptMgt));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDeptMgt>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(deptMgt: IDeptMgt): void {
    this.editForm.patchValue({
      id: deptMgt.id,
      deptCode: deptMgt.deptCode,
      deptName: deptMgt.deptName,
      status: deptMgt.status,
      parentDeptCode: deptMgt.parentDeptCode,
    });
  }

  protected createFromForm(): IDeptMgt {
    return {
      ...new DeptMgt(),
      id: this.editForm.get(['id'])!.value,
      deptCode: this.editForm.get(['deptCode'])!.value,
      deptName: this.editForm.get(['deptName'])!.value,
      status: this.editForm.get(['status'])!.value,
      parentDeptCode: this.editForm.get(['parentDeptCode'])!.value,
    };
  }
}
