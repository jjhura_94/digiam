import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IDutyMgt } from '../duty-mgt.model';
import { DutyMgtService } from '../service/duty-mgt.service';

@Component({
  templateUrl: './duty-mgt-delete-dialog.component.html',
})
export class DutyMgtDeleteDialogComponent {
  dutyMgt?: IDutyMgt;

  constructor(protected dutyMgtService: DutyMgtService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.dutyMgtService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
