import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DutyMgtDetailComponent } from './duty-mgt-detail.component';

describe('Component Tests', () => {
  describe('DutyMgt Management Detail Component', () => {
    let comp: DutyMgtDetailComponent;
    let fixture: ComponentFixture<DutyMgtDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [DutyMgtDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ dutyMgt: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(DutyMgtDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DutyMgtDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load dutyMgt on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.dutyMgt).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
