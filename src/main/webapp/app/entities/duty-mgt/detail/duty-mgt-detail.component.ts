import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDutyMgt } from '../duty-mgt.model';

@Component({
  selector: 'jhi-duty-mgt-detail',
  templateUrl: './duty-mgt-detail.component.html',
})
export class DutyMgtDetailComponent implements OnInit {
  dutyMgt: IDutyMgt | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dutyMgt }) => {
      this.dutyMgt = dutyMgt;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
