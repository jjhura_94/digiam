import * as dayjs from 'dayjs';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';

export interface IDutyMgt {
  id?: number;
  dutyCode?: string | null;
  dutyName?: string | null;
  useYn?: string | null;
  dutyOrd?: string | null;
  createdBy?: string | null;
  createdDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  empMgts?: IEmpMgt[] | null;
}

export class DutyMgt implements IDutyMgt {
  constructor(
    public id?: number,
    public dutyCode?: string | null,
    public dutyName?: string | null,
    public useYn?: string | null,
    public dutyOrd?: string | null,
    public createdBy?: string | null,
    public createdDate?: dayjs.Dayjs | null,
    public updatedBy?: string | null,
    public updatedDate?: dayjs.Dayjs | null,
    public empMgts?: IEmpMgt[] | null
  ) {}
}

export function getDutyMgtIdentifier(dutyMgt: IDutyMgt): number | undefined {
  return dutyMgt.id;
}
