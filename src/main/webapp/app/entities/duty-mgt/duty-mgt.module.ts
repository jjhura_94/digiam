import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DutyMgtComponent } from './list/duty-mgt.component';
import { DutyMgtDetailComponent } from './detail/duty-mgt-detail.component';
import { DutyMgtUpdateComponent } from './update/duty-mgt-update.component';
import { DutyMgtDeleteDialogComponent } from './delete/duty-mgt-delete-dialog.component';
import { DutyMgtRoutingModule } from './route/duty-mgt-routing.module';

@NgModule({
  imports: [SharedModule, DutyMgtRoutingModule],
  declarations: [DutyMgtComponent, DutyMgtDetailComponent, DutyMgtUpdateComponent, DutyMgtDeleteDialogComponent],
  entryComponents: [DutyMgtDeleteDialogComponent],
})
export class DutyMgtModule {}
