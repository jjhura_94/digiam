import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DutyMgtService } from '../service/duty-mgt.service';

import { DutyMgtComponent } from './duty-mgt.component';

describe('Component Tests', () => {
  describe('DutyMgt Management Component', () => {
    let comp: DutyMgtComponent;
    let fixture: ComponentFixture<DutyMgtComponent>;
    let service: DutyMgtService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DutyMgtComponent],
      })
        .overrideTemplate(DutyMgtComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DutyMgtComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(DutyMgtService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.dutyMgts?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
