import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDutyMgt } from '../duty-mgt.model';
import { DutyMgtService } from '../service/duty-mgt.service';
import { DutyMgtDeleteDialogComponent } from '../delete/duty-mgt-delete-dialog.component';

@Component({
  selector: 'jhi-duty-mgt',
  templateUrl: './duty-mgt.component.html',
})
export class DutyMgtComponent implements OnInit {
  dutyMgts?: IDutyMgt[];
  isLoading = false;

  constructor(protected dutyMgtService: DutyMgtService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.dutyMgtService.query().subscribe(
      (res: HttpResponse<IDutyMgt[]>) => {
        this.isLoading = false;
        this.dutyMgts = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IDutyMgt): number {
    return item.id!;
  }

  delete(dutyMgt: IDutyMgt): void {
    const modalRef = this.modalService.open(DutyMgtDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.dutyMgt = dutyMgt;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
