jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IDutyMgt, DutyMgt } from '../duty-mgt.model';
import { DutyMgtService } from '../service/duty-mgt.service';

import { DutyMgtRoutingResolveService } from './duty-mgt-routing-resolve.service';

describe('Service Tests', () => {
  describe('DutyMgt routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: DutyMgtRoutingResolveService;
    let service: DutyMgtService;
    let resultDutyMgt: IDutyMgt | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(DutyMgtRoutingResolveService);
      service = TestBed.inject(DutyMgtService);
      resultDutyMgt = undefined;
    });

    describe('resolve', () => {
      it('should return IDutyMgt returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDutyMgt = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDutyMgt).toEqual({ id: 123 });
      });

      it('should return new IDutyMgt if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDutyMgt = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultDutyMgt).toEqual(new DutyMgt());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as DutyMgt })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultDutyMgt = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultDutyMgt).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
