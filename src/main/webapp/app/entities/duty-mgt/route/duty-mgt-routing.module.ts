import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DutyMgtComponent } from '../list/duty-mgt.component';
import { DutyMgtDetailComponent } from '../detail/duty-mgt-detail.component';
import { DutyMgtUpdateComponent } from '../update/duty-mgt-update.component';
import { DutyMgtRoutingResolveService } from './duty-mgt-routing-resolve.service';

const dutyMgtRoute: Routes = [
  {
    path: '',
    component: DutyMgtComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DutyMgtDetailComponent,
    resolve: {
      dutyMgt: DutyMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DutyMgtUpdateComponent,
    resolve: {
      dutyMgt: DutyMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DutyMgtUpdateComponent,
    resolve: {
      dutyMgt: DutyMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(dutyMgtRoute)],
  exports: [RouterModule],
})
export class DutyMgtRoutingModule {}
