import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IDutyMgt, DutyMgt } from '../duty-mgt.model';

import { DutyMgtService } from './duty-mgt.service';

describe('Service Tests', () => {
  describe('DutyMgt Service', () => {
    let service: DutyMgtService;
    let httpMock: HttpTestingController;
    let elemDefault: IDutyMgt;
    let expectedResult: IDutyMgt | IDutyMgt[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DutyMgtService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        dutyCode: 'AAAAAAA',
        dutyName: 'AAAAAAA',
        useYn: 'AAAAAAA',
        dutyOrd: 'AAAAAAA',
        createdBy: 'AAAAAAA',
        createdDate: currentDate,
        updatedBy: 'AAAAAAA',
        updatedDate: currentDate,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DutyMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.create(new DutyMgt()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DutyMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            dutyCode: 'BBBBBB',
            dutyName: 'BBBBBB',
            useYn: 'BBBBBB',
            dutyOrd: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DutyMgt', () => {
        const patchObject = Object.assign(
          {
            dutyName: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
          },
          new DutyMgt()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DutyMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            dutyCode: 'BBBBBB',
            dutyName: 'BBBBBB',
            useYn: 'BBBBBB',
            dutyOrd: 'BBBBBB',
            createdBy: 'BBBBBB',
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DutyMgt', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDutyMgtToCollectionIfMissing', () => {
        it('should add a DutyMgt to an empty array', () => {
          const dutyMgt: IDutyMgt = { id: 123 };
          expectedResult = service.addDutyMgtToCollectionIfMissing([], dutyMgt);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dutyMgt);
        });

        it('should not add a DutyMgt to an array that contains it', () => {
          const dutyMgt: IDutyMgt = { id: 123 };
          const dutyMgtCollection: IDutyMgt[] = [
            {
              ...dutyMgt,
            },
            { id: 456 },
          ];
          expectedResult = service.addDutyMgtToCollectionIfMissing(dutyMgtCollection, dutyMgt);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DutyMgt to an array that doesn't contain it", () => {
          const dutyMgt: IDutyMgt = { id: 123 };
          const dutyMgtCollection: IDutyMgt[] = [{ id: 456 }];
          expectedResult = service.addDutyMgtToCollectionIfMissing(dutyMgtCollection, dutyMgt);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dutyMgt);
        });

        it('should add only unique DutyMgt to an array', () => {
          const dutyMgtArray: IDutyMgt[] = [{ id: 123 }, { id: 456 }, { id: 22050 }];
          const dutyMgtCollection: IDutyMgt[] = [{ id: 123 }];
          expectedResult = service.addDutyMgtToCollectionIfMissing(dutyMgtCollection, ...dutyMgtArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dutyMgt: IDutyMgt = { id: 123 };
          const dutyMgt2: IDutyMgt = { id: 456 };
          expectedResult = service.addDutyMgtToCollectionIfMissing([], dutyMgt, dutyMgt2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dutyMgt);
          expect(expectedResult).toContain(dutyMgt2);
        });

        it('should accept null and undefined values', () => {
          const dutyMgt: IDutyMgt = { id: 123 };
          expectedResult = service.addDutyMgtToCollectionIfMissing([], null, dutyMgt, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dutyMgt);
        });

        it('should return initial array if no DutyMgt is added', () => {
          const dutyMgtCollection: IDutyMgt[] = [{ id: 123 }];
          expectedResult = service.addDutyMgtToCollectionIfMissing(dutyMgtCollection, undefined, null);
          expect(expectedResult).toEqual(dutyMgtCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
