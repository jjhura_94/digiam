import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDutyMgt, getDutyMgtIdentifier } from '../duty-mgt.model';

export type EntityResponseType = HttpResponse<IDutyMgt>;
export type EntityArrayResponseType = HttpResponse<IDutyMgt[]>;

@Injectable({ providedIn: 'root' })
export class DutyMgtService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/duty-mgts');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(dutyMgt: IDutyMgt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dutyMgt);
    return this.http
      .post<IDutyMgt>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(dutyMgt: IDutyMgt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dutyMgt);
    return this.http
      .put<IDutyMgt>(`${this.resourceUrl}/${getDutyMgtIdentifier(dutyMgt) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(dutyMgt: IDutyMgt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(dutyMgt);
    return this.http
      .patch<IDutyMgt>(`${this.resourceUrl}/${getDutyMgtIdentifier(dutyMgt) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDutyMgt>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDutyMgt[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDutyMgtToCollectionIfMissing(dutyMgtCollection: IDutyMgt[], ...dutyMgtsToCheck: (IDutyMgt | null | undefined)[]): IDutyMgt[] {
    const dutyMgts: IDutyMgt[] = dutyMgtsToCheck.filter(isPresent);
    if (dutyMgts.length > 0) {
      const dutyMgtCollectionIdentifiers = dutyMgtCollection.map(dutyMgtItem => getDutyMgtIdentifier(dutyMgtItem)!);
      const dutyMgtsToAdd = dutyMgts.filter(dutyMgtItem => {
        const dutyMgtIdentifier = getDutyMgtIdentifier(dutyMgtItem);
        if (dutyMgtIdentifier == null || dutyMgtCollectionIdentifiers.includes(dutyMgtIdentifier)) {
          return false;
        }
        dutyMgtCollectionIdentifiers.push(dutyMgtIdentifier);
        return true;
      });
      return [...dutyMgtsToAdd, ...dutyMgtCollection];
    }
    return dutyMgtCollection;
  }

  protected convertDateFromClient(dutyMgt: IDutyMgt): IDutyMgt {
    return Object.assign({}, dutyMgt, {
      createdDate: dutyMgt.createdDate?.isValid() ? dutyMgt.createdDate.toJSON() : undefined,
      updatedDate: dutyMgt.updatedDate?.isValid() ? dutyMgt.updatedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.updatedDate = res.body.updatedDate ? dayjs(res.body.updatedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((dutyMgt: IDutyMgt) => {
        dutyMgt.createdDate = dutyMgt.createdDate ? dayjs(dutyMgt.createdDate) : undefined;
        dutyMgt.updatedDate = dutyMgt.updatedDate ? dayjs(dutyMgt.updatedDate) : undefined;
      });
    }
    return res;
  }
}
