jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { DutyMgtService } from '../service/duty-mgt.service';
import { IDutyMgt, DutyMgt } from '../duty-mgt.model';

import { DutyMgtUpdateComponent } from './duty-mgt-update.component';

describe('Component Tests', () => {
  describe('DutyMgt Management Update Component', () => {
    let comp: DutyMgtUpdateComponent;
    let fixture: ComponentFixture<DutyMgtUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let dutyMgtService: DutyMgtService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DutyMgtUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(DutyMgtUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DutyMgtUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      dutyMgtService = TestBed.inject(DutyMgtService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const dutyMgt: IDutyMgt = { id: 456 };

        activatedRoute.data = of({ dutyMgt });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(dutyMgt));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DutyMgt>>();
        const dutyMgt = { id: 123 };
        jest.spyOn(dutyMgtService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dutyMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dutyMgt }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(dutyMgtService.update).toHaveBeenCalledWith(dutyMgt);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DutyMgt>>();
        const dutyMgt = new DutyMgt();
        jest.spyOn(dutyMgtService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dutyMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: dutyMgt }));
        saveSubject.complete();

        // THEN
        expect(dutyMgtService.create).toHaveBeenCalledWith(dutyMgt);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<DutyMgt>>();
        const dutyMgt = { id: 123 };
        jest.spyOn(dutyMgtService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ dutyMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(dutyMgtService.update).toHaveBeenCalledWith(dutyMgt);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
