import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IDutyMgt, DutyMgt } from '../duty-mgt.model';
import { DutyMgtService } from '../service/duty-mgt.service';

@Component({
  selector: 'jhi-duty-mgt-update',
  templateUrl: './duty-mgt-update.component.html',
})
export class DutyMgtUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    dutyCode: [],
    dutyName: [],
    useYn: [],
    dutyOrd: [],
    createdBy: [],
    createdDate: [],
    updatedBy: [],
    updatedDate: [],
  });

  constructor(protected dutyMgtService: DutyMgtService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dutyMgt }) => {
      if (dutyMgt.id === undefined) {
        const today = dayjs().startOf('day');
        dutyMgt.createdDate = today;
        dutyMgt.updatedDate = today;
      }

      this.updateForm(dutyMgt);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dutyMgt = this.createFromForm();
    if (dutyMgt.id !== undefined) {
      this.subscribeToSaveResponse(this.dutyMgtService.update(dutyMgt));
    } else {
      this.subscribeToSaveResponse(this.dutyMgtService.create(dutyMgt));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDutyMgt>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dutyMgt: IDutyMgt): void {
    this.editForm.patchValue({
      id: dutyMgt.id,
      dutyCode: dutyMgt.dutyCode,
      dutyName: dutyMgt.dutyName,
      useYn: dutyMgt.useYn,
      dutyOrd: dutyMgt.dutyOrd,
      createdBy: dutyMgt.createdBy,
      createdDate: dutyMgt.createdDate ? dutyMgt.createdDate.format(DATE_TIME_FORMAT) : null,
      updatedBy: dutyMgt.updatedBy,
      updatedDate: dutyMgt.updatedDate ? dutyMgt.updatedDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  protected createFromForm(): IDutyMgt {
    return {
      ...new DutyMgt(),
      id: this.editForm.get(['id'])!.value,
      dutyCode: this.editForm.get(['dutyCode'])!.value,
      dutyName: this.editForm.get(['dutyName'])!.value,
      useYn: this.editForm.get(['useYn'])!.value,
      dutyOrd: this.editForm.get(['dutyOrd'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      updatedDate: this.editForm.get(['updatedDate'])!.value
        ? dayjs(this.editForm.get(['updatedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
    };
  }
}
