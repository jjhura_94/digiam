import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmpJoinMeeting } from '../emp-join-meeting.model';
import { EmpJoinMeetingService } from '../service/emp-join-meeting.service';

@Component({
  templateUrl: './emp-join-meeting-delete-dialog.component.html',
})
export class EmpJoinMeetingDeleteDialogComponent {
  empJoinMeeting?: IEmpJoinMeeting;

  constructor(protected empJoinMeetingService: EmpJoinMeetingService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.empJoinMeetingService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
