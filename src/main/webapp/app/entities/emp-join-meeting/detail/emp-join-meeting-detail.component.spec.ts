import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EmpJoinMeetingDetailComponent } from './emp-join-meeting-detail.component';

describe('Component Tests', () => {
  describe('EmpJoinMeeting Management Detail Component', () => {
    let comp: EmpJoinMeetingDetailComponent;
    let fixture: ComponentFixture<EmpJoinMeetingDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [EmpJoinMeetingDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ empJoinMeeting: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(EmpJoinMeetingDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EmpJoinMeetingDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load empJoinMeeting on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.empJoinMeeting).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
