import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEmpJoinMeeting } from '../emp-join-meeting.model';

@Component({
  selector: 'jhi-emp-join-meeting-detail',
  templateUrl: './emp-join-meeting-detail.component.html',
})
export class EmpJoinMeetingDetailComponent implements OnInit {
  empJoinMeeting: IEmpJoinMeeting | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ empJoinMeeting }) => {
      this.empJoinMeeting = empJoinMeeting;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
