import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { IMeeting } from 'app/entities/meeting/meeting.model';

export interface IEmpJoinMeeting {
  id?: number;
  emp?: IEmpMgt | null;
  meeting?: IMeeting | null;
}

export class EmpJoinMeeting implements IEmpJoinMeeting {
  constructor(public id?: number, public emp?: IEmpMgt | null, public meeting?: IMeeting | null) {}
}

export function getEmpJoinMeetingIdentifier(empJoinMeeting: IEmpJoinMeeting): number | undefined {
  return empJoinMeeting.id;
}
