import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EmpJoinMeetingComponent } from './list/emp-join-meeting.component';
import { EmpJoinMeetingDetailComponent } from './detail/emp-join-meeting-detail.component';
import { EmpJoinMeetingUpdateComponent } from './update/emp-join-meeting-update.component';
import { EmpJoinMeetingDeleteDialogComponent } from './delete/emp-join-meeting-delete-dialog.component';
import { EmpJoinMeetingRoutingModule } from './route/emp-join-meeting-routing.module';

@NgModule({
  imports: [SharedModule, EmpJoinMeetingRoutingModule],
  declarations: [
    EmpJoinMeetingComponent,
    EmpJoinMeetingDetailComponent,
    EmpJoinMeetingUpdateComponent,
    EmpJoinMeetingDeleteDialogComponent,
  ],
  entryComponents: [EmpJoinMeetingDeleteDialogComponent],
})
export class EmpJoinMeetingModule {}
