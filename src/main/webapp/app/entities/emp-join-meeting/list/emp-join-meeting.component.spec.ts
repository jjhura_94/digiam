import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { EmpJoinMeetingService } from '../service/emp-join-meeting.service';

import { EmpJoinMeetingComponent } from './emp-join-meeting.component';

describe('Component Tests', () => {
  describe('EmpJoinMeeting Management Component', () => {
    let comp: EmpJoinMeetingComponent;
    let fixture: ComponentFixture<EmpJoinMeetingComponent>;
    let service: EmpJoinMeetingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [EmpJoinMeetingComponent],
      })
        .overrideTemplate(EmpJoinMeetingComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmpJoinMeetingComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(EmpJoinMeetingService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.empJoinMeetings?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
