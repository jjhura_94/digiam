import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmpJoinMeeting } from '../emp-join-meeting.model';
import { EmpJoinMeetingService } from '../service/emp-join-meeting.service';
import { EmpJoinMeetingDeleteDialogComponent } from '../delete/emp-join-meeting-delete-dialog.component';

@Component({
  selector: 'jhi-emp-join-meeting',
  templateUrl: './emp-join-meeting.component.html',
})
export class EmpJoinMeetingComponent implements OnInit {
  empJoinMeetings?: IEmpJoinMeeting[];
  isLoading = false;

  constructor(protected empJoinMeetingService: EmpJoinMeetingService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.empJoinMeetingService.query().subscribe(
      (res: HttpResponse<IEmpJoinMeeting[]>) => {
        this.isLoading = false;
        this.empJoinMeetings = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IEmpJoinMeeting): number {
    return item.id!;
  }

  delete(empJoinMeeting: IEmpJoinMeeting): void {
    const modalRef = this.modalService.open(EmpJoinMeetingDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.empJoinMeeting = empJoinMeeting;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
