jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IEmpJoinMeeting, EmpJoinMeeting } from '../emp-join-meeting.model';
import { EmpJoinMeetingService } from '../service/emp-join-meeting.service';

import { EmpJoinMeetingRoutingResolveService } from './emp-join-meeting-routing-resolve.service';

describe('Service Tests', () => {
  describe('EmpJoinMeeting routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: EmpJoinMeetingRoutingResolveService;
    let service: EmpJoinMeetingService;
    let resultEmpJoinMeeting: IEmpJoinMeeting | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(EmpJoinMeetingRoutingResolveService);
      service = TestBed.inject(EmpJoinMeetingService);
      resultEmpJoinMeeting = undefined;
    });

    describe('resolve', () => {
      it('should return IEmpJoinMeeting returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultEmpJoinMeeting = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultEmpJoinMeeting).toEqual({ id: 123 });
      });

      it('should return new IEmpJoinMeeting if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultEmpJoinMeeting = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultEmpJoinMeeting).toEqual(new EmpJoinMeeting());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as EmpJoinMeeting })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultEmpJoinMeeting = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultEmpJoinMeeting).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
