import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEmpJoinMeeting, EmpJoinMeeting } from '../emp-join-meeting.model';
import { EmpJoinMeetingService } from '../service/emp-join-meeting.service';

@Injectable({ providedIn: 'root' })
export class EmpJoinMeetingRoutingResolveService implements Resolve<IEmpJoinMeeting> {
  constructor(protected service: EmpJoinMeetingService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEmpJoinMeeting> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((empJoinMeeting: HttpResponse<EmpJoinMeeting>) => {
          if (empJoinMeeting.body) {
            return of(empJoinMeeting.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EmpJoinMeeting());
  }
}
