import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EmpJoinMeetingComponent } from '../list/emp-join-meeting.component';
import { EmpJoinMeetingDetailComponent } from '../detail/emp-join-meeting-detail.component';
import { EmpJoinMeetingUpdateComponent } from '../update/emp-join-meeting-update.component';
import { EmpJoinMeetingRoutingResolveService } from './emp-join-meeting-routing-resolve.service';

const empJoinMeetingRoute: Routes = [
  {
    path: '',
    component: EmpJoinMeetingComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EmpJoinMeetingDetailComponent,
    resolve: {
      empJoinMeeting: EmpJoinMeetingRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EmpJoinMeetingUpdateComponent,
    resolve: {
      empJoinMeeting: EmpJoinMeetingRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EmpJoinMeetingUpdateComponent,
    resolve: {
      empJoinMeeting: EmpJoinMeetingRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(empJoinMeetingRoute)],
  exports: [RouterModule],
})
export class EmpJoinMeetingRoutingModule {}
