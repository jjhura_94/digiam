import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEmpJoinMeeting, getEmpJoinMeetingIdentifier } from '../emp-join-meeting.model';

export type EntityResponseType = HttpResponse<IEmpJoinMeeting>;
export type EntityArrayResponseType = HttpResponse<IEmpJoinMeeting[]>;

@Injectable({ providedIn: 'root' })
export class EmpJoinMeetingService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/emp-join-meetings');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(empJoinMeeting: IEmpJoinMeeting): Observable<EntityResponseType> {
    return this.http.post<IEmpJoinMeeting>(this.resourceUrl, empJoinMeeting, { observe: 'response' });
  }

  update(empJoinMeeting: IEmpJoinMeeting): Observable<EntityResponseType> {
    return this.http.put<IEmpJoinMeeting>(`${this.resourceUrl}/${getEmpJoinMeetingIdentifier(empJoinMeeting) as number}`, empJoinMeeting, {
      observe: 'response',
    });
  }

  partialUpdate(empJoinMeeting: IEmpJoinMeeting): Observable<EntityResponseType> {
    return this.http.patch<IEmpJoinMeeting>(
      `${this.resourceUrl}/${getEmpJoinMeetingIdentifier(empJoinMeeting) as number}`,
      empJoinMeeting,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEmpJoinMeeting>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmpJoinMeeting[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEmpJoinMeetingToCollectionIfMissing(
    empJoinMeetingCollection: IEmpJoinMeeting[],
    ...empJoinMeetingsToCheck: (IEmpJoinMeeting | null | undefined)[]
  ): IEmpJoinMeeting[] {
    const empJoinMeetings: IEmpJoinMeeting[] = empJoinMeetingsToCheck.filter(isPresent);
    if (empJoinMeetings.length > 0) {
      const empJoinMeetingCollectionIdentifiers = empJoinMeetingCollection.map(
        empJoinMeetingItem => getEmpJoinMeetingIdentifier(empJoinMeetingItem)!
      );
      const empJoinMeetingsToAdd = empJoinMeetings.filter(empJoinMeetingItem => {
        const empJoinMeetingIdentifier = getEmpJoinMeetingIdentifier(empJoinMeetingItem);
        if (empJoinMeetingIdentifier == null || empJoinMeetingCollectionIdentifiers.includes(empJoinMeetingIdentifier)) {
          return false;
        }
        empJoinMeetingCollectionIdentifiers.push(empJoinMeetingIdentifier);
        return true;
      });
      return [...empJoinMeetingsToAdd, ...empJoinMeetingCollection];
    }
    return empJoinMeetingCollection;
  }
}
