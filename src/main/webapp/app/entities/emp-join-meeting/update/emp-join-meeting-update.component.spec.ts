jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { EmpJoinMeetingService } from '../service/emp-join-meeting.service';
import { IEmpJoinMeeting, EmpJoinMeeting } from '../emp-join-meeting.model';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';
import { IMeeting } from 'app/entities/meeting/meeting.model';
import { MeetingService } from 'app/entities/meeting/service/meeting.service';

import { EmpJoinMeetingUpdateComponent } from './emp-join-meeting-update.component';

describe('Component Tests', () => {
  describe('EmpJoinMeeting Management Update Component', () => {
    let comp: EmpJoinMeetingUpdateComponent;
    let fixture: ComponentFixture<EmpJoinMeetingUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let empJoinMeetingService: EmpJoinMeetingService;
    let empMgtService: EmpMgtService;
    let meetingService: MeetingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [EmpJoinMeetingUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(EmpJoinMeetingUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmpJoinMeetingUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      empJoinMeetingService = TestBed.inject(EmpJoinMeetingService);
      empMgtService = TestBed.inject(EmpMgtService);
      meetingService = TestBed.inject(MeetingService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call EmpMgt query and add missing value', () => {
        const empJoinMeeting: IEmpJoinMeeting = { id: 456 };
        const emp: IEmpMgt = { id: 6903 };
        empJoinMeeting.emp = emp;

        const empMgtCollection: IEmpMgt[] = [{ id: 36117 }];
        jest.spyOn(empMgtService, 'query').mockReturnValue(of(new HttpResponse({ body: empMgtCollection })));
        const additionalEmpMgts = [emp];
        const expectedCollection: IEmpMgt[] = [...additionalEmpMgts, ...empMgtCollection];
        jest.spyOn(empMgtService, 'addEmpMgtToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ empJoinMeeting });
        comp.ngOnInit();

        expect(empMgtService.query).toHaveBeenCalled();
        expect(empMgtService.addEmpMgtToCollectionIfMissing).toHaveBeenCalledWith(empMgtCollection, ...additionalEmpMgts);
        expect(comp.empMgtsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Meeting query and add missing value', () => {
        const empJoinMeeting: IEmpJoinMeeting = { id: 456 };
        const meeting: IMeeting = { id: 61904 };
        empJoinMeeting.meeting = meeting;

        const meetingCollection: IMeeting[] = [{ id: 21604 }];
        jest.spyOn(meetingService, 'query').mockReturnValue(of(new HttpResponse({ body: meetingCollection })));
        const additionalMeetings = [meeting];
        const expectedCollection: IMeeting[] = [...additionalMeetings, ...meetingCollection];
        jest.spyOn(meetingService, 'addMeetingToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ empJoinMeeting });
        comp.ngOnInit();

        expect(meetingService.query).toHaveBeenCalled();
        expect(meetingService.addMeetingToCollectionIfMissing).toHaveBeenCalledWith(meetingCollection, ...additionalMeetings);
        expect(comp.meetingsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const empJoinMeeting: IEmpJoinMeeting = { id: 456 };
        const emp: IEmpMgt = { id: 88401 };
        empJoinMeeting.emp = emp;
        const meeting: IMeeting = { id: 28753 };
        empJoinMeeting.meeting = meeting;

        activatedRoute.data = of({ empJoinMeeting });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(empJoinMeeting));
        expect(comp.empMgtsSharedCollection).toContain(emp);
        expect(comp.meetingsSharedCollection).toContain(meeting);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<EmpJoinMeeting>>();
        const empJoinMeeting = { id: 123 };
        jest.spyOn(empJoinMeetingService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ empJoinMeeting });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: empJoinMeeting }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(empJoinMeetingService.update).toHaveBeenCalledWith(empJoinMeeting);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<EmpJoinMeeting>>();
        const empJoinMeeting = new EmpJoinMeeting();
        jest.spyOn(empJoinMeetingService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ empJoinMeeting });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: empJoinMeeting }));
        saveSubject.complete();

        // THEN
        expect(empJoinMeetingService.create).toHaveBeenCalledWith(empJoinMeeting);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<EmpJoinMeeting>>();
        const empJoinMeeting = { id: 123 };
        jest.spyOn(empJoinMeetingService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ empJoinMeeting });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(empJoinMeetingService.update).toHaveBeenCalledWith(empJoinMeeting);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackEmpMgtById', () => {
        it('Should return tracked EmpMgt primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackEmpMgtById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackMeetingById', () => {
        it('Should return tracked Meeting primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMeetingById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
