import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IEmpJoinMeeting, EmpJoinMeeting } from '../emp-join-meeting.model';
import { EmpJoinMeetingService } from '../service/emp-join-meeting.service';
import { IEmpMgt } from 'app/entities/emp-mgt/emp-mgt.model';
import { EmpMgtService } from 'app/entities/emp-mgt/service/emp-mgt.service';
import { IMeeting } from 'app/entities/meeting/meeting.model';
import { MeetingService } from 'app/entities/meeting/service/meeting.service';

@Component({
  selector: 'jhi-emp-join-meeting-update',
  templateUrl: './emp-join-meeting-update.component.html',
})
export class EmpJoinMeetingUpdateComponent implements OnInit {
  isSaving = false;

  empMgtsSharedCollection: IEmpMgt[] = [];
  meetingsSharedCollection: IMeeting[] = [];

  editForm = this.fb.group({
    id: [],
    emp: [],
    meeting: [],
  });

  constructor(
    protected empJoinMeetingService: EmpJoinMeetingService,
    protected empMgtService: EmpMgtService,
    protected meetingService: MeetingService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ empJoinMeeting }) => {
      this.updateForm(empJoinMeeting);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const empJoinMeeting = this.createFromForm();
    if (empJoinMeeting.id !== undefined) {
      this.subscribeToSaveResponse(this.empJoinMeetingService.update(empJoinMeeting));
    } else {
      this.subscribeToSaveResponse(this.empJoinMeetingService.create(empJoinMeeting));
    }
  }

  trackEmpMgtById(index: number, item: IEmpMgt): number {
    return item.id!;
  }

  trackMeetingById(index: number, item: IMeeting): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmpJoinMeeting>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(empJoinMeeting: IEmpJoinMeeting): void {
    this.editForm.patchValue({
      id: empJoinMeeting.id,
      emp: empJoinMeeting.emp,
      meeting: empJoinMeeting.meeting,
    });

    this.empMgtsSharedCollection = this.empMgtService.addEmpMgtToCollectionIfMissing(this.empMgtsSharedCollection, empJoinMeeting.emp);
    this.meetingsSharedCollection = this.meetingService.addMeetingToCollectionIfMissing(
      this.meetingsSharedCollection,
      empJoinMeeting.meeting
    );
  }

  protected loadRelationshipsOptions(): void {
    this.empMgtService
      .query()
      .pipe(map((res: HttpResponse<IEmpMgt[]>) => res.body ?? []))
      .pipe(map((empMgts: IEmpMgt[]) => this.empMgtService.addEmpMgtToCollectionIfMissing(empMgts, this.editForm.get('emp')!.value)))
      .subscribe((empMgts: IEmpMgt[]) => (this.empMgtsSharedCollection = empMgts));

    this.meetingService
      .query()
      .pipe(map((res: HttpResponse<IMeeting[]>) => res.body ?? []))
      .pipe(
        map((meetings: IMeeting[]) => this.meetingService.addMeetingToCollectionIfMissing(meetings, this.editForm.get('meeting')!.value))
      )
      .subscribe((meetings: IMeeting[]) => (this.meetingsSharedCollection = meetings));
  }

  protected createFromForm(): IEmpJoinMeeting {
    return {
      ...new EmpJoinMeeting(),
      id: this.editForm.get(['id'])!.value,
      emp: this.editForm.get(['emp'])!.value,
      meeting: this.editForm.get(['meeting'])!.value,
    };
  }
}
