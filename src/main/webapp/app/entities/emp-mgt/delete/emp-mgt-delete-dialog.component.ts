import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmpMgt } from '../emp-mgt.model';
import { EmpMgtService } from '../service/emp-mgt.service';

@Component({
  templateUrl: './emp-mgt-delete-dialog.component.html',
})
export class EmpMgtDeleteDialogComponent {
  empMgt?: IEmpMgt;

  constructor(protected empMgtService: EmpMgtService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.empMgtService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
