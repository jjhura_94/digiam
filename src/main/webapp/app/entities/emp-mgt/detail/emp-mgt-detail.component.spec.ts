import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EmpMgtDetailComponent } from './emp-mgt-detail.component';

describe('Component Tests', () => {
  describe('EmpMgt Management Detail Component', () => {
    let comp: EmpMgtDetailComponent;
    let fixture: ComponentFixture<EmpMgtDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [EmpMgtDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ empMgt: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(EmpMgtDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EmpMgtDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load empMgt on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.empMgt).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
