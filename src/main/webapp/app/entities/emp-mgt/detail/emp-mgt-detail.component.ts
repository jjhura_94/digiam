import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEmpMgt } from '../emp-mgt.model';

@Component({
  selector: 'jhi-emp-mgt-detail',
  templateUrl: './emp-mgt-detail.component.html',
})
export class EmpMgtDetailComponent implements OnInit {
  empMgt: IEmpMgt | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ empMgt }) => {
      this.empMgt = empMgt;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
