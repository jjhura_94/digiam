import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';
import { IDayOffUser } from 'app/entities/day-off-user/day-off-user.model';
import { IEmpJoinMeeting } from 'app/entities/emp-join-meeting/emp-join-meeting.model';
import { IAsset } from 'app/entities/asset/asset.model';
import { IAssetHistory } from 'app/entities/asset-history/asset-history.model';
import { IDeptMgt } from 'app/entities/dept-mgt/dept-mgt.model';
import { IDutyMgt } from 'app/entities/duty-mgt/duty-mgt.model';

export interface IEmpMgt {
  id?: number;
  empNo?: string | null;
  name?: string | null;
  startDate?: dayjs.Dayjs | null;
  endDate?: dayjs.Dayjs | null;
  dob?: dayjs.Dayjs | null;
  email?: string | null;
  cellPhone?: string | null;
  extNumber?: number | null;
  zipCode?: string | null;
  address?: string | null;
  gender?: string | null;
  imgPath?: string | null;
  status?: string | null;
  user?: IUser | null;
  dayOffUsers?: IDayOffUser[] | null;
  empJoinMeetings?: IEmpJoinMeeting[] | null;
  assets?: IAsset[] | null;
  assetHistories?: IAssetHistory[] | null;
  dept?: IDeptMgt | null;
  duty?: IDutyMgt | null;
}

export class EmpMgt implements IEmpMgt {
  constructor(
    public id?: number,
    public empNo?: string | null,
    public name?: string | null,
    public startDate?: dayjs.Dayjs | null,
    public endDate?: dayjs.Dayjs | null,
    public dob?: dayjs.Dayjs | null,
    public email?: string | null,
    public cellPhone?: string | null,
    public extNumber?: number | null,
    public zipCode?: string | null,
    public address?: string | null,
    public gender?: string | null,
    public imgPath?: string | null,
    public status?: string | null,
    public user?: IUser | null,
    public dayOffUsers?: IDayOffUser[] | null,
    public empJoinMeetings?: IEmpJoinMeeting[] | null,
    public assets?: IAsset[] | null,
    public assetHistories?: IAssetHistory[] | null,
    public dept?: IDeptMgt | null,
    public duty?: IDutyMgt | null
  ) {}
}

export function getEmpMgtIdentifier(empMgt: IEmpMgt): number | undefined {
  return empMgt.id;
}
