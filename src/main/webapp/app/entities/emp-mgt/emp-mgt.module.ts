import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EmpMgtComponent } from './list/emp-mgt.component';
import { EmpMgtDetailComponent } from './detail/emp-mgt-detail.component';
import { EmpMgtUpdateComponent } from './update/emp-mgt-update.component';
import { EmpMgtDeleteDialogComponent } from './delete/emp-mgt-delete-dialog.component';
import { EmpMgtRoutingModule } from './route/emp-mgt-routing.module';

@NgModule({
  imports: [SharedModule, EmpMgtRoutingModule],
  declarations: [EmpMgtComponent, EmpMgtDetailComponent, EmpMgtUpdateComponent, EmpMgtDeleteDialogComponent],
  entryComponents: [EmpMgtDeleteDialogComponent],
})
export class EmpMgtModule {}
