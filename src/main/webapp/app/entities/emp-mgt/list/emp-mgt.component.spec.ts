import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { EmpMgtService } from '../service/emp-mgt.service';

import { EmpMgtComponent } from './emp-mgt.component';

describe('Component Tests', () => {
  describe('EmpMgt Management Component', () => {
    let comp: EmpMgtComponent;
    let fixture: ComponentFixture<EmpMgtComponent>;
    let service: EmpMgtService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [EmpMgtComponent],
      })
        .overrideTemplate(EmpMgtComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmpMgtComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(EmpMgtService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.empMgts?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
