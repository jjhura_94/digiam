import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmpMgt } from '../emp-mgt.model';
import { EmpMgtService } from '../service/emp-mgt.service';
import { EmpMgtDeleteDialogComponent } from '../delete/emp-mgt-delete-dialog.component';

@Component({
  selector: 'jhi-emp-mgt',
  templateUrl: './emp-mgt.component.html',
})
export class EmpMgtComponent implements OnInit {
  empMgts?: IEmpMgt[];
  isLoading = false;

  constructor(protected empMgtService: EmpMgtService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.empMgtService.query().subscribe(
      (res: HttpResponse<IEmpMgt[]>) => {
        this.isLoading = false;
        this.empMgts = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IEmpMgt): number {
    return item.id!;
  }

  delete(empMgt: IEmpMgt): void {
    const modalRef = this.modalService.open(EmpMgtDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.empMgt = empMgt;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
