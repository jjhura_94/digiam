import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEmpMgt, EmpMgt } from '../emp-mgt.model';
import { EmpMgtService } from '../service/emp-mgt.service';

@Injectable({ providedIn: 'root' })
export class EmpMgtRoutingResolveService implements Resolve<IEmpMgt> {
  constructor(protected service: EmpMgtService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEmpMgt> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((empMgt: HttpResponse<EmpMgt>) => {
          if (empMgt.body) {
            return of(empMgt.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EmpMgt());
  }
}
