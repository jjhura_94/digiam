import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EmpMgtComponent } from '../list/emp-mgt.component';
import { EmpMgtDetailComponent } from '../detail/emp-mgt-detail.component';
import { EmpMgtUpdateComponent } from '../update/emp-mgt-update.component';
import { EmpMgtRoutingResolveService } from './emp-mgt-routing-resolve.service';

const empMgtRoute: Routes = [
  {
    path: '',
    component: EmpMgtComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EmpMgtDetailComponent,
    resolve: {
      empMgt: EmpMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EmpMgtUpdateComponent,
    resolve: {
      empMgt: EmpMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EmpMgtUpdateComponent,
    resolve: {
      empMgt: EmpMgtRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(empMgtRoute)],
  exports: [RouterModule],
})
export class EmpMgtRoutingModule {}
