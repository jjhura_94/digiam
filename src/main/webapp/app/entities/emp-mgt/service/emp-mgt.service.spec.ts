import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IEmpMgt, EmpMgt } from '../emp-mgt.model';

import { EmpMgtService } from './emp-mgt.service';

describe('Service Tests', () => {
  describe('EmpMgt Service', () => {
    let service: EmpMgtService;
    let httpMock: HttpTestingController;
    let elemDefault: IEmpMgt;
    let expectedResult: IEmpMgt | IEmpMgt[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(EmpMgtService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        empNo: 'AAAAAAA',
        name: 'AAAAAAA',
        startDate: currentDate,
        endDate: currentDate,
        dob: currentDate,
        email: 'AAAAAAA',
        cellPhone: 'AAAAAAA',
        extNumber: 0,
        zipCode: 'AAAAAAA',
        address: 'AAAAAAA',
        gender: 'AAAAAAA',
        imgPath: 'AAAAAAA',
        status: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            startDate: currentDate.format(DATE_TIME_FORMAT),
            endDate: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a EmpMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            startDate: currentDate.format(DATE_TIME_FORMAT),
            endDate: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startDate: currentDate,
            endDate: currentDate,
            dob: currentDate,
          },
          returnedFromService
        );

        service.create(new EmpMgt()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a EmpMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            empNo: 'BBBBBB',
            name: 'BBBBBB',
            startDate: currentDate.format(DATE_TIME_FORMAT),
            endDate: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT),
            email: 'BBBBBB',
            cellPhone: 'BBBBBB',
            extNumber: 1,
            zipCode: 'BBBBBB',
            address: 'BBBBBB',
            gender: 'BBBBBB',
            imgPath: 'BBBBBB',
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startDate: currentDate,
            endDate: currentDate,
            dob: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a EmpMgt', () => {
        const patchObject = Object.assign(
          {
            empNo: 'BBBBBB',
            startDate: currentDate.format(DATE_TIME_FORMAT),
            endDate: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT),
            cellPhone: 'BBBBBB',
            extNumber: 1,
            zipCode: 'BBBBBB',
            address: 'BBBBBB',
          },
          new EmpMgt()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            startDate: currentDate,
            endDate: currentDate,
            dob: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of EmpMgt', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            empNo: 'BBBBBB',
            name: 'BBBBBB',
            startDate: currentDate.format(DATE_TIME_FORMAT),
            endDate: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT),
            email: 'BBBBBB',
            cellPhone: 'BBBBBB',
            extNumber: 1,
            zipCode: 'BBBBBB',
            address: 'BBBBBB',
            gender: 'BBBBBB',
            imgPath: 'BBBBBB',
            status: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startDate: currentDate,
            endDate: currentDate,
            dob: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a EmpMgt', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addEmpMgtToCollectionIfMissing', () => {
        it('should add a EmpMgt to an empty array', () => {
          const empMgt: IEmpMgt = { id: 123 };
          expectedResult = service.addEmpMgtToCollectionIfMissing([], empMgt);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(empMgt);
        });

        it('should not add a EmpMgt to an array that contains it', () => {
          const empMgt: IEmpMgt = { id: 123 };
          const empMgtCollection: IEmpMgt[] = [
            {
              ...empMgt,
            },
            { id: 456 },
          ];
          expectedResult = service.addEmpMgtToCollectionIfMissing(empMgtCollection, empMgt);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a EmpMgt to an array that doesn't contain it", () => {
          const empMgt: IEmpMgt = { id: 123 };
          const empMgtCollection: IEmpMgt[] = [{ id: 456 }];
          expectedResult = service.addEmpMgtToCollectionIfMissing(empMgtCollection, empMgt);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(empMgt);
        });

        it('should add only unique EmpMgt to an array', () => {
          const empMgtArray: IEmpMgt[] = [{ id: 123 }, { id: 456 }, { id: 33252 }];
          const empMgtCollection: IEmpMgt[] = [{ id: 123 }];
          expectedResult = service.addEmpMgtToCollectionIfMissing(empMgtCollection, ...empMgtArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const empMgt: IEmpMgt = { id: 123 };
          const empMgt2: IEmpMgt = { id: 456 };
          expectedResult = service.addEmpMgtToCollectionIfMissing([], empMgt, empMgt2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(empMgt);
          expect(expectedResult).toContain(empMgt2);
        });

        it('should accept null and undefined values', () => {
          const empMgt: IEmpMgt = { id: 123 };
          expectedResult = service.addEmpMgtToCollectionIfMissing([], null, empMgt, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(empMgt);
        });

        it('should return initial array if no EmpMgt is added', () => {
          const empMgtCollection: IEmpMgt[] = [{ id: 123 }];
          expectedResult = service.addEmpMgtToCollectionIfMissing(empMgtCollection, undefined, null);
          expect(expectedResult).toEqual(empMgtCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
