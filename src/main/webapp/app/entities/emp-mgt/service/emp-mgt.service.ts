import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEmpMgt, getEmpMgtIdentifier } from '../emp-mgt.model';

export type EntityResponseType = HttpResponse<IEmpMgt>;
export type EntityArrayResponseType = HttpResponse<IEmpMgt[]>;

@Injectable({ providedIn: 'root' })
export class EmpMgtService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/emp-mgts');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(empMgt: IEmpMgt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(empMgt);
    return this.http
      .post<IEmpMgt>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(empMgt: IEmpMgt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(empMgt);
    return this.http
      .put<IEmpMgt>(`${this.resourceUrl}/${getEmpMgtIdentifier(empMgt) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(empMgt: IEmpMgt): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(empMgt);
    return this.http
      .patch<IEmpMgt>(`${this.resourceUrl}/${getEmpMgtIdentifier(empMgt) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEmpMgt>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEmpMgt[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEmpMgtToCollectionIfMissing(empMgtCollection: IEmpMgt[], ...empMgtsToCheck: (IEmpMgt | null | undefined)[]): IEmpMgt[] {
    const empMgts: IEmpMgt[] = empMgtsToCheck.filter(isPresent);
    if (empMgts.length > 0) {
      const empMgtCollectionIdentifiers = empMgtCollection.map(empMgtItem => getEmpMgtIdentifier(empMgtItem)!);
      const empMgtsToAdd = empMgts.filter(empMgtItem => {
        const empMgtIdentifier = getEmpMgtIdentifier(empMgtItem);
        if (empMgtIdentifier == null || empMgtCollectionIdentifiers.includes(empMgtIdentifier)) {
          return false;
        }
        empMgtCollectionIdentifiers.push(empMgtIdentifier);
        return true;
      });
      return [...empMgtsToAdd, ...empMgtCollection];
    }
    return empMgtCollection;
  }

  protected convertDateFromClient(empMgt: IEmpMgt): IEmpMgt {
    return Object.assign({}, empMgt, {
      startDate: empMgt.startDate?.isValid() ? empMgt.startDate.toJSON() : undefined,
      endDate: empMgt.endDate?.isValid() ? empMgt.endDate.toJSON() : undefined,
      dob: empMgt.dob?.isValid() ? empMgt.dob.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? dayjs(res.body.startDate) : undefined;
      res.body.endDate = res.body.endDate ? dayjs(res.body.endDate) : undefined;
      res.body.dob = res.body.dob ? dayjs(res.body.dob) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((empMgt: IEmpMgt) => {
        empMgt.startDate = empMgt.startDate ? dayjs(empMgt.startDate) : undefined;
        empMgt.endDate = empMgt.endDate ? dayjs(empMgt.endDate) : undefined;
        empMgt.dob = empMgt.dob ? dayjs(empMgt.dob) : undefined;
      });
    }
    return res;
  }
}
