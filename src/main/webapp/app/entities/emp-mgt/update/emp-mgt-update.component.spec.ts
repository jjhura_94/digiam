jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { EmpMgtService } from '../service/emp-mgt.service';
import { IEmpMgt, EmpMgt } from '../emp-mgt.model';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IDeptMgt } from 'app/entities/dept-mgt/dept-mgt.model';
import { DeptMgtService } from 'app/entities/dept-mgt/service/dept-mgt.service';
import { IDutyMgt } from 'app/entities/duty-mgt/duty-mgt.model';
import { DutyMgtService } from 'app/entities/duty-mgt/service/duty-mgt.service';

import { EmpMgtUpdateComponent } from './emp-mgt-update.component';

describe('Component Tests', () => {
  describe('EmpMgt Management Update Component', () => {
    let comp: EmpMgtUpdateComponent;
    let fixture: ComponentFixture<EmpMgtUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let empMgtService: EmpMgtService;
    let userService: UserService;
    let deptMgtService: DeptMgtService;
    let dutyMgtService: DutyMgtService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [EmpMgtUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(EmpMgtUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmpMgtUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      empMgtService = TestBed.inject(EmpMgtService);
      userService = TestBed.inject(UserService);
      deptMgtService = TestBed.inject(DeptMgtService);
      dutyMgtService = TestBed.inject(DutyMgtService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call User query and add missing value', () => {
        const empMgt: IEmpMgt = { id: 456 };
        const user: IUser = { id: 82660 };
        empMgt.user = user;

        const userCollection: IUser[] = [{ id: 96404 }];
        jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
        const additionalUsers = [user];
        const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
        jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ empMgt });
        comp.ngOnInit();

        expect(userService.query).toHaveBeenCalled();
        expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
        expect(comp.usersSharedCollection).toEqual(expectedCollection);
      });

      it('Should call DeptMgt query and add missing value', () => {
        const empMgt: IEmpMgt = { id: 456 };
        const dept: IDeptMgt = { id: 74538 };
        empMgt.dept = dept;

        const deptMgtCollection: IDeptMgt[] = [{ id: 3020 }];
        jest.spyOn(deptMgtService, 'query').mockReturnValue(of(new HttpResponse({ body: deptMgtCollection })));
        const additionalDeptMgts = [dept];
        const expectedCollection: IDeptMgt[] = [...additionalDeptMgts, ...deptMgtCollection];
        jest.spyOn(deptMgtService, 'addDeptMgtToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ empMgt });
        comp.ngOnInit();

        expect(deptMgtService.query).toHaveBeenCalled();
        expect(deptMgtService.addDeptMgtToCollectionIfMissing).toHaveBeenCalledWith(deptMgtCollection, ...additionalDeptMgts);
        expect(comp.deptMgtsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call DutyMgt query and add missing value', () => {
        const empMgt: IEmpMgt = { id: 456 };
        const duty: IDutyMgt = { id: 73064 };
        empMgt.duty = duty;

        const dutyMgtCollection: IDutyMgt[] = [{ id: 18237 }];
        jest.spyOn(dutyMgtService, 'query').mockReturnValue(of(new HttpResponse({ body: dutyMgtCollection })));
        const additionalDutyMgts = [duty];
        const expectedCollection: IDutyMgt[] = [...additionalDutyMgts, ...dutyMgtCollection];
        jest.spyOn(dutyMgtService, 'addDutyMgtToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ empMgt });
        comp.ngOnInit();

        expect(dutyMgtService.query).toHaveBeenCalled();
        expect(dutyMgtService.addDutyMgtToCollectionIfMissing).toHaveBeenCalledWith(dutyMgtCollection, ...additionalDutyMgts);
        expect(comp.dutyMgtsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const empMgt: IEmpMgt = { id: 456 };
        const user: IUser = { id: 57252 };
        empMgt.user = user;
        const dept: IDeptMgt = { id: 25261 };
        empMgt.dept = dept;
        const duty: IDutyMgt = { id: 9807 };
        empMgt.duty = duty;

        activatedRoute.data = of({ empMgt });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(empMgt));
        expect(comp.usersSharedCollection).toContain(user);
        expect(comp.deptMgtsSharedCollection).toContain(dept);
        expect(comp.dutyMgtsSharedCollection).toContain(duty);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<EmpMgt>>();
        const empMgt = { id: 123 };
        jest.spyOn(empMgtService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ empMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: empMgt }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(empMgtService.update).toHaveBeenCalledWith(empMgt);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<EmpMgt>>();
        const empMgt = new EmpMgt();
        jest.spyOn(empMgtService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ empMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: empMgt }));
        saveSubject.complete();

        // THEN
        expect(empMgtService.create).toHaveBeenCalledWith(empMgt);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<EmpMgt>>();
        const empMgt = { id: 123 };
        jest.spyOn(empMgtService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ empMgt });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(empMgtService.update).toHaveBeenCalledWith(empMgt);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackUserById', () => {
        it('Should return tracked User primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackUserById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackDeptMgtById', () => {
        it('Should return tracked DeptMgt primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackDeptMgtById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackDutyMgtById', () => {
        it('Should return tracked DutyMgt primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackDutyMgtById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
