import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IEmpMgt, EmpMgt } from '../emp-mgt.model';
import { EmpMgtService } from '../service/emp-mgt.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';
import { IDeptMgt } from 'app/entities/dept-mgt/dept-mgt.model';
import { DeptMgtService } from 'app/entities/dept-mgt/service/dept-mgt.service';
import { IDutyMgt } from 'app/entities/duty-mgt/duty-mgt.model';
import { DutyMgtService } from 'app/entities/duty-mgt/service/duty-mgt.service';

@Component({
  selector: 'jhi-emp-mgt-update',
  templateUrl: './emp-mgt-update.component.html',
})
export class EmpMgtUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];
  deptMgtsSharedCollection: IDeptMgt[] = [];
  dutyMgtsSharedCollection: IDutyMgt[] = [];

  editForm = this.fb.group({
    id: [],
    empNo: [],
    name: [],
    startDate: [],
    endDate: [],
    dob: [],
    email: [],
    cellPhone: [],
    extNumber: [],
    zipCode: [],
    address: [],
    gender: [],
    imgPath: [],
    status: [],
    user: [],
    dept: [],
    duty: [],
  });

  constructor(
    protected empMgtService: EmpMgtService,
    protected userService: UserService,
    protected deptMgtService: DeptMgtService,
    protected dutyMgtService: DutyMgtService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ empMgt }) => {
      if (empMgt.id === undefined) {
        const today = dayjs().startOf('day');
        empMgt.startDate = today;
        empMgt.endDate = today;
        empMgt.dob = today;
      }

      this.updateForm(empMgt);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const empMgt = this.createFromForm();
    if (empMgt.id !== undefined) {
      this.subscribeToSaveResponse(this.empMgtService.update(empMgt));
    } else {
      this.subscribeToSaveResponse(this.empMgtService.create(empMgt));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  trackDeptMgtById(index: number, item: IDeptMgt): number {
    return item.id!;
  }

  trackDutyMgtById(index: number, item: IDutyMgt): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmpMgt>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(empMgt: IEmpMgt): void {
    this.editForm.patchValue({
      id: empMgt.id,
      empNo: empMgt.empNo,
      name: empMgt.name,
      startDate: empMgt.startDate ? empMgt.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: empMgt.endDate ? empMgt.endDate.format(DATE_TIME_FORMAT) : null,
      dob: empMgt.dob ? empMgt.dob.format(DATE_TIME_FORMAT) : null,
      email: empMgt.email,
      cellPhone: empMgt.cellPhone,
      extNumber: empMgt.extNumber,
      zipCode: empMgt.zipCode,
      address: empMgt.address,
      gender: empMgt.gender,
      imgPath: empMgt.imgPath,
      status: empMgt.status,
      user: empMgt.user,
      dept: empMgt.dept,
      duty: empMgt.duty,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, empMgt.user);
    this.deptMgtsSharedCollection = this.deptMgtService.addDeptMgtToCollectionIfMissing(this.deptMgtsSharedCollection, empMgt.dept);
    this.dutyMgtsSharedCollection = this.dutyMgtService.addDutyMgtToCollectionIfMissing(this.dutyMgtsSharedCollection, empMgt.duty);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));

    this.deptMgtService
      .query()
      .pipe(map((res: HttpResponse<IDeptMgt[]>) => res.body ?? []))
      .pipe(map((deptMgts: IDeptMgt[]) => this.deptMgtService.addDeptMgtToCollectionIfMissing(deptMgts, this.editForm.get('dept')!.value)))
      .subscribe((deptMgts: IDeptMgt[]) => (this.deptMgtsSharedCollection = deptMgts));

    this.dutyMgtService
      .query()
      .pipe(map((res: HttpResponse<IDutyMgt[]>) => res.body ?? []))
      .pipe(map((dutyMgts: IDutyMgt[]) => this.dutyMgtService.addDutyMgtToCollectionIfMissing(dutyMgts, this.editForm.get('duty')!.value)))
      .subscribe((dutyMgts: IDutyMgt[]) => (this.dutyMgtsSharedCollection = dutyMgts));
  }

  protected createFromForm(): IEmpMgt {
    return {
      ...new EmpMgt(),
      id: this.editForm.get(['id'])!.value,
      empNo: this.editForm.get(['empNo'])!.value,
      name: this.editForm.get(['name'])!.value,
      startDate: this.editForm.get(['startDate'])!.value ? dayjs(this.editForm.get(['startDate'])!.value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate'])!.value ? dayjs(this.editForm.get(['endDate'])!.value, DATE_TIME_FORMAT) : undefined,
      dob: this.editForm.get(['dob'])!.value ? dayjs(this.editForm.get(['dob'])!.value, DATE_TIME_FORMAT) : undefined,
      email: this.editForm.get(['email'])!.value,
      cellPhone: this.editForm.get(['cellPhone'])!.value,
      extNumber: this.editForm.get(['extNumber'])!.value,
      zipCode: this.editForm.get(['zipCode'])!.value,
      address: this.editForm.get(['address'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      imgPath: this.editForm.get(['imgPath'])!.value,
      status: this.editForm.get(['status'])!.value,
      user: this.editForm.get(['user'])!.value,
      dept: this.editForm.get(['dept'])!.value,
      duty: this.editForm.get(['duty'])!.value,
    };
  }
}
