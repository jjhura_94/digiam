import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'emp-mgt',
        data: { pageTitle: 'EmpMgts' },
        loadChildren: () => import('./emp-mgt/emp-mgt.module').then(m => m.EmpMgtModule),
      },
      {
        path: 'duty-mgt',
        data: { pageTitle: 'DutyMgts' },
        loadChildren: () => import('./duty-mgt/duty-mgt.module').then(m => m.DutyMgtModule),
      },
      {
        path: 'dept-mgt',
        data: { pageTitle: 'DeptMgts' },
        loadChildren: () => import('./dept-mgt/dept-mgt.module').then(m => m.DeptMgtModule),
      },
      {
        path: 'asset-type',
        data: { pageTitle: 'AssetTypes' },
        loadChildren: () => import('./asset-type/asset-type.module').then(m => m.AssetTypeModule),
      },
      {
        path: 'asset',
        data: { pageTitle: 'Assets' },
        loadChildren: () => import('./asset/asset.module').then(m => m.AssetModule),
      },
      {
        path: 'asset-set',
        data: { pageTitle: 'AssetSets' },
        loadChildren: () => import('./asset-set/asset-set.module').then(m => m.AssetSetModule),
      },
      {
        path: 'asset-history',
        data: { pageTitle: 'AssetHistories' },
        loadChildren: () => import('./asset-history/asset-history.module').then(m => m.AssetHistoryModule),
      },
      {
        path: 'day-off-type',
        data: { pageTitle: 'DayOffTypes' },
        loadChildren: () => import('./day-off-type/day-off-type.module').then(m => m.DayOffTypeModule),
      },
      {
        path: 'day-off-user',
        data: { pageTitle: 'DayOffUsers' },
        loadChildren: () => import('./day-off-user/day-off-user.module').then(m => m.DayOffUserModule),
      },
      {
        path: 'day-off-ticket',
        data: { pageTitle: 'DayOffTickets' },
        loadChildren: () => import('./day-off-ticket/day-off-ticket.module').then(m => m.DayOffTicketModule),
      },
      {
        path: 'meeting-room',
        data: { pageTitle: 'MeetingRooms' },
        loadChildren: () => import('./meeting-room/meeting-room.module').then(m => m.MeetingRoomModule),
      },
      {
        path: 'meeting',
        data: { pageTitle: 'Meetings' },
        loadChildren: () => import('./meeting/meeting.module').then(m => m.MeetingModule),
      },
      {
        path: 'assets-in-room',
        data: { pageTitle: 'AssetsInRooms' },
        loadChildren: () => import('./assets-in-room/assets-in-room.module').then(m => m.AssetsInRoomModule),
      },
      {
        path: 'emp-join-meeting',
        data: { pageTitle: 'EmpJoinMeetings' },
        loadChildren: () => import('./emp-join-meeting/emp-join-meeting.module').then(m => m.EmpJoinMeetingModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
