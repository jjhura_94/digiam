import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMeetingRoom } from '../meeting-room.model';
import { MeetingRoomService } from '../service/meeting-room.service';

@Component({
  templateUrl: './meeting-room-delete-dialog.component.html',
})
export class MeetingRoomDeleteDialogComponent {
  meetingRoom?: IMeetingRoom;

  constructor(protected meetingRoomService: MeetingRoomService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.meetingRoomService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
