import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MeetingRoomDetailComponent } from './meeting-room-detail.component';

describe('Component Tests', () => {
  describe('MeetingRoom Management Detail Component', () => {
    let comp: MeetingRoomDetailComponent;
    let fixture: ComponentFixture<MeetingRoomDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [MeetingRoomDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ meetingRoom: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(MeetingRoomDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MeetingRoomDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load meetingRoom on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.meetingRoom).toEqual(expect.objectContaining({ id: 123 }));
      });
    });
  });
});
