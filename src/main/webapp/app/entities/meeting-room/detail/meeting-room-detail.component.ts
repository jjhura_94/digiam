import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMeetingRoom } from '../meeting-room.model';

@Component({
  selector: 'jhi-meeting-room-detail',
  templateUrl: './meeting-room-detail.component.html',
})
export class MeetingRoomDetailComponent implements OnInit {
  meetingRoom: IMeetingRoom | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ meetingRoom }) => {
      this.meetingRoom = meetingRoom;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
