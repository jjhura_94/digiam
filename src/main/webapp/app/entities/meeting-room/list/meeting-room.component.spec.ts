import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MeetingRoomService } from '../service/meeting-room.service';

import { MeetingRoomComponent } from './meeting-room.component';

describe('Component Tests', () => {
  describe('MeetingRoom Management Component', () => {
    let comp: MeetingRoomComponent;
    let fixture: ComponentFixture<MeetingRoomComponent>;
    let service: MeetingRoomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MeetingRoomComponent],
      })
        .overrideTemplate(MeetingRoomComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MeetingRoomComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(MeetingRoomService);

      const headers = new HttpHeaders().append('link', 'link;link');
      jest.spyOn(service, 'query').mockReturnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.meetingRooms?.[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
