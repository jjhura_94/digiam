import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMeetingRoom } from '../meeting-room.model';
import { MeetingRoomService } from '../service/meeting-room.service';
import { MeetingRoomDeleteDialogComponent } from '../delete/meeting-room-delete-dialog.component';

@Component({
  selector: 'jhi-meeting-room',
  templateUrl: './meeting-room.component.html',
})
export class MeetingRoomComponent implements OnInit {
  meetingRooms?: IMeetingRoom[];
  isLoading = false;

  constructor(protected meetingRoomService: MeetingRoomService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.meetingRoomService.query().subscribe(
      (res: HttpResponse<IMeetingRoom[]>) => {
        this.isLoading = false;
        this.meetingRooms = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMeetingRoom): number {
    return item.id!;
  }

  delete(meetingRoom: IMeetingRoom): void {
    const modalRef = this.modalService.open(MeetingRoomDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.meetingRoom = meetingRoom;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
