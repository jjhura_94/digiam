import * as dayjs from 'dayjs';
import { IAssetsInRoom } from 'app/entities/assets-in-room/assets-in-room.model';
import { IMeeting } from 'app/entities/meeting/meeting.model';

export interface IMeetingRoom {
  id?: number;
  roomCode?: string | null;
  roomName?: string | null;
  location?: string | null;
  slot?: number | null;
  maxSlot?: number | null;
  createdDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  assetsInRooms?: IAssetsInRoom[] | null;
  meetings?: IMeeting[] | null;
}

export class MeetingRoom implements IMeetingRoom {
  constructor(
    public id?: number,
    public roomCode?: string | null,
    public roomName?: string | null,
    public location?: string | null,
    public slot?: number | null,
    public maxSlot?: number | null,
    public createdDate?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public updatedDate?: dayjs.Dayjs | null,
    public updatedBy?: string | null,
    public assetsInRooms?: IAssetsInRoom[] | null,
    public meetings?: IMeeting[] | null
  ) {}
}

export function getMeetingRoomIdentifier(meetingRoom: IMeetingRoom): number | undefined {
  return meetingRoom.id;
}
