import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MeetingRoomComponent } from './list/meeting-room.component';
import { MeetingRoomDetailComponent } from './detail/meeting-room-detail.component';
import { MeetingRoomUpdateComponent } from './update/meeting-room-update.component';
import { MeetingRoomDeleteDialogComponent } from './delete/meeting-room-delete-dialog.component';
import { MeetingRoomRoutingModule } from './route/meeting-room-routing.module';

@NgModule({
  imports: [SharedModule, MeetingRoomRoutingModule],
  declarations: [MeetingRoomComponent, MeetingRoomDetailComponent, MeetingRoomUpdateComponent, MeetingRoomDeleteDialogComponent],
  entryComponents: [MeetingRoomDeleteDialogComponent],
})
export class MeetingRoomModule {}
