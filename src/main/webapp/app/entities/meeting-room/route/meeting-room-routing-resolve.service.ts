import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMeetingRoom, MeetingRoom } from '../meeting-room.model';
import { MeetingRoomService } from '../service/meeting-room.service';

@Injectable({ providedIn: 'root' })
export class MeetingRoomRoutingResolveService implements Resolve<IMeetingRoom> {
  constructor(protected service: MeetingRoomService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMeetingRoom> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((meetingRoom: HttpResponse<MeetingRoom>) => {
          if (meetingRoom.body) {
            return of(meetingRoom.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MeetingRoom());
  }
}
