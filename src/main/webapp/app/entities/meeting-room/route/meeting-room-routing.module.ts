import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MeetingRoomComponent } from '../list/meeting-room.component';
import { MeetingRoomDetailComponent } from '../detail/meeting-room-detail.component';
import { MeetingRoomUpdateComponent } from '../update/meeting-room-update.component';
import { MeetingRoomRoutingResolveService } from './meeting-room-routing-resolve.service';

const meetingRoomRoute: Routes = [
  {
    path: '',
    component: MeetingRoomComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MeetingRoomDetailComponent,
    resolve: {
      meetingRoom: MeetingRoomRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MeetingRoomUpdateComponent,
    resolve: {
      meetingRoom: MeetingRoomRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MeetingRoomUpdateComponent,
    resolve: {
      meetingRoom: MeetingRoomRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(meetingRoomRoute)],
  exports: [RouterModule],
})
export class MeetingRoomRoutingModule {}
