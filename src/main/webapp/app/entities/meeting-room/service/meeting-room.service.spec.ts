import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IMeetingRoom, MeetingRoom } from '../meeting-room.model';

import { MeetingRoomService } from './meeting-room.service';

describe('Service Tests', () => {
  describe('MeetingRoom Service', () => {
    let service: MeetingRoomService;
    let httpMock: HttpTestingController;
    let elemDefault: IMeetingRoom;
    let expectedResult: IMeetingRoom | IMeetingRoom[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(MeetingRoomService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        roomCode: 'AAAAAAA',
        roomName: 'AAAAAAA',
        location: 'AAAAAAA',
        slot: 0,
        maxSlot: 0,
        createdDate: currentDate,
        createdBy: 'AAAAAAA',
        updatedDate: currentDate,
        updatedBy: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a MeetingRoom', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.create(new MeetingRoom()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a MeetingRoom', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            roomCode: 'BBBBBB',
            roomName: 'BBBBBB',
            location: 'BBBBBB',
            slot: 1,
            maxSlot: 1,
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            createdBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a MeetingRoom', () => {
        const patchObject = Object.assign(
          {
            roomName: 'BBBBBB',
            slot: 1,
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            createdBy: 'BBBBBB',
            updatedBy: 'BBBBBB',
          },
          new MeetingRoom()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of MeetingRoom', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            roomCode: 'BBBBBB',
            roomName: 'BBBBBB',
            location: 'BBBBBB',
            slot: 1,
            maxSlot: 1,
            createdDate: currentDate.format(DATE_TIME_FORMAT),
            createdBy: 'BBBBBB',
            updatedDate: currentDate.format(DATE_TIME_FORMAT),
            updatedBy: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createdDate: currentDate,
            updatedDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MeetingRoom', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addMeetingRoomToCollectionIfMissing', () => {
        it('should add a MeetingRoom to an empty array', () => {
          const meetingRoom: IMeetingRoom = { id: 123 };
          expectedResult = service.addMeetingRoomToCollectionIfMissing([], meetingRoom);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(meetingRoom);
        });

        it('should not add a MeetingRoom to an array that contains it', () => {
          const meetingRoom: IMeetingRoom = { id: 123 };
          const meetingRoomCollection: IMeetingRoom[] = [
            {
              ...meetingRoom,
            },
            { id: 456 },
          ];
          expectedResult = service.addMeetingRoomToCollectionIfMissing(meetingRoomCollection, meetingRoom);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a MeetingRoom to an array that doesn't contain it", () => {
          const meetingRoom: IMeetingRoom = { id: 123 };
          const meetingRoomCollection: IMeetingRoom[] = [{ id: 456 }];
          expectedResult = service.addMeetingRoomToCollectionIfMissing(meetingRoomCollection, meetingRoom);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(meetingRoom);
        });

        it('should add only unique MeetingRoom to an array', () => {
          const meetingRoomArray: IMeetingRoom[] = [{ id: 123 }, { id: 456 }, { id: 45796 }];
          const meetingRoomCollection: IMeetingRoom[] = [{ id: 123 }];
          expectedResult = service.addMeetingRoomToCollectionIfMissing(meetingRoomCollection, ...meetingRoomArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const meetingRoom: IMeetingRoom = { id: 123 };
          const meetingRoom2: IMeetingRoom = { id: 456 };
          expectedResult = service.addMeetingRoomToCollectionIfMissing([], meetingRoom, meetingRoom2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(meetingRoom);
          expect(expectedResult).toContain(meetingRoom2);
        });

        it('should accept null and undefined values', () => {
          const meetingRoom: IMeetingRoom = { id: 123 };
          expectedResult = service.addMeetingRoomToCollectionIfMissing([], null, meetingRoom, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(meetingRoom);
        });

        it('should return initial array if no MeetingRoom is added', () => {
          const meetingRoomCollection: IMeetingRoom[] = [{ id: 123 }];
          expectedResult = service.addMeetingRoomToCollectionIfMissing(meetingRoomCollection, undefined, null);
          expect(expectedResult).toEqual(meetingRoomCollection);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
