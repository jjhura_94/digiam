import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMeetingRoom, getMeetingRoomIdentifier } from '../meeting-room.model';

export type EntityResponseType = HttpResponse<IMeetingRoom>;
export type EntityArrayResponseType = HttpResponse<IMeetingRoom[]>;

@Injectable({ providedIn: 'root' })
export class MeetingRoomService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/meeting-rooms');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(meetingRoom: IMeetingRoom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(meetingRoom);
    return this.http
      .post<IMeetingRoom>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(meetingRoom: IMeetingRoom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(meetingRoom);
    return this.http
      .put<IMeetingRoom>(`${this.resourceUrl}/${getMeetingRoomIdentifier(meetingRoom) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(meetingRoom: IMeetingRoom): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(meetingRoom);
    return this.http
      .patch<IMeetingRoom>(`${this.resourceUrl}/${getMeetingRoomIdentifier(meetingRoom) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMeetingRoom>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMeetingRoom[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMeetingRoomToCollectionIfMissing(
    meetingRoomCollection: IMeetingRoom[],
    ...meetingRoomsToCheck: (IMeetingRoom | null | undefined)[]
  ): IMeetingRoom[] {
    const meetingRooms: IMeetingRoom[] = meetingRoomsToCheck.filter(isPresent);
    if (meetingRooms.length > 0) {
      const meetingRoomCollectionIdentifiers = meetingRoomCollection.map(meetingRoomItem => getMeetingRoomIdentifier(meetingRoomItem)!);
      const meetingRoomsToAdd = meetingRooms.filter(meetingRoomItem => {
        const meetingRoomIdentifier = getMeetingRoomIdentifier(meetingRoomItem);
        if (meetingRoomIdentifier == null || meetingRoomCollectionIdentifiers.includes(meetingRoomIdentifier)) {
          return false;
        }
        meetingRoomCollectionIdentifiers.push(meetingRoomIdentifier);
        return true;
      });
      return [...meetingRoomsToAdd, ...meetingRoomCollection];
    }
    return meetingRoomCollection;
  }

  protected convertDateFromClient(meetingRoom: IMeetingRoom): IMeetingRoom {
    return Object.assign({}, meetingRoom, {
      createdDate: meetingRoom.createdDate?.isValid() ? meetingRoom.createdDate.toJSON() : undefined,
      updatedDate: meetingRoom.updatedDate?.isValid() ? meetingRoom.updatedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.updatedDate = res.body.updatedDate ? dayjs(res.body.updatedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((meetingRoom: IMeetingRoom) => {
        meetingRoom.createdDate = meetingRoom.createdDate ? dayjs(meetingRoom.createdDate) : undefined;
        meetingRoom.updatedDate = meetingRoom.updatedDate ? dayjs(meetingRoom.updatedDate) : undefined;
      });
    }
    return res;
  }
}
