jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MeetingRoomService } from '../service/meeting-room.service';
import { IMeetingRoom, MeetingRoom } from '../meeting-room.model';

import { MeetingRoomUpdateComponent } from './meeting-room-update.component';

describe('Component Tests', () => {
  describe('MeetingRoom Management Update Component', () => {
    let comp: MeetingRoomUpdateComponent;
    let fixture: ComponentFixture<MeetingRoomUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let meetingRoomService: MeetingRoomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MeetingRoomUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(MeetingRoomUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MeetingRoomUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      meetingRoomService = TestBed.inject(MeetingRoomService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const meetingRoom: IMeetingRoom = { id: 456 };

        activatedRoute.data = of({ meetingRoom });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(meetingRoom));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<MeetingRoom>>();
        const meetingRoom = { id: 123 };
        jest.spyOn(meetingRoomService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ meetingRoom });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: meetingRoom }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(meetingRoomService.update).toHaveBeenCalledWith(meetingRoom);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<MeetingRoom>>();
        const meetingRoom = new MeetingRoom();
        jest.spyOn(meetingRoomService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ meetingRoom });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: meetingRoom }));
        saveSubject.complete();

        // THEN
        expect(meetingRoomService.create).toHaveBeenCalledWith(meetingRoom);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<MeetingRoom>>();
        const meetingRoom = { id: 123 };
        jest.spyOn(meetingRoomService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ meetingRoom });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(meetingRoomService.update).toHaveBeenCalledWith(meetingRoom);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
