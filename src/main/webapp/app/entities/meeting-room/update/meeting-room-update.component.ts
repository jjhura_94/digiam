import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IMeetingRoom, MeetingRoom } from '../meeting-room.model';
import { MeetingRoomService } from '../service/meeting-room.service';

@Component({
  selector: 'jhi-meeting-room-update',
  templateUrl: './meeting-room-update.component.html',
})
export class MeetingRoomUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    roomCode: [],
    roomName: [],
    location: [],
    slot: [],
    maxSlot: [],
    createdDate: [],
    createdBy: [],
    updatedDate: [],
    updatedBy: [],
  });

  constructor(protected meetingRoomService: MeetingRoomService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ meetingRoom }) => {
      if (meetingRoom.id === undefined) {
        const today = dayjs().startOf('day');
        meetingRoom.createdDate = today;
        meetingRoom.updatedDate = today;
      }

      this.updateForm(meetingRoom);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const meetingRoom = this.createFromForm();
    if (meetingRoom.id !== undefined) {
      this.subscribeToSaveResponse(this.meetingRoomService.update(meetingRoom));
    } else {
      this.subscribeToSaveResponse(this.meetingRoomService.create(meetingRoom));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMeetingRoom>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(meetingRoom: IMeetingRoom): void {
    this.editForm.patchValue({
      id: meetingRoom.id,
      roomCode: meetingRoom.roomCode,
      roomName: meetingRoom.roomName,
      location: meetingRoom.location,
      slot: meetingRoom.slot,
      maxSlot: meetingRoom.maxSlot,
      createdDate: meetingRoom.createdDate ? meetingRoom.createdDate.format(DATE_TIME_FORMAT) : null,
      createdBy: meetingRoom.createdBy,
      updatedDate: meetingRoom.updatedDate ? meetingRoom.updatedDate.format(DATE_TIME_FORMAT) : null,
      updatedBy: meetingRoom.updatedBy,
    });
  }

  protected createFromForm(): IMeetingRoom {
    return {
      ...new MeetingRoom(),
      id: this.editForm.get(['id'])!.value,
      roomCode: this.editForm.get(['roomCode'])!.value,
      roomName: this.editForm.get(['roomName'])!.value,
      location: this.editForm.get(['location'])!.value,
      slot: this.editForm.get(['slot'])!.value,
      maxSlot: this.editForm.get(['maxSlot'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      updatedDate: this.editForm.get(['updatedDate'])!.value
        ? dayjs(this.editForm.get(['updatedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
    };
  }
}
