import * as dayjs from 'dayjs';
import { IEmpJoinMeeting } from 'app/entities/emp-join-meeting/emp-join-meeting.model';
import { IMeetingRoom } from 'app/entities/meeting-room/meeting-room.model';

export interface IMeeting {
  id?: number;
  meetingFrom?: dayjs.Dayjs | null;
  meetingTo?: dayjs.Dayjs | null;
  offline?: boolean | null;
  roomOnlineLink?: string | null;
  createdDate?: dayjs.Dayjs | null;
  createdBy?: string | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  empJoinMeetings?: IEmpJoinMeeting[] | null;
  room?: IMeetingRoom | null;
}

export class Meeting implements IMeeting {
  constructor(
    public id?: number,
    public meetingFrom?: dayjs.Dayjs | null,
    public meetingTo?: dayjs.Dayjs | null,
    public offline?: boolean | null,
    public roomOnlineLink?: string | null,
    public createdDate?: dayjs.Dayjs | null,
    public createdBy?: string | null,
    public updatedDate?: dayjs.Dayjs | null,
    public updatedBy?: string | null,
    public empJoinMeetings?: IEmpJoinMeeting[] | null,
    public room?: IMeetingRoom | null
  ) {
    this.offline = this.offline ?? false;
  }
}

export function getMeetingIdentifier(meeting: IMeeting): number | undefined {
  return meeting.id;
}
