jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MeetingService } from '../service/meeting.service';
import { IMeeting, Meeting } from '../meeting.model';
import { IMeetingRoom } from 'app/entities/meeting-room/meeting-room.model';
import { MeetingRoomService } from 'app/entities/meeting-room/service/meeting-room.service';

import { MeetingUpdateComponent } from './meeting-update.component';

describe('Component Tests', () => {
  describe('Meeting Management Update Component', () => {
    let comp: MeetingUpdateComponent;
    let fixture: ComponentFixture<MeetingUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let meetingService: MeetingService;
    let meetingRoomService: MeetingRoomService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MeetingUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(MeetingUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MeetingUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      meetingService = TestBed.inject(MeetingService);
      meetingRoomService = TestBed.inject(MeetingRoomService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call MeetingRoom query and add missing value', () => {
        const meeting: IMeeting = { id: 456 };
        const room: IMeetingRoom = { id: 33129 };
        meeting.room = room;

        const meetingRoomCollection: IMeetingRoom[] = [{ id: 43215 }];
        jest.spyOn(meetingRoomService, 'query').mockReturnValue(of(new HttpResponse({ body: meetingRoomCollection })));
        const additionalMeetingRooms = [room];
        const expectedCollection: IMeetingRoom[] = [...additionalMeetingRooms, ...meetingRoomCollection];
        jest.spyOn(meetingRoomService, 'addMeetingRoomToCollectionIfMissing').mockReturnValue(expectedCollection);

        activatedRoute.data = of({ meeting });
        comp.ngOnInit();

        expect(meetingRoomService.query).toHaveBeenCalled();
        expect(meetingRoomService.addMeetingRoomToCollectionIfMissing).toHaveBeenCalledWith(
          meetingRoomCollection,
          ...additionalMeetingRooms
        );
        expect(comp.meetingRoomsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const meeting: IMeeting = { id: 456 };
        const room: IMeetingRoom = { id: 7253 };
        meeting.room = room;

        activatedRoute.data = of({ meeting });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(meeting));
        expect(comp.meetingRoomsSharedCollection).toContain(room);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Meeting>>();
        const meeting = { id: 123 };
        jest.spyOn(meetingService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ meeting });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: meeting }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(meetingService.update).toHaveBeenCalledWith(meeting);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Meeting>>();
        const meeting = new Meeting();
        jest.spyOn(meetingService, 'create').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ meeting });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: meeting }));
        saveSubject.complete();

        // THEN
        expect(meetingService.create).toHaveBeenCalledWith(meeting);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject<HttpResponse<Meeting>>();
        const meeting = { id: 123 };
        jest.spyOn(meetingService, 'update').mockReturnValue(saveSubject);
        jest.spyOn(comp, 'previousState');
        activatedRoute.data = of({ meeting });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(meetingService.update).toHaveBeenCalledWith(meeting);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackMeetingRoomById', () => {
        it('Should return tracked MeetingRoom primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMeetingRoomById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
