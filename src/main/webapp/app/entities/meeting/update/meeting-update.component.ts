import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IMeeting, Meeting } from '../meeting.model';
import { MeetingService } from '../service/meeting.service';
import { IMeetingRoom } from 'app/entities/meeting-room/meeting-room.model';
import { MeetingRoomService } from 'app/entities/meeting-room/service/meeting-room.service';

@Component({
  selector: 'jhi-meeting-update',
  templateUrl: './meeting-update.component.html',
})
export class MeetingUpdateComponent implements OnInit {
  isSaving = false;

  meetingRoomsSharedCollection: IMeetingRoom[] = [];

  editForm = this.fb.group({
    id: [],
    meetingFrom: [],
    meetingTo: [],
    offline: [],
    roomOnlineLink: [],
    createdDate: [],
    createdBy: [],
    updatedDate: [],
    updatedBy: [],
    room: [],
  });

  constructor(
    protected meetingService: MeetingService,
    protected meetingRoomService: MeetingRoomService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ meeting }) => {
      if (meeting.id === undefined) {
        const today = dayjs().startOf('day');
        meeting.meetingFrom = today;
        meeting.meetingTo = today;
        meeting.createdDate = today;
        meeting.updatedDate = today;
      }

      this.updateForm(meeting);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const meeting = this.createFromForm();
    if (meeting.id !== undefined) {
      this.subscribeToSaveResponse(this.meetingService.update(meeting));
    } else {
      this.subscribeToSaveResponse(this.meetingService.create(meeting));
    }
  }

  trackMeetingRoomById(index: number, item: IMeetingRoom): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMeeting>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(meeting: IMeeting): void {
    this.editForm.patchValue({
      id: meeting.id,
      meetingFrom: meeting.meetingFrom ? meeting.meetingFrom.format(DATE_TIME_FORMAT) : null,
      meetingTo: meeting.meetingTo ? meeting.meetingTo.format(DATE_TIME_FORMAT) : null,
      offline: meeting.offline,
      roomOnlineLink: meeting.roomOnlineLink,
      createdDate: meeting.createdDate ? meeting.createdDate.format(DATE_TIME_FORMAT) : null,
      createdBy: meeting.createdBy,
      updatedDate: meeting.updatedDate ? meeting.updatedDate.format(DATE_TIME_FORMAT) : null,
      updatedBy: meeting.updatedBy,
      room: meeting.room,
    });

    this.meetingRoomsSharedCollection = this.meetingRoomService.addMeetingRoomToCollectionIfMissing(
      this.meetingRoomsSharedCollection,
      meeting.room
    );
  }

  protected loadRelationshipsOptions(): void {
    this.meetingRoomService
      .query()
      .pipe(map((res: HttpResponse<IMeetingRoom[]>) => res.body ?? []))
      .pipe(
        map((meetingRooms: IMeetingRoom[]) =>
          this.meetingRoomService.addMeetingRoomToCollectionIfMissing(meetingRooms, this.editForm.get('room')!.value)
        )
      )
      .subscribe((meetingRooms: IMeetingRoom[]) => (this.meetingRoomsSharedCollection = meetingRooms));
  }

  protected createFromForm(): IMeeting {
    return {
      ...new Meeting(),
      id: this.editForm.get(['id'])!.value,
      meetingFrom: this.editForm.get(['meetingFrom'])!.value
        ? dayjs(this.editForm.get(['meetingFrom'])!.value, DATE_TIME_FORMAT)
        : undefined,
      meetingTo: this.editForm.get(['meetingTo'])!.value ? dayjs(this.editForm.get(['meetingTo'])!.value, DATE_TIME_FORMAT) : undefined,
      offline: this.editForm.get(['offline'])!.value,
      roomOnlineLink: this.editForm.get(['roomOnlineLink'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      createdBy: this.editForm.get(['createdBy'])!.value,
      updatedDate: this.editForm.get(['updatedDate'])!.value
        ? dayjs(this.editForm.get(['updatedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      updatedBy: this.editForm.get(['updatedBy'])!.value,
      room: this.editForm.get(['room'])!.value,
    };
  }
}
