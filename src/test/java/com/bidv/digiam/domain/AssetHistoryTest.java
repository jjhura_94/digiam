package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AssetHistoryTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssetHistory.class);
        AssetHistory assetHistory1 = new AssetHistory();
        assetHistory1.setId(1L);
        AssetHistory assetHistory2 = new AssetHistory();
        assetHistory2.setId(assetHistory1.getId());
        assertThat(assetHistory1).isEqualTo(assetHistory2);
        assetHistory2.setId(2L);
        assertThat(assetHistory1).isNotEqualTo(assetHistory2);
        assetHistory1.setId(null);
        assertThat(assetHistory1).isNotEqualTo(assetHistory2);
    }
}
