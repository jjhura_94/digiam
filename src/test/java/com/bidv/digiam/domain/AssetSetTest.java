package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AssetSetTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssetSet.class);
        AssetSet assetSet1 = new AssetSet();
        assetSet1.setId(1L);
        AssetSet assetSet2 = new AssetSet();
        assetSet2.setId(assetSet1.getId());
        assertThat(assetSet1).isEqualTo(assetSet2);
        assetSet2.setId(2L);
        assertThat(assetSet1).isNotEqualTo(assetSet2);
        assetSet1.setId(null);
        assertThat(assetSet1).isNotEqualTo(assetSet2);
    }
}
