package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AssetsInRoomTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssetsInRoom.class);
        AssetsInRoom assetsInRoom1 = new AssetsInRoom();
        assetsInRoom1.setId(1L);
        AssetsInRoom assetsInRoom2 = new AssetsInRoom();
        assetsInRoom2.setId(assetsInRoom1.getId());
        assertThat(assetsInRoom1).isEqualTo(assetsInRoom2);
        assetsInRoom2.setId(2L);
        assertThat(assetsInRoom1).isNotEqualTo(assetsInRoom2);
        assetsInRoom1.setId(null);
        assertThat(assetsInRoom1).isNotEqualTo(assetsInRoom2);
    }
}
