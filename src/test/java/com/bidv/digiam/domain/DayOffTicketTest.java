package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DayOffTicketTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DayOffTicket.class);
        DayOffTicket dayOffTicket1 = new DayOffTicket();
        dayOffTicket1.setId(1L);
        DayOffTicket dayOffTicket2 = new DayOffTicket();
        dayOffTicket2.setId(dayOffTicket1.getId());
        assertThat(dayOffTicket1).isEqualTo(dayOffTicket2);
        dayOffTicket2.setId(2L);
        assertThat(dayOffTicket1).isNotEqualTo(dayOffTicket2);
        dayOffTicket1.setId(null);
        assertThat(dayOffTicket1).isNotEqualTo(dayOffTicket2);
    }
}
