package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DayOffTypeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DayOffType.class);
        DayOffType dayOffType1 = new DayOffType();
        dayOffType1.setId(1L);
        DayOffType dayOffType2 = new DayOffType();
        dayOffType2.setId(dayOffType1.getId());
        assertThat(dayOffType1).isEqualTo(dayOffType2);
        dayOffType2.setId(2L);
        assertThat(dayOffType1).isNotEqualTo(dayOffType2);
        dayOffType1.setId(null);
        assertThat(dayOffType1).isNotEqualTo(dayOffType2);
    }
}
