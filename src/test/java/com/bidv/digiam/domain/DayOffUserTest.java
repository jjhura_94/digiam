package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DayOffUserTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DayOffUser.class);
        DayOffUser dayOffUser1 = new DayOffUser();
        dayOffUser1.setId(1L);
        DayOffUser dayOffUser2 = new DayOffUser();
        dayOffUser2.setId(dayOffUser1.getId());
        assertThat(dayOffUser1).isEqualTo(dayOffUser2);
        dayOffUser2.setId(2L);
        assertThat(dayOffUser1).isNotEqualTo(dayOffUser2);
        dayOffUser1.setId(null);
        assertThat(dayOffUser1).isNotEqualTo(dayOffUser2);
    }
}
