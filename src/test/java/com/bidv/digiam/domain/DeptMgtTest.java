package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DeptMgtTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeptMgt.class);
        DeptMgt deptMgt1 = new DeptMgt();
        deptMgt1.setId(1L);
        DeptMgt deptMgt2 = new DeptMgt();
        deptMgt2.setId(deptMgt1.getId());
        assertThat(deptMgt1).isEqualTo(deptMgt2);
        deptMgt2.setId(2L);
        assertThat(deptMgt1).isNotEqualTo(deptMgt2);
        deptMgt1.setId(null);
        assertThat(deptMgt1).isNotEqualTo(deptMgt2);
    }
}
