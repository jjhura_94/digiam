package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DutyMgtTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DutyMgt.class);
        DutyMgt dutyMgt1 = new DutyMgt();
        dutyMgt1.setId(1L);
        DutyMgt dutyMgt2 = new DutyMgt();
        dutyMgt2.setId(dutyMgt1.getId());
        assertThat(dutyMgt1).isEqualTo(dutyMgt2);
        dutyMgt2.setId(2L);
        assertThat(dutyMgt1).isNotEqualTo(dutyMgt2);
        dutyMgt1.setId(null);
        assertThat(dutyMgt1).isNotEqualTo(dutyMgt2);
    }
}
