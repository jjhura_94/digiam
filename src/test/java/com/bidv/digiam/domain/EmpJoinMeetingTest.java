package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EmpJoinMeetingTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmpJoinMeeting.class);
        EmpJoinMeeting empJoinMeeting1 = new EmpJoinMeeting();
        empJoinMeeting1.setId(1L);
        EmpJoinMeeting empJoinMeeting2 = new EmpJoinMeeting();
        empJoinMeeting2.setId(empJoinMeeting1.getId());
        assertThat(empJoinMeeting1).isEqualTo(empJoinMeeting2);
        empJoinMeeting2.setId(2L);
        assertThat(empJoinMeeting1).isNotEqualTo(empJoinMeeting2);
        empJoinMeeting1.setId(null);
        assertThat(empJoinMeeting1).isNotEqualTo(empJoinMeeting2);
    }
}
