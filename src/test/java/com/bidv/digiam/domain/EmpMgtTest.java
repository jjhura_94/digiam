package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EmpMgtTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmpMgt.class);
        EmpMgt empMgt1 = new EmpMgt();
        empMgt1.setId(1L);
        EmpMgt empMgt2 = new EmpMgt();
        empMgt2.setId(empMgt1.getId());
        assertThat(empMgt1).isEqualTo(empMgt2);
        empMgt2.setId(2L);
        assertThat(empMgt1).isNotEqualTo(empMgt2);
        empMgt1.setId(null);
        assertThat(empMgt1).isNotEqualTo(empMgt2);
    }
}
