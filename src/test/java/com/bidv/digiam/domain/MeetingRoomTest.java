package com.bidv.digiam.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.bidv.digiam.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MeetingRoomTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MeetingRoom.class);
        MeetingRoom meetingRoom1 = new MeetingRoom();
        meetingRoom1.setId(1L);
        MeetingRoom meetingRoom2 = new MeetingRoom();
        meetingRoom2.setId(meetingRoom1.getId());
        assertThat(meetingRoom1).isEqualTo(meetingRoom2);
        meetingRoom2.setId(2L);
        assertThat(meetingRoom1).isNotEqualTo(meetingRoom2);
        meetingRoom1.setId(null);
        assertThat(meetingRoom1).isNotEqualTo(meetingRoom2);
    }
}
