package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.AssetHistory;
import com.bidv.digiam.repository.AssetHistoryRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AssetHistoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AssetHistoryResourceIT {

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/asset-histories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AssetHistoryRepository assetHistoryRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssetHistoryMockMvc;

    private AssetHistory assetHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetHistory createEntity(EntityManager em) {
        AssetHistory assetHistory = new AssetHistory()
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .status(DEFAULT_STATUS)
            .note(DEFAULT_NOTE);
        return assetHistory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetHistory createUpdatedEntity(EntityManager em) {
        AssetHistory assetHistory = new AssetHistory()
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .status(UPDATED_STATUS)
            .note(UPDATED_NOTE);
        return assetHistory;
    }

    @BeforeEach
    public void initTest() {
        assetHistory = createEntity(em);
    }

    @Test
    @Transactional
    void createAssetHistory() throws Exception {
        int databaseSizeBeforeCreate = assetHistoryRepository.findAll().size();
        // Create the AssetHistory
        restAssetHistoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetHistory)))
            .andExpect(status().isCreated());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        AssetHistory testAssetHistory = assetHistoryList.get(assetHistoryList.size() - 1);
        assertThat(testAssetHistory.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testAssetHistory.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testAssetHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAssetHistory.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    void createAssetHistoryWithExistingId() throws Exception {
        // Create the AssetHistory with an existing ID
        assetHistory.setId(1L);

        int databaseSizeBeforeCreate = assetHistoryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssetHistoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetHistory)))
            .andExpect(status().isBadRequest());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAssetHistories() throws Exception {
        // Initialize the database
        assetHistoryRepository.saveAndFlush(assetHistory);

        // Get all the assetHistoryList
        restAssetHistoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assetHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)));
    }

    @Test
    @Transactional
    void getAssetHistory() throws Exception {
        // Initialize the database
        assetHistoryRepository.saveAndFlush(assetHistory);

        // Get the assetHistory
        restAssetHistoryMockMvc
            .perform(get(ENTITY_API_URL_ID, assetHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assetHistory.getId().intValue()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE));
    }

    @Test
    @Transactional
    void getNonExistingAssetHistory() throws Exception {
        // Get the assetHistory
        restAssetHistoryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAssetHistory() throws Exception {
        // Initialize the database
        assetHistoryRepository.saveAndFlush(assetHistory);

        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();

        // Update the assetHistory
        AssetHistory updatedAssetHistory = assetHistoryRepository.findById(assetHistory.getId()).get();
        // Disconnect from session so that the updates on updatedAssetHistory are not directly saved in db
        em.detach(updatedAssetHistory);
        updatedAssetHistory.updatedBy(UPDATED_UPDATED_BY).updatedDate(UPDATED_UPDATED_DATE).status(UPDATED_STATUS).note(UPDATED_NOTE);

        restAssetHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAssetHistory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAssetHistory))
            )
            .andExpect(status().isOk());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
        AssetHistory testAssetHistory = assetHistoryList.get(assetHistoryList.size() - 1);
        assertThat(testAssetHistory.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testAssetHistory.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testAssetHistory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAssetHistory.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    void putNonExistingAssetHistory() throws Exception {
        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();
        assetHistory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, assetHistory.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(assetHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAssetHistory() throws Exception {
        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();
        assetHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetHistoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(assetHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAssetHistory() throws Exception {
        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();
        assetHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetHistoryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetHistory)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAssetHistoryWithPatch() throws Exception {
        // Initialize the database
        assetHistoryRepository.saveAndFlush(assetHistory);

        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();

        // Update the assetHistory using partial update
        AssetHistory partialUpdatedAssetHistory = new AssetHistory();
        partialUpdatedAssetHistory.setId(assetHistory.getId());

        partialUpdatedAssetHistory.note(UPDATED_NOTE);

        restAssetHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAssetHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAssetHistory))
            )
            .andExpect(status().isOk());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
        AssetHistory testAssetHistory = assetHistoryList.get(assetHistoryList.size() - 1);
        assertThat(testAssetHistory.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testAssetHistory.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testAssetHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAssetHistory.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    void fullUpdateAssetHistoryWithPatch() throws Exception {
        // Initialize the database
        assetHistoryRepository.saveAndFlush(assetHistory);

        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();

        // Update the assetHistory using partial update
        AssetHistory partialUpdatedAssetHistory = new AssetHistory();
        partialUpdatedAssetHistory.setId(assetHistory.getId());

        partialUpdatedAssetHistory
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .status(UPDATED_STATUS)
            .note(UPDATED_NOTE);

        restAssetHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAssetHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAssetHistory))
            )
            .andExpect(status().isOk());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
        AssetHistory testAssetHistory = assetHistoryList.get(assetHistoryList.size() - 1);
        assertThat(testAssetHistory.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testAssetHistory.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testAssetHistory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAssetHistory.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    void patchNonExistingAssetHistory() throws Exception {
        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();
        assetHistory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, assetHistory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(assetHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAssetHistory() throws Exception {
        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();
        assetHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(assetHistory))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAssetHistory() throws Exception {
        int databaseSizeBeforeUpdate = assetHistoryRepository.findAll().size();
        assetHistory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetHistoryMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(assetHistory))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AssetHistory in the database
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAssetHistory() throws Exception {
        // Initialize the database
        assetHistoryRepository.saveAndFlush(assetHistory);

        int databaseSizeBeforeDelete = assetHistoryRepository.findAll().size();

        // Delete the assetHistory
        restAssetHistoryMockMvc
            .perform(delete(ENTITY_API_URL_ID, assetHistory.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AssetHistory> assetHistoryList = assetHistoryRepository.findAll();
        assertThat(assetHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
