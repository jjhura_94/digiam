package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.Asset;
import com.bidv.digiam.repository.AssetRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AssetResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AssetResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;

    private static final Instant DEFAULT_EXPIRY_DATE_FROM = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EXPIRY_DATE_FROM = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_EXPIRY_DATE_TO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EXPIRY_DATE_TO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final String DEFAULT_IMAGE = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE = "BBBBBBBBBB";

    private static final String DEFAULT_QR_CODE = "AAAAAAAAAA";
    private static final String UPDATED_QR_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_SERIAL_NO = "AAAAAAAAAA";
    private static final String UPDATED_SERIAL_NO = "BBBBBBBBBB";

    private static final Instant DEFAULT_GUARANTEE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_GUARANTEE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/assets";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssetMockMvc;

    private Asset asset;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asset createEntity(EntityManager em) {
        Asset asset = new Asset()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .price(DEFAULT_PRICE)
            .expiryDateFrom(DEFAULT_EXPIRY_DATE_FROM)
            .expiryDateTo(DEFAULT_EXPIRY_DATE_TO)
            .status(DEFAULT_STATUS)
            .image(DEFAULT_IMAGE)
            .qrCode(DEFAULT_QR_CODE)
            .location(DEFAULT_LOCATION)
            .note(DEFAULT_NOTE)
            .serialNo(DEFAULT_SERIAL_NO)
            .guarantee(DEFAULT_GUARANTEE)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY);
        return asset;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Asset createUpdatedEntity(EntityManager em) {
        Asset asset = new Asset()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .price(UPDATED_PRICE)
            .expiryDateFrom(UPDATED_EXPIRY_DATE_FROM)
            .expiryDateTo(UPDATED_EXPIRY_DATE_TO)
            .status(UPDATED_STATUS)
            .image(UPDATED_IMAGE)
            .qrCode(UPDATED_QR_CODE)
            .location(UPDATED_LOCATION)
            .note(UPDATED_NOTE)
            .serialNo(UPDATED_SERIAL_NO)
            .guarantee(UPDATED_GUARANTEE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        return asset;
    }

    @BeforeEach
    public void initTest() {
        asset = createEntity(em);
    }

    @Test
    @Transactional
    void createAsset() throws Exception {
        int databaseSizeBeforeCreate = assetRepository.findAll().size();
        // Create the Asset
        restAssetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isCreated());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeCreate + 1);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAsset.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testAsset.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testAsset.getExpiryDateFrom()).isEqualTo(DEFAULT_EXPIRY_DATE_FROM);
        assertThat(testAsset.getExpiryDateTo()).isEqualTo(DEFAULT_EXPIRY_DATE_TO);
        assertThat(testAsset.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAsset.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testAsset.getQrCode()).isEqualTo(DEFAULT_QR_CODE);
        assertThat(testAsset.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testAsset.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testAsset.getSerialNo()).isEqualTo(DEFAULT_SERIAL_NO);
        assertThat(testAsset.getGuarantee()).isEqualTo(DEFAULT_GUARANTEE);
        assertThat(testAsset.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testAsset.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testAsset.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testAsset.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createAssetWithExistingId() throws Exception {
        // Create the Asset with an existing ID
        asset.setId(1L);

        int databaseSizeBeforeCreate = assetRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAssets() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        // Get all the assetList
        restAssetMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(asset.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].expiryDateFrom").value(hasItem(DEFAULT_EXPIRY_DATE_FROM.toString())))
            .andExpect(jsonPath("$.[*].expiryDateTo").value(hasItem(DEFAULT_EXPIRY_DATE_TO.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.[*].qrCode").value(hasItem(DEFAULT_QR_CODE)))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].serialNo").value(hasItem(DEFAULT_SERIAL_NO)))
            .andExpect(jsonPath("$.[*].guarantee").value(hasItem(DEFAULT_GUARANTEE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getAsset() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        // Get the asset
        restAssetMockMvc
            .perform(get(ENTITY_API_URL_ID, asset.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(asset.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.expiryDateFrom").value(DEFAULT_EXPIRY_DATE_FROM.toString()))
            .andExpect(jsonPath("$.expiryDateTo").value(DEFAULT_EXPIRY_DATE_TO.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.image").value(DEFAULT_IMAGE))
            .andExpect(jsonPath("$.qrCode").value(DEFAULT_QR_CODE))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.serialNo").value(DEFAULT_SERIAL_NO))
            .andExpect(jsonPath("$.guarantee").value(DEFAULT_GUARANTEE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingAsset() throws Exception {
        // Get the asset
        restAssetMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAsset() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        int databaseSizeBeforeUpdate = assetRepository.findAll().size();

        // Update the asset
        Asset updatedAsset = assetRepository.findById(asset.getId()).get();
        // Disconnect from session so that the updates on updatedAsset are not directly saved in db
        em.detach(updatedAsset);
        updatedAsset
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .price(UPDATED_PRICE)
            .expiryDateFrom(UPDATED_EXPIRY_DATE_FROM)
            .expiryDateTo(UPDATED_EXPIRY_DATE_TO)
            .status(UPDATED_STATUS)
            .image(UPDATED_IMAGE)
            .qrCode(UPDATED_QR_CODE)
            .location(UPDATED_LOCATION)
            .note(UPDATED_NOTE)
            .serialNo(UPDATED_SERIAL_NO)
            .guarantee(UPDATED_GUARANTEE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restAssetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAsset.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAsset))
            )
            .andExpect(status().isOk());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAsset.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testAsset.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testAsset.getExpiryDateFrom()).isEqualTo(UPDATED_EXPIRY_DATE_FROM);
        assertThat(testAsset.getExpiryDateTo()).isEqualTo(UPDATED_EXPIRY_DATE_TO);
        assertThat(testAsset.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAsset.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testAsset.getQrCode()).isEqualTo(UPDATED_QR_CODE);
        assertThat(testAsset.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testAsset.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testAsset.getSerialNo()).isEqualTo(UPDATED_SERIAL_NO);
        assertThat(testAsset.getGuarantee()).isEqualTo(UPDATED_GUARANTEE);
        assertThat(testAsset.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAsset.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testAsset.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testAsset.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();
        asset.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, asset.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(asset))
            )
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();
        asset.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(asset))
            )
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();
        asset.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAssetWithPatch() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        int databaseSizeBeforeUpdate = assetRepository.findAll().size();

        // Update the asset using partial update
        Asset partialUpdatedAsset = new Asset();
        partialUpdatedAsset.setId(asset.getId());

        partialUpdatedAsset
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE)
            .expiryDateTo(UPDATED_EXPIRY_DATE_TO)
            .status(UPDATED_STATUS)
            .location(UPDATED_LOCATION)
            .serialNo(UPDATED_SERIAL_NO)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restAssetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAsset.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAsset))
            )
            .andExpect(status().isOk());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAsset.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testAsset.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testAsset.getExpiryDateFrom()).isEqualTo(DEFAULT_EXPIRY_DATE_FROM);
        assertThat(testAsset.getExpiryDateTo()).isEqualTo(UPDATED_EXPIRY_DATE_TO);
        assertThat(testAsset.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAsset.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testAsset.getQrCode()).isEqualTo(DEFAULT_QR_CODE);
        assertThat(testAsset.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testAsset.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testAsset.getSerialNo()).isEqualTo(UPDATED_SERIAL_NO);
        assertThat(testAsset.getGuarantee()).isEqualTo(DEFAULT_GUARANTEE);
        assertThat(testAsset.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAsset.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testAsset.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testAsset.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateAssetWithPatch() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        int databaseSizeBeforeUpdate = assetRepository.findAll().size();

        // Update the asset using partial update
        Asset partialUpdatedAsset = new Asset();
        partialUpdatedAsset.setId(asset.getId());

        partialUpdatedAsset
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .price(UPDATED_PRICE)
            .expiryDateFrom(UPDATED_EXPIRY_DATE_FROM)
            .expiryDateTo(UPDATED_EXPIRY_DATE_TO)
            .status(UPDATED_STATUS)
            .image(UPDATED_IMAGE)
            .qrCode(UPDATED_QR_CODE)
            .location(UPDATED_LOCATION)
            .note(UPDATED_NOTE)
            .serialNo(UPDATED_SERIAL_NO)
            .guarantee(UPDATED_GUARANTEE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restAssetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAsset.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAsset))
            )
            .andExpect(status().isOk());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
        Asset testAsset = assetList.get(assetList.size() - 1);
        assertThat(testAsset.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAsset.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testAsset.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testAsset.getExpiryDateFrom()).isEqualTo(UPDATED_EXPIRY_DATE_FROM);
        assertThat(testAsset.getExpiryDateTo()).isEqualTo(UPDATED_EXPIRY_DATE_TO);
        assertThat(testAsset.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAsset.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testAsset.getQrCode()).isEqualTo(UPDATED_QR_CODE);
        assertThat(testAsset.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testAsset.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testAsset.getSerialNo()).isEqualTo(UPDATED_SERIAL_NO);
        assertThat(testAsset.getGuarantee()).isEqualTo(UPDATED_GUARANTEE);
        assertThat(testAsset.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAsset.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testAsset.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testAsset.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();
        asset.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, asset.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(asset))
            )
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();
        asset.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(asset))
            )
            .andExpect(status().isBadRequest());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAsset() throws Exception {
        int databaseSizeBeforeUpdate = assetRepository.findAll().size();
        asset.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(asset)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Asset in the database
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAsset() throws Exception {
        // Initialize the database
        assetRepository.saveAndFlush(asset);

        int databaseSizeBeforeDelete = assetRepository.findAll().size();

        // Delete the asset
        restAssetMockMvc
            .perform(delete(ENTITY_API_URL_ID, asset.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Asset> assetList = assetRepository.findAll();
        assertThat(assetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
