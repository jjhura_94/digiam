package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.AssetSet;
import com.bidv.digiam.repository.AssetSetRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AssetSetResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AssetSetResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/asset-sets";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AssetSetRepository assetSetRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssetSetMockMvc;

    private AssetSet assetSet;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetSet createEntity(EntityManager em) {
        AssetSet assetSet = new AssetSet()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return assetSet;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetSet createUpdatedEntity(EntityManager em) {
        AssetSet assetSet = new AssetSet()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        return assetSet;
    }

    @BeforeEach
    public void initTest() {
        assetSet = createEntity(em);
    }

    @Test
    @Transactional
    void createAssetSet() throws Exception {
        int databaseSizeBeforeCreate = assetSetRepository.findAll().size();
        // Create the AssetSet
        restAssetSetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetSet)))
            .andExpect(status().isCreated());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeCreate + 1);
        AssetSet testAssetSet = assetSetList.get(assetSetList.size() - 1);
        assertThat(testAssetSet.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAssetSet.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testAssetSet.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testAssetSet.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    void createAssetSetWithExistingId() throws Exception {
        // Create the AssetSet with an existing ID
        assetSet.setId(1L);

        int databaseSizeBeforeCreate = assetSetRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssetSetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetSet)))
            .andExpect(status().isBadRequest());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAssetSets() throws Exception {
        // Initialize the database
        assetSetRepository.saveAndFlush(assetSet);

        // Get all the assetSetList
        restAssetSetMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assetSet.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    void getAssetSet() throws Exception {
        // Initialize the database
        assetSetRepository.saveAndFlush(assetSet);

        // Get the assetSet
        restAssetSetMockMvc
            .perform(get(ENTITY_API_URL_ID, assetSet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assetSet.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingAssetSet() throws Exception {
        // Get the assetSet
        restAssetSetMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAssetSet() throws Exception {
        // Initialize the database
        assetSetRepository.saveAndFlush(assetSet);

        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();

        // Update the assetSet
        AssetSet updatedAssetSet = assetSetRepository.findById(assetSet.getId()).get();
        // Disconnect from session so that the updates on updatedAssetSet are not directly saved in db
        em.detach(updatedAssetSet);
        updatedAssetSet.name(UPDATED_NAME).code(UPDATED_CODE).updatedBy(UPDATED_UPDATED_BY).updatedDate(UPDATED_UPDATED_DATE);

        restAssetSetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAssetSet.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAssetSet))
            )
            .andExpect(status().isOk());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
        AssetSet testAssetSet = assetSetList.get(assetSetList.size() - 1);
        assertThat(testAssetSet.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAssetSet.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testAssetSet.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testAssetSet.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingAssetSet() throws Exception {
        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();
        assetSet.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetSetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, assetSet.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(assetSet))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAssetSet() throws Exception {
        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();
        assetSet.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetSetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(assetSet))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAssetSet() throws Exception {
        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();
        assetSet.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetSetMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetSet)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAssetSetWithPatch() throws Exception {
        // Initialize the database
        assetSetRepository.saveAndFlush(assetSet);

        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();

        // Update the assetSet using partial update
        AssetSet partialUpdatedAssetSet = new AssetSet();
        partialUpdatedAssetSet.setId(assetSet.getId());

        partialUpdatedAssetSet.name(UPDATED_NAME).updatedBy(UPDATED_UPDATED_BY).updatedDate(UPDATED_UPDATED_DATE);

        restAssetSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAssetSet.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAssetSet))
            )
            .andExpect(status().isOk());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
        AssetSet testAssetSet = assetSetList.get(assetSetList.size() - 1);
        assertThat(testAssetSet.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAssetSet.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testAssetSet.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testAssetSet.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void fullUpdateAssetSetWithPatch() throws Exception {
        // Initialize the database
        assetSetRepository.saveAndFlush(assetSet);

        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();

        // Update the assetSet using partial update
        AssetSet partialUpdatedAssetSet = new AssetSet();
        partialUpdatedAssetSet.setId(assetSet.getId());

        partialUpdatedAssetSet.name(UPDATED_NAME).code(UPDATED_CODE).updatedBy(UPDATED_UPDATED_BY).updatedDate(UPDATED_UPDATED_DATE);

        restAssetSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAssetSet.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAssetSet))
            )
            .andExpect(status().isOk());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
        AssetSet testAssetSet = assetSetList.get(assetSetList.size() - 1);
        assertThat(testAssetSet.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAssetSet.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testAssetSet.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testAssetSet.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingAssetSet() throws Exception {
        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();
        assetSet.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, assetSet.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(assetSet))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAssetSet() throws Exception {
        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();
        assetSet.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetSetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(assetSet))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAssetSet() throws Exception {
        int databaseSizeBeforeUpdate = assetSetRepository.findAll().size();
        assetSet.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetSetMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(assetSet)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the AssetSet in the database
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAssetSet() throws Exception {
        // Initialize the database
        assetSetRepository.saveAndFlush(assetSet);

        int databaseSizeBeforeDelete = assetSetRepository.findAll().size();

        // Delete the assetSet
        restAssetSetMockMvc
            .perform(delete(ENTITY_API_URL_ID, assetSet.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AssetSet> assetSetList = assetSetRepository.findAll();
        assertThat(assetSetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
