package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.AssetsInRoom;
import com.bidv.digiam.repository.AssetsInRoomRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AssetsInRoomResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AssetsInRoomResourceIT {

    private static final String ENTITY_API_URL = "/api/assets-in-rooms";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AssetsInRoomRepository assetsInRoomRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssetsInRoomMockMvc;

    private AssetsInRoom assetsInRoom;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetsInRoom createEntity(EntityManager em) {
        AssetsInRoom assetsInRoom = new AssetsInRoom();
        return assetsInRoom;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssetsInRoom createUpdatedEntity(EntityManager em) {
        AssetsInRoom assetsInRoom = new AssetsInRoom();
        return assetsInRoom;
    }

    @BeforeEach
    public void initTest() {
        assetsInRoom = createEntity(em);
    }

    @Test
    @Transactional
    void createAssetsInRoom() throws Exception {
        int databaseSizeBeforeCreate = assetsInRoomRepository.findAll().size();
        // Create the AssetsInRoom
        restAssetsInRoomMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetsInRoom)))
            .andExpect(status().isCreated());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeCreate + 1);
        AssetsInRoom testAssetsInRoom = assetsInRoomList.get(assetsInRoomList.size() - 1);
    }

    @Test
    @Transactional
    void createAssetsInRoomWithExistingId() throws Exception {
        // Create the AssetsInRoom with an existing ID
        assetsInRoom.setId(1L);

        int databaseSizeBeforeCreate = assetsInRoomRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssetsInRoomMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetsInRoom)))
            .andExpect(status().isBadRequest());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAssetsInRooms() throws Exception {
        // Initialize the database
        assetsInRoomRepository.saveAndFlush(assetsInRoom);

        // Get all the assetsInRoomList
        restAssetsInRoomMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assetsInRoom.getId().intValue())));
    }

    @Test
    @Transactional
    void getAssetsInRoom() throws Exception {
        // Initialize the database
        assetsInRoomRepository.saveAndFlush(assetsInRoom);

        // Get the assetsInRoom
        restAssetsInRoomMockMvc
            .perform(get(ENTITY_API_URL_ID, assetsInRoom.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assetsInRoom.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingAssetsInRoom() throws Exception {
        // Get the assetsInRoom
        restAssetsInRoomMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAssetsInRoom() throws Exception {
        // Initialize the database
        assetsInRoomRepository.saveAndFlush(assetsInRoom);

        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();

        // Update the assetsInRoom
        AssetsInRoom updatedAssetsInRoom = assetsInRoomRepository.findById(assetsInRoom.getId()).get();
        // Disconnect from session so that the updates on updatedAssetsInRoom are not directly saved in db
        em.detach(updatedAssetsInRoom);

        restAssetsInRoomMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedAssetsInRoom.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedAssetsInRoom))
            )
            .andExpect(status().isOk());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
        AssetsInRoom testAssetsInRoom = assetsInRoomList.get(assetsInRoomList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingAssetsInRoom() throws Exception {
        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();
        assetsInRoom.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetsInRoomMockMvc
            .perform(
                put(ENTITY_API_URL_ID, assetsInRoom.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(assetsInRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAssetsInRoom() throws Exception {
        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();
        assetsInRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetsInRoomMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(assetsInRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAssetsInRoom() throws Exception {
        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();
        assetsInRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetsInRoomMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(assetsInRoom)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAssetsInRoomWithPatch() throws Exception {
        // Initialize the database
        assetsInRoomRepository.saveAndFlush(assetsInRoom);

        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();

        // Update the assetsInRoom using partial update
        AssetsInRoom partialUpdatedAssetsInRoom = new AssetsInRoom();
        partialUpdatedAssetsInRoom.setId(assetsInRoom.getId());

        restAssetsInRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAssetsInRoom.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAssetsInRoom))
            )
            .andExpect(status().isOk());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
        AssetsInRoom testAssetsInRoom = assetsInRoomList.get(assetsInRoomList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateAssetsInRoomWithPatch() throws Exception {
        // Initialize the database
        assetsInRoomRepository.saveAndFlush(assetsInRoom);

        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();

        // Update the assetsInRoom using partial update
        AssetsInRoom partialUpdatedAssetsInRoom = new AssetsInRoom();
        partialUpdatedAssetsInRoom.setId(assetsInRoom.getId());

        restAssetsInRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAssetsInRoom.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAssetsInRoom))
            )
            .andExpect(status().isOk());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
        AssetsInRoom testAssetsInRoom = assetsInRoomList.get(assetsInRoomList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingAssetsInRoom() throws Exception {
        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();
        assetsInRoom.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssetsInRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, assetsInRoom.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(assetsInRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAssetsInRoom() throws Exception {
        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();
        assetsInRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetsInRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(assetsInRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAssetsInRoom() throws Exception {
        int databaseSizeBeforeUpdate = assetsInRoomRepository.findAll().size();
        assetsInRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAssetsInRoomMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(assetsInRoom))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AssetsInRoom in the database
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAssetsInRoom() throws Exception {
        // Initialize the database
        assetsInRoomRepository.saveAndFlush(assetsInRoom);

        int databaseSizeBeforeDelete = assetsInRoomRepository.findAll().size();

        // Delete the assetsInRoom
        restAssetsInRoomMockMvc
            .perform(delete(ENTITY_API_URL_ID, assetsInRoom.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AssetsInRoom> assetsInRoomList = assetsInRoomRepository.findAll();
        assertThat(assetsInRoomList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
