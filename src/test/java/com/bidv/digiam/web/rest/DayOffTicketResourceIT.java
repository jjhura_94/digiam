package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.DayOffTicket;
import com.bidv.digiam.repository.DayOffTicketRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DayOffTicketResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DayOffTicketResourceIT {

    private static final Integer DEFAULT_NUMBER_OF_DAY_LEFT = 1;
    private static final Integer UPDATED_NUMBER_OF_DAY_LEFT = 2;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_WATCHER = "AAAAAAAAAA";
    private static final String UPDATED_WATCHER = "BBBBBBBBBB";

    private static final Boolean DEFAULT_STATUS = false;
    private static final Boolean UPDATED_STATUS = true;

    private static final Instant DEFAULT_FROM_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FROM_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_TO_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TO_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/day-off-tickets";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DayOffTicketRepository dayOffTicketRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDayOffTicketMockMvc;

    private DayOffTicket dayOffTicket;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayOffTicket createEntity(EntityManager em) {
        DayOffTicket dayOffTicket = new DayOffTicket()
            .numberOfDayLeft(DEFAULT_NUMBER_OF_DAY_LEFT)
            .reason(DEFAULT_REASON)
            .watcher(DEFAULT_WATCHER)
            .status(DEFAULT_STATUS)
            .fromDate(DEFAULT_FROM_DATE)
            .toDate(DEFAULT_TO_DATE)
            .note(DEFAULT_NOTE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return dayOffTicket;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayOffTicket createUpdatedEntity(EntityManager em) {
        DayOffTicket dayOffTicket = new DayOffTicket()
            .numberOfDayLeft(UPDATED_NUMBER_OF_DAY_LEFT)
            .reason(UPDATED_REASON)
            .watcher(UPDATED_WATCHER)
            .status(UPDATED_STATUS)
            .fromDate(UPDATED_FROM_DATE)
            .toDate(UPDATED_TO_DATE)
            .note(UPDATED_NOTE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);
        return dayOffTicket;
    }

    @BeforeEach
    public void initTest() {
        dayOffTicket = createEntity(em);
    }

    @Test
    @Transactional
    void createDayOffTicket() throws Exception {
        int databaseSizeBeforeCreate = dayOffTicketRepository.findAll().size();
        // Create the DayOffTicket
        restDayOffTicketMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffTicket)))
            .andExpect(status().isCreated());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeCreate + 1);
        DayOffTicket testDayOffTicket = dayOffTicketList.get(dayOffTicketList.size() - 1);
        assertThat(testDayOffTicket.getNumberOfDayLeft()).isEqualTo(DEFAULT_NUMBER_OF_DAY_LEFT);
        assertThat(testDayOffTicket.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testDayOffTicket.getWatcher()).isEqualTo(DEFAULT_WATCHER);
        assertThat(testDayOffTicket.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDayOffTicket.getFromDate()).isEqualTo(DEFAULT_FROM_DATE);
        assertThat(testDayOffTicket.getToDate()).isEqualTo(DEFAULT_TO_DATE);
        assertThat(testDayOffTicket.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testDayOffTicket.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDayOffTicket.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testDayOffTicket.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    void createDayOffTicketWithExistingId() throws Exception {
        // Create the DayOffTicket with an existing ID
        dayOffTicket.setId(1L);

        int databaseSizeBeforeCreate = dayOffTicketRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDayOffTicketMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffTicket)))
            .andExpect(status().isBadRequest());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDayOffTickets() throws Exception {
        // Initialize the database
        dayOffTicketRepository.saveAndFlush(dayOffTicket);

        // Get all the dayOffTicketList
        restDayOffTicketMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayOffTicket.getId().intValue())))
            .andExpect(jsonPath("$.[*].numberOfDayLeft").value(hasItem(DEFAULT_NUMBER_OF_DAY_LEFT)))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].watcher").value(hasItem(DEFAULT_WATCHER)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.booleanValue())))
            .andExpect(jsonPath("$.[*].fromDate").value(hasItem(DEFAULT_FROM_DATE.toString())))
            .andExpect(jsonPath("$.[*].toDate").value(hasItem(DEFAULT_TO_DATE.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    void getDayOffTicket() throws Exception {
        // Initialize the database
        dayOffTicketRepository.saveAndFlush(dayOffTicket);

        // Get the dayOffTicket
        restDayOffTicketMockMvc
            .perform(get(ENTITY_API_URL_ID, dayOffTicket.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dayOffTicket.getId().intValue()))
            .andExpect(jsonPath("$.numberOfDayLeft").value(DEFAULT_NUMBER_OF_DAY_LEFT))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON))
            .andExpect(jsonPath("$.watcher").value(DEFAULT_WATCHER))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.booleanValue()))
            .andExpect(jsonPath("$.fromDate").value(DEFAULT_FROM_DATE.toString()))
            .andExpect(jsonPath("$.toDate").value(DEFAULT_TO_DATE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingDayOffTicket() throws Exception {
        // Get the dayOffTicket
        restDayOffTicketMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDayOffTicket() throws Exception {
        // Initialize the database
        dayOffTicketRepository.saveAndFlush(dayOffTicket);

        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();

        // Update the dayOffTicket
        DayOffTicket updatedDayOffTicket = dayOffTicketRepository.findById(dayOffTicket.getId()).get();
        // Disconnect from session so that the updates on updatedDayOffTicket are not directly saved in db
        em.detach(updatedDayOffTicket);
        updatedDayOffTicket
            .numberOfDayLeft(UPDATED_NUMBER_OF_DAY_LEFT)
            .reason(UPDATED_REASON)
            .watcher(UPDATED_WATCHER)
            .status(UPDATED_STATUS)
            .fromDate(UPDATED_FROM_DATE)
            .toDate(UPDATED_TO_DATE)
            .note(UPDATED_NOTE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDayOffTicketMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDayOffTicket.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDayOffTicket))
            )
            .andExpect(status().isOk());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
        DayOffTicket testDayOffTicket = dayOffTicketList.get(dayOffTicketList.size() - 1);
        assertThat(testDayOffTicket.getNumberOfDayLeft()).isEqualTo(UPDATED_NUMBER_OF_DAY_LEFT);
        assertThat(testDayOffTicket.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testDayOffTicket.getWatcher()).isEqualTo(UPDATED_WATCHER);
        assertThat(testDayOffTicket.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDayOffTicket.getFromDate()).isEqualTo(UPDATED_FROM_DATE);
        assertThat(testDayOffTicket.getToDate()).isEqualTo(UPDATED_TO_DATE);
        assertThat(testDayOffTicket.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testDayOffTicket.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDayOffTicket.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testDayOffTicket.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingDayOffTicket() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();
        dayOffTicket.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDayOffTicketMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dayOffTicket.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dayOffTicket))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDayOffTicket() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();
        dayOffTicket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTicketMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dayOffTicket))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDayOffTicket() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();
        dayOffTicket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTicketMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffTicket)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDayOffTicketWithPatch() throws Exception {
        // Initialize the database
        dayOffTicketRepository.saveAndFlush(dayOffTicket);

        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();

        // Update the dayOffTicket using partial update
        DayOffTicket partialUpdatedDayOffTicket = new DayOffTicket();
        partialUpdatedDayOffTicket.setId(dayOffTicket.getId());

        partialUpdatedDayOffTicket
            .reason(UPDATED_REASON)
            .watcher(UPDATED_WATCHER)
            .status(UPDATED_STATUS)
            .toDate(UPDATED_TO_DATE)
            .note(UPDATED_NOTE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDayOffTicketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDayOffTicket.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDayOffTicket))
            )
            .andExpect(status().isOk());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
        DayOffTicket testDayOffTicket = dayOffTicketList.get(dayOffTicketList.size() - 1);
        assertThat(testDayOffTicket.getNumberOfDayLeft()).isEqualTo(DEFAULT_NUMBER_OF_DAY_LEFT);
        assertThat(testDayOffTicket.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testDayOffTicket.getWatcher()).isEqualTo(UPDATED_WATCHER);
        assertThat(testDayOffTicket.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDayOffTicket.getFromDate()).isEqualTo(DEFAULT_FROM_DATE);
        assertThat(testDayOffTicket.getToDate()).isEqualTo(UPDATED_TO_DATE);
        assertThat(testDayOffTicket.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testDayOffTicket.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDayOffTicket.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testDayOffTicket.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void fullUpdateDayOffTicketWithPatch() throws Exception {
        // Initialize the database
        dayOffTicketRepository.saveAndFlush(dayOffTicket);

        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();

        // Update the dayOffTicket using partial update
        DayOffTicket partialUpdatedDayOffTicket = new DayOffTicket();
        partialUpdatedDayOffTicket.setId(dayOffTicket.getId());

        partialUpdatedDayOffTicket
            .numberOfDayLeft(UPDATED_NUMBER_OF_DAY_LEFT)
            .reason(UPDATED_REASON)
            .watcher(UPDATED_WATCHER)
            .status(UPDATED_STATUS)
            .fromDate(UPDATED_FROM_DATE)
            .toDate(UPDATED_TO_DATE)
            .note(UPDATED_NOTE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDayOffTicketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDayOffTicket.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDayOffTicket))
            )
            .andExpect(status().isOk());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
        DayOffTicket testDayOffTicket = dayOffTicketList.get(dayOffTicketList.size() - 1);
        assertThat(testDayOffTicket.getNumberOfDayLeft()).isEqualTo(UPDATED_NUMBER_OF_DAY_LEFT);
        assertThat(testDayOffTicket.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testDayOffTicket.getWatcher()).isEqualTo(UPDATED_WATCHER);
        assertThat(testDayOffTicket.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDayOffTicket.getFromDate()).isEqualTo(UPDATED_FROM_DATE);
        assertThat(testDayOffTicket.getToDate()).isEqualTo(UPDATED_TO_DATE);
        assertThat(testDayOffTicket.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testDayOffTicket.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDayOffTicket.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testDayOffTicket.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingDayOffTicket() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();
        dayOffTicket.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDayOffTicketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dayOffTicket.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dayOffTicket))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDayOffTicket() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();
        dayOffTicket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTicketMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dayOffTicket))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDayOffTicket() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTicketRepository.findAll().size();
        dayOffTicket.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTicketMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dayOffTicket))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DayOffTicket in the database
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDayOffTicket() throws Exception {
        // Initialize the database
        dayOffTicketRepository.saveAndFlush(dayOffTicket);

        int databaseSizeBeforeDelete = dayOffTicketRepository.findAll().size();

        // Delete the dayOffTicket
        restDayOffTicketMockMvc
            .perform(delete(ENTITY_API_URL_ID, dayOffTicket.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DayOffTicket> dayOffTicketList = dayOffTicketRepository.findAll();
        assertThat(dayOffTicketList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
