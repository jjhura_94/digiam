package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.DayOffType;
import com.bidv.digiam.repository.DayOffTypeRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DayOffTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DayOffTypeResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMBER_OF_DAY = 1;
    private static final Integer UPDATED_NUMBER_OF_DAY = 2;

    private static final Boolean DEFAULT_NO_SALARY = false;
    private static final Boolean UPDATED_NO_SALARY = true;

    private static final String ENTITY_API_URL = "/api/day-off-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DayOffTypeRepository dayOffTypeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDayOffTypeMockMvc;

    private DayOffType dayOffType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayOffType createEntity(EntityManager em) {
        DayOffType dayOffType = new DayOffType().title(DEFAULT_TITLE).numberOfDay(DEFAULT_NUMBER_OF_DAY).noSalary(DEFAULT_NO_SALARY);
        return dayOffType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayOffType createUpdatedEntity(EntityManager em) {
        DayOffType dayOffType = new DayOffType().title(UPDATED_TITLE).numberOfDay(UPDATED_NUMBER_OF_DAY).noSalary(UPDATED_NO_SALARY);
        return dayOffType;
    }

    @BeforeEach
    public void initTest() {
        dayOffType = createEntity(em);
    }

    @Test
    @Transactional
    void createDayOffType() throws Exception {
        int databaseSizeBeforeCreate = dayOffTypeRepository.findAll().size();
        // Create the DayOffType
        restDayOffTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffType)))
            .andExpect(status().isCreated());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeCreate + 1);
        DayOffType testDayOffType = dayOffTypeList.get(dayOffTypeList.size() - 1);
        assertThat(testDayOffType.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testDayOffType.getNumberOfDay()).isEqualTo(DEFAULT_NUMBER_OF_DAY);
        assertThat(testDayOffType.getNoSalary()).isEqualTo(DEFAULT_NO_SALARY);
    }

    @Test
    @Transactional
    void createDayOffTypeWithExistingId() throws Exception {
        // Create the DayOffType with an existing ID
        dayOffType.setId(1L);

        int databaseSizeBeforeCreate = dayOffTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDayOffTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffType)))
            .andExpect(status().isBadRequest());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDayOffTypes() throws Exception {
        // Initialize the database
        dayOffTypeRepository.saveAndFlush(dayOffType);

        // Get all the dayOffTypeList
        restDayOffTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayOffType.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].numberOfDay").value(hasItem(DEFAULT_NUMBER_OF_DAY)))
            .andExpect(jsonPath("$.[*].noSalary").value(hasItem(DEFAULT_NO_SALARY.booleanValue())));
    }

    @Test
    @Transactional
    void getDayOffType() throws Exception {
        // Initialize the database
        dayOffTypeRepository.saveAndFlush(dayOffType);

        // Get the dayOffType
        restDayOffTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, dayOffType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dayOffType.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.numberOfDay").value(DEFAULT_NUMBER_OF_DAY))
            .andExpect(jsonPath("$.noSalary").value(DEFAULT_NO_SALARY.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingDayOffType() throws Exception {
        // Get the dayOffType
        restDayOffTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDayOffType() throws Exception {
        // Initialize the database
        dayOffTypeRepository.saveAndFlush(dayOffType);

        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();

        // Update the dayOffType
        DayOffType updatedDayOffType = dayOffTypeRepository.findById(dayOffType.getId()).get();
        // Disconnect from session so that the updates on updatedDayOffType are not directly saved in db
        em.detach(updatedDayOffType);
        updatedDayOffType.title(UPDATED_TITLE).numberOfDay(UPDATED_NUMBER_OF_DAY).noSalary(UPDATED_NO_SALARY);

        restDayOffTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDayOffType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDayOffType))
            )
            .andExpect(status().isOk());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
        DayOffType testDayOffType = dayOffTypeList.get(dayOffTypeList.size() - 1);
        assertThat(testDayOffType.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testDayOffType.getNumberOfDay()).isEqualTo(UPDATED_NUMBER_OF_DAY);
        assertThat(testDayOffType.getNoSalary()).isEqualTo(UPDATED_NO_SALARY);
    }

    @Test
    @Transactional
    void putNonExistingDayOffType() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();
        dayOffType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDayOffTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dayOffType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dayOffType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDayOffType() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();
        dayOffType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dayOffType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDayOffType() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();
        dayOffType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTypeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffType)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDayOffTypeWithPatch() throws Exception {
        // Initialize the database
        dayOffTypeRepository.saveAndFlush(dayOffType);

        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();

        // Update the dayOffType using partial update
        DayOffType partialUpdatedDayOffType = new DayOffType();
        partialUpdatedDayOffType.setId(dayOffType.getId());

        restDayOffTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDayOffType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDayOffType))
            )
            .andExpect(status().isOk());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
        DayOffType testDayOffType = dayOffTypeList.get(dayOffTypeList.size() - 1);
        assertThat(testDayOffType.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testDayOffType.getNumberOfDay()).isEqualTo(DEFAULT_NUMBER_OF_DAY);
        assertThat(testDayOffType.getNoSalary()).isEqualTo(DEFAULT_NO_SALARY);
    }

    @Test
    @Transactional
    void fullUpdateDayOffTypeWithPatch() throws Exception {
        // Initialize the database
        dayOffTypeRepository.saveAndFlush(dayOffType);

        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();

        // Update the dayOffType using partial update
        DayOffType partialUpdatedDayOffType = new DayOffType();
        partialUpdatedDayOffType.setId(dayOffType.getId());

        partialUpdatedDayOffType.title(UPDATED_TITLE).numberOfDay(UPDATED_NUMBER_OF_DAY).noSalary(UPDATED_NO_SALARY);

        restDayOffTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDayOffType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDayOffType))
            )
            .andExpect(status().isOk());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
        DayOffType testDayOffType = dayOffTypeList.get(dayOffTypeList.size() - 1);
        assertThat(testDayOffType.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testDayOffType.getNumberOfDay()).isEqualTo(UPDATED_NUMBER_OF_DAY);
        assertThat(testDayOffType.getNoSalary()).isEqualTo(UPDATED_NO_SALARY);
    }

    @Test
    @Transactional
    void patchNonExistingDayOffType() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();
        dayOffType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDayOffTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dayOffType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dayOffType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDayOffType() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();
        dayOffType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dayOffType))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDayOffType() throws Exception {
        int databaseSizeBeforeUpdate = dayOffTypeRepository.findAll().size();
        dayOffType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffTypeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dayOffType))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DayOffType in the database
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDayOffType() throws Exception {
        // Initialize the database
        dayOffTypeRepository.saveAndFlush(dayOffType);

        int databaseSizeBeforeDelete = dayOffTypeRepository.findAll().size();

        // Delete the dayOffType
        restDayOffTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, dayOffType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DayOffType> dayOffTypeList = dayOffTypeRepository.findAll();
        assertThat(dayOffTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
