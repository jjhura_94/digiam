package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.DayOffUser;
import com.bidv.digiam.repository.DayOffUserRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DayOffUserResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DayOffUserResourceIT {

    private static final Integer DEFAULT_DAY_LEFT = 1;
    private static final Integer UPDATED_DAY_LEFT = 2;

    private static final Integer DEFAULT_YEAR = 1;
    private static final Integer UPDATED_YEAR = 2;

    private static final String ENTITY_API_URL = "/api/day-off-users";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DayOffUserRepository dayOffUserRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDayOffUserMockMvc;

    private DayOffUser dayOffUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayOffUser createEntity(EntityManager em) {
        DayOffUser dayOffUser = new DayOffUser().dayLeft(DEFAULT_DAY_LEFT).year(DEFAULT_YEAR);
        return dayOffUser;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayOffUser createUpdatedEntity(EntityManager em) {
        DayOffUser dayOffUser = new DayOffUser().dayLeft(UPDATED_DAY_LEFT).year(UPDATED_YEAR);
        return dayOffUser;
    }

    @BeforeEach
    public void initTest() {
        dayOffUser = createEntity(em);
    }

    @Test
    @Transactional
    void createDayOffUser() throws Exception {
        int databaseSizeBeforeCreate = dayOffUserRepository.findAll().size();
        // Create the DayOffUser
        restDayOffUserMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffUser)))
            .andExpect(status().isCreated());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeCreate + 1);
        DayOffUser testDayOffUser = dayOffUserList.get(dayOffUserList.size() - 1);
        assertThat(testDayOffUser.getDayLeft()).isEqualTo(DEFAULT_DAY_LEFT);
        assertThat(testDayOffUser.getYear()).isEqualTo(DEFAULT_YEAR);
    }

    @Test
    @Transactional
    void createDayOffUserWithExistingId() throws Exception {
        // Create the DayOffUser with an existing ID
        dayOffUser.setId(1L);

        int databaseSizeBeforeCreate = dayOffUserRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDayOffUserMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffUser)))
            .andExpect(status().isBadRequest());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDayOffUsers() throws Exception {
        // Initialize the database
        dayOffUserRepository.saveAndFlush(dayOffUser);

        // Get all the dayOffUserList
        restDayOffUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayOffUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].dayLeft").value(hasItem(DEFAULT_DAY_LEFT)))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)));
    }

    @Test
    @Transactional
    void getDayOffUser() throws Exception {
        // Initialize the database
        dayOffUserRepository.saveAndFlush(dayOffUser);

        // Get the dayOffUser
        restDayOffUserMockMvc
            .perform(get(ENTITY_API_URL_ID, dayOffUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dayOffUser.getId().intValue()))
            .andExpect(jsonPath("$.dayLeft").value(DEFAULT_DAY_LEFT))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR));
    }

    @Test
    @Transactional
    void getNonExistingDayOffUser() throws Exception {
        // Get the dayOffUser
        restDayOffUserMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDayOffUser() throws Exception {
        // Initialize the database
        dayOffUserRepository.saveAndFlush(dayOffUser);

        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();

        // Update the dayOffUser
        DayOffUser updatedDayOffUser = dayOffUserRepository.findById(dayOffUser.getId()).get();
        // Disconnect from session so that the updates on updatedDayOffUser are not directly saved in db
        em.detach(updatedDayOffUser);
        updatedDayOffUser.dayLeft(UPDATED_DAY_LEFT).year(UPDATED_YEAR);

        restDayOffUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDayOffUser.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDayOffUser))
            )
            .andExpect(status().isOk());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
        DayOffUser testDayOffUser = dayOffUserList.get(dayOffUserList.size() - 1);
        assertThat(testDayOffUser.getDayLeft()).isEqualTo(UPDATED_DAY_LEFT);
        assertThat(testDayOffUser.getYear()).isEqualTo(UPDATED_YEAR);
    }

    @Test
    @Transactional
    void putNonExistingDayOffUser() throws Exception {
        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();
        dayOffUser.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDayOffUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dayOffUser.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dayOffUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDayOffUser() throws Exception {
        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();
        dayOffUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dayOffUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDayOffUser() throws Exception {
        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();
        dayOffUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffUserMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dayOffUser)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDayOffUserWithPatch() throws Exception {
        // Initialize the database
        dayOffUserRepository.saveAndFlush(dayOffUser);

        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();

        // Update the dayOffUser using partial update
        DayOffUser partialUpdatedDayOffUser = new DayOffUser();
        partialUpdatedDayOffUser.setId(dayOffUser.getId());

        partialUpdatedDayOffUser.dayLeft(UPDATED_DAY_LEFT);

        restDayOffUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDayOffUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDayOffUser))
            )
            .andExpect(status().isOk());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
        DayOffUser testDayOffUser = dayOffUserList.get(dayOffUserList.size() - 1);
        assertThat(testDayOffUser.getDayLeft()).isEqualTo(UPDATED_DAY_LEFT);
        assertThat(testDayOffUser.getYear()).isEqualTo(DEFAULT_YEAR);
    }

    @Test
    @Transactional
    void fullUpdateDayOffUserWithPatch() throws Exception {
        // Initialize the database
        dayOffUserRepository.saveAndFlush(dayOffUser);

        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();

        // Update the dayOffUser using partial update
        DayOffUser partialUpdatedDayOffUser = new DayOffUser();
        partialUpdatedDayOffUser.setId(dayOffUser.getId());

        partialUpdatedDayOffUser.dayLeft(UPDATED_DAY_LEFT).year(UPDATED_YEAR);

        restDayOffUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDayOffUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDayOffUser))
            )
            .andExpect(status().isOk());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
        DayOffUser testDayOffUser = dayOffUserList.get(dayOffUserList.size() - 1);
        assertThat(testDayOffUser.getDayLeft()).isEqualTo(UPDATED_DAY_LEFT);
        assertThat(testDayOffUser.getYear()).isEqualTo(UPDATED_YEAR);
    }

    @Test
    @Transactional
    void patchNonExistingDayOffUser() throws Exception {
        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();
        dayOffUser.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDayOffUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dayOffUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dayOffUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDayOffUser() throws Exception {
        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();
        dayOffUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dayOffUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDayOffUser() throws Exception {
        int databaseSizeBeforeUpdate = dayOffUserRepository.findAll().size();
        dayOffUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDayOffUserMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dayOffUser))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DayOffUser in the database
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDayOffUser() throws Exception {
        // Initialize the database
        dayOffUserRepository.saveAndFlush(dayOffUser);

        int databaseSizeBeforeDelete = dayOffUserRepository.findAll().size();

        // Delete the dayOffUser
        restDayOffUserMockMvc
            .perform(delete(ENTITY_API_URL_ID, dayOffUser.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DayOffUser> dayOffUserList = dayOffUserRepository.findAll();
        assertThat(dayOffUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
