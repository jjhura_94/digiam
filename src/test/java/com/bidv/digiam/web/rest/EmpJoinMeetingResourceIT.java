package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.EmpJoinMeeting;
import com.bidv.digiam.repository.EmpJoinMeetingRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EmpJoinMeetingResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EmpJoinMeetingResourceIT {

    private static final String ENTITY_API_URL = "/api/emp-join-meetings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EmpJoinMeetingRepository empJoinMeetingRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEmpJoinMeetingMockMvc;

    private EmpJoinMeeting empJoinMeeting;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmpJoinMeeting createEntity(EntityManager em) {
        EmpJoinMeeting empJoinMeeting = new EmpJoinMeeting();
        return empJoinMeeting;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmpJoinMeeting createUpdatedEntity(EntityManager em) {
        EmpJoinMeeting empJoinMeeting = new EmpJoinMeeting();
        return empJoinMeeting;
    }

    @BeforeEach
    public void initTest() {
        empJoinMeeting = createEntity(em);
    }

    @Test
    @Transactional
    void createEmpJoinMeeting() throws Exception {
        int databaseSizeBeforeCreate = empJoinMeetingRepository.findAll().size();
        // Create the EmpJoinMeeting
        restEmpJoinMeetingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(empJoinMeeting))
            )
            .andExpect(status().isCreated());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeCreate + 1);
        EmpJoinMeeting testEmpJoinMeeting = empJoinMeetingList.get(empJoinMeetingList.size() - 1);
    }

    @Test
    @Transactional
    void createEmpJoinMeetingWithExistingId() throws Exception {
        // Create the EmpJoinMeeting with an existing ID
        empJoinMeeting.setId(1L);

        int databaseSizeBeforeCreate = empJoinMeetingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmpJoinMeetingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(empJoinMeeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllEmpJoinMeetings() throws Exception {
        // Initialize the database
        empJoinMeetingRepository.saveAndFlush(empJoinMeeting);

        // Get all the empJoinMeetingList
        restEmpJoinMeetingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(empJoinMeeting.getId().intValue())));
    }

    @Test
    @Transactional
    void getEmpJoinMeeting() throws Exception {
        // Initialize the database
        empJoinMeetingRepository.saveAndFlush(empJoinMeeting);

        // Get the empJoinMeeting
        restEmpJoinMeetingMockMvc
            .perform(get(ENTITY_API_URL_ID, empJoinMeeting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(empJoinMeeting.getId().intValue()));
    }

    @Test
    @Transactional
    void getNonExistingEmpJoinMeeting() throws Exception {
        // Get the empJoinMeeting
        restEmpJoinMeetingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEmpJoinMeeting() throws Exception {
        // Initialize the database
        empJoinMeetingRepository.saveAndFlush(empJoinMeeting);

        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();

        // Update the empJoinMeeting
        EmpJoinMeeting updatedEmpJoinMeeting = empJoinMeetingRepository.findById(empJoinMeeting.getId()).get();
        // Disconnect from session so that the updates on updatedEmpJoinMeeting are not directly saved in db
        em.detach(updatedEmpJoinMeeting);

        restEmpJoinMeetingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEmpJoinMeeting.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEmpJoinMeeting))
            )
            .andExpect(status().isOk());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
        EmpJoinMeeting testEmpJoinMeeting = empJoinMeetingList.get(empJoinMeetingList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingEmpJoinMeeting() throws Exception {
        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();
        empJoinMeeting.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmpJoinMeetingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, empJoinMeeting.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(empJoinMeeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEmpJoinMeeting() throws Exception {
        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();
        empJoinMeeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpJoinMeetingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(empJoinMeeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEmpJoinMeeting() throws Exception {
        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();
        empJoinMeeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpJoinMeetingMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(empJoinMeeting)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEmpJoinMeetingWithPatch() throws Exception {
        // Initialize the database
        empJoinMeetingRepository.saveAndFlush(empJoinMeeting);

        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();

        // Update the empJoinMeeting using partial update
        EmpJoinMeeting partialUpdatedEmpJoinMeeting = new EmpJoinMeeting();
        partialUpdatedEmpJoinMeeting.setId(empJoinMeeting.getId());

        restEmpJoinMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmpJoinMeeting.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmpJoinMeeting))
            )
            .andExpect(status().isOk());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
        EmpJoinMeeting testEmpJoinMeeting = empJoinMeetingList.get(empJoinMeetingList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateEmpJoinMeetingWithPatch() throws Exception {
        // Initialize the database
        empJoinMeetingRepository.saveAndFlush(empJoinMeeting);

        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();

        // Update the empJoinMeeting using partial update
        EmpJoinMeeting partialUpdatedEmpJoinMeeting = new EmpJoinMeeting();
        partialUpdatedEmpJoinMeeting.setId(empJoinMeeting.getId());

        restEmpJoinMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmpJoinMeeting.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmpJoinMeeting))
            )
            .andExpect(status().isOk());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
        EmpJoinMeeting testEmpJoinMeeting = empJoinMeetingList.get(empJoinMeetingList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingEmpJoinMeeting() throws Exception {
        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();
        empJoinMeeting.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmpJoinMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, empJoinMeeting.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(empJoinMeeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEmpJoinMeeting() throws Exception {
        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();
        empJoinMeeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpJoinMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(empJoinMeeting))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEmpJoinMeeting() throws Exception {
        int databaseSizeBeforeUpdate = empJoinMeetingRepository.findAll().size();
        empJoinMeeting.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpJoinMeetingMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(empJoinMeeting))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmpJoinMeeting in the database
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEmpJoinMeeting() throws Exception {
        // Initialize the database
        empJoinMeetingRepository.saveAndFlush(empJoinMeeting);

        int databaseSizeBeforeDelete = empJoinMeetingRepository.findAll().size();

        // Delete the empJoinMeeting
        restEmpJoinMeetingMockMvc
            .perform(delete(ENTITY_API_URL_ID, empJoinMeeting.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EmpJoinMeeting> empJoinMeetingList = empJoinMeetingRepository.findAll();
        assertThat(empJoinMeetingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
