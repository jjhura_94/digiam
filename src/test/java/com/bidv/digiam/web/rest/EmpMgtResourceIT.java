package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.EmpMgt;
import com.bidv.digiam.repository.EmpMgtRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EmpMgtResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EmpMgtResourceIT {

    private static final String DEFAULT_EMP_NO = "AAAAAAAAAA";
    private static final String UPDATED_EMP_NO = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DOB = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DOB = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_CELL_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_CELL_PHONE = "BBBBBBBBBB";

    private static final Integer DEFAULT_EXT_NUMBER = 1;
    private static final Integer UPDATED_EXT_NUMBER = 2;

    private static final String DEFAULT_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_GENDER = "AAAAAAAAAA";
    private static final String UPDATED_GENDER = "BBBBBBBBBB";

    private static final String DEFAULT_IMG_PATH = "AAAAAAAAAA";
    private static final String UPDATED_IMG_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/emp-mgts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EmpMgtRepository empMgtRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEmpMgtMockMvc;

    private EmpMgt empMgt;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmpMgt createEntity(EntityManager em) {
        EmpMgt empMgt = new EmpMgt()
            .empNo(DEFAULT_EMP_NO)
            .name(DEFAULT_NAME)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .dob(DEFAULT_DOB)
            .email(DEFAULT_EMAIL)
            .cellPhone(DEFAULT_CELL_PHONE)
            .extNumber(DEFAULT_EXT_NUMBER)
            .zipCode(DEFAULT_ZIP_CODE)
            .address(DEFAULT_ADDRESS)
            .gender(DEFAULT_GENDER)
            .imgPath(DEFAULT_IMG_PATH)
            .status(DEFAULT_STATUS);
        return empMgt;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EmpMgt createUpdatedEntity(EntityManager em) {
        EmpMgt empMgt = new EmpMgt()
            .empNo(UPDATED_EMP_NO)
            .name(UPDATED_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .dob(UPDATED_DOB)
            .email(UPDATED_EMAIL)
            .cellPhone(UPDATED_CELL_PHONE)
            .extNumber(UPDATED_EXT_NUMBER)
            .zipCode(UPDATED_ZIP_CODE)
            .address(UPDATED_ADDRESS)
            .gender(UPDATED_GENDER)
            .imgPath(UPDATED_IMG_PATH)
            .status(UPDATED_STATUS);
        return empMgt;
    }

    @BeforeEach
    public void initTest() {
        empMgt = createEntity(em);
    }

    @Test
    @Transactional
    void createEmpMgt() throws Exception {
        int databaseSizeBeforeCreate = empMgtRepository.findAll().size();
        // Create the EmpMgt
        restEmpMgtMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(empMgt)))
            .andExpect(status().isCreated());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeCreate + 1);
        EmpMgt testEmpMgt = empMgtList.get(empMgtList.size() - 1);
        assertThat(testEmpMgt.getEmpNo()).isEqualTo(DEFAULT_EMP_NO);
        assertThat(testEmpMgt.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEmpMgt.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEmpMgt.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEmpMgt.getDob()).isEqualTo(DEFAULT_DOB);
        assertThat(testEmpMgt.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testEmpMgt.getCellPhone()).isEqualTo(DEFAULT_CELL_PHONE);
        assertThat(testEmpMgt.getExtNumber()).isEqualTo(DEFAULT_EXT_NUMBER);
        assertThat(testEmpMgt.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testEmpMgt.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testEmpMgt.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testEmpMgt.getImgPath()).isEqualTo(DEFAULT_IMG_PATH);
        assertThat(testEmpMgt.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void createEmpMgtWithExistingId() throws Exception {
        // Create the EmpMgt with an existing ID
        empMgt.setId(1L);

        int databaseSizeBeforeCreate = empMgtRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmpMgtMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(empMgt)))
            .andExpect(status().isBadRequest());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllEmpMgts() throws Exception {
        // Initialize the database
        empMgtRepository.saveAndFlush(empMgt);

        // Get all the empMgtList
        restEmpMgtMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(empMgt.getId().intValue())))
            .andExpect(jsonPath("$.[*].empNo").value(hasItem(DEFAULT_EMP_NO)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].cellPhone").value(hasItem(DEFAULT_CELL_PHONE)))
            .andExpect(jsonPath("$.[*].extNumber").value(hasItem(DEFAULT_EXT_NUMBER)))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].imgPath").value(hasItem(DEFAULT_IMG_PATH)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    void getEmpMgt() throws Exception {
        // Initialize the database
        empMgtRepository.saveAndFlush(empMgt);

        // Get the empMgt
        restEmpMgtMockMvc
            .perform(get(ENTITY_API_URL_ID, empMgt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(empMgt.getId().intValue()))
            .andExpect(jsonPath("$.empNo").value(DEFAULT_EMP_NO))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.dob").value(DEFAULT_DOB.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.cellPhone").value(DEFAULT_CELL_PHONE))
            .andExpect(jsonPath("$.extNumber").value(DEFAULT_EXT_NUMBER))
            .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.imgPath").value(DEFAULT_IMG_PATH))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    void getNonExistingEmpMgt() throws Exception {
        // Get the empMgt
        restEmpMgtMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEmpMgt() throws Exception {
        // Initialize the database
        empMgtRepository.saveAndFlush(empMgt);

        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();

        // Update the empMgt
        EmpMgt updatedEmpMgt = empMgtRepository.findById(empMgt.getId()).get();
        // Disconnect from session so that the updates on updatedEmpMgt are not directly saved in db
        em.detach(updatedEmpMgt);
        updatedEmpMgt
            .empNo(UPDATED_EMP_NO)
            .name(UPDATED_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .dob(UPDATED_DOB)
            .email(UPDATED_EMAIL)
            .cellPhone(UPDATED_CELL_PHONE)
            .extNumber(UPDATED_EXT_NUMBER)
            .zipCode(UPDATED_ZIP_CODE)
            .address(UPDATED_ADDRESS)
            .gender(UPDATED_GENDER)
            .imgPath(UPDATED_IMG_PATH)
            .status(UPDATED_STATUS);

        restEmpMgtMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEmpMgt.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEmpMgt))
            )
            .andExpect(status().isOk());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
        EmpMgt testEmpMgt = empMgtList.get(empMgtList.size() - 1);
        assertThat(testEmpMgt.getEmpNo()).isEqualTo(UPDATED_EMP_NO);
        assertThat(testEmpMgt.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEmpMgt.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testEmpMgt.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testEmpMgt.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testEmpMgt.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testEmpMgt.getCellPhone()).isEqualTo(UPDATED_CELL_PHONE);
        assertThat(testEmpMgt.getExtNumber()).isEqualTo(UPDATED_EXT_NUMBER);
        assertThat(testEmpMgt.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testEmpMgt.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testEmpMgt.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testEmpMgt.getImgPath()).isEqualTo(UPDATED_IMG_PATH);
        assertThat(testEmpMgt.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingEmpMgt() throws Exception {
        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();
        empMgt.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmpMgtMockMvc
            .perform(
                put(ENTITY_API_URL_ID, empMgt.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(empMgt))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEmpMgt() throws Exception {
        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();
        empMgt.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpMgtMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(empMgt))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEmpMgt() throws Exception {
        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();
        empMgt.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpMgtMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(empMgt)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEmpMgtWithPatch() throws Exception {
        // Initialize the database
        empMgtRepository.saveAndFlush(empMgt);

        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();

        // Update the empMgt using partial update
        EmpMgt partialUpdatedEmpMgt = new EmpMgt();
        partialUpdatedEmpMgt.setId(empMgt.getId());

        partialUpdatedEmpMgt.dob(UPDATED_DOB).email(UPDATED_EMAIL).address(UPDATED_ADDRESS);

        restEmpMgtMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmpMgt.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmpMgt))
            )
            .andExpect(status().isOk());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
        EmpMgt testEmpMgt = empMgtList.get(empMgtList.size() - 1);
        assertThat(testEmpMgt.getEmpNo()).isEqualTo(DEFAULT_EMP_NO);
        assertThat(testEmpMgt.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEmpMgt.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEmpMgt.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEmpMgt.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testEmpMgt.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testEmpMgt.getCellPhone()).isEqualTo(DEFAULT_CELL_PHONE);
        assertThat(testEmpMgt.getExtNumber()).isEqualTo(DEFAULT_EXT_NUMBER);
        assertThat(testEmpMgt.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
        assertThat(testEmpMgt.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testEmpMgt.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testEmpMgt.getImgPath()).isEqualTo(DEFAULT_IMG_PATH);
        assertThat(testEmpMgt.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    void fullUpdateEmpMgtWithPatch() throws Exception {
        // Initialize the database
        empMgtRepository.saveAndFlush(empMgt);

        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();

        // Update the empMgt using partial update
        EmpMgt partialUpdatedEmpMgt = new EmpMgt();
        partialUpdatedEmpMgt.setId(empMgt.getId());

        partialUpdatedEmpMgt
            .empNo(UPDATED_EMP_NO)
            .name(UPDATED_NAME)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .dob(UPDATED_DOB)
            .email(UPDATED_EMAIL)
            .cellPhone(UPDATED_CELL_PHONE)
            .extNumber(UPDATED_EXT_NUMBER)
            .zipCode(UPDATED_ZIP_CODE)
            .address(UPDATED_ADDRESS)
            .gender(UPDATED_GENDER)
            .imgPath(UPDATED_IMG_PATH)
            .status(UPDATED_STATUS);

        restEmpMgtMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEmpMgt.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEmpMgt))
            )
            .andExpect(status().isOk());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
        EmpMgt testEmpMgt = empMgtList.get(empMgtList.size() - 1);
        assertThat(testEmpMgt.getEmpNo()).isEqualTo(UPDATED_EMP_NO);
        assertThat(testEmpMgt.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEmpMgt.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testEmpMgt.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testEmpMgt.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testEmpMgt.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testEmpMgt.getCellPhone()).isEqualTo(UPDATED_CELL_PHONE);
        assertThat(testEmpMgt.getExtNumber()).isEqualTo(UPDATED_EXT_NUMBER);
        assertThat(testEmpMgt.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
        assertThat(testEmpMgt.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testEmpMgt.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testEmpMgt.getImgPath()).isEqualTo(UPDATED_IMG_PATH);
        assertThat(testEmpMgt.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingEmpMgt() throws Exception {
        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();
        empMgt.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmpMgtMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, empMgt.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(empMgt))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEmpMgt() throws Exception {
        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();
        empMgt.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpMgtMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(empMgt))
            )
            .andExpect(status().isBadRequest());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEmpMgt() throws Exception {
        int databaseSizeBeforeUpdate = empMgtRepository.findAll().size();
        empMgt.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEmpMgtMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(empMgt)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the EmpMgt in the database
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEmpMgt() throws Exception {
        // Initialize the database
        empMgtRepository.saveAndFlush(empMgt);

        int databaseSizeBeforeDelete = empMgtRepository.findAll().size();

        // Delete the empMgt
        restEmpMgtMockMvc
            .perform(delete(ENTITY_API_URL_ID, empMgt.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EmpMgt> empMgtList = empMgtRepository.findAll();
        assertThat(empMgtList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
