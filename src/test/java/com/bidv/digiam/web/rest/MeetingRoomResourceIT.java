package com.bidv.digiam.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bidv.digiam.IntegrationTest;
import com.bidv.digiam.domain.MeetingRoom;
import com.bidv.digiam.repository.MeetingRoomRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MeetingRoomResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MeetingRoomResourceIT {

    private static final String DEFAULT_ROOM_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ROOM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ROOM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_SLOT = 1;
    private static final Integer UPDATED_SLOT = 2;

    private static final Integer DEFAULT_MAX_SLOT = 1;
    private static final Integer UPDATED_MAX_SLOT = 2;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/meeting-rooms";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MeetingRoomRepository meetingRoomRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMeetingRoomMockMvc;

    private MeetingRoom meetingRoom;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeetingRoom createEntity(EntityManager em) {
        MeetingRoom meetingRoom = new MeetingRoom()
            .roomCode(DEFAULT_ROOM_CODE)
            .roomName(DEFAULT_ROOM_NAME)
            .location(DEFAULT_LOCATION)
            .slot(DEFAULT_SLOT)
            .maxSlot(DEFAULT_MAX_SLOT)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY);
        return meetingRoom;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MeetingRoom createUpdatedEntity(EntityManager em) {
        MeetingRoom meetingRoom = new MeetingRoom()
            .roomCode(UPDATED_ROOM_CODE)
            .roomName(UPDATED_ROOM_NAME)
            .location(UPDATED_LOCATION)
            .slot(UPDATED_SLOT)
            .maxSlot(UPDATED_MAX_SLOT)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        return meetingRoom;
    }

    @BeforeEach
    public void initTest() {
        meetingRoom = createEntity(em);
    }

    @Test
    @Transactional
    void createMeetingRoom() throws Exception {
        int databaseSizeBeforeCreate = meetingRoomRepository.findAll().size();
        // Create the MeetingRoom
        restMeetingRoomMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(meetingRoom)))
            .andExpect(status().isCreated());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeCreate + 1);
        MeetingRoom testMeetingRoom = meetingRoomList.get(meetingRoomList.size() - 1);
        assertThat(testMeetingRoom.getRoomCode()).isEqualTo(DEFAULT_ROOM_CODE);
        assertThat(testMeetingRoom.getRoomName()).isEqualTo(DEFAULT_ROOM_NAME);
        assertThat(testMeetingRoom.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testMeetingRoom.getSlot()).isEqualTo(DEFAULT_SLOT);
        assertThat(testMeetingRoom.getMaxSlot()).isEqualTo(DEFAULT_MAX_SLOT);
        assertThat(testMeetingRoom.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testMeetingRoom.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMeetingRoom.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testMeetingRoom.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void createMeetingRoomWithExistingId() throws Exception {
        // Create the MeetingRoom with an existing ID
        meetingRoom.setId(1L);

        int databaseSizeBeforeCreate = meetingRoomRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMeetingRoomMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(meetingRoom)))
            .andExpect(status().isBadRequest());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMeetingRooms() throws Exception {
        // Initialize the database
        meetingRoomRepository.saveAndFlush(meetingRoom);

        // Get all the meetingRoomList
        restMeetingRoomMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(meetingRoom.getId().intValue())))
            .andExpect(jsonPath("$.[*].roomCode").value(hasItem(DEFAULT_ROOM_CODE)))
            .andExpect(jsonPath("$.[*].roomName").value(hasItem(DEFAULT_ROOM_NAME)))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].slot").value(hasItem(DEFAULT_SLOT)))
            .andExpect(jsonPath("$.[*].maxSlot").value(hasItem(DEFAULT_MAX_SLOT)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));
    }

    @Test
    @Transactional
    void getMeetingRoom() throws Exception {
        // Initialize the database
        meetingRoomRepository.saveAndFlush(meetingRoom);

        // Get the meetingRoom
        restMeetingRoomMockMvc
            .perform(get(ENTITY_API_URL_ID, meetingRoom.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(meetingRoom.getId().intValue()))
            .andExpect(jsonPath("$.roomCode").value(DEFAULT_ROOM_CODE))
            .andExpect(jsonPath("$.roomName").value(DEFAULT_ROOM_NAME))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION))
            .andExpect(jsonPath("$.slot").value(DEFAULT_SLOT))
            .andExpect(jsonPath("$.maxSlot").value(DEFAULT_MAX_SLOT))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY));
    }

    @Test
    @Transactional
    void getNonExistingMeetingRoom() throws Exception {
        // Get the meetingRoom
        restMeetingRoomMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMeetingRoom() throws Exception {
        // Initialize the database
        meetingRoomRepository.saveAndFlush(meetingRoom);

        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();

        // Update the meetingRoom
        MeetingRoom updatedMeetingRoom = meetingRoomRepository.findById(meetingRoom.getId()).get();
        // Disconnect from session so that the updates on updatedMeetingRoom are not directly saved in db
        em.detach(updatedMeetingRoom);
        updatedMeetingRoom
            .roomCode(UPDATED_ROOM_CODE)
            .roomName(UPDATED_ROOM_NAME)
            .location(UPDATED_LOCATION)
            .slot(UPDATED_SLOT)
            .maxSlot(UPDATED_MAX_SLOT)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restMeetingRoomMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMeetingRoom.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMeetingRoom))
            )
            .andExpect(status().isOk());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
        MeetingRoom testMeetingRoom = meetingRoomList.get(meetingRoomList.size() - 1);
        assertThat(testMeetingRoom.getRoomCode()).isEqualTo(UPDATED_ROOM_CODE);
        assertThat(testMeetingRoom.getRoomName()).isEqualTo(UPDATED_ROOM_NAME);
        assertThat(testMeetingRoom.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testMeetingRoom.getSlot()).isEqualTo(UPDATED_SLOT);
        assertThat(testMeetingRoom.getMaxSlot()).isEqualTo(UPDATED_MAX_SLOT);
        assertThat(testMeetingRoom.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMeetingRoom.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeetingRoom.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testMeetingRoom.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void putNonExistingMeetingRoom() throws Exception {
        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();
        meetingRoom.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetingRoomMockMvc
            .perform(
                put(ENTITY_API_URL_ID, meetingRoom.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meetingRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMeetingRoom() throws Exception {
        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();
        meetingRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingRoomMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(meetingRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMeetingRoom() throws Exception {
        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();
        meetingRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingRoomMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(meetingRoom)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMeetingRoomWithPatch() throws Exception {
        // Initialize the database
        meetingRoomRepository.saveAndFlush(meetingRoom);

        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();

        // Update the meetingRoom using partial update
        MeetingRoom partialUpdatedMeetingRoom = new MeetingRoom();
        partialUpdatedMeetingRoom.setId(meetingRoom.getId());

        partialUpdatedMeetingRoom
            .roomName(UPDATED_ROOM_NAME)
            .location(UPDATED_LOCATION)
            .slot(UPDATED_SLOT)
            .maxSlot(UPDATED_MAX_SLOT)
            .createdDate(UPDATED_CREATED_DATE);

        restMeetingRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeetingRoom.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeetingRoom))
            )
            .andExpect(status().isOk());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
        MeetingRoom testMeetingRoom = meetingRoomList.get(meetingRoomList.size() - 1);
        assertThat(testMeetingRoom.getRoomCode()).isEqualTo(DEFAULT_ROOM_CODE);
        assertThat(testMeetingRoom.getRoomName()).isEqualTo(UPDATED_ROOM_NAME);
        assertThat(testMeetingRoom.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testMeetingRoom.getSlot()).isEqualTo(UPDATED_SLOT);
        assertThat(testMeetingRoom.getMaxSlot()).isEqualTo(UPDATED_MAX_SLOT);
        assertThat(testMeetingRoom.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMeetingRoom.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMeetingRoom.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testMeetingRoom.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
    }

    @Test
    @Transactional
    void fullUpdateMeetingRoomWithPatch() throws Exception {
        // Initialize the database
        meetingRoomRepository.saveAndFlush(meetingRoom);

        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();

        // Update the meetingRoom using partial update
        MeetingRoom partialUpdatedMeetingRoom = new MeetingRoom();
        partialUpdatedMeetingRoom.setId(meetingRoom.getId());

        partialUpdatedMeetingRoom
            .roomCode(UPDATED_ROOM_CODE)
            .roomName(UPDATED_ROOM_NAME)
            .location(UPDATED_LOCATION)
            .slot(UPDATED_SLOT)
            .maxSlot(UPDATED_MAX_SLOT)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);

        restMeetingRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMeetingRoom.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMeetingRoom))
            )
            .andExpect(status().isOk());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
        MeetingRoom testMeetingRoom = meetingRoomList.get(meetingRoomList.size() - 1);
        assertThat(testMeetingRoom.getRoomCode()).isEqualTo(UPDATED_ROOM_CODE);
        assertThat(testMeetingRoom.getRoomName()).isEqualTo(UPDATED_ROOM_NAME);
        assertThat(testMeetingRoom.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testMeetingRoom.getSlot()).isEqualTo(UPDATED_SLOT);
        assertThat(testMeetingRoom.getMaxSlot()).isEqualTo(UPDATED_MAX_SLOT);
        assertThat(testMeetingRoom.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMeetingRoom.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMeetingRoom.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testMeetingRoom.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    void patchNonExistingMeetingRoom() throws Exception {
        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();
        meetingRoom.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMeetingRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, meetingRoom.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meetingRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMeetingRoom() throws Exception {
        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();
        meetingRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingRoomMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(meetingRoom))
            )
            .andExpect(status().isBadRequest());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMeetingRoom() throws Exception {
        int databaseSizeBeforeUpdate = meetingRoomRepository.findAll().size();
        meetingRoom.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMeetingRoomMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(meetingRoom))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MeetingRoom in the database
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMeetingRoom() throws Exception {
        // Initialize the database
        meetingRoomRepository.saveAndFlush(meetingRoom);

        int databaseSizeBeforeDelete = meetingRoomRepository.findAll().size();

        // Delete the meetingRoom
        restMeetingRoomMockMvc
            .perform(delete(ENTITY_API_URL_ID, meetingRoom.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MeetingRoom> meetingRoomList = meetingRoomRepository.findAll();
        assertThat(meetingRoomList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
